// module.exports = {
//     style: {
//         postcss: {
//             plugins: [require("tailwindcss"), require("autoprefixer")],
//         },
//     },
// };

const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const Dotenv = require("dotenv-webpack");
const { InjectManifest } = require("workbox-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
// require("babel-polyfill");

const webpackPlugins = [
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, "public/index.html"),
    filename: "index.html",
  }),
  new Dotenv({
    path: "./.env", // Path to .env file (this is the default)
    systemvars: true,
  }),
  new CopyPlugin({
    patterns: [
      //   { from: "./public/images/header-logo.png", to: "" },
      { from: "./public/manifest.json", to: "" },
      // { from: "./src/logo192.png", to: "" },
      // { from: "./src/logo512.png", to: "" },
    ],
  }),
];

if ("production" === process.env.NODE_ENV) {
  webpackPlugins.push(
    new InjectManifest({
      swSrc: "./src/service-worker.js",
      swDest: "sw.js",
    })
  );
}

module.exports = {
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
  babel: {
    presets: [
      [
        "@babel/preset-env",
        {
          modules: false,
          targets: {
            browsers: [
              "last 2 Chrome versions",
              "last 2 Firefox versions",
              "last 2 Safari versions",
              "last 2 iOS versions",
              "last 1 Android version",
              "last 1 ChromeAndroid version",
              "ie 11",
            ],
          },
        },
      ],
      "@babel/preset-react",
    ],
    env: {
      development: {
        compact: true,
      },
    },
    plugins: ["@babel/plugin-proposal-class-properties"],
  },
  webpack: {
    context: __dirname,
    entry: ["babel-polyfill", "./src/index.js"],
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "main.js",
      publicPath: "/",
    },
    devServer: {
      historyApiFallback: true,
    },
    resolve: {
      fallback: {
        fs: false,
        tls: false,
        net: false,
        path: false,
        zlib: false,
        http: false,
        https: false,
        stream: false,
        crypto: false,
        "crypto-browserify": require.resolve("crypto-browserify"), //if you want to use this module also don't forget npm i crypto-browserify
      },
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: "babel-loader",
          query: { compact: true },
        },
        {
          test: /\.css?$/,
          use: ["style-loader", "css-loader", "postcss-loader"],
        },
        {
          test: /\.(png|j?g|svg|gif)?$/,
          use: "file-loader?name=./images/[name].[ext]",
        },
        {
          test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
          use: {
            loader: "url-loader",
            options: {
              limit: 100000,
              name: "[name].[ext]",
            },
          },
        },
      ],
    },
    alias: {},
    plugins: {
      add: webpackPlugins /* An array of plugins */,
    },
    // configure: {
    //   /* Any webpack configuration options: https://webpack.js.org/configuration */
    // },
    configure: (webpackConfig, { env, paths }) => {
      return webpackConfig;
    },
  },
  //   webpack: {
  //     configure: (webpackConfig, { env, paths }) => {
  //       webpackConfig.entry = {
  //         mylib: "./src/index.js",
  //       };

  //       webpackConfig.output = {
  //         path: path.resolve(__dirname, "dist"),
  //         filename: "main.js",
  //         publicPath: "/",
  //       };
  //       console.log(webpackConfig);
  //       return webpackConfig;
  //     },
  //   },
  //   devServer: {
  //     port: 3000,
  //     open: true,
  //   },
};
