/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable react/style-prop-object */
import React, { Component } from "react";
import PageHeading from "../pages/PageHeading";
// import { AiOutlinePlus } from "react-icons/ai";
// import { AiOutlineMinus } from "react-icons/ai";
import { CgAsterisk } from "react-icons/cg";
import ReactPhoneInput from "react-phone-input-2";

class Setting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // admin setting
      appName: "",
      noOfRecords: "",
      footerTitle: "",

      adminError: false,
      appNameError: "",
      noOfRecordsError: "",
      footerTitleError: "",

      // front end Setting
      siteTitle: "",
      siteTagLine: "",
      siteCopyrightText: "",
      siteMetaDescription: "",

      FrontendError: false,
      siteTitleError: "",
      siteTagLineError: "",
      siteCopyrightTextError: "",

      // app download links
      iosLink: "",
      androidLink: "",

      // company Details
      companyName: "",
      addressLine1: "",
      addressLine2: "",
      city: "",
      state: "",
      country: "",
      ZipCode: null,
      phone: "",
      email: "",
      googleMapCode: (
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3220.8028446848048!2d-86.78386438477024!3d36.171351980082946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x886466f60caf6ae7%3A0xd4db82ec4192b543!2sDevDigital!5e0!3m2!1sen!2sin!4v1574155490019!5m2!1sen!2sin"
          width="600"
          height="310"
          frameborder="0"
          style="border:0;"
          allowfullscreen=""
        ></iframe>
      ),

      companyDetailsError: false,
      companyNameError: "",
      addressLine1Error: "",
      addressLine2Error: "",
      cityError: "",
      stateError: "",
      countryError: "",
      ZipCodeError: "",

      // email setting
      fromName: "",
      fromEmail: "",
      adminEmail: "",

      emailSettingError: false,
      fromNameError: "",
      fromEmailError: "",
      adminEmailError: "",

      // password Setting
      passwordStrength: "",
      noOfAttempt: null,
      minPasswordLength: null,
      userBlockTime: null,

      passwordSettingError: false,
      passwordStrengthError: "",
      noOfAttemptError: null,
      minPasswordLengthError: null,
      userBlockTimeError: null,

      // social networking Links
      facebook: "",
      twitter: "",
      linkedin: "",

      // google captcha
      googleReCaptchaKey: "",
      googleReCaptchaSecretKey: "",

      // seo setting
      robotsMetaTag: "",
      googleAnalyticsCode: "",

      // api setting
      apiAccessToken: null,
      apiRefreshToken: null,

      apiSettingError: false,
      apiAccessTokenError: null,
      apiRefreshTokenError: null,
    };
  }
  createSetting = () => {
    let adminError = false;
    let appNameError = "";
    let noOfRecordsError = "";
    let footerTitleError = "";

    let FrontendError = false;
    let siteTitleError = "";
    let siteTagLineError = "";
    let siteCopyrightTextError = "";

    let companyDetailsError = false;
    let companyNameError = "";
    let addressLine1Error = "";
    let addressLine2Error = "";
    let cityError = "";
    let stateError = "";
    let countryError = "";
    let ZipCodeError = "";

    let emailSettingError = false;
    let fromNameError = "";
    let fromEmailError = "";
    let adminEmailError = "";

    let passwordSettingError = false;
    let passwordStrengthError = "";
    let noOfAttemptError = "";
    let minPasswordLengthError = "";
    let userBlockTimeError = "";

    let apiSettingError = false;
    let apiAccessTokenError = "";
    let apiRefreshTokenError = "";

    if (this.state.appName.trim() === "") {
      appNameError = "Enter app name";
      adminError = true;
    }
    if (this.state.noOfRecords === "") {
      adminError = true;
      noOfRecordsError = "Select No Record";
    }
    if (this.state.footerTitle.trim() === "") {
      adminError = true;
      footerTitleError = "Enter Footer Title";
    }
    if (this.state.siteTitle.trim() === "") {
      FrontendError = true;
      siteTitleError = "Enter Site Title";
    }
    if (this.state.siteTagLine.trim() === "") {
      FrontendError = true;
      siteTagLineError = "Enter Site Tag line";
    }
    if (this.state.siteCopyrightText.trim() === "") {
      FrontendError = true;
      siteCopyrightTextError = "Enter Site copyright text";
    }
    if (this.state.companyName.trim() === "") {
      companyDetailsError = true;
      companyNameError = "Enter company copyright name";
    }
    if (this.state.addressLine1.trim() === "") {
      companyDetailsError = true;
      addressLine1Error = "Enter address line 1";
    }
    if (this.state.addressLine2Error.trim() === "") {
      companyDetailsError = true;
      addressLine2Error = "Enter address line 2";
    }
    if (this.state.city.trim() === "") {
      companyDetailsError = true;
      cityError = "Enter city name";
    }
    if (this.state.state.trim() === "") {
      companyDetailsError = true;
      stateError = "Enter state name";
    }
    if (this.state.country.trim() === "") {
      companyDetailsError = true;
      countryError = "Enter country name";
    }
    if (this.state.ZipCode === null) {
      companyDetailsError = true;
      ZipCodeError = "Enter ZipCode";
    }
    if (this.state.fromName.trim() === "") {
      emailSettingError = true;
      fromNameError = "Enter name";
    }
    if (this.state.fromEmail.trim() === "") {
      emailSettingError = true;
      fromEmailError = "Enter email";
    }
    if (this.state.adminEmail.trim() === "") {
      emailSettingError = true;
      adminEmailError = "Enter admin email";
    }
    if (this.state.passwordStrength.trim() === "") {
      passwordSettingError = true;
      passwordStrengthError = "Select password strength";
    }
    if (this.state.noOfAttempt === null) {
      passwordSettingError = true;
      noOfAttemptError = "Enter no of attempt";
    }
    if (this.state.minPasswordLength === null) {
      passwordSettingError = true;
      minPasswordLengthError = "Enter password length";
    }
    if (this.state.userBlockTime === null) {
      passwordSettingError = true;
      userBlockTimeError = "Enter user block time";
    }
    if (this.state.apiAccessToken === null) {
      apiSettingError = true;
      apiAccessTokenError = "Enter access token lifetime time";
    }
    if (this.state.apiRefreshToken === null) {
      apiSettingError = true;
      apiRefreshTokenError = "Enter refresh token lifetime time";
    }

    if (
      adminError === true ||
      FrontendError === true ||
      companyDetailsError === true ||
      emailSettingError === true ||
      passwordSettingError === true ||
      apiSettingError === true
    ) {
      this.setState({
        adminError: adminError,
        appNameError: appNameError,
        noOfRecordsError: noOfRecordsError,
        footerTitleError: footerTitleError,

        FrontendError: FrontendError,
        siteTitleError: siteTitleError,
        siteTagLineError: siteTagLineError,
        siteCopyrightTextError: siteCopyrightTextError,

        companyDetailsError: companyDetailsError,
        companyNameError: companyNameError,
        addressLine1Error: addressLine1Error,
        addressLine2Error: addressLine2Error,
        cityError: cityError,
        stateError: stateError,
        countryError: countryError,
        ZipCodeError: ZipCodeError,

        emailSettingError: emailSettingError,
        fromNameError: fromNameError,
        fromEmailError: fromEmailError,
        adminEmailError: adminEmailError,

        passwordSettingError: passwordSettingError,
        passwordStrengthError: passwordStrengthError,
        noOfAttemptError: noOfAttemptError,
        minPasswordLengthError: minPasswordLengthError,
        userBlockTimeError: userBlockTimeError,

        apiSettingError: apiSettingError,
        apiAccessTokenError: apiAccessTokenError,
        apiRefreshTokenError: apiRefreshTokenError,
      });
      return;
    }

    let setting = {
      appName: this.state.appName,
      noOfRecords: this.state.noOfRecords,
      footerTitle: this.state.footerTitle,
      siteTitle: this.state.siteTitle,
      siteTagLine: this.state.siteTagLine,
      siteCopyrightText: this.state.siteCopyrightText,
      siteMetaDescription: this.state.siteMetaDescription,
      iosLink: this.state.iosLink,
      androidLink: this.state.androidLink,
      companyName: this.state.companyName,
      addressLine1: this.state.addressLine1,
      addressLine2: this.state.addressLine2,
      city: this.state.city,
      state: this.state.state,
      country: this.state.country,
      ZipCode: this.state.ZipCode,
      phone: this.state.phone,
      email: this.state.email,
      googleMapCode: this.state.googleMapCode,
      fromName: this.state.fromName,
      fromEmail: this.state.fromEmail,
      adminEmail: this.state.adminEmail,
      passwordStrength: this.state.passwordStrength,
      noOfAttempt: this.state.noOfAttempt,
      minPasswordLength: this.state.minPasswordLength,
      userBlockTime: this.state.userBlockTime,
      facebook: this.state.facebook,
      twitter: this.state.twitter,
      linkedin: this.state.linkedin,
      googleReCaptchaKey: this.state.googleReCaptchaKey,
      googleReCaptchaSecretKey: this.state.googleReCaptchaSecretKey,
      robotsMetaTag: this.state.robotsMetaTag,
      googleAnalyticsCode: this.state.googleAnalyticsCode,
      apiAccessToken: this.state.apiAccessToken,
      apiRefreshToken: this.state.apiRefreshToken,
    };
    console.log(setting);
  };

  render() {
    return (
      <div
        className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
          this.props.Sidebar
            ? "w-full sm:content md:content lg:content xl:content"
            : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
        }`}
      >
        <div className="flex flex-col items-center w-full">
          <PageHeading
            pageHeading="Setting"
            showSaveOptionsBtn={true}
            save={true}
            createUser={true}
            handleSave={this.createSetting}
          />
          <div className="relative w-full px-4 my-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 ">
            <div className="w-full shadow-md">
              <div className="w-full border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-one"
                  type="checkbox"
                  checked={this.state.adminError}
                  onChange={() =>
                    this.setState({
                      adminError: !this.state.adminError,
                    })
                  }
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-one"
                >
                  Admin Setting
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      App Name <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      placeholder="CMS Admin"
                      value={this.state.appName}
                      onChange={(e) => {
                        this.setState({
                          appNameError: "",
                          appName: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.appNameError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                    />
                    {this.state.appNameError && (
                      <span className="text-xs text-red-500">
                        {this.state.appNameError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      No of Records per page{" "}
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <select
                      value={this.state.noOfRecords}
                      onChange={(e) => {
                        this.setState({
                          noOfRecordsError: "",
                          noOfRecords: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.noOfRecordsError
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                    >
                      <option>Select No Records</option>
                      <option value="100">100</option>
                      <option value="150">150</option>
                      <option value="200">200</option>
                      <option value="250">250</option>
                      <option value="300">300</option>
                      <option value="350">350</option>
                      <option value="400">400</option>
                      <option value="450">450</option>
                      <option value="500">500</option>
                      <option value="all">All</option>
                    </select>
                    {this.state.noOfRecordsError && (
                      <span className="text-xs text-red-500">
                        {this.state.noOfRecordsError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Footer Title for Admin{" "}
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      value={this.state.footerTitle}
                      onChange={(e) => {
                        this.setState({
                          footerTitleError: "",
                          footerTitle: e.target.value,
                        });
                      }}
                      type="text"
                      className={`${
                        this.state.footerTitleError
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                    />
                    {this.state.footerTitleError && (
                      <span className="text-xs text-red-500">
                        {this.state.footerTitleError}
                      </span>
                    )}
                  </div>
                </div>
              </div>

              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-two"
                  type="checkbox"
                  checked={this.state.FrontendError}
                  onChange={() =>
                    this.setState({
                      FrontendError: !this.state.FrontendError,
                    })
                  }
                  name="tabs"
                />

                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-two"
                >
                  Front End Setting
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Site Title <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      value={this.state.siteTitle}
                      onChange={(e) => {
                        this.setState({
                          siteTitleError: "",
                          siteTitle: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.siteTitleError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="Site title"
                    />
                    {this.state.siteTitleError && (
                      <span className="text-xs text-red-500">
                        {this.state.siteTitleError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Tag Line
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      value={this.state.siteTagLine}
                      onChange={(e) => {
                        this.setState({
                          siteTagLineError: "",
                          siteTagLine: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.siteTagLineError
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="Site Tagline"
                    />
                    {this.state.siteTagLineError && (
                      <span className="text-xs text-red-500">
                        {this.state.siteTagLineError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Copyright Text
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      value={this.state.siteCopyrightText}
                      onChange={(e) => {
                        this.setState({
                          siteCopyrightTextError: "",
                          siteCopyrightText: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.siteCopyrightTextError
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="Site Copyright Text"
                    />
                    {this.state.siteCopyrightTextError && (
                      <span className="text-xs text-red-500">
                        {this.state.siteCopyrightTextError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3 lg:col-span-3">
                    <label className="flex items-center mb-2 text-sm">
                      Meta Description
                    </label>
                    <textarea
                      type="text"
                      onChange={(e) => {
                        this.setState({
                          siteMetaDescription: e.target.value,
                        });
                      }}
                      className="h-20 px-2 text-sm font-medium border rounded "
                      placeholder="Site Copyright Text"
                    />
                  </div>
                </div>
              </div>
              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-three"
                  type="checkbox"
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-three"
                >
                  App Download Setting
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      iOS App Download Link
                    </label>
                    <input
                      type="text"
                      value={this.state.iosLink}
                      onChange={(e) => {
                        this.setState({
                          iosLink: e.target.value,
                        });
                      }}
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="iOS App Download Link"
                    />
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Android App Download Link
                    </label>
                    <input
                      type="text"
                      value={this.state.androidLink}
                      onChange={(e) => {
                        this.setState({
                          androidLink: e.target.value,
                        });
                      }}
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="Android App Download Link"
                    />
                  </div>
                </div>
              </div>
              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-four"
                  type="checkbox"
                  checked={this.state.companyDetailsError}
                  onChange={() =>
                    this.setState({
                      companyDetailsError: !this.state.companyDetailsError,
                    })
                  }
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-four"
                >
                  Company Details
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Company Name
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      value={this.state.companyName}
                      onChange={(e) => {
                        this.setState({
                          companyNameError: "",
                          companyName: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.companyNameError
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder=" Company Name"
                    />
                    {this.state.companyNameError && (
                      <span className="text-xs text-red-500">
                        {this.state.companyNameError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Address Line 1{" "}
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      value={this.state.addressLine1}
                      onChange={(e) => {
                        this.setState({
                          addressLine1Error: "",
                          addressLine1: e.target.value,
                        });
                      }}
                      type="text"
                      className={`${
                        this.state.addressLine1Error
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="Address Line One"
                    />
                    {this.state.addressLine1Error && (
                      <span className="text-xs text-red-500">
                        {this.state.addressLine1Error}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Address Line 2{" "}
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      value={this.state.addressLine2}
                      onChange={(e) => {
                        this.setState({
                          addressLine2Error: "",
                          addressLine2: e.target.value,
                        });
                      }}
                      type="text"
                      className={`${
                        this.state.addressLine2Error
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="Address Line two"
                    />
                    {this.state.addressLine2Error && (
                      <span className="text-xs text-red-500">
                        {this.state.addressLine2Error}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      City
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      value={this.state.city}
                      onChange={(e) => {
                        this.setState({
                          cityError: "",
                          city: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.cityError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="City"
                    />
                    {this.state.cityError && (
                      <span className="text-xs text-red-500">
                        {this.state.cityError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      State
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      value={this.state.state}
                      onChange={(e) => {
                        this.setState({
                          stateError: "",
                          state: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.stateError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="State"
                    />
                    {this.state.stateError && (
                      <span className="text-xs text-red-500">
                        {this.state.stateError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Country
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      value={this.state.country}
                      onChange={(e) => {
                        this.setState({
                          countryError: "",
                          country: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.countryError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="Country"
                    />
                    {this.state.countryError && (
                      <span className="text-xs text-red-500">
                        {this.state.countryError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      ZipCode
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="number"
                      value={this.state.ZipCode}
                      onChange={(e) => {
                        this.setState({
                          ZipCodeError: "",
                          ZipCode: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.ZipCodeError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="ZipCode"
                    />
                    {this.state.ZipCodeError && (
                      <span className="text-xs text-red-500">
                        {this.state.ZipCodeError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Phone
                    </label>
                    <ReactPhoneInput
                      country={"in"}
                      inputStyle={{
                        width: "100%",
                        height: "2.5rem",
                        fontSize: "13px",
                        paddingLeft: "48px",
                        borderRadius: "5px",
                        fontWeight: "600",
                        border: "1px solid lightgray",
                      }}
                      value={this.state.phone}
                      onChange={(phone) => {
                        this.setState({
                          phone,
                        });
                      }}
                    />
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Email
                    </label>
                    <input
                      value={this.state.email}
                      onChange={(e) => {
                        this.setState({
                          email: e.target.value,
                        });
                      }}
                      type="email"
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="Email"
                    />
                  </div>
                  <div className="flex flex-col p-3 lg:col-span-3">
                    <label className="flex items-center mb-2 text-sm">
                      Google map embedded html code
                    </label>
                    <textarea
                      value={this.state.googleMapCode}
                      onChange={(e) => {
                        this.setState({
                          googleMapCode: e.target.value,
                        });
                      }}
                      className="h-20 px-2 text-sm font-medium border rounded max-h-20 "
                    />
                    <div>
                      <h1 className="mt-2 mb-2 text-xs">
                        (Tips: How to get embedded code from Google maps)
                      </h1>
                      <ul className="text-xs">
                        <li className="mb-2">
                          Step 1: Open https://www.google.com/maps and search
                          "company name"
                        </li>
                        <li className="mb-2">
                          Step 2: Once the business listing is opened, click on
                          the "share" button
                        </li>
                        <li>Step 3: Click on "embedded map" then COPY Html</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-six"
                  type="checkbox"
                  checked={this.state.emailSettingError}
                  onChange={() =>
                    this.setState({
                      emailSettingError: !this.state.emailSettingError,
                    })
                  }
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-six"
                >
                  Email Setting
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      From Name
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="text"
                      value={this.state.fromName}
                      onChange={(e) => {
                        this.setState({
                          fromNameError: "",
                          fromName: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.fromNameError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="From name"
                    />
                    {this.state.fromNameError && (
                      <span className="text-xs text-red-500">
                        {this.state.fromNameError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      From Email
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="email"
                      placeholder="From Email"
                      value={this.state.fromEmail}
                      onChange={(e) => {
                        this.setState({
                          fromEmailError: "",
                          fromEmail: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.fromEmailError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                    />
                    {this.state.fromEmailError && (
                      <span className="text-xs text-red-500">
                        {this.state.fromEmailError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Admin Email
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="email"
                      placeholder="Admin Email"
                      value={this.state.adminEmail}
                      onChange={(e) => {
                        this.setState({
                          adminEmailError: "",
                          adminEmail: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.adminEmailError ? "border-red-500" : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                    />
                    {this.state.adminEmailError && (
                      <span className="text-xs text-red-500">
                        {this.state.adminEmailError}
                      </span>
                    )}
                  </div>
                </div>
              </div>
              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-seven"
                  type="checkbox"
                  checked={this.state.passwordSettingError}
                  onChange={() =>
                    this.setState({
                      passwordSettingError: !this.state.passwordSettingError,
                    })
                  }
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-seven"
                >
                  Password Setting
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Password Strength
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <select
                      value={this.state.passwordStrength}
                      onChange={(e) => {
                        this.setState({
                          passwordStrengthError: "",
                          passwordStrength: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.passwordStrengthError
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                    >
                      <option>Select Strength</option>
                      <option value="easy">Easy</option>
                      <option value="medium">Medium</option>
                      <option value="strong">Strong</option>
                    </select>
                    {this.state.passwordStrengthError && (
                      <span className="text-xs text-red-500">
                        {this.state.passwordStrengthError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      No of attempt to block user while login
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="number"
                      value={this.state.noOfAttempt}
                      onChange={(e) => {
                        this.setState({
                          noOfAttemptError: "",
                          noOfAttempt: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.noOfAttemptError
                          ? "border-red-500"
                          : "border"
                      } h-10 rounded px-2 text-sm font-medium `}
                      placeholder="No of attempt to block user while login "
                    />
                    {this.state.noOfAttemptError && (
                      <span className="text-xs text-red-500">
                        {this.state.noOfAttemptError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Minimum Password Length
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      placeholder="Enter Minimum Password Length"
                      type="number"
                      value={this.state.minPasswordLength}
                      onChange={(e) => {
                        this.setState({
                          minPasswordLengthError: "",
                          minPasswordLength: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.minPasswordLengthError
                          ? "border-red-500"
                          : "border"
                      } h-10 rounded px-2 text-sm font-medium `}
                    />
                    {this.state.minPasswordLengthError && (
                      <span className="text-xs text-red-500">
                        {this.state.minPasswordLengthError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      User Block Time
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      placeholder="User Block Time"
                      type="number"
                      value={this.state.userBlockTime}
                      onChange={(e) => {
                        this.setState({
                          userBlockTimeError: "",
                          userBlockTime: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.userBlockTimeError
                          ? "border-red-500"
                          : "border"
                      } h-10 rounded px-2 text-sm font-medium `}
                    />
                    {this.state.userBlockTimeError && (
                      <span className="text-xs text-red-500">
                        {this.state.userBlockTimeError}
                      </span>
                    )}
                  </div>
                </div>
              </div>
              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-eight"
                  type="checkbox"
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-eight"
                >
                  Social Networking Links
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Facebook
                    </label>
                    <input
                      type="text"
                      value={this.state.facebook}
                      onChange={(e) =>
                        this.setState({
                          facebook: e.target.value,
                        })
                      }
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="Enter Facebook Link"
                    />
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Twitter
                    </label>
                    <input
                      type="text"
                      value={this.state.twitter}
                      onChange={(e) =>
                        this.setState({
                          twitter: e.target.value,
                        })
                      }
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="Enter Twitter Link"
                    />
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      LinkedIn
                    </label>
                    <input
                      type="text"
                      value={this.state.linkedin}
                      onChange={(e) =>
                        this.setState({
                          linkedin: e.target.value,
                        })
                      }
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="Enter LinkedIn Link"
                    />
                  </div>
                </div>
              </div>
              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-nine"
                  type="checkbox"
                  onC
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-nine"
                >
                  Google Captcha
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Google Recaptcha Site Key
                    </label>
                    <input
                      type="text"
                      value={this.state.googleReCaptchaKey}
                      onChange={(e) =>
                        this.setState({
                          googleReCaptchaKey: e.target.value,
                        })
                      }
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="Google Recaptcha Site Key"
                    />
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Google Recaptcha Secret Key
                    </label>
                    <input
                      type="text"
                      value={this.state.googleReCaptchaSecretKey}
                      onChange={(e) =>
                        this.setState({
                          googleReCaptchaSecretKey: e.target.value,
                        })
                      }
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="Google Recaptcha Secert Key"
                    />
                  </div>
                </div>
              </div>
              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-ten"
                  type="checkbox"
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-ten"
                >
                  SEO Setting
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      Robots Meta Tag
                    </label>
                    <input
                      type="text"
                      value={this.state.robotsMetaTag}
                      onChange={(e) =>
                        this.setState({
                          robotsMetaTag: e.target.value,
                        })
                      }
                      className="h-10 px-2 text-sm font-medium border rounded "
                      placeholder="Robots Meta Tag"
                    />
                  </div>
                  <div className="flex flex-col p-3 lg:col-span-3">
                    <label className="flex items-center mb-2 text-sm">
                      Google Analytics Code
                    </label>
                    <textarea
                      value={this.state.googleAnalyticsCode}
                      onChange={(e) =>
                        this.setState({
                          googleAnalyticsCode: e.target.value,
                        })
                      }
                      className="h-20 px-2 text-sm font-medium border rounded max-h-20 "
                    />
                  </div>
                </div>
              </div>
              <div className="w-full overflow-hidden border-t tab">
                <input
                  className="absolute opacity-0"
                  id="tab-multi-eleven"
                  type="checkbox"
                  checked={this.state.apiSettingError}
                  onChange={() =>
                    this.setState({
                      apiSettingError: !this.state.apiSettingError,
                    })
                  }
                  name="tabs"
                />
                <label
                  className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                  for="tab-multi-eleven"
                >
                  API Setting
                </label>
                <div className="w-full overflow-hidden leading-normal border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      API Access Token Lifetime{" "}
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="number"
                      value={this.state.apiAccessToken}
                      onChange={(e) => {
                        this.setState({
                          apiAccessTokenError: "",
                          apiAccessToken: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.apiAccessTokenError
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="Robots Meta Tag"
                    />
                    {this.state.apiAccessTokenError && (
                      <span className="text-xs text-red-500">
                        {this.state.apiAccessTokenError}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-col p-3">
                    <label className="flex items-center mb-2 text-sm">
                      API Refresh Token Lifetime{" "}
                      <CgAsterisk className="inline text-red-500" />
                    </label>
                    <input
                      type="number"
                      value={this.state.apiRefreshToken}
                      onChange={(e) => {
                        this.setState({
                          apiRefreshTokenError: "",
                          apiRefreshToken: e.target.value,
                        });
                      }}
                      className={`${
                        this.state.apiRefreshTokenError
                          ? "border-red-500"
                          : "border"
                      } border h-10 rounded px-2 text-sm font-medium `}
                      placeholder="Robots Meta Tag"
                    />
                    {this.state.apiRefreshTokenError && (
                      <span className="text-xs text-red-500">
                        {this.state.apiRefreshTokenError}
                      </span>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Setting;
