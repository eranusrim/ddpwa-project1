/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import PageHeading from "../pages/PageHeading";
import CommentTable from "./CommentTable";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { notify } from "../../utility";
import moment from "moment";

const BlogComments = (props) => {
  let history = useHistory();
  const [comments, setComment] = useState([]);
  const [historyComments, setHistoryComments] = useState([]);

  const [search, setSearch] = useState("");
  const [pageSearch, setPageSearch] = useState(true);
  const [activeInactive, setActiveInactive] = useState(false);

  let [selectAllCheckbox, setSelectAllCheckbox] = useState(false);

  let [currentPage, setCurrentPage] = useState(1);
  let [commentPerPage, setCommentPerPage] = useState(3);

  let [sortByComment, setSortByComment] = useState(true);
  let [sortByName, setSortByName] = useState(false);
  let [createdBy, setCreatedBy] = useState(false);

  let userInfo = JSON.parse(localStorage.getItem("userInfo"));
  let access_token = userInfo.access_token;

  let id = props.match.params.id;
  const fetchComments = () => {
    let config = {
      method: "get",
      url: "http://localhost:5000/blogs/allComment/" + id,
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };

    axios(config)
      .then((response) => {
        console.log("comments response", response.data.comments);
        setHistoryComments(response.data.comments);
        setCommentPerPage(
          response.data.comments.length < 10
            ? response.data.comments.length
            : 10
        );
        setComment(
          response.data.comments.map((comment) => {
            return {
              select: false,
              id: comment.id,
              comment: comment.comment,
              name: comment.commentBy,
              createdOn: comment.commentDate
                ? moment(comment.commentDate).format("YYYY-MM-DD")
                : null,
              status: comment.status,
            };
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    fetchComments();
  }, []);

  const indexOfLastComment = currentPage * commentPerPage;
  const indexOfFirstComment = indexOfLastComment - commentPerPage;
  const currentComments = comments.slice(
    indexOfFirstComment,
    indexOfLastComment
  );
  const paginate = (number) => {
    setCurrentPage(number);
  };
  const setNumberOfEvent = (number) => {
    setCommentPerPage(parseInt(number));
  };
  const selectAll = (e) => {
    let checked = e.target.checked;
    if (checked) {
      setPageSearch(false);
      setActiveInactive(true);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
    setComment(
      comments.map((d) => {
        d.select = checked;
        return d;
      })
    );

    console.log(comments);
  };
  const selectSingle = (e, id) => {
    let checked = e.target.checked;
    setComment(
      comments.map((comment) => {
        if (id === comment.id) {
          comment.select = checked;
          console.log(comment);
        }

        return comment;
      })
    );

    console.log(comments);

    const result = comments.some(function (data) {
      return data.select === true;
    });
    if (result) {
      setActiveInactive(true);
      setPageSearch(false);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
  };
  const handleDelete = () => {
    let a = window.confirm("Are you sure you want to delete this");
    console.log("deleting");
    if (a) {
      let arrayId = [];
      comments.forEach((d) => {
        if (d.select) {
          arrayId.push(d.id);
        }
      });
      axios
        .delete("http://localhost:5000/blogs/comments/deleteComment", {
          data: {
            blog_id: props.match.params.id,
            id: arrayId,
          },
        })
        .then((response) => {
          console.log("response", response);
          notify(response.data.message);
          setActiveInactive(!activeInactive);
          setPageSearch(!pageSearch);
          setSelectAllCheckbox(false);
          fetchComments();
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  };
  const handleActive = () => {
    let activeID = [];
    comments.forEach((active) => {
      if (active.select) {
        activeID.push(active.id);
      }
    });
    let id = `(${activeID.join(",")})`;
    console.log("id", id);
    const data = {
      status: 1,
      id: id,
    };
    axios
      .put("http://localhost:5000/blogs/activeInactiveComment", data)
      .then((response) => {
        console.log("edit response", response);
        if (response.status === 200) {
          notify(response.data.message);
          setSelectAllCheckbox(false);
          setActiveInactive(false);
          setPageSearch(true);
          fetchComments();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handleInActive = () => {
    let activeID = [];
    comments.forEach((active) => {
      if (active.select) {
        activeID.push(active.id);
      }
    });
    let id = `(${activeID.join(",")})`;
    console.log("id", id);
    const data = {
      status: 0,
      id: id,
    };
    axios
      .put("http://localhost:5000/blogs/activeInactiveComment", data)
      .then((response) => {
        console.log("edit response", response);
        if (response.status === 200) {
          notify(response.data.message);
          setSelectAllCheckbox(false);
          setActiveInactive(false);
          setPageSearch(true);
          fetchComments();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handleSearch = () => {
    let searchData = comments.filter((d) => {
      if (d.comment.toLocaleLowerCase().includes(search.toLocaleLowerCase())) {
        console.log(d);
        return d;
      }
    });
    console.log("length", searchData.length);
    console.log(searchData);
    if (searchData.length === 0) {
      setComment(comments);
    } else {
      setComment(searchData);
    }
  };
  const handleReset = () => {
    setSearch("");
    if (historyComments) {
      setComment(
        historyComments.map((comment) => {
          return {
            select: false,
            id: comment.id,
            comment: comment.comment,
            name: comment.name,
            createdOn: comment.createdOn,
            status: comment.status,
          };
        })
      );
    }
  };
  const handleClose = () => {
    setSearch("");
    fetchComments();
    // if (historyComments) {
    //     setComment(
    //         historyComments.map((comment) => {
    //             return {
    //                 select: false,
    //                 id: comment.id,
    //                 comment: comment.comment,
    //                 name: comment.commentBy,
    //                 createdOn: comment.commentDate,
    //                 status: comment.status,
    //             };
    //         })
    //     );
    // }
  };
  const sortComment = (sortByComment) => {
    const sorted = comments.sort((a, b) => {
      const isReverse = sortByComment === true ? 1 : -1;
      return isReverse * a.comment.localeCompare(b.comment);
    });
    setComment(sorted);
  };
  const sortName = (sortByName) => {
    const sorted = comments.sort((a, b) => {
      const isReverse = sortByName === true ? 1 : -1;
      return isReverse * a.name.localeCompare(b.name);
    });
    console.log("sorted", sorted);
    setComment(sorted);
  };
  const sortCreated = (createdBy) => {
    const sorted = comments.sort((a, b) => {
      let dateA = new Date(a.createdOn);
      let dateB = new Date(b.createdOn);
      return createdBy ? dateA - dateB : dateB - dateA;
    });
    setComment(sorted);
  };

  return (
    <div
      className={`content-container relative bg-gray-100 dark:bg-gray-700 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex-col items-center w-full">
        <PageHeading
          pageHeading={history.location.title}
          searchLabel={"Title"}
          pageSearch={pageSearch}
          backBtn={true}
          activeInactive={activeInactive}
          handleActive={handleActive}
          handleInActive={handleInActive}
          deleteBtn={true}
          handleDelete={handleDelete}
          search={search}
          setSearch={setSearch}
          handleSearch={handleSearch}
          handleReset={handleReset}
          handleClose={handleClose}
        />
        <CommentTable
          comments={currentComments}
          commentPerPage={commentPerPage}
          totalComments={comments.length}
          paginate={paginate}
          currentPage={currentPage}
          setNumberOfEvent={setNumberOfEvent}
          selectAll={selectAll}
          selectSingle={selectSingle}
          selectAllCheckbox={selectAllCheckbox}
          setSelectAllCheckbox={setSelectAllCheckbox}
          sortComment={sortComment}
          sortByComment={sortByComment}
          setSortByComment={setSortByComment}
          sortName={sortName}
          sortByName={sortByName}
          setSortByName={setSortByName}
          sortCreated={sortCreated}
          createdBy={createdBy}
          setCreatedBy={setCreatedBy}
        />
      </div>
    </div>
  );
};

export default BlogComments;
