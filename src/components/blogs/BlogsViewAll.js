/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PageHeading from "../pages/PageHeading";
import { BlogsData } from "./BlogsData";
import {
  FETCH_BLOGS_REQUEST,
  FETCH_BLOGS_SUCCESS,
  FETCH_BLOGS_FAIL,
  DELETE_BLOGS,
  ACTIVE_BLOGS,
  INACTIVE_BLOGS,
} from "../../Redux/actionTypes";
import BlogsTable from "./BlogsTable";
import axios from "axios";
import { notify } from "../../utility";
import moment from "moment";

const BlogsViewAll = (props) => {
  const dispatch = useDispatch();
  const [blogs, setBlogs] = useState([]);

  const [search, setSearch] = useState("");
  const [pageSearch, setPageSearch] = useState(true);
  const [activeInactive, setActiveInactive] = useState(false);

  let [selectAllCheckbox, setSelectAllCheckbox] = useState(false);

  let [currentPage, setCurrentPage] = useState(1);
  let [blogsPerPage, setBlogsPerPage] = useState(3);

  let [sortByTitle, setSortByTitle] = useState(true);
  let [sortByAuthor, setSortByAuthor] = useState(false);
  let [sortByPublish, setSortByPublish] = useState(false);
  let [sortByModified, setSortByModified] = useState(false);
  let [sortByBlogCategory, setSortByBlogCategory] = useState(false);

  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const access_token = userInfo.access_token;

  const fetchBlogs = () => {
    let config = {
      method: "get",
      url: "http://localhost:5000/blogs",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };
    dispatch({
      type: FETCH_BLOGS_REQUEST,
    });
    axios(config)
      .then(function (response) {
        const data = response.data.blogs;
        console.log("response eee", response.data.blogs);
        dispatch({
          type: FETCH_BLOGS_SUCCESS,
          payload: data,
        });
        setBlogsPerPage(data.length < 10 ? data.length : 10);
        setBlogs(
          data.map((blog) => {
            return {
              select: false,
              id: blog.blog_id,
              title: blog.title,
              authorName: blog.author,
              publishDate: blog.publishDate
                ? moment(blog.publishDate).format("YYYY-MM-DD")
                : null,
              modifiedOn: blog.modifiedDate
                ? moment(blog.modifiedDate).format("YYYY-MM-DD")
                : null,
              blogCategory: blog.category,
              comments: blog.comments,
              status: blog.status,
            };
          })
        );
      })
      .catch(function (error) {
        console.log(error);
      });
    dispatch({
      type: FETCH_BLOGS_SUCCESS,
      payload: BlogsData,
    });
  };
  const ReducerBlogs = useSelector((state) => state.BlogsReducer.blogs);
  useEffect(() => {
    fetchBlogs();
  }, []);
  const indexOfLastBlog = currentPage * blogsPerPage;
  const indexOfFirstBlog = indexOfLastBlog - blogsPerPage;
  const currentBlogs = blogs.slice(indexOfFirstBlog, indexOfLastBlog);

  const paginate = (number) => {
    setCurrentPage(number);
  };
  const setNumberOfEvent = (number) => {
    setBlogsPerPage(parseInt(number));
  };

  const selectAll = (e) => {
    let checked = e.target.checked;
    if (checked) {
      setPageSearch(false);
      setActiveInactive(true);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
    setBlogs(
      blogs.map((d) => {
        d.select = checked;
        return d;
      })
    );

    console.log(blogs);
  };
  const selectSingle = (e, id) => {
    let checked = e.target.checked;
    setBlogs(
      blogs.map((blog) => {
        if (id === blog.id) {
          blog.select = checked;
          console.log(blog);
        }

        return blog;
      })
    );

    console.log(blogs);

    const result = blogs.some(function (data) {
      return data.select === true;
    });
    if (result) {
      setActiveInactive(true);
      setPageSearch(false);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
  };
  const handleSearch = () => {
    let searchData = blogs.filter((d) => {
      if (d.title.toLocaleLowerCase().includes(search.toLocaleLowerCase())) {
        console.log(d);
        return d;
      }
    });
    console.log("length", searchData.length);
    console.log(searchData);
    if (searchData.length === 0) {
      setBlogs(blogs);
    } else {
      setBlogs(searchData);
      setCurrentPage(1);
    }
  };
  const handleReset = () => {
    setSearch("");
    if (ReducerBlogs) {
      setBlogs(
        ReducerBlogs.map((blog) => {
          return {
            select: false,
            id: blog.blog_id,
            title: blog.title,
            authorName: blog.author,
            publishDate: blog.publishDate,
            modifiedOn: blog.modifiedDate,
            blogCategory: blog.category,
            comments: blog.comments,
            status: blog.status,
          };
        })
      );
    }
  };
  const handleClose = () => {
    setSearch("");
    if (ReducerBlogs) {
      setBlogs(
        ReducerBlogs.map((blog) => {
          return {
            select: false,
            id: blog.blog_id,
            title: blog.title,
            authorName: blog.author,
            publishDate: blog.publishDate,
            modifiedOn: blog.modifiedDate,
            blogCategory: blog.category,
            comments: blog.comments,
            status: blog.status,
          };
        })
      );
    }
  };
  const handleDelete = () => {
    let a = window.confirm("Are you sure you want to delete this");
    console.log("deleting");
    if (a) {
      let arrayId = [];
      blogs.forEach((d) => {
        if (d.select) {
          arrayId.push(d.id);
        }
      });
      axios
        .delete("http://localhost:5000/blogs/deleteBlogs", {
          data: {
            blog_ids: arrayId,
          },
        })
        .then((response) => {
          console.log("response", response);
          notify(response.data.message);
          setActiveInactive(!activeInactive);
          setPageSearch(!pageSearch);
          setSelectAllCheckbox(false);
          fetchBlogs();
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  };
  const handleActive = () => {
    let activeID = [];
    blogs.forEach((active) => {
      if (active.select) {
        activeID.push(active.id);
      }
    });
    const data = {
      status: 1,
      blog_ids: activeID,
    };
    axios
      .put("http://localhost:5000/blogs/activeInactiveBlog", data)
      .then((response) => {
        console.log("edit response", response);
        if (response.status === 200) {
          notify(response.data.message);
          setSelectAllCheckbox(false);
          setActiveInactive(false);
          setPageSearch(true);
          fetchBlogs();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handleInActive = () => {
    let activeID = [];
    blogs.forEach((active) => {
      if (active.select) {
        activeID.push(active.id);
      }
    });
    const data = {
      status: 0,
      blog_ids: activeID,
    };
    axios
      .put("http://localhost:5000/blogs/activeInactiveBlog", data)
      .then((response) => {
        console.log("edit response", response);
        if (response.status === 200) {
          notify(response.data.message);
          setSelectAllCheckbox(false);
          setActiveInactive(false);
          setPageSearch(true);
          fetchBlogs();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const sortTitle = (sortByTitle) => {
    const sorted = blogs.sort((a, b) => {
      const isReverse = sortByTitle === true ? 1 : -1;
      return isReverse * a.title.localeCompare(b.title);
    });
    setBlogs(sorted);
  };
  const sortAuthor = (sortByAuthor) => {
    const sorted = blogs.sort((a, b) => {
      const isReverse = sortByAuthor === true ? 1 : -1;
      return isReverse * a.authorName.localeCompare(b.authorName);
    });
    setBlogs(sorted);
  };
  const sortPublish = (sortByPublish) => {
    const sorted = blogs.sort((a, b) => {
      let dateA = new Date(a.publishDate);
      let dateB = new Date(b.publishDate);
      return sortByPublish ? dateA - dateB : dateB - dateA;
    });
    setBlogs(sorted);
  };
  const sortModified = (sortByModified) => {
    const sorted = blogs.sort((a, b) => {
      let dateA = new Date(a.modifiedOn);
      let dateB = new Date(b.modifiedOn);
      return sortByModified ? dateA - dateB : dateB - dateA;
    });
    setBlogs(sorted);
  };
  const sortBlogCategory = (sortByBlogCategory) => {
    const sorted = blogs.sort((a, b) => {
      const isReverse = sortByBlogCategory === true ? 1 : -1;
      return isReverse * a.blogCategory.localeCompare(b.blogCategory);
    });
    setBlogs(sorted);
  };
  return (
    <div
      className={`content-container relative bg-gray-100 dark:bg-gray-700 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex-col items-center w-full">
        <PageHeading
          pageHeading={"Blogs"}
          searchLabel={"Title"}
          pageSearch={pageSearch}
          activeInactive={activeInactive}
          handleActive={handleActive}
          handleInActive={handleInActive}
          deleteBtn={true}
          handleDelete={handleDelete}
          search={search}
          setSearch={setSearch}
          handleSearch={handleSearch}
          handleReset={handleReset}
          handleClose={handleClose}
          path="/admin/blog/addBlog"
        />
        <BlogsTable
          blogs={currentBlogs}
          blogsPerPage={blogsPerPage}
          totalBlogs={blogs.length}
          paginate={paginate}
          currentPage={currentPage}
          setNumberOfEvent={setNumberOfEvent}
          selectAll={selectAll}
          selectSingle={selectSingle}
          selectAllCheckbox={selectAllCheckbox}
          setSelectAllCheckbox={setSelectAllCheckbox}
          sortTitle={sortTitle}
          sortByTitle={sortByTitle}
          setSortByTitle={setSortByTitle}
          sortAuthor={sortAuthor}
          sortByAuthor={sortByAuthor}
          setSortByAuthor={setSortByAuthor}
          sortPublish={sortPublish}
          sortByPublish={sortByPublish}
          setSortByPublish={setSortByPublish}
          sortModified={sortModified}
          sortByModified={sortByModified}
          setSortByModified={setSortByModified}
          sortBlogCategory={sortBlogCategory}
          sortByBlogCategory={sortByBlogCategory}
          setSortByBlogCategory={setSortByBlogCategory}
        />
      </div>
    </div>
  );
};

export default BlogsViewAll;
