/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable eqeqeq */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import PageHeading from "../pages/PageHeading";
import { CgAsterisk } from "react-icons/cg";
import TinyMCE from "react-tinymce";
import Dropzone from "react-dropzone";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { notify } from "../../utility";
import moment from "moment";

const AddBlog = (props) => {
  let history = useHistory();
  const [title, setTitle] = useState("");
  const [BlogCategory, setBlogCategory] = useState("");
  const [authorFname, setAuthorFname] = useState("");
  const [authorLname, setAuthorLname] = useState("");
  const [status, setStatus] = useState("1");
  const [imageAltText, setImageAltText] = useState("");
  const [publishDate, setPublishDate] = useState();
  const [image, setImage] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const [description, setDescription] = useState("");
  const [metaTitle, setMetaTitle] = useState("");
  const [metaDescription, setMetaDescription] = useState("");

  const [titleError, setTitleError] = useState("");
  const [imageError, setImageError] = useState("");
  const [imageAltError, setImageAltError] = useState("");
  const [publishDateError, setPublishDateError] = useState("");
  const [descriptionError, setDescriptionError] = useState("");
  const [metaTitleError, setMetaTitleError] = useState("");
  const [metaDescriptionError, setMetaDescriptionError] = useState("");

  let userInfo = JSON.parse(localStorage.getItem("userInfo"));
  let access_token = userInfo.access_token;

  const onImageDrop = (acceptedFiles) => {
    if (acceptedFiles.length > 0) {
      const uploads = acceptedFiles.map((image) => {
        let data = new FormData();
        data.append("folder", "blogsImages");
        data.append("image", image);
        const config = {
          method: "post",
          url: "http://localhost:5000/uploads",
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
          data: data,
        };
        axios(config)
          .then(function (response) {
            console.log("res", response.data);
            console.log("image", image);
            setImageUrl(response.data.imageUrl);
            setImage(response.data.imageName);
          })
          .catch(function (error) {
            console.log(error);
          });
      });
    }
  };
  const removeImg = () => {
    let arr = imageUrl.split("/");
    let image = arr[arr.length - 1];
    const config = {
      method: "delete",
      url: "http://localhost:5000/uploads",
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      data: {
        folder: "blogsImages",
        image: image,
      },
    };
    axios(config)
      .then(function (response) {
        setImageUrl(null);
        setImage(null);
        console.log("response", response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const addEditHandle = () => {
    let titleError = "";
    let imageError = "";
    let imageAltError = "";
    let publishDateError = "";
    let descriptionError = "";
    let metaTitleError = "";
    let metaDescriptionError = "";

    if (title.trim() === "") {
      titleError = "Enter title";
    }
    if (image.trim() === "") {
      imageError = "Upload image";
    }
    if (imageAltText.trim() === "") {
      imageAltError = "Enter image alt text";
    }
    if (publishDate === null) {
      publishDateError = "Select publish date";
    }
    if (description.trim() === "") {
      descriptionError = "Enter Blog Description";
    }
    if (metaTitle.trim() === "") {
      metaTitleError = "Enter meta title";
    }
    if (metaDescription.trim() === "") {
      metaDescriptionError = "Enter meta description";
    }
    if (
      titleError !== "" ||
      imageError !== "" ||
      imageAltError !== "" ||
      publishDateError !== "" ||
      descriptionError !== "" ||
      metaTitleError != "" ||
      metaDescriptionError != ""
    ) {
      setTitleError(titleError);
      setImageError(imageError);
      setImageAltError(imageAltError);
      setPublishDateError(publishDateError);
      setDescriptionError(descriptionError);
      setMetaTitleError(metaTitleError);
      setMetaDescriptionError(metaDescriptionError);
      return;
    }
    let blog = {
      title: title,
      category: BlogCategory,
      author_firstName: authorFname,
      author_lastName: authorLname,
      publishDate: moment(publishDate).format("YYYY-MM-DD"),
      status: status,
      imageAlt: imageAltText,
      image: image,
      content: description,
      metaTitle: metaTitle,
      metaDescription: metaDescription,
    };

    if (props.match.params.id) {
      let config = {
        method: "put",
        url: "http://localhost:5000/blogs/editBlog",
        data: blog,
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${access_token}`,
        },
      };
      axios(config)
        .then(function (response) {
          const data = response.data;
          console.log("response", response.data);
          notify(data.message);
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      let config = {
        method: "post",
        url: "http://localhost:5000/blogs",
        data: blog,
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${access_token}`,
        },
      };
      axios(config)
        .then(function (response) {
          const data = response.data;
          console.log("response", response.data);
          notify(data.message);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  const getBlog = (id) => {
    let config = {
      method: "get",
      url: "http://localhost:5000/blogs/" + id,
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };
    axios(config)
      .then((response) => {
        const data = response.data;
        setTitle(data.title);
        setBlogCategory(data.category);
        setAuthorFname(data.author_firstName);
        setAuthorLname(data.author_lastName);
        setStatus(data.status);
        setImageAltText(data.imageAlt);
        let date = moment(data.publishDate).toDate();
        setPublishDate(date);
        setImage(data.image);
        setImageUrl(data.imageUrl);
        setDescription(data.content);
        setMetaTitle(data.metaTitle);
        setMetaDescription(data.metaDescription);
        // TinyMCE.get("myTextarea").setContent(data.content);
        console.log("data.content", data.content);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const handleCancelBtn = () => {
    history.goBack();
  };
  useEffect(() => {
    let id = props.match.params.id;
    if (id) {
      getBlog(id);
    }
  }, []);
  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <PageHeading
          pageHeading="Add Category"
          showSaveOptionsBtn={true}
          cancel={true}
          handleCancelBtn={handleCancelBtn}
          save={true}
          saveAndContinue={true}
          handleSave={addEditHandle}
        />
        <div className="flex flex-col items-center w-full px-4 pb-10 sm:px-10 md:px-10 lg:px-10 xl:px-10">
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label className="mb-1 font-medium">
                Title
                <CgAsterisk className="inline text-red-500" />
              </label>
              <input
                value={title}
                onChange={(e) => {
                  setTitleError("");
                  setTitle(e.target.value);
                }}
                type="text"
                className={`${
                  titleError ? "border-red-500 border" : "border"
                } h-10 rounded px-2 text-sm font-medium `}
                placeholder="Enter title"
              />
              {titleError && (
                <span className="text-xs text-red-500">{titleError}</span>
              )}
            </div>
            <div className="flex flex-col">
              <label className="flex items-center mb-1 font-medium">
                Blog Category
              </label>
              <select
                value={BlogCategory}
                onChange={(e) => {
                  setBlogCategory(e.target.value);
                }}
                className="h-10 px-2 text-sm font-medium border rounded"
              >
                <option>Select Blog Category</option>
                <option value="server">Server</option>
                <option value="upcoming blogs">Upcoming Blogs</option>
                <option value="business">Business</option>
                <option value="technology">Technology</option>
                <option value="SEO">SEO</option>
                <option value="visual designer">Visual Designer</option>
              </select>
            </div>
          </div>
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label className="mb-1 font-medium">Author First Name</label>
              <input
                value={authorFname}
                onChange={(e) => {
                  setAuthorFname(e.target.value);
                }}
                type="text"
                className="h-10 px-2 text-sm font-medium border rounded "
              />
            </div>
            <div className="flex flex-col">
              <label className="mb-1 font-medium">Author Last Name</label>
              <input
                value={authorLname}
                onChange={(e) => {
                  setAuthorLname(e.target.value);
                }}
                type="text"
                className="h-10 px-2 text-sm font-medium border rounded "
              />
            </div>
          </div>
          <div className="w-full mt-5">
            <div>
              <label className="mb-1 font-medium">
                Image <CgAsterisk className="inline text-red-500" />
              </label>
              <div className="relative flex items-center justify-center w-full h-56 mt-2 border-2 border-gray-700 border-dashed hover:bg-black hover:bg-opacity-40">
                {image && (
                  <img
                    src={imageUrl}
                    alt="no img"
                    className="object-contain w-full h-4/5"
                  />
                )}
                {image && (
                  <button
                    onClick={() => {
                      removeImg();
                    }}
                    className="absolute z-20 text-white bg-gray-900 top-1 right-1 btn"
                  >
                    Remove
                  </button>
                )}
                {!image && (
                  <Dropzone
                    accept="image/*"
                    onDrop={(acceptedFiles) => {
                      onImageDrop(acceptedFiles);
                    }}
                  >
                    {({ getRootProps, getInputProps }) => (
                      <div
                        {...getRootProps()}
                        className="flex items-center justify-center w-full h-full"
                      >
                        <input {...getInputProps()} />
                        <p>
                          Drag 'n' drop some files here, or click to select
                          files
                        </p>
                      </div>
                    )}
                  </Dropzone>
                )}
              </div>
              {imageError && (
                <span className="text-xs text-red-500">{imageError}</span>
              )}
            </div>
            <div className="mt-1 text-xs">
              Max size 5(MB) and Recommended Size: 1900PX x 1080PX (Allowed only
              jpg, jpeg, png and gif images)
            </div>
          </div>
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label className="mb-1 font-medium">
                Image Alt <CgAsterisk className="inline text-red-500" />
              </label>
              <input
                value={imageAltText}
                onChange={(e) => {
                  setImageAltText(e.target.value);
                  setImageAltError("");
                }}
                type="text"
                className={`${
                  imageAltError ? "border-red-500 border" : "border"
                } h-10 rounded px-2 text-sm font-medium `}
              />
              {imageAltError && (
                <span className="text-xs text-red-500">{imageAltError}</span>
              )}
            </div>
            <div className="flex flex-col">
              <label className="flex items-center mb-1 font-medium">
                Publish Date
                <CgAsterisk className="inline text-red-500" />
              </label>
              <DatePicker
                selected={publishDate}
                dateFormat="dd-MM-yyyy"
                placeholderText="Publish Date"
                isClearable
                showYearDropdown
                scrollableYearDropdown
                onChange={(date) => {
                  setPublishDate(date);
                  setPublishDateError("");
                }}
                className={`${
                  publishDateError ? "border-red-500" : "border"
                } border h-10 rounded px-2 text-sm font-medium w-full`}
              />
              {publishDateError && (
                <span className="text-xs text-red-500">{publishDateError}</span>
              )}
            </div>
          </div>
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label
                for="status"
                className="flex items-center mb-1 font-medium"
              >
                Status
              </label>
              <select
                value={status}
                onChange={(e) => {
                  setStatus(e.target.value);
                }}
                className="h-10 px-2 text-sm font-medium border rounded"
              >
                <option>Select status</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
              </select>
            </div>
          </div>
          <div className="w-full my-5">
            <label for="status" className="flex items-center mb-1 font-medium">
              Description <CgAsterisk className="inline text-red-500" />
            </label>
            <div className="w-full pb-5">
              <TinyMCE
                content={description}
                config={{
                  plugins: "autolink link image lists print preview",
                  toolbar:
                    "undo redo | bold italic | alignleft aligncenter alignright",
                  menubar: "edit | insert | view | format",
                  height: 300,
                  zIndex: -1,
                }}
                onChange={(e) => {
                  setDescription(e.target.getContent());
                  setDescriptionError("");
                }}
              />
              {descriptionError && (
                <span className="text-xs text-red-500">{descriptionError}</span>
              )}
            </div>
          </div>
          <div className="flex flex-col w-full mb-5">
            <div className="w-full">
              <h1 className="text-2xl font-medium">SEO</h1>
            </div>
            <div className="flex flex-col w-full mt-2 lg:flex-row lg:space-x-2">
              <div className="flex flex-col w-full">
                <label className="mb-1 font-medium">
                  Meta Title
                  <CgAsterisk className="inline text-red-500" />
                </label>
                <input
                  value={metaTitle}
                  type="text"
                  onChange={(e) => {
                    setMetaTitle(e.target.value);
                    setMetaTitleError("");
                  }}
                  className={`${
                    metaTitleError ? "border-red-500 border" : "border"
                  } h-10 rounded px-2 text-sm font-medium `}
                />
                {metaTitleError && (
                  <span className="text-xs text-red-500">{metaTitleError}</span>
                )}
              </div>
              <div className="flex flex-col w-full">
                <label className="mb-1 font-medium">Meta Description</label>
                <textarea
                  value={metaDescription}
                  onChange={(e) => {
                    setMetaDescription(e.target.value);
                    setMetaDescriptionError("");
                  }}
                  className={`${
                    metaDescriptionError ? "border border-red-500" : "border"
                  } h-10 rounded px-2 text-sm font-medium border`}
                ></textarea>
                {metaDescriptionError && (
                  <span className="text-xs text-red-500">
                    {metaDescriptionError}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddBlog;
