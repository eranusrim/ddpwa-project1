import React, { useState } from "react";
import { CgAsterisk } from "react-icons/cg";
import PageHeading from "../pages/PageHeading";

const BlogsSetting = (props) => {
  const [recordNo, setRecordNo] = useState("5");
  const [recordError, setRecordNoError] = useState("");

  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <PageHeading
          pageHeading="Setting"
          pageSearch={false}
          showSaveOptionsBtn={true}
          save={true}
        />
        <div className="relative w-full px-4 my-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 ">
          <div className="w-full shadow-md">
            <div className="w-full border-t tab">
              <input
                className="absolute opacity-0"
                id="tab-multi-one"
                type="checkbox"
                name="tabs"
              />
              <label
                className="block px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                for="tab-multi-one"
              >
                Slider Setting
              </label>
              <div className="w-full overflow-hidden leading-normal transition-all border-l-4 border-blue-400 tab-content dark:border-red-500 lg:grid lg:grid-cols-3">
                <div className="flex flex-col p-3">
                  <label className="flex items-center mb-2 text-sm">
                    Record per page on blog listing page of front side
                    <CgAsterisk className="inline text-red-500" />
                  </label>
                  <input
                    type="text"
                    placeholder="CMS Admin"
                    value={recordNo}
                    onChange={(e) => {
                      setRecordNo(e.target.value);
                      setRecordNoError("");
                    }}
                    className={`${
                      recordError ? "border-red-500" : "border"
                    } border h-10 rounded px-2 text-sm font-medium `}
                  />
                  {recordError && (
                    <span className="text-xs text-red-500">{recordError}</span>
                  )}
                </div>
                <div className="flex flex-col p-3">
                  <label className="flex items-center mb-2 text-sm">
                    Display slider title and overlay on Home Page
                  </label>
                  <select className="h-10 px-2 text-sm font-medium border rounded">
                    <option value="yes with mail notification">
                      Yes with mail notification
                    </option>
                    <option value="yes without mail notification">
                      Yes without mail notification
                    </option>
                    <option value="no">No</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BlogsSetting;
