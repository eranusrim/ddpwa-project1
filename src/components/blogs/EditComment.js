/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import PageHeading from "../pages/PageHeading";
import { useHistory } from "react-router-dom";
import axios from "axios";
import FormData from "form-data";
import { notify } from "../../utility";

const EditComment = (props) => {
  const history = useHistory();
  const [fname, setFame] = useState("");
  const [lname, setLame] = useState("");
  const [status, setStatus] = useState("1");
  const [comment, setComment] = useState("");
  let userInfo = JSON.parse(localStorage.getItem("userInfo"));
  let access_token = userInfo.access_token;

  const [commentError, setCommentError] = useState("");
  let id = props.match.params.id;

  const handleEdit = () => {
    let commentError = "";
    if (comment.trim() === "") {
      commentError = "Please Enter comment";
    }
    if (commentError !== "") {
      setCommentError(commentError);
      return;
    }
    const data = {
      comment: comment,
      status: status,
      id: id,
    };
    axios
      .put("http://localhost:5000/blogs/editComment", data)
      .then((response) => {
        console.log("edit response", response);
        if (response.status === 200) {
          notify(response.data.message);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const fetchComment = () => {
    let config = {
      method: "get",
      url: "http://localhost:5000/blogs/blogComment/" + id,
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };

    axios(config)
      .then((response) => {
        console.log("comments response", response.data.comments);
        response.data.comments.map((comment) => {
          setFame(comment.commentBy);
          setStatus(comment.status);
          setComment(comment.comment);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handleCancelBtn = () => {
    history.goBack();
  };
  useEffect(() => {
    fetchComment();
  }, []);
  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex-col items-center w-full">
        <PageHeading
          pageHeading="Edit Comment"
          showSaveOptionsBtn={true}
          cancel={true}
          handleCancelBtn={handleCancelBtn}
          save={true}
          saveAndContinue={true}
          handleSave={handleEdit}
        />
        <div className="flex flex-col items-center w-full px-4 pb-10 sm:px-10 md:px-10 lg:px-10 xl:px-10">
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label className="mb-1 font-medium">First Name</label>
              <input
                value={fname}
                disabled
                type="text"
                className="h-10 px-2 text-sm font-medium border rounded "
              />
            </div>
            <div className="flex flex-col">
              <label className="mb-1 font-medium">Last Name</label>
              <input
                value={lname}
                disabled
                type="text"
                className="h-10 px-2 text-sm font-medium border rounded "
              />
            </div>
          </div>
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col w-full">
              <label className="mb-1 font-medium">Comment</label>
              <textarea
                value={comment}
                onChange={(e) => {
                  setComment(e.target.value);
                  setCommentError("");
                }}
                className={`${
                  commentError ? "border border-red-500" : "border"
                } h-10 rounded px-2 text-sm font-medium border`}
              ></textarea>
              {commentError && (
                <span className="text-xs text-red-500">{commentError}</span>
              )}
            </div>
            <div className="flex flex-col">
              <label
                for="status"
                className="flex items-center mb-1 font-medium"
              >
                Status
              </label>
              <select
                value={status}
                onChange={(e) => {
                  setStatus(e.target.value);
                }}
                className="h-10 px-2 text-sm font-medium border rounded"
              >
                <option>Select status</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditComment;
