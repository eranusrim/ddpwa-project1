import React, { useState, useEffect } from "react";
import { RiArrowDownFill, RiArrowUpFill } from "react-icons/ri";
import { useHistory } from "react-router-dom";

const BlogsTable = ({
    blogs,
    blogsPerPage,
    totalBlogs,
    paginate,
    currentPage,
    setNumberOfEvent,
    selectAll,
    selectSingle,
    selectAllCheckbox,
    setSelectAllCheckbox,

    sortTitle,
    sortByTitle,
    setSortByTitle,
    sortAuthor,
    sortByAuthor,
    setSortByAuthor,
    sortPublish,
    sortByPublish,
    setSortByPublish,
    sortModified,
    sortByModified,
    setSortByModified,
    sortBlogCategory,
    sortByBlogCategory,
    setSortByBlogCategory,
}) => {
    const pageNumbers = [];
    let history = useHistory();
    for (let i = 1; i <= Math.ceil(totalBlogs / blogsPerPage); i++) {
        pageNumbers.push(i);
    }
    let [sortTitleArrows, setSortTitleArrows] = useState(false);
    let [sortAuthorArrows, setSortAuthorArrows] = useState(true);
    let [sortPublishArrows, setSortPublishArrows] = useState(true);
    let [sortModifiedArrows, setSortModifiedArrows] = useState(true);
    let [sortBlogCategoryArrows, setSortBlogCategoryArrows] = useState(true);

    useEffect(() => {
        console.log("blogs", blogs);
    }, [blogs]);
    return (
        <div className="overflow-x-scroll scrollbar-hide  dark:bg-gray-700 py-10 px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10">
            <table className="w-full  border-r  hidden sm:table md:table lg:table xl:table border-gray-200 border-b">
                <thead className="bg-gray-200 dark:bg-gray-300 dark:text-white ">
                    <tr className="border-l-8 border-gray-400">
                        <th
                            scope="col"
                            className="dark:text-gray-700 px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            <input
                                checked={selectAllCheckbox}
                                onChange={(e) => {
                                    setSelectAllCheckbox(!selectAllCheckbox);
                                    selectAll(e);
                                }}
                                type="checkbox"
                            />
                        </th>
                        <th
                            onClick={() => {
                                sortTitle(sortByTitle);
                                setSortByTitle(!sortByTitle);
                                setSortTitleArrows(false);
                                setSortAuthorArrows(true);
                                setSortPublishArrows(true);
                                setSortModifiedArrows(true);
                                setSortBlogCategoryArrows(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700 cursor-pointer px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            <span className="inline-block mr-2">Title</span>
                            {sortTitleArrows && (
                                <>
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortTitleArrows && (
                                <>
                                    {sortByTitle === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                sortAuthor(sortByAuthor);
                                setSortByAuthor(!sortByAuthor);
                                setSortAuthorArrows(false);
                                setSortTitleArrows(true);
                                setSortPublishArrows(true);
                                setSortModifiedArrows(true);
                                setSortBlogCategoryArrows(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 cursor-pointer text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Author
                            {sortAuthorArrows && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortAuthorArrows && (
                                <>
                                    {sortByAuthor === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                sortPublish(sortByPublish);
                                setSortByPublish(!sortByPublish);
                                setSortPublishArrows(false);
                                setSortTitleArrows(true);
                                setSortAuthorArrows(true);
                                setSortModifiedArrows(true);
                                setSortBlogCategoryArrows(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 cursor-pointer text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Publish Date
                            {sortPublishArrows && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortPublishArrows && (
                                <>
                                    {sortByPublish === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                sortModified(sortByModified);
                                setSortByModified(!sortByModified);
                                setSortModifiedArrows(false);
                                setSortPublishArrows(true);
                                setSortTitleArrows(true);
                                setSortAuthorArrows(true);
                                setSortBlogCategoryArrows(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 cursor-pointer text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            {" "}
                            Modified On
                            {sortModifiedArrows && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortModifiedArrows && (
                                <>
                                    {sortByModified === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                sortBlogCategory(sortByBlogCategory);
                                setSortByBlogCategory(!sortByBlogCategory);
                                setSortBlogCategoryArrows(false);
                                setSortModifiedArrows(true);
                                setSortPublishArrows(true);
                                setSortTitleArrows(true);
                                setSortAuthorArrows(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 cursor-pointer text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Blog Category
                            {sortBlogCategoryArrows && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortBlogCategoryArrows && (
                                <>
                                    {sortByBlogCategory === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {}}
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 cursor-pointer text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Comment
                            {/* {sortSlugArrows && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortSlugArrows && (
                                <>
                                    {sortBySlug === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )} */}
                        </th>
                        <th
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Edit
                        </th>
                    </tr>
                </thead>
                <tbody className="bg-white dark:bg-gray-700">
                    {blogs &&
                        blogs.map((blog) => (
                            <tr
                                key={blog.id}
                                className={`${
                                    blog.status === 1
                                        ? "border-left-green-8 "
                                        : "border-left-red-8"
                                } dark:hover:bg-gray-500 hover:bg-gray-200  border-b-gray-500 border-b`}
                            >
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500 dark:text-white">
                                    <input
                                        checked={blog.select}
                                        onChange={(e) =>
                                            selectSingle(e, blog.id)
                                        }
                                        type="checkbox"
                                    />
                                </td>
                                <td className="px-6 py-4 cursor-pointer  text-sm capitalize text-gray-600 dark:text-white">
                                    <span
                                        onClick={() => {
                                            history.push({
                                                pathname: "/admin/blog/addBlog",
                                                blog: blog,
                                            });
                                        }}
                                        className="hover:underline pb-2 inline-block hover:text-blue-500 hover:font-semibold"
                                    >
                                        {blog.title}
                                    </span>
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    {blog.authorName}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    {blog.publishDate}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    {blog.modifiedOn}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    {blog.blogCategory}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    <span
                                        onClick={() => {
                                            if (
                                                blog.comments &&
                                                blog.comments.length > 0
                                            ) {
                                                console.log("comments", blog);
                                                history.push(
                                                    "/admin/blog/comment/" +
                                                        blog.id
                                                );
                                            }
                                        }}
                                        className="text-blue-400 underline pb-1 cursor-pointer"
                                    >
                                        Comment [{blog.comments.length}]
                                    </span>
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap  text-sm font-medium text-left">
                                    <button
                                        onClick={() => {
                                            history.push(
                                                "/admin/blog/editBlog/" +
                                                    blog.id
                                            );
                                        }}
                                        className=" text-blue-400 hover:text-blue-500"
                                    >
                                        Edit
                                    </button>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </table>

            <div className="overflow-hidden  block sm:hidden md:hidden lg:hidden xl:hidden border-r dark:bg-gray-600">
                <div className="bg-gray-200 px-5 py-3 w-full border-t">
                    <input
                        className="ml-3"
                        checked={selectAllCheckbox}
                        onChange={(e) => {
                            setSelectAllCheckbox(!selectAllCheckbox);
                            selectAll(e);
                        }}
                        type="checkbox"
                    />
                    <label
                        onClick={() => {
                            sortTitle(sortByTitle);
                            setSortByTitle(!sortByTitle);
                            setSortTitleArrows(false);
                            setSortAuthorArrows(true);
                            setSortPublishArrows(true);
                            setSortModifiedArrows(true);
                            setSortBlogCategoryArrows(true);
                        }}
                        className="px-5 py-3 text-sm font-medium leading-normal"
                    >
                        Title{" "}
                        {sortTitleArrows && (
                            <>
                                <RiArrowUpFill className="inline-block ml-1" />
                                <RiArrowDownFill className="inline-block ml-1" />
                            </>
                        )}
                    </label>
                </div>
                {blogs &&
                    blogs.map((blog) => (
                        <div
                            className={`tab w-full border-t ${
                                blog.status === "1"
                                    ? "border-left-green-8 "
                                    : "border-left-red-8"
                            }`}
                        >
                            <input
                                className="absolute opacity-0"
                                id={blog.id}
                                type="checkbox"
                                name="tabs"
                            />
                            <label
                                className="flex items-center justify-between px-5 py-3 text-sm font-medium leading-normal cursor-pointer bg-blue-400 text-white dark:bg-gray-700 dark:text-white"
                                for={blog.id}
                            >
                                <input
                                    checked={blog.select}
                                    onChange={(e) => selectSingle(e, blog.id)}
                                    type="checkbox"
                                />
                                <span className="px-5 block w-11/12">
                                    {blog.title}
                                </span>
                            </label>
                            <div className="tab-content border-t   overflow-hidden w-full">
                                <div className="p-4">
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Author:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {blog.authorName}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Publish Date:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {blog.publishDate}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Modified on:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {blog.modifiedOn}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Category:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {blog.blogCategory}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Comments:
                                        </h1>
                                        <h1
                                            onClick={() => {
                                                if (
                                                    blog.comments &&
                                                    blog.comments.length > 0
                                                ) {
                                                    console.log(
                                                        "comments",
                                                        blog
                                                    );
                                                    history.push(
                                                        "/admin/blog/comment/" +
                                                            blog.id
                                                    );
                                                }
                                            }}
                                            className="text-blue-400 underline cursor-pointer text-left pl-2"
                                        >
                                            Comment [{blog.comments.length}]
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Edit:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            <h1
                                                onClick={() => {
                                                    history.push(
                                                        "/admin/blog/editBlog/" +
                                                            blog.id
                                                    );
                                                }}
                                                className="cursor-pointer underline text-blue-400 hover:text-blue-500"
                                            >
                                                Edit
                                            </h1>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
            </div>
            {/* pagination */}
            <div className="w-full flex flex-col sm:flex-row md:flex-row lg:flex-row xl:flex-row justify-between mt-5 mb-3">
                <ul className="flex items-center justify-start cursor-pointer space-x-2">
                    {pageNumbers.map((number) => (
                        <li
                            key={number}
                            className={`p-3 text-sm ${
                                currentPage === number
                                    ? "bg-red-500"
                                    : "bg-blue-400"
                            }  text-white liTags`}
                            onClick={() => paginate(number)}
                        >
                            {number}
                        </li>
                    ))}
                </ul>
                <div className="flex items-center justify center mt-3 sm:mt-0 md:mt-0 lg:mt-0 xl:mt-0">
                    <span className="pr-2 dark:text-white">Show</span>
                    <input
                        type="number"
                        value={blogsPerPage}
                        className="px-1 py-1 w-24 border border-black"
                        onChange={(e) => setNumberOfEvent(e.target.value)}
                    />
                    <span className="pl-2 dark:text-white">Entries</span>
                </div>
            </div>
        </div>
    );
};
export default BlogsTable;
