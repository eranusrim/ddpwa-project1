import React from "react";
import PageHeading from "../pages/PageHeading";

const SliderSetting = (props) => {
    return (
        <div
            className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
                props.Sidebar
                    ? "w-full sm:content md:content lg:content xl:content"
                    : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
            }`}
        >
            <div className="flex flex-col items-center w-full">
                <PageHeading
                    pageHeading="Setting"
                    pageSearch={false}
                    showSaveOptionsBtn={true}
                    save={true}
                />
                <div className="w-full px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 my-4 relative ">
                    <div className="shadow-md w-full">
                        <div className="tab w-full border-t">
                            <input
                                className="absolute opacity-0"
                                id="tab-multi-one"
                                type="checkbox"
                                name="tabs"
                            />
                            <label
                                className="block px-5 py-3 text-sm font-medium leading-normal cursor-pointer bg-blue-400 text-white dark:bg-gray-700 dark:text-white"
                                for="tab-multi-one"
                            >
                                Slider Setting
                            </label>
                            <div className="tab-content overflow-hidden transition-all  border-l-4 border-blue-400 dark:border-red-500  leading-normal lg:grid lg:grid-cols-3 w-full">
                                <div className="p-3 flex flex-col">
                                    <label className="text-sm mb-2 flex items-center">
                                        Display slider title and overlay on Home
                                        Page
                                    </label>
                                    <select className="border h-10 rounded px-2 text-sm font-medium">
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SliderSetting;
