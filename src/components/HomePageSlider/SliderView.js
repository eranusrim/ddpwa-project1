/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";
import {
  AiOutlineSearch,
  AiOutlinePlus,
  AiOutlineDelete,
  AiOutlineEye,
} from "react-icons/ai";
import { MdDone } from "react-icons/md";
import { BsPencil } from "react-icons/bs";
import { RiRecordCircleLine } from "react-icons/ri";
import { VscCircleOutline } from "react-icons/vsc";
import { useSelector, useDispatch } from "react-redux";
import {
  changeAlternativeText,
  deleteSliderImage,
  deleteAll,
} from "../../Redux/actions/fetchSliderImagesAction";
import { useHistory } from "react-router-dom";
import { useDropzone } from "react-dropzone";

function AcceptMaxFiles(props) {
  // let [uploadArray, setUploadArray] = useState([]);
  let imgSize = 10485760;
  const { acceptedFiles, getRootProps, getInputProps, open, fileRejections } =
    useDropzone({
      accept: "image/*",
      noKeyboard: true,
      noClick: true,
      maxSize: imgSize,
    });
  // acceptedFiles.map((file) => {
  //     setUploadArray((arr) => [...arr, URL.createObjectURL(file)]);
  // });
  fileRejections.map((file) => alert("file size must be less  than 10 mb"));

  const acceptedFileItems = acceptedFiles.map((file) => (
    <li
      key={file.path}
      className="flex items-center justify-between w-full px-1 pb-2 my-4 border-b border-gray-800"
    >
      <img
        className="w-14 h-14"
        alt={file.path}
        src={URL.createObjectURL(file)}
      />
      <h1 className="px-2 text-sm">{file.path}</h1>
      <h1>{file.size} bytes</h1>
      <div className="flex space-x-1">
        <button className="text-sm bg-blue-400 btn">Upload</button>
        <button
          className="text-sm bg-black btn"
          onClick={() => {
            let index = acceptedFiles.indexOf(file);
            console.log("index", index, file);
            acceptedFiles.splice(index, 1);
          }}
        >
          Cancel
        </button>
      </div>
    </li>
  ));
  return (
    <section className="container">
      <div className="flex w-full space-x-3">
        <button
          type="button"
          className="text-xs bg-green-600 btn"
          onClick={open}
        >
          Add Files
        </button>
        <button
          type="button"
          className="text-xs bg-blue-400 btn"
          onClick={open}
        >
          Start Upload
        </button>
      </div>
      <div class="w-full mt-2">
        <span className="text-sm">
          Max size 10(MB) and Recommended Size: 1900PX x 1080PX (Allowed only
          jpg, jpeg, png and gif images)
        </span>
      </div>
      <div
        {...getRootProps({ className: "dropzone" })}
        className="flex items-center justify-center w-full h-40 my-4 bg-gray-200 border-2 border-black border-dashed rounded cursor-pointer"
      >
        <input {...getInputProps()} />
        <p>Drag 'n' drop some files here</p>
      </div>

      <ul className="w-full">{acceptedFileItems}</ul>
    </section>
  );
}

const SliderView = (props) => {
  let dispatch = useDispatch();
  let history = useHistory();
  let [searchBar, showSearch] = useState(false);
  let [search, setSearch] = useState("");
  let [activeValue, setActive] = useState("");
  const images = useSelector((state) => state.SliderImagesReducer.images);
  const [imagesData, setImagesData] = useState([]);
  const [alternativeText, setAlternativeText] = useState("");
  const [showImgTextEditor, setShowImgTextEditor] = useState("");
  const [viewImg, setViewImg] = useState("");
  const [allImgChecked, setAllChecked] = useState(false);

  useEffect(() => {
    if (images) {
      setImagesData(
        images.map((d) => {
          return {
            select: false,
            id: d.id,
            image: d.image,
            alternativeText: d.alternativeText,
            activeSlider: d.activeSlider,
          };
        })
      );
    }
  }, [images]);
  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <div className="flex flex-col items-start justify-between w-full px-4 py-5 bg-gray-300 sm:px-10 md:px-10 lg:px-10 xl:px-10 dark:bg-gray-400">
          <div className="flex items-center justify-between w-full">
            <div className="flex items-center justify-start">
              <h1 className="text-xl font-semibold sm:text-2xl md:text-3xl lg:text-4xl xl:text-4xl sm:font-normal">
                Home Page Slider
              </h1>
            </div>
            {!allImgChecked ? (
              <div className="flex items-center space-x-1">
                <button
                  onClick={() => showSearch(true)}
                  className="flex items-center text-xs bg-blue-600 btn"
                >
                  <AiOutlineSearch />
                  <span className="pl-1">Search</span>
                </button>
                <a
                  href="#imagesAdd"
                  className="flex items-center text-xs bg-blue-600 btn"
                >
                  <AiOutlinePlus />
                  <span className="pl-1">Add</span>
                </a>
              </div>
            ) : (
              <div className="flex items-center space-x-1">
                <button className="flex items-center text-xs bg-blue-600 btn">
                  <RiRecordCircleLine />
                  <span className="pl-1">Active</span>
                </button>
                <button className="flex items-center text-xs bg-blue-600 btn">
                  <VscCircleOutline size={18} />
                  <span className="pl-1">Inactive</span>
                </button>
                <button
                  onClick={() => {
                    let a = window.confirm(
                      "Are you sure you want to delete this"
                    );
                    if (a) {
                      dispatch(deleteAll(imagesData));
                    }
                  }}
                  className="flex items-center text-xs bg-blue-600 btn"
                >
                  <AiOutlineDelete />
                  <span className="pl-1">Delete</span>
                </button>
              </div>
            )}
          </div>
          {searchBar && (
            <div className="w-full mt-4">
              <div className="flex w-full space-x-2">
                <div className="flex flex-col w-52">
                  <label fro="search" className="text-sm font-medium">
                    Search Home Sliders
                  </label>
                  <input
                    type="Search"
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                    name="search"
                    placeholder="Search Here..."
                    className="px-2 text-sm border-none rounded h-9 bg"
                  />
                </div>
                <div className="flex flex-col w-52">
                  <label for="active" className="text-sm font-medium">
                    Status
                  </label>
                  <select
                    value={activeValue}
                    onChange={(e) => {
                      let a = e.target.value;
                      console.log(a);
                      setActive(e.target.value);
                      let filterData = [];
                      if (a) {
                        filterData = imagesData.filter((d) => {
                          if (d.activeSlider === true) {
                            console.log(d);

                            return d;
                          }
                        });
                      } else {
                        filterData = imagesData.filter((d) => {
                          if (d.activeSlider === false) {
                            console.log(d);

                            return d;
                          }
                        });
                      }
                      console.log(filterData);
                      if (filterData.length === 0) {
                        setImagesData(imagesData);
                      } else {
                        setImagesData(filterData);
                      }
                    }}
                    name="active"
                    className="text-sm border-none rounded h-9 bg"
                  >
                    <option>Select status</option>
                    <option value={true}>Active</option>
                    <option value={false}>Inactive</option>
                  </select>
                </div>
              </div>
              <div className="flex mt-4 space-x-2">
                <button
                  onClick={() => {
                    let searchData = imagesData.filter((d) => {
                      if (
                        d.alternativeText
                          .toLocaleLowerCase()
                          .includes(search.toLocaleLowerCase())
                      ) {
                        console.log(d);
                        return d;
                      }
                    });
                    console.log("length", searchData.length);
                    console.log(searchData);
                    if (searchData.length === 0) {
                      setImagesData(imagesData);
                    } else {
                      setImagesData(searchData);
                    }
                  }}
                  className="flex items-center bg-blue-600 btn"
                >
                  Search
                </button>
                <button
                  onClick={() => {
                    setSearch("");
                    if (images) {
                      setImagesData(
                        images.map((d) => {
                          return {
                            select: false,
                            id: d.id,
                            image: d.image,
                            alternativeText: d.alternativeText,
                            activeSlider: d.activeSlider,
                          };
                        })
                      );
                    }
                  }}
                  className="flex items-center bg-blue-600 btn"
                >
                  Reset
                </button>
                <button
                  onClick={() => {
                    showSearch(false);
                    if (images) {
                      setImagesData(
                        images.map((d) => {
                          return {
                            select: false,
                            id: d.id,
                            image: d.image,
                            alternativeText: d.alternativeText,
                            activeSlider: d.activeSlider,
                          };
                        })
                      );
                    }
                  }}
                  className="flex items-center bg-gray-900 btn"
                >
                  Close
                </button>
              </div>
            </div>
          )}
        </div>
        <div className="w-full px-10 mt-3">
          <input
            type="checkbox"
            onChange={(e) => {
              let checked = e.target.checked;
              if (checked) {
                setAllChecked(true);
              } else {
                setAllChecked(false);
              }
              setImagesData(
                imagesData.map((d) => {
                  d.select = checked;
                  return d;
                })
              );

              console.log(imagesData);
            }}
          />
          <label className="pl-2">Select/Unselect All</label>
        </div>
        <div className="grid w-full gap-4 px-10 my-2 lg:grid-cols-3">
          {imagesData &&
            imagesData.map((imageData) => (
              <div
                key={imageData.id}
                className={`${
                  imageData.activeSlider ? "border-green-400" : "border-white"
                } border-2 bg-white dark:bg-gray-400  rounded shadow-lg`}
              >
                <div className="relative w-full h-44 imgHover">
                  <img
                    src={imageData.image}
                    alt={imageData.alternativeText}
                    className="object-cover w-full h-full"
                  />
                  <div className="absolute inset-0 items-center justify-center hidden w-full h-full space-x-2 text-white bg-black imgHoverDisplay bg-opacity-70">
                    <button
                      onClick={() => {
                        setViewImg(imageData.id);
                      }}
                      className="flex items-center justify-center w-8 h-8 bg-blue-400 rounded-full"
                    >
                      <AiOutlineEye />
                    </button>
                    <button
                      onClick={() => {
                        history.push({
                          pathname: "/admin/sliders/edit",
                          state: {
                            title: imageData.alternativeText,
                            id: imageData.id,
                            image: imageData.image,
                            alternativeText: imageData.alternativeText,
                            activeSlider: imageData.activeSlider,
                          },
                        });
                      }}
                      className="flex items-center justify-center w-8 h-8 bg-green-500 rounded-full"
                    >
                      <BsPencil />
                    </button>
                    <button
                      onClick={() => {
                        let a = window.confirm(
                          "Are you sure you want to delete this"
                        );
                        if (a) {
                          dispatch(deleteSliderImage(imageData));
                        }
                      }}
                      className="flex items-center justify-center w-8 h-8 text-lg bg-black rounded-full"
                    >
                      x
                    </button>
                  </div>
                </div>
                {viewImg === imageData.id && (
                  <div className="absolute inset-0 z-20 flex items-center justify-center bg-black bg-opacity-30">
                    <div className="flex flex-col items-center justify-center w-1/2">
                      <div className="flex justify-end w-full">
                        <div className="mb-2">
                          <label
                            onClick={() => {
                              setViewImg("");
                            }}
                            className="flex items-center justify-center w-6 h-6 text-xl text-white bg-black rounded-full cursor-pointer"
                          >
                            x
                          </label>
                        </div>
                      </div>
                      <img
                        className="object-contain"
                        src={imageData.image}
                        alt={imageData.alternativeText}
                      />
                    </div>
                  </div>
                )}
                <div className="flex items-center justify-between w-full px-2 py-3 h-14">
                  <div>
                    <input
                      type="checkbox"
                      name="checkImg"
                      checked={imageData.select}
                      onChange={(e) => {
                        let checked = e.target.checked;
                        setImagesData(
                          imagesData.map((data) => {
                            if (imageData.id === data.id) {
                              data.select = checked;
                            }
                            return data;
                          })
                        );

                        console.log(imagesData);
                        const result = imagesData.some(function (data) {
                          return data.select === true;
                        });
                        setAllChecked(result);
                      }}
                    />
                  </div>
                  <div className="flex justify-end w-1/2">
                    {showImgTextEditor === imageData.id ? (
                      <div className="flex space-x-2">
                        <input
                          type="text"
                          alternativeText
                          onChange={(e) => setAlternativeText(e.target.value)}
                          className="px-2 text-sm border border-blue-400"
                        />
                        <button
                          onClick={() => {
                            dispatch(
                              changeAlternativeText(
                                imageData.id,
                                alternativeText
                              )
                            );

                            setShowImgTextEditor("");
                          }}
                          className="bg-blue-400 border btn"
                        >
                          <MdDone />
                        </button>
                        <button
                          onClick={() => {
                            setShowImgTextEditor("");
                          }}
                          className="text-white bg-black border btn"
                        >
                          x
                        </button>
                      </div>
                    ) : (
                      <label
                        onClick={() => {
                          setShowImgTextEditor(imageData.id);
                        }}
                        className="inline-block overflow-hidden tracking-wide text-blue-400 border-b border-blue-400 border-dashed cursor-pointer dark:text-white"
                      >
                        {imageData.alternativeText}
                      </label>
                    )}
                  </div>
                </div>
              </div>
            ))}
        </div>
        <div className="w-full px-10 my-4" id="imagesAdd">
          <AcceptMaxFiles />
        </div>
      </div>
    </div>
  );
};

export default SliderView;
