/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useDropzone } from "react-dropzone";

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

function MyDropzone(props) {
  const [files, setFiles] = useState([]);

  const onDrop = useCallback(
    (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
      props.getBanner(files);
      console.log(acceptedFiles);
    },
    [files]
  );

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    onDrop,
    accept: "image/*",
    multiple: false,
    maxSize: 10485760,
  });

  useMemo(
    () => ({
      // ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject, isDragAccept]
  );

  const thumbs = files.map((file) => (
    <div key={file.name} className="w-full h-48">
      <img
        src={file.preview}
        alt={file.name}
        className="object-contain w-full h-full"
      />
    </div>
  ));

  // clean up
  useEffect(
    () => () => {
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },

    [files]
  );

  return (
    <>
      <section className="">
        <div
          {...getRootProps()}
          className="flex items-center justify-center p-3 text-gray-800 bg-gray-300 border-2 border-gray-500 border-dashed rounded  h-52 dark:bg-gray-100"
        >
          {files.length <= 0 && <input {...getInputProps()} />}
          {files.length > 0 ? (
            <aside className="relative flex items-center justify-center w-full h-full">
              {thumbs}
              <div className="absolute flex items-center justify-center">
                <button
                  className="text-white bg-gray-500 btn"
                  onClick={() => {
                    setFiles([]);
                  }}
                >
                  Remove
                </button>
              </div>
            </aside>
          ) : (
            <div className="flex items-center justify-center w-full h-full">
              Drag and drop your images here.
            </div>
          )}
        </div>
      </section>
      {props.imageError && (
        <span className="text-xs text-red-500">{props.imageError}</span>
      )}
    </>
  );
}

export default MyDropzone;
