import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PageHeading from "../pages/PageHeading";
import { CgAsterisk } from "react-icons/cg";
import MyDropzone from "./SliderImageEdit";

const SliderEdit = (props) => {
    const history = useHistory();
    const [title, setTitle] = useState("");
    const [order, setOrder] = useState("");
    const [overlayText, setOverlayText] = useState("");
    const [image, setImage] = useState("");
    const [ImageAltText, setImageAltText] = useState("");
    const [status, setStatus] = useState("");

    const [titleError, setTitleError] = useState("");
    const [imageError, setImageError] = useState("");
    const [imageAltError, setImageAltError] = useState("");

    const saveEdit = () => {
        if (title.trim() === "") {
            setTitleError("Enter title");
            return;
        }
        if (image.length === 0) {
            setImageError("Upload image");
            return;
        }
        if (ImageAltText.trim() === "") {
            setImageAltError("Enter alternative text");
            return;
        }

        let editData = {
            title: title,
            order: order,
            overlayText: overlayText,
            image: image,
            alternativeText: ImageAltText,
            status: status,
        };
        console.log(editData);
    };

    const handleCancelBtn = () => {
        history.goBack();
    };
    useEffect(() => {
        setTitle(history.location.state.title);
        setImage(history.location.state.image);
        setStatus(history.location.state.status);
        setImageAltText(history.location.state.alternativeText);
    }, [history.location.state]);

    const getBanner = (banner) => {
        setImage(banner);
    };
    return (
        <div
            className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
                props.Sidebar
                    ? "w-full sm:content md:content lg:content xl:content"
                    : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
            }`}
        >
            <div className="flex-col items-center w-full">
                <PageHeading
                    pageHeading={"Edit Slider"}
                    showSaveOptionsBtn={true}
                    saveAndContinue={true}
                    save={true}
                    handleCancelBtn={handleCancelBtn}
                    cancel={true}
                    handleSave={saveEdit}
                />
                <div className="w-full px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 flex items-center my-4">
                    <div className="grid lg:grid-cols-2  gap-5 w-full">
                        <div className="flex flex-col w-full">
                            <label
                                for="fname"
                                className="font-medium mb-1 flex items-center"
                            >
                                Title
                                <CgAsterisk className="inline text-red-500" />
                            </label>
                            <input
                                type="text"
                                value={title}
                                onChange={(e) => {
                                    setTitleError("");
                                    setTitle(e.target.value);
                                }}
                                className=" 
                                border h-10 rounded px-2 text-sm font-medium "
                                name="title"
                            />
                            {titleError && (
                                <span className="text-red-500 text-xs">
                                    {titleError}
                                </span>
                            )}
                        </div>
                        <div className="flex flex-col w-full">
                            <label
                                for="fname"
                                className="font-medium mb-1 flex items-center"
                            >
                                Display Order
                            </label>
                            <select
                                value={order}
                                onChange={(e) => {
                                    setOrder(e.target.value);
                                }}
                                name="order"
                                className="border h-10 rounded px-2 text-sm font-medium"
                            >
                                <option>Select order</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                            </select>
                        </div>
                        <div className="flex flex-col w-full col-span-2">
                            <label
                                for="fname"
                                className="font-medium mb-1 flex items-center"
                            >
                                Overlay Text
                            </label>
                            <input
                                type="text"
                                value={overlayText}
                                onChange={(e) => {
                                    setOverlayText(e.target.value);
                                }}
                                className=" 
                                border h-10 rounded px-2 text-sm font-medium "
                                name="overlayText"
                            />
                        </div>
                        <div className="flex flex-col w-full col-span-2">
                            <label
                                for="fname"
                                className="font-medium mb-1 flex items-center"
                            >
                                Image
                                <CgAsterisk className="inline text-red-500" />
                            </label>
                            <div class="w-full mt-1">
                                <span className="text-sm">
                                    Max size 10(MB) and Recommended Size: 1900PX
                                    x 1080PX (Allowed only jpg, jpeg, png and
                                    gif images)
                                </span>
                            </div>
                            <MyDropzone
                                getBanner={getBanner}
                                setImageError={setImageError}
                                imageError={imageError}
                                img={history.location.state.image}
                            />
                            {/* <img
                                src={editData.image}
                                className=" 
                                border h-44 rounded  text-sm font-medium   object-contain"
                            /> */}
                        </div>
                        <div className="flex flex-col w-full">
                            <label
                                for="fname"
                                className="font-medium mb-1 flex items-center"
                            >
                                Image Alt
                                <CgAsterisk className="inline text-red-500" />
                            </label>
                            <input
                                type="text"
                                value={ImageAltText}
                                className="border h-10 rounded px-2 text-sm font-medium "
                                name="altText"
                                onChange={(e) => {
                                    setImageAltError("");
                                    setImageAltText(e.target.value);
                                }}
                            />
                            {imageAltError && (
                                <span className="text-red-500 text-xs">
                                    {imageAltError}
                                </span>
                            )}
                        </div>
                        <div className="flex flex-col w-full">
                            <label
                                for="fname"
                                className="font-medium mb-1 flex items-center"
                            >
                                Status
                            </label>
                            <select
                                value={status ? "active" : "inactive"}
                                onChange={(e) => {
                                    setStatus(e.target.value);
                                }}
                                name="order"
                                className="border h-10 rounded px-2 text-sm font-medium"
                            >
                                <option>Select Status</option>
                                <option value={false}>Inactive</option>
                                <option value={true}>Active</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SliderEdit;
