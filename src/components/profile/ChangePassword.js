/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import PageHeading from "../pages/PageHeading";
import { CgAsterisk } from "react-icons/cg";
import { notify } from "../../utility";
import axios from "axios";
import FormData from "form-data";

const ChangePassword = (props) => {
  let history = useHistory();
  // let [userPassword, setUserPassword] = useState("");

  const [currentPassword, setCurrentPassword] = useState("");
  const [currentPasswordError, setCurrentPasswordError] = useState("");

  const [newPassword, setNewPassword] = useState("");
  const [newPasswordError, setNewPasswordError] = useState("");

  const [confirmPassword, setConfirmPassword] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");

  const data = new FormData();
  let userInfo = JSON.parse(localStorage.getItem("userInfo"));
  let access_token = userInfo.access_token;

  let regEX =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{5,10}$/;

  const handleCancelBtn = () => {
    history.goBack();
  };
  let ChangePassword = () => {
    let currentPasswordError = "";
    let newPasswordError = "";
    let confirmPasswordError = "";

    if (currentPassword.trim() === "") {
      currentPasswordError = "Enter current password";
    }
    if (newPassword.trim() === "") {
      newPasswordError = "Enter new password before save";
    }
    if (confirmPassword.trim() === "") {
      confirmPasswordError = "Enter Confirm password before save";
    }
    if (newPassword.length < 5) {
      newPasswordError = "Password length must be between 5 and 10";
    }

    if (
      currentPasswordError !== "" ||
      newPasswordError !== "" ||
      confirmPasswordError !== ""
    ) {
      setCurrentPasswordError(currentPasswordError);
      setNewPasswordError(newPasswordError);
      setConfirmPasswordError(confirmPasswordError);
      return;
    }

    if (newPassword !== "" && newPassword.match(regEX)) {
      setNewPassword(newPassword);
    } else {
      setNewPasswordError(
        "Please check following condition to set new password"
      );
      return;
    }
    if (newPassword === confirmPassword) {
      setConfirmPassword(confirmPassword);
    } else {
      setConfirmPasswordError("new password and confirm password is not same");
      return;
    }
    axios
      .put("http://localhost:5000/users/changPassword", {
        email: userInfo.email,
        currentPassword: currentPassword,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
      })
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        notify("Password changed successfully");
      })
      .catch(function (error) {
        console.log(error);
      });
    setCurrentPassword("");
    setNewPassword("");
    setConfirmPassword("");
    setCurrentPasswordError("");
    setNewPasswordError("");
    setConfirmPasswordError("");

    history.push("/admin/dashboard");
  };

  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <PageHeading
          pageHeading={"Change Password"}
          showSaveOptionsBtn={true}
          save={true}
          cancel={true}
          handleCancelBtn={handleCancelBtn}
          handleSave={ChangePassword}
        />
        <div className="grid w-full gap-4 px-4 mt-5 overflow-hidden lg:grid-cols-3 sm:px-10 md:px-10 lg:px-10 xl:px-10">
          <div className="flex flex-col xl:col-span-3 ">
            <label className="flex items-center mb-1 font-medium">
              <span className="pr-1 font-normal">Username:</span>
              <span>
                {userInfo.first_name} {userInfo.last_name}
              </span>
            </label>
          </div>
          <div className="flex flex-col">
            <label className="flex items-center mb-1 font-medium">
              Current Password
              <CgAsterisk className="inline text-red-500" />
            </label>
            <input
              type="password"
              value={currentPassword}
              className={`${
                currentPasswordError ? "border-red-500" : "border"
              } border h-10 rounded px-2 text-sm font-medium `}
              onChange={(e) => {
                setCurrentPasswordError("");
                setCurrentPassword(e.target.value);
              }}
              onBlur={() => {
                if (currentPassword.trim() === "") {
                  setCurrentPasswordError(
                    "The current-password field is required."
                  );
                }
              }}
            />
            {currentPasswordError && (
              <span className="text-xs text-red-500">
                {currentPasswordError}
              </span>
            )}
          </div>
          <div className="flex flex-col">
            <label for="fname" className="flex items-center mb-1 font-medium">
              New Password
              <CgAsterisk className="inline text-red-500" />
            </label>
            <input
              type="password"
              value={newPassword}
              className={`${
                newPasswordError ? "border-red-500" : "border"
              } border h-10 rounded px-2 text-sm font-medium `}
              onChange={(e) => {
                setNewPasswordError("");
                setNewPassword(e.target.value);
              }}
              onBlur={() => {
                if (newPassword.trim() === "") {
                  setNewPasswordError("New  password is required");
                }
              }}
            />
            {newPasswordError && (
              <span className="text-xs text-red-500">{newPasswordError}</span>
            )}
            <p className="text-xs">
              Your password must be at least 5 characters long and your password
              should contain.
            </p>
            <ol className="pl-3 text-xs list-decimal">
              <li>At least one upper case letter</li>
              <li>At least one lower case letter</li>
              <li>
                At least one digit. Only (! \" # . @ _ ` ~ $ % ^ * : , ; | -)
                special characters are allowed in the password.
              </li>
            </ol>
          </div>
          <div className="flex flex-col">
            <label className="flex items-center mb-1 font-medium">
              Confirm Password
              <CgAsterisk className="inline text-red-500" />
            </label>
            <input
              type="password"
              value={confirmPassword}
              className={`${
                confirmPasswordError ? "border-red-500" : "border"
              } border h-10 rounded px-2 text-sm font-medium `}
              onChange={(e) => {
                setConfirmPasswordError("");
                setConfirmPassword(e.target.value);
              }}
              onBlur={() => {
                if (confirmPassword.trim() === "") {
                  setConfirmPasswordError(
                    "The Confirm-password field is required."
                  );
                }
              }}
            />
            {confirmPasswordError && (
              <span className="text-xs text-red-500">
                {confirmPasswordError}
              </span>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ChangePassword;
