/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import PageHeading from "../pages/PageHeading";
import { CgAsterisk } from "react-icons/cg";
import { useEffect } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Dropzone from "react-dropzone";
import FormData from "form-data";
import { notify } from "../../utility";

const Profile = (props) => {
  let history = useHistory();
  const [fname, setFname] = useState("");
  const [fnameError, setFnameError] = useState("");

  const [lname, setLname] = useState("");

  const [profileImg, setProfileImg] = useState("");
  const [profileImgError, setProfileImgError] = useState("");
  const [imageName, setImageName] = useState(null);

  const [email, setEmail] = useState("");

  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const access_token = userInfo.access_token;

  const onImageDrop = (acceptedFiles) => {
    if (acceptedFiles.length > 0) {
      const uploads = acceptedFiles.map((image) => {
        let data = new FormData();
        data.append("folder", "profileImages");
        data.append("image", image);
        const config = {
          method: "post",
          url: "http://localhost:5000/uploads",
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
          data: data,
        };
        axios(config)
          .then(function (response) {
            console.log("res", response.data);
            console.log("image", image);
            setProfileImg(response.data.imageUrl);
            setImageName(response.data.imageName);
          })
          .catch(function (error) {
            console.log(error);
          });
      });
    }
  };
  useEffect(async () => {
    setFname(userInfo.first_name);
    setLname(userInfo.last_name);
    setEmail(userInfo.email);
    setProfileImg(userInfo.profile_image);
  }, [setProfileImg]);

  const handleEdit = () => {
    let fnameError = "";
    let profileImgError = "";

    if (fname.trim() === "") {
      fnameError = "Enter First Name";
    }

    if (fnameError !== "") {
      setFnameError(fnameError);
      return;
    }
    console.log("imageName", imageName);
    let updateProfile = {
      method: "put",
      url: "http://localhost:5000/users/updateProfile",
      params: {
        id: parseInt(userInfo.id),
        first_name: fname,
        last_name: lname,
        profile_image: imageName,
      },
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };
    axios(updateProfile)
      .then(function (response) {
        const data = response.data;
        console.log("response", response.data);
        notify(data.message);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const removeImg = () => {
    let arr = profileImg.split("/");
    let image = arr[arr.length - 1];
    const config = {
      method: "delete",
      url: "http://localhost:5000/uploads",
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      data: {
        folder: "profileImages",
        image: image,
      },
    };
    axios(config)
      .then(function (response) {
        setProfileImg(null);
        setImageName(null);
        console.log("response", response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <PageHeading
          pageHeading={"My Profile"}
          showSaveOptionsBtn={true}
          save={true}
          cancel={true}
          handleSave={handleEdit}
        />
        <div className="grid w-full grid-cols-1 gap-4 px-4 my-5 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 sm:px-10 md:px-10 lg:px-10 xl:px-10">
          <div className="grid gap-2 xl:grid-cols-2 xl:h-32">
            <div className="flex flex-col col-span-2 xl:col-span-1 sm:col-span-2">
              <label for="fname" className="flex items-center mb-1 font-medium">
                First Name
                <CgAsterisk className="inline text-red-500" />
              </label>
              <input
                type="text"
                value={fname}
                className={`${
                  fnameError ? "border-red-500" : "border"
                } border h-10 rounded px-2 text-sm font-medium `}
                placeholder="First Name"
                onChange={(e) => {
                  setFname(e.target.value);
                  setFnameError("");
                }}
                name="fname"
              />
              {fnameError && (
                <span className="text-xs text-red-500">{fnameError}</span>
              )}
            </div>
            <div className="flex flex-col col-span-2 xl:col-span-1 sm:col-span-2">
              <label for="lname" className="flex items-center mb-1 font-medium">
                Last Name
              </label>
              <input
                type="text"
                value={lname}
                className="h-10 px-2 text-sm font-medium border rounded"
                placeholder="Last Name"
                onChange={(e) => {
                  setLname(e.target.value);
                }}
                name="lname"
              />
            </div>
            <div className="flex flex-col col-span-2">
              <label for="email" className="flex items-center mb-1 font-medium">
                Email
              </label>
              <input
                type="email"
                disabled
                value={email}
                className="h-10 px-2 text-sm font-medium bg-white border rounded"
                name="email"
              />
            </div>
          </div>
          <div className="flex flex-col">
            <label className="flex items-center mb-1 font-medium">
              Profile Image
            </label>
            <div className="relative flex items-center justify-center w-full h-56 border-2 border-gray-700 border-dashed hover:bg-black hover:bg-opacity-40">
              {profileImg && (
                <img
                  src={profileImg}
                  alt="no img"
                  className="object-contain w-full h-4/5"
                />
              )}
              {profileImg && (
                <button
                  onClick={() => {
                    removeImg(profileImg);
                  }}
                  className="absolute z-20 text-white bg-gray-900 top-1 right-1 btn"
                >
                  Remove
                </button>
              )}
              {!profileImg && (
                <Dropzone
                  accept="image/*"
                  onDrop={(acceptedFiles) => {
                    onImageDrop(acceptedFiles);
                  }}
                >
                  {({ getRootProps, getInputProps }) => (
                    <div
                      {...getRootProps()}
                      className="flex items-center justify-center w-full h-full"
                    >
                      <input {...getInputProps()} />
                      <p>
                        Drag 'n' drop some files here, or click to select files
                      </p>
                    </div>
                  )}
                </Dropzone>
              )}
            </div>
            {profileImgError && (
              <span className="text-xs text-red-500">{profileImgError}</span>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
