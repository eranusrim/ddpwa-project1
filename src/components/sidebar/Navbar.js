/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import * as FaIcons from "react-icons/fa";
import { FaChevronDown, FaUserAlt } from "react-icons/fa";
import { AiOutlineLogout } from "react-icons/ai";
import { HiOutlineLightBulb } from "react-icons/hi";
import useDarkMode from "../../useDarkMode";
import { useHistory } from "react-router-dom";
import Auth from "../login/Auth";
import axios from "axios";

const Navbar = (props) => {
  const [colorTheme, setTheme] = useDarkMode();
  const [showProfileMenu, setProfileMenu] = useState(false);
  let userInfo = JSON.parse(localStorage.getItem("userInfo"));
  let access_token = userInfo.access_token;
  const config = {
    method: "post",
    url: "http://laravelcms.devdigdev.com/api/v1/logout",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  };
  let history = useHistory();
  return (
    <>
      <div className="sticky top-0 z-20 flex items-center justify-between w-full h-16 bg-white shadow-lg dark:bg-gray-800">
        <div className="flex items-center w-full h-full text-2xl dark:text-white h-26 sm:w-auto md:w-auto lg:w-auto xl:w-auto">
          <div className="flex items-center justify-center w-12 h-full">
            <FaIcons.FaBars
              onClick={props.showSidebar}
              className="cursor-pointer"
            />
          </div>
          <div className="w-full pl-2 text-lg font-semibold text-center dark:text-white sm:w-auto md:w-auto lg:w-auto xl:w-auto sm:text-lg md:text-lg lg:text-lg xl:text-lg">
            Dev Digital
          </div>
        </div>
        <div className="flex items-center pr-2 dark:text-white ">
          <div className="relative flex items-center ">
            <p className="hidden pr-2 text-sm capitalize sm:text-base md:text-base lg:text-base xl:text-base sm:flex md:flex lg:flex xl:flex">
              Welcome: {userInfo.first_name} {userInfo.last_name}
            </p>
            <div className="flex items-center justify-center w-8 h-8 mr-1 overflow-hidden bg-white rounded-full shadow">
              {userInfo.profile_image ? (
                <img
                  className="object-contain w-full"
                  src={userInfo.profile_image}
                />
              ) : (
                <FaUserAlt className="dark:text-gray-700" />
              )}
            </div>
            <FaChevronDown
              className="cursor-pointer group"
              onClick={() => setProfileMenu(!showProfileMenu)}
            />
            {showProfileMenu && (
              <div className="absolute flex-col items-start justify-center w-48 text-sm bg-white shadow-lg -right-8 sm:w-full md:w-full lg:w-full xl:w-full top-12 dark:bg-gray-700">
                <p
                  onClick={() => {
                    history.push("/admin/profile");
                    setProfileMenu(!showProfileMenu);
                  }}
                  className="flex items-center w-full h-10 p-3 border-b-2 border-black cursor-pointer dark:border-white"
                >
                  My Profile
                </p>
                <p
                  onClick={() => {
                    history.push("/admin/changePassword");
                    setProfileMenu(!showProfileMenu);
                  }}
                  className="flex items-center w-full h-10 p-3 border-b-2 border-black cursor-pointer dark:border-white"
                >
                  Change Password
                </p>
                <p
                  onClick={() => {
                    history.push("/admin/setting");
                    setProfileMenu(!showProfileMenu);
                  }}
                  className="flex items-center w-full h-10 p-3 border-b-2 border-black cursor-pointer dark:border-white"
                >
                  Setting
                </p>
                <p
                  onClick={() => {
                    props.setLogin(false);
                    Auth.signout();
                    localStorage.removeItem("userInfo");
                    history.push("/admin/login");
                  }}
                  className="flex items-center w-full h-10 p-3 text-white bg-black cursor-pointer dark:bg-gray-400 dark:text-black dark:font-bold "
                >
                  <AiOutlineLogout /> <span className="pl-3">Logout</span>
                </p>
              </div>
            )}
          </div>
          <div
            onClick={() => setTheme(colorTheme)}
            className={`border-1 border-black p-2 ml-2 cursor-pointer rounded-full ${
              colorTheme === "light" ? "bg-white" : "bg-gray-800"
            }`}
          >
            <HiOutlineLightBulb
              className={`${
                colorTheme === "light" ? "text-black" : "text-white"
              } `}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
