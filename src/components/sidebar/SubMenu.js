import React, { useState } from "react";
import { NavLink } from "react-router-dom";

const SubMenu = ({ item, hideSidebarFromMenu, Sidebar }) => {
    const [subnav, setSubnav] = useState(false);

    const showSubNav = () => {
        setSubnav(!subnav);
    };

    return (
        <>
            <NavLink
                activeClassName="active"
                to={!item.subNav ? item.path : item.subNav}
                onClick={() => {
                    if (Sidebar) {
                        item.subNav ? showSubNav() : hideSidebarFromMenu(false);
                    } else {
                        item.subNav && showSubNav();
                    }
                }}
                className="w-56 flex items-center flex-shrink-0 overflow-hidden hover:text-white hover:bg-gray-500 box-border h-16"
            >
                <div className="flex items-center justify-start flex-nowrap dark:text-white w-full border-box">
                    <span className="w-12 border-box flex items-center justify-center ">
                        {item.icon}
                    </span>

                    <span className="dark:text-white capitalize text-sm">
                        {item.title}
                    </span>
                </div>
                <div className="dark:text-white pr-3 ">
                    {item.subNav && subnav
                        ? item.iconOpened
                        : item.subNav
                        ? item.iconClosed
                        : null}
                </div>
            </NavLink>
            <div className="dark:bg-gray-600 bg-gray-200 w-56 box-border min-h-16">
                {subnav &&
                    item.subNav.map((item, index) => {
                        return (
                            <NavLink
                                activeClassName="active2"
                                key={index}
                                to={item.path}
                                onClick={() => {
                                    if (!Sidebar) {
                                        hideSidebarFromMenu(false);
                                    } else {
                                        hideSidebarFromMenu(false);
                                    }
                                }}
                                className="flex  text-black dark:text-white items-center hover:text-white justify-between hover:bg-gray-500 w-56 h-16"
                            >
                                <div className="w-full flex items-center justify-start ">
                                    <span className="dark:text-white w-12 flex items-center justify-center">
                                        {item.icon}
                                    </span>
                                    <span className="dark:text-white text-sm capitalize">
                                        {item.title}
                                    </span>
                                </div>
                            </NavLink>
                        );
                    })}
            </div>
        </>
    );
};
export default SubMenu;
