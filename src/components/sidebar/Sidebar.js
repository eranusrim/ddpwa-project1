import React from "react";
import { SidebarData } from "./SidebarData";
import SubMenu from "./SubMenu";

const SidebarComp = (props) => {
    return (
        <div
            className={`dark:bg-gray-800 ${
                props.Sidebar
                    ? ""
                    : "-left-96 sm:inset-0 md:inset-0 lg:inset-0 xl:inset-0 transition transform duration-500 "
            } bg-white shadow-lg overflow-x-hidden content-container hover:w-56 hover:sm:w-64 md:hover:w-72 lg:hover:w-64 xl:hover:w-64 box-border 
             overflow-y-scroll scrollbar-hide absolute z-10  sm:relative md:relative lg:relative xl:relative flex justify-start flex-col  transition-all transition-duration:200ms delay-100
              ${
                  props.Sidebar
                      ? "sm:w-64 md:w-72 lg:w-64 xl:w-64"
                      : "sm:w-12 md:w-12 lg:w-12 xl:w-12"
              }
             `}
        >
            {SidebarData.map((item, index) => {
                return (
                    <SubMenu
                        item={item}
                        key={index}
                        hideSidebarFromMenu={props.hideSidebarFromMenu}
                        Sidebar={props.Sidebar}
                    />
                );
            })}
        </div>
    );
};

export default SidebarComp;
