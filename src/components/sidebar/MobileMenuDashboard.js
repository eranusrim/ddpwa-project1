/* eslint-disable no-unused-vars */
import React from "react";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { AiFillDashboard } from "react-icons/ai";
import { IoIosPaper } from "react-icons/io";
import { FaBlogger } from "react-icons/fa";
import { AiOutlineSetting } from "react-icons/ai";

const MobileMenuDashboard = (props) => {
  const history = useHistory();
  return (
    <div className="flex items-center w-full h-16 justify-evenly sm:hidden md:hidden lg:hidden xl:hidden dark:bg-gray-800">
      <NavLink
        to="/admin/dashboard"
        onClick={() => props.hideSidebarFromMenu(false)}
        activeClassName="text-blue-400 dark:text-blue-400"
        className="dark:text-white"
      >
        <AiFillDashboard size="22" />
      </NavLink>
      <NavLink
        to="/admin/pages"
        onClick={() => props.hideSidebarFromMenu(false)}
        activeClassName="text-blue-400 dark:text-blue-400"
        className="dark:text-white"
      >
        <IoIosPaper size="22" />
      </NavLink>
      <NavLink
        to="/admin/blog/view"
        onClick={() => props.hideSidebarFromMenu(false)}
        activeClassName="text-blue-400 dark:text-blue-400"
        className="dark:text-white"
      >
        <FaBlogger size="22" />
      </NavLink>
      <NavLink
        to="/admin/setting"
        onClick={() => props.hideSidebarFromMenu(false)}
        activeClassName="text-blue-400 dark:text-blue-400"
        className="dark:text-white"
      >
        <AiOutlineSetting size="22" />
      </NavLink>
    </div>
  );
};

export default MobileMenuDashboard;
