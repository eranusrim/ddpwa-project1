import React from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import * as RiIcons from "react-icons/ri";
import * as BoxIcons from "react-icons/bi";
import * as BootstrapIcon from "react-icons/bs";

export const SidebarData = [
    {
        title: "Dashboard",
        path: "/admin/dashboard",
        icon: <AiIcons.AiFillDashboard />,
    },
    {
        title: "Users",
        path: "/admin/users",
        icon: <AiIcons.AiOutlineUser />,
    },
    {
        title: "Pages",
        path: "/admin/pages",
        icon: <IoIcons.IoIosPaper />,
    },
    // {
    //     title: "Our team",
    //     path: "/team",
    //     icon: <AiIcons.AiFillHome />,
    //     iconClosed: <RiIcons.RiArrowDownFill />,
    //     iconOpened: <RiIcons.RiArrowUpFill />,
    //     subNav: [
    //         {
    //             title: "View All",
    //             path: "/team/view",
    //             icon: <IoIcons.IoIosPaper />,
    //         },
    //         {
    //             title: "setting",
    //             path: "/team/setting",
    //             icon: <IoIcons.IoIosPaper />,
    //         },
    //     ],
    // },
    {
        title: "Categories",
        path: "/admin/categories",
        icon: <AiIcons.AiFillAppstore />,
    },
    // {
    //     title: "Testimonials",
    //     path: "/testimonials",
    //     icon: <BoxIcons.BiMessageCheck />,
    //     iconClosed: <RiIcons.RiArrowDownFill />,
    //     iconOpened: <RiIcons.RiArrowUpFill />,
    //     subNav: [
    //         {
    //             title: "View All",
    //             path: "/testimonials/view",
    //             icon: <IoIcons.IoIosPaper />,
    //         },
    //         {
    //             title: "Setting",
    //             path: "/testimonials/setting",
    //             icon: <IoIcons.IoIosPaper />,
    //         },
    //     ],
    // },
    {
        title: "Blogs",
        path: "/admin/blog/view",
        icon: <FaIcons.FaBlogger />,
        iconClosed: <RiIcons.RiArrowDownFill />,
        iconOpened: <RiIcons.RiArrowUpFill />,
        subNav: [
            {
                title: "View All",
                path: "/admin/blog/view",
                icon: <IoIcons.IoIosPaper />,
            },
            {
                title: "Setting",
                path: "/admin/blog/setting",
                icon: <IoIcons.IoIosPaper />,
            },
        ],
    },
    {
        title: "FAQs",
        path: "/admin/faqs",
        icon: <FaIcons.FaQuestionCircle />,
    },
    {
        title: "Home Page Sliders",
        path: "/admin/sliders",
        icon: <FaIcons.FaSlidersH />,
        iconClosed: <RiIcons.RiArrowDownFill />,
        iconOpened: <RiIcons.RiArrowUpFill />,
        subNav: [
            {
                title: "View All",
                path: "/admin/sliders/view",
                icon: <IoIcons.IoIosPaper />,
            },
            {
                title: "Setting",
                path: "/admin/sliders/setting",
                icon: <IoIcons.IoIosPaper />,
            },
        ],
    },
    {
        title: "Galleries",
        path: "/admin/galleries",
        icon: <BootstrapIcon.BsFillImageFill />,
    },
    {
        title: "Products",
        path: "/admin/products",
        icon: <FaIcons.FaProductHunt />,
    },
    {
        title: "Contact Us",
        path: "/admin/contact/view",
        icon: <RiIcons.RiContactsBookLine />,
        iconClosed: <RiIcons.RiArrowDownFill />,
        iconOpened: <RiIcons.RiArrowUpFill />,
        subNav: [
            {
                title: "View All",
                path: "/admin/contact/view",
                icon: <IoIcons.IoIosPaper />,
            },
            {
                title: "Setting",
                path: "/admin/contact/setting",
                icon: <IoIcons.IoIosPaper />,
            },
        ],
    },
    {
        title: "Events",
        path: "/admin/events",
        icon: <BoxIcons.BiCalendarStar />,
    },
    {
        title: "Setting",
        path: "/admin/setting",
        icon: <AiIcons.AiOutlineSetting />,
    },
    // {
    //     title: "IP Tracker",
    //     path: "/iptracker",
    //     icon: <BoxIcons.BiCurrentLocation />,
    // },

    {
        title: "Chatbot",
        path: "/admin/chatbot",
        icon: <AiIcons.AiFillRobot />,
    },
];
