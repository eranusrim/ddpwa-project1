/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useReducer, useState } from "react";
import PageHeading from "../pages/PageHeading";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RiArrowDownFill, RiArrowUpFill } from "react-icons/ri";
import axios from "axios";
import { notify } from "../../utility";
// import { UsersReducer, initialState } from "../../Redux/reducers/usersReducers";
import {
    FETCH_USERS_REQUEST,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAIL,
} from "../../Redux/actionTypes";
import moment from "moment";

const Users = (props) => {
    let dispatch = useDispatch();

    let [selectAllCheckbox, setSelectAllCheckbox] = useState(false);

    let [sortNameAsc, setSortNameDesc] = useState(false);
    let [sortNameArrow, setSortNameArrow] = useState(false);

    let [sortEmail, setSortEmail] = useState(false);
    let [sortEmailArrow, setSortEmailArrow] = useState(true);

    let [sortLogin, setSortLogin] = useState(false);
    let [sortLoginArrow, setSortLoginArrow] = useState(true);

    let [sortCreated, setSortCreated] = useState(false);
    let [sortCreatedArrow, setSortCreatedArrow] = useState(true);

    let [search, setSearch] = useState("");
    let [pageSearch, setPageSearch] = useState(true);
    let [activeInactive, setActiveInactive] = useState(false);
    let [people, setPeople] = useState([]);
    const history = useHistory();
    // const [state] = useReducer(UsersReducer, initialState);
    let state = useSelector((state) => state.UsersReducer);
    let userInfo = JSON.parse(localStorage.getItem("userInfo"));
    console.log("userInfo", userInfo.access_token);
    let access_token = userInfo.access_token;
    let config = {
        method: "get",
        url: "http://localhost:5000/users",
        headers: {
            "Content-type": "application/json",
            Authorization: `Bearer ${access_token}`,
        },
    };
    const getUsers = () => {
        dispatch({
            type: FETCH_USERS_REQUEST,
        });
        axios(config)
            .then(function (response) {
                let users = response.data.users;
                console.log("users", response.data.users);
                console.log("users", users);
                dispatch({
                    type: FETCH_USERS_SUCCESS,
                    payload: users,
                });

                setPeople(
                    users.map((user) => {
                        console.log("last_login_at", user.last_login_at);
                        return {
                            select: false,
                            created_at: moment(user.created_at).format(
                                "YYYY-MM-DD"
                            ),
                            created_by: user.created_by,
                            deleted_at: user.deleted_at,
                            email: user.email,
                            email_verified_at: user.email_verified_at,
                            first_name: user.first_name,
                            id: user.id,
                            last_login_at: moment(user.last_login_at).format(
                                "YYYY-MM-DD"
                            ),
                            last_login_ip: user.last_login_ip,
                            last_name: user.last_name,
                            profile_image: user.profile_image,
                            status: user.status,
                            updated_at: user.updated_at,
                            user_type_id: user.user_type_id,
                        };
                    })
                );
            })
            .catch(function (error) {
                console.log(error);
                dispatch({
                    type: FETCH_USERS_FAIL,
                    payload: error,
                });
            });
    };
    useEffect(() => {
        getUsers();
    }, []);

    const sortByName = (sortNameAsc) => {
        const sorted = people.sort((a, b) => {
            const isReverse = sortNameAsc === true ? 1 : -1;
            return isReverse * a.first_name.localeCompare(b.first_name);
        });
        console.log("sorted name", sorted);
        setPeople(sorted);
    };
    const sortByEmail = (sortEmail) => {
        const sorted = people.sort((a, b) => {
            const isReverse = sortEmail === true ? 1 : -1;
            return isReverse * a.email.localeCompare(b.email);
        });
        console.log("sorted email", sorted);
        setPeople(sorted);
    };
    const sortByLogin = (sortLogin) => {
        const sorted = people.sort((a, b) => {
            let dateA = new Date(a.last_login_at);
            let dateB = new Date(b.last_login_at);
            return sortLogin ? dateA - dateB : dateB - dateA;
        });
        console.log("sorted end date", sorted);
        setPeople(sorted);
    };
    const sortByCreated = (sortCreated) => {
        const sorted = people.sort((a, b) => {
            let dateA = new Date(a.created_at);
            let dateB = new Date(b.created_at);
            return sortCreated ? dateA - dateB : dateB - dateA;
        });
        setPeople(sorted);
    };

    const handleSearch = () => {
        let searchData = people.filter((d) => {
            if (
                d.first_name
                    .toLocaleLowerCase()
                    .includes(search.toLocaleLowerCase()) ||
                d.email.toLocaleLowerCase().includes(search.toLocaleLowerCase())
            ) {
                console.log(d);
                return d;
            }
        });
        console.log("length", searchData.length);
        console.log(searchData);
        if (searchData.length === 0) {
            setPeople(people);
        } else {
            setPeople(searchData);
        }
    };
    const handleReset = () => {
        setSearch("");
        if (state.Users) {
            setPeople(
                state.Users.map((user) => {
                    return {
                        created_at: user.created_at,
                        created_by: user.created_by,
                        deleted_at: user.deleted_at,
                        email: user.email,
                        email_verified_at: user.email_verified_at,
                        first_name: user.first_name,
                        id: user.id,
                        last_login: user.PageHeadinglast_login,
                        last_login_at: user.last_login_at,
                        last_login_ip: user.last_login_ip,
                        last_name: user.last_name,
                        profile_image: user.profile_image,
                        status: user.status,
                        updated_at: user.updated_at,
                        updated_by: user.updated_by,
                        user_type_id: user.user_type_id,
                    };
                })
            );
        }
    };
    const handleClose = () => {
        setSearch("");
        if (state.Users) {
            setPeople(
                state.Users.map((user) => {
                    return {
                        created_at: user.created_at,
                        created_by: user.created_by,
                        deleted_at: user.deleted_at,
                        email: user.email,
                        email_verified_at: user.email_verified_at,
                        first_name: user.first_name,
                        id: user.id,
                        last_login: user.PageHeadinglast_login,
                        last_login_at: user.last_login_at,
                        last_login_ip: user.last_login_ip,
                        last_name: user.last_name,
                        profile_image: user.profile_image,
                        status: user.status,
                        updated_at: user.updated_at,
                        updated_by: user.updated_by,
                        user_type_id: user.user_type_id,
                    };
                })
            );
        }
    };
    const handleActive = () => {
        people.map((p) => {
            if (p.select === true) {
                console.log(p.id, p.first_name, p.email, p.status);
                let updateUser = {
                    method: "put",
                    url: "http://laravelcms.devdigdev.com/api/v1/users/" + p.id,
                    params: {
                        first_name: p.first_name,
                        email: p.email,
                        status: "1",
                    },
                    headers: {
                        "Content-type": "application/json",
                        Authorization: `Bearer ${access_token}`,
                    },
                };
                axios(updateUser)
                    .then(function (response) {
                        console.log("success", JSON.stringify(response.data));
                        notify(`${p.first_name} now active`);
                        getUsers();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
            p.select = false;
        });

        console.log("active", people);
        setSelectAllCheckbox(false);
        setActiveInactive(false);
        setPageSearch(true);
    };
    const handleInActive = () => {
        people.map((p) => {
            if (p.select === true) {
                console.log(p.id, p.first_name, p.email, p.status);
                let updateUser = {
                    method: "put",
                    url: "http://laravelcms.devdigdev.com/api/v1/users/" + p.id,
                    params: {
                        first_name: p.first_name,
                        email: p.email,
                        status: "0",
                    },
                    headers: {
                        "Content-type": "application/json",
                        Authorization: `Bearer ${access_token}`,
                    },
                };
                axios(updateUser)
                    .then(function (response) {
                        console.log("success", JSON.stringify(response.data));
                        notify(`${p.first_name} now Inactive`);
                        getUsers();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
            p.select = false;
        });

        console.log("inactive", people);
        setSelectAllCheckbox(false);
        setActiveInactive(false);
        setPageSearch(true);
    };

    const selectAll = (e) => {
        let checked = e.target.checked;
        if (checked) {
            setPageSearch(false);
            setActiveInactive(true);
        } else {
            setPageSearch(true);
            setActiveInactive(false);
        }
        setPeople(
            people.map((d) => {
                d.select = checked;
                return d;
            })
        );
    };

    const selectSingle = (e, id) => {
        let checked = e.target.checked;
        setPeople(
            people.map((p) => {
                if (id === p.id) {
                    p.select = checked;
                }

                return p;
            })
        );

        const result = people.some(function (data) {
            return data.select === true;
        });
        if (result) {
            setActiveInactive(true);
            setPageSearch(false);
        } else {
            setPageSearch(true);
            setActiveInactive(false);
        }
    };

    return (
        <div
            className={`content-container relative  bg-gray-100 overflow-y-scroll scrollbar-hide ${
                props.Sidebar
                    ? "w-full sm:content md:content lg:content xl:content"
                    : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
            }`}
        >
            <div className="flex-col items-center w-full">
                {!state.loading && (
                    <PageHeading
                        pageHeading={"Users"}
                        searchLabel={"Name/Email"}
                        pageSearch={pageSearch}
                        activeInactive={activeInactive}
                        search={search}
                        setSearch={setSearch}
                        handleActive={handleActive}
                        handleInActive={handleInActive}
                        handleSearch={handleSearch}
                        handleReset={handleReset}
                        handleClose={handleClose}
                        path="/admin/users/create"
                    />
                )}
                <div
                    className={`w-full overflow-x-scroll scrollbar-hide py-10 px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10  ${
                        state.loading ? "" : "dark:bg-gray-700"
                    }`}
                >
                    {state.loading ? (
                        // <div className="flex items-center justify-center w-full pt-20 pb-10">
                        //     <h1 className="text-xl lg:text-3xl ">Loading...</h1>
                        // </div>
                        <>
                            <table className="hidden w-full border-2 border-gray-200 sm:table md:table lg:table xl:table">
                                <thead className="w-full bg-gray-50 dark:bg-gray-400 dark:text-white">
                                    <tr className="h-10 border-l-8 border-gray-400"></tr>
                                </thead>
                                <tbody className="bg-white dark:bg-gray-700 animate-pulse">
                                    {Array(15)
                                        .fill()
                                        .map(() => (
                                            <tr
                                                className={`
                                            border-l-8 border-gray-400 border-b bg-gray-700 h-12 `}
                                            >
                                                <td className="px-3 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white"></td>
                                                <td className="px-3 py-4 whitespace-nowrap">
                                                    <div className="flex items-center">
                                                        <div>
                                                            <div className="text-sm font-medium text-gray-900 capitalize dark:text-white"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td className="px-3 py-4 whitespace-nowrap">
                                                    <div className="text-sm text-gray-900 dark:text-white"></div>
                                                </td>

                                                <td className="px-3 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white"></td>
                                                <td className="px-3 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white"></td>
                                                <td className="px-6 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white"></td>
                                                <td className="px-3 py-4 text-sm font-medium text-right whitespace-nowrap"></td>
                                            </tr>
                                        ))}
                                </tbody>
                            </table>
                            <div className="block overflow-hidden border-r sm:hidden md:hidden lg:hidden xl:hidden">
                                <div className="w-full px-5 py-3 bg-gray-200 border-t">
                                    <label className="px-5 py-3 text-sm font-medium leading-normal"></label>
                                </div>
                                {Array(15)
                                    .fill()
                                    .map((i) => (
                                        <div
                                            className={`tab w-full border-t border-left-green-8 animate-pulse `}
                                        >
                                            <label className="flex items-center justify-between px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white">
                                                <span className="block w-11/12 h-6 px-5"></span>
                                            </label>
                                            <div className="w-full overflow-hidden border-t tab-content">
                                                <div className="p-4">
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white"></h1>
                                                        <h1 className="pl-2 text-left dark:text-white"></h1>
                                                    </div>
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white"></h1>
                                                        <h1 className="pl-2 text-left dark:text-white"></h1>
                                                    </div>
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white"></h1>
                                                        <h1 className="pl-2 text-left dark:text-white"></h1>
                                                    </div>
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white"></h1>
                                                        <h1 className="pl-2 text-left dark:text-white"></h1>
                                                    </div>
                                                    <div className="flex py-1">
                                                        <h1 className="pl-2 text-left dark:text-white"></h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </>
                    ) : (
                        <>
                            <table className="hidden w-full border-b border-r border-gray-200 sm:table md:table lg:table xl:table">
                                <thead className="w-full bg-gray-50 dark:bg-gray-300 dark:text-white">
                                    <tr className="border-l-8 border-gray-400">
                                        <th
                                            scope="col"
                                            className="px-3 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-gray-700"
                                        >
                                            <input
                                                checked={selectAllCheckbox}
                                                onChange={(e) => {
                                                    setSelectAllCheckbox(
                                                        !selectAllCheckbox
                                                    );
                                                    selectAll(e);
                                                }}
                                                type="checkbox"
                                            />
                                        </th>

                                        <th
                                            onClick={() => {
                                                sortByName(sortNameAsc);
                                                setSortNameDesc(!sortNameAsc);
                                                setSortNameArrow(false);
                                                setSortEmailArrow(true);
                                                setSortLoginArrow(true);
                                                setSortCreatedArrow(true);
                                            }}
                                            scope="col"
                                            className="px-3 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700 "
                                        >
                                            <span className="inline-block mr-2">
                                                Name
                                            </span>
                                            {sortNameArrow && (
                                                <>
                                                    <RiArrowUpFill className="inline-block ml-1" />
                                                    <RiArrowDownFill className="inline-block ml-1" />
                                                </>
                                            )}
                                            {!sortNameArrow && (
                                                <>
                                                    {sortNameAsc === true ? (
                                                        <RiArrowUpFill className="inline-block ml-2" />
                                                    ) : (
                                                        <RiArrowDownFill className="inline-block ml-2" />
                                                    )}
                                                </>
                                            )}
                                        </th>
                                        <th
                                            onClick={() => {
                                                sortByEmail(sortEmail);
                                                setSortEmail(!sortEmail);
                                                setSortEmailArrow(false);
                                                setSortNameArrow(true);
                                                setSortLoginArrow(true);
                                                setSortCreatedArrow(true);
                                            }}
                                            scope="col"
                                            className="px-3 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700"
                                        >
                                            Email
                                            {sortEmailArrow && (
                                                <>
                                                    <RiArrowUpFill className="inline-block ml-1" />
                                                    <RiArrowDownFill className="inline-block ml-1" />
                                                </>
                                            )}
                                            {!sortEmailArrow && (
                                                <>
                                                    {sortEmail === true ? (
                                                        <RiArrowUpFill className="inline-block ml-2" />
                                                    ) : (
                                                        <RiArrowDownFill className="inline-block ml-2" />
                                                    )}
                                                </>
                                            )}
                                        </th>
                                        <th
                                            onClick={() => {
                                                sortByLogin(sortLogin);
                                                setSortLogin(!sortLogin);
                                                setSortLoginArrow(false);
                                                setSortNameArrow(true);
                                                setSortEmailArrow(true);
                                                setSortCreatedArrow(true);
                                            }}
                                            scope="col"
                                            className="px-3 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700"
                                        >
                                            Last Login{" "}
                                            {sortLoginArrow && (
                                                <>
                                                    <RiArrowUpFill className="inline-block ml-1" />
                                                    <RiArrowDownFill className="inline-block ml-1" />
                                                </>
                                            )}
                                            {!sortLoginArrow && (
                                                <>
                                                    {sortLogin === true ? (
                                                        <RiArrowUpFill className="inline-block ml-2" />
                                                    ) : (
                                                        <RiArrowDownFill className="inline-block ml-2" />
                                                    )}
                                                </>
                                            )}
                                        </th>
                                        <th
                                            onClick={() => {
                                                sortByCreated(sortCreated);
                                                setSortCreated(!sortCreated);
                                                setSortCreatedArrow(false);
                                                setSortNameArrow(true);
                                                setSortEmailArrow(true);
                                                setSortLoginArrow(true);
                                            }}
                                            scope="col"
                                            className="px-3 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700"
                                        >
                                            Created On
                                            {sortCreatedArrow && (
                                                <>
                                                    <RiArrowUpFill className="inline-block ml-1" />
                                                    <RiArrowDownFill className="inline-block ml-1" />
                                                </>
                                            )}
                                            {!sortCreatedArrow && (
                                                <>
                                                    {sortCreated === true ? (
                                                        <RiArrowUpFill className="inline-block ml-2" />
                                                    ) : (
                                                        <RiArrowDownFill className="inline-block ml-2" />
                                                    )}
                                                </>
                                            )}
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-3 py-3 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-700"
                                        >
                                            Reset Password
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-3 py-3 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-700"
                                        >
                                            Edit
                                        </th>
                                    </tr>
                                </thead>
                                <tbody className="bg-white dark:bg-gray-700">
                                    {people.map((person) => (
                                        <tr
                                            key={person.id}
                                            className={`${
                                                person.status === "1"
                                                    ? "border-left-green-8 "
                                                    : "border-left-red-8"
                                            } dark:hover:bg-gray-500 hover:bg-gray-200  border-b-gray-500 border-b`}
                                        >
                                            <td className="px-3 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                                                <input
                                                    checked={person.select}
                                                    onChange={(e) =>
                                                        selectSingle(
                                                            e,
                                                            person.id
                                                        )
                                                    }
                                                    type="checkbox"
                                                />
                                            </td>
                                            <td className="px-3 py-4 whitespace-nowrap">
                                                <div className="flex items-center">
                                                    <div>
                                                        <div className="text-sm font-medium text-gray-900 capitalize dark:text-white">
                                                            {person.first_name}{" "}
                                                            {person.last_name}
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td className="px-3 py-4 whitespace-nowrap">
                                                <div className="text-sm text-gray-900 dark:text-white">
                                                    {person.email}
                                                </div>
                                            </td>

                                            <td className="px-3 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                                                {person.last_login_at}
                                            </td>
                                            <td className="px-3 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                                                {person.created_at}
                                            </td>
                                            <td className="px-6 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                                                {person.status === "1" ? (
                                                    <a
                                                        href="#"
                                                        className="text-blue-400 underline"
                                                    >
                                                        Reset Password
                                                    </a>
                                                ) : (
                                                    <a
                                                        href="#"
                                                        className="text-blue-400 underline"
                                                    >
                                                        Account Activation
                                                    </a>
                                                )}
                                            </td>
                                            <td className="px-3 py-4 text-sm font-medium text-right whitespace-nowrap">
                                                <button
                                                    onClick={() => {
                                                        history.push({
                                                            pathname:
                                                                "/admin/users/create",
                                                            user: person,
                                                        });
                                                    }}
                                                    className="text-indigo-600 hover:text-indigo-900 dark:text-blue-400 dark:hover:text-blue-500"
                                                >
                                                    Edit
                                                </button>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                            <div className="block overflow-hidden border-r sm:hidden md:hidden lg:hidden xl:hidden">
                                <div className="w-full px-5 py-3 bg-gray-200 border-t">
                                    <input
                                        className="ml-3"
                                        onChange={(e) => selectAll(e)}
                                        type="checkbox"
                                    />
                                    <label
                                        onClick={() => {
                                            sortByName(sortNameAsc);
                                            setSortNameDesc(!sortNameAsc);
                                        }}
                                        className="px-5 py-3 text-sm font-medium leading-normal"
                                    >
                                        Name{" "}
                                        {sortNameAsc === true ? (
                                            <RiArrowUpFill className="inline-block" />
                                        ) : (
                                            <RiArrowDownFill className="inline-block" />
                                        )}
                                    </label>
                                </div>
                                {people &&
                                    people.map((person) => (
                                        <div
                                            className={`tab w-full border-t ${
                                                person.status === "1"
                                                    ? "border-left-green-8 "
                                                    : "border-left-red-8"
                                            }`}
                                        >
                                            <input
                                                className="absolute opacity-0"
                                                id={person.id}
                                                type="checkbox"
                                                name="tabs"
                                            />
                                            <label
                                                className="flex items-center justify-between px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                                                for={person.id}
                                            >
                                                <input
                                                    checked={person.select}
                                                    onChange={(e) =>
                                                        selectSingle(
                                                            e,
                                                            person.id
                                                        )
                                                    }
                                                    type="checkbox"
                                                />
                                                <span className="block w-11/12 px-5">
                                                    {person.first_name}{" "}
                                                    {person.last_name}
                                                </span>
                                            </label>
                                            <div className="w-full overflow-hidden border-t tab-content">
                                                <div className="p-4">
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white">
                                                            Email:
                                                        </h1>
                                                        <h1 className="pl-2 text-left dark:text-white">
                                                            {person.email}
                                                        </h1>
                                                    </div>
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white">
                                                            Last Login:
                                                        </h1>
                                                        <h1 className="pl-2 text-left dark:text-white">
                                                            {person.updated_at}
                                                        </h1>
                                                    </div>
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white">
                                                            Created on:
                                                        </h1>
                                                        <h1 className="pl-2 text-left dark:text-white">
                                                            {person.created_at}
                                                        </h1>
                                                    </div>
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white">
                                                            Password:
                                                        </h1>
                                                        <h1 className="pl-2 text-left dark:text-white">
                                                            <a
                                                                href="#"
                                                                className="text-blue-400 underline"
                                                            >
                                                                Reset Password
                                                            </a>
                                                        </h1>
                                                    </div>
                                                    <div className="flex py-1">
                                                        <h1 className="font-semibold dark:text-white">
                                                            Edit:
                                                        </h1>
                                                        <h1 className="pl-2 text-left dark:text-white">
                                                            <h1
                                                                onClick={() => {
                                                                    history.push(
                                                                        {
                                                                            pathname:
                                                                                "/admin/users/create",
                                                                            user: person,
                                                                        }
                                                                    );
                                                                }}
                                                                className="text-indigo-600 underline cursor-pointer hover:text-indigo-900 dark:text-blue-400 dark:hover:text-blue-500"
                                                            >
                                                                Edit
                                                            </h1>
                                                        </h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </>
                    )}
                </div>
            </div>
        </div>
    );
};
export default Users;
