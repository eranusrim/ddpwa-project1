import React, { Component } from "react";
import PageHeading from "../pages/PageHeading";
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";
// import ReactPhoneInput from "react-phone-input-2";
// import "react-phone-input-2/lib/style.css";

import { CgAsterisk } from "react-icons/cg";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { notify } from "../../utility";

class CreateUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            first_name: "",
            last_name: "",
            email: "",
            // number: null,
            // gender: "",
            // status: "",
            // date: "",

            first_nameError: "",
            emailError: "",
        };
    }
    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    handleCancelBtn = () => {
        this.props.history.goBack();
    };

    onSaveNewUser = () => {
        let first_nameError = "";
        let emailError = "";
        if (this.state.first_name.trim() === "") {
            first_nameError = "Please Enter First Name";
        }
        if (this.state.email.trim() === "") {
            emailError = "Please Enter Email";
        }

        if (first_nameError !== "" || emailError !== "") {
            this.setState({
                first_nameError: first_nameError,
                emailError: emailError,
            });
            return;
        }
        let userInfo = JSON.parse(localStorage.getItem("userInfo"));
        let access_token = userInfo.access_token;

        let config = {
            method: "post",
            url: "http://laravelcms.devdigdev.com/api/v1/users",
            data: {
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                email: this.state.email,
            },
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${access_token}`,
            },
        };
        if (!this.props.history.location.user) {
            axios(config)
                .then(function (response) {
                    console.log("success", JSON.stringify(response.data));
                    notify("New User Added");
                    this.props.history.push("/admin/users/");
                })
                .catch(function (error) {
                    console.log(error);
                    notify(error);
                });
        }

        if (this.props.history.location.user) {
            let updateUser = {
                method: "put",
                url: "http://localhost:5000/users",
                params: {
                    id: this.props.history.location.user.id,
                    first_name: this.state.first_name,
                    last_name: this.state.last_name,
                },
                headers: {
                    "Content-type": "application/json",
                    Authorization: `Bearer ${access_token}`,
                },
            };
            axios(updateUser)
                .then(function (response) {
                    const data = response.data;
                    console.log("response", response.data);
                    notify(data.message);
                    this.props.history.push("/admin/users/");
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    };
    componentDidMount() {
        console.log("props", this.props);
        console.log("data", this.props.history.location.user);
        let user = this.props.history.location.user;
        if (this.props.history.location.user) {
            this.setState({
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email,
            });
        }
    }

    render() {
        return (
            <div
                className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
                    this.props.Sidebar
                        ? "w-full sm:content md:content lg:content xl:content"
                        : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
                }`}
            >
                <div className="flex flex-col items-center w-full">
                    <PageHeading
                        pageHeading={"Create User"}
                        pageSearch={false}
                        showSaveOptionsBtn={true}
                        cancel={true}
                        handleCancelBtn={this.handleCancelBtn}
                        save={true}
                        saveAndContinue={true}
                        handleSave={this.onSaveNewUser}
                    />
                    <div className="w-full px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 flex items-center my-5">
                        <div className="grid lg:grid-cols-2  gap-5 w-full">
                            <div className="flex flex-col w-full">
                                <label
                                    for="first_name"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    First Name
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <input
                                    type="text"
                                    value={this.state.first_name}
                                    className={`${
                                        this.state.first_nameError
                                            ? "border-red-500"
                                            : "border"
                                    } border h-10 rounded px-2 text-sm font-medium `}
                                    placeholder="First Name"
                                    onChange={(e) => {
                                        this.setState({ first_nameError: "" });
                                        this.handleInput(e);
                                    }}
                                    name="first_name"
                                />
                                {this.state.first_nameError && (
                                    <span className="text-red-500 text-xs">
                                        {this.state.first_nameError}
                                    </span>
                                )}
                            </div>
                            <div className="flex flex-col w-full">
                                <label
                                    for="last_name"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Last Name
                                </label>
                                <input
                                    type="text"
                                    value={this.state.last_name}
                                    onChange={(e) => {
                                        this.handleInput(e);
                                    }}
                                    className="border h-10 rounded px-2 text-sm font-medium "
                                    placeholder="Last Name"
                                    name="last_name"
                                />
                            </div>
                            <div className="flex flex-col w-full">
                                <label
                                    for="email"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Email{" "}
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <input
                                    value={this.state.email}
                                    disabled={
                                        this.props.history.location.user
                                            ? true
                                            : false
                                    }
                                    onChange={(e) => {
                                        this.setState({ emailError: "" });
                                        this.handleInput(e);
                                    }}
                                    className={`${
                                        this.state.emailError
                                            ? "border-red-500"
                                            : "border"
                                    } border h-10 rounded px-2 text-sm font-medium `}
                                    type="email"
                                    placeholder="Email"
                                    name="email"
                                />
                                {this.state.emailError && (
                                    <span className="text-red-500 text-xs">
                                        {this.state.emailError}
                                    </span>
                                )}
                            </div>
                            {/* <div className="flex flex-col w-full">
                                <label
                                    for="number"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Phone Number{" "}
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <ReactPhoneInput
                                    country={"in"}
                                    inputStyle={{
                                        width: "100%",
                                        height: "2.5rem",
                                        fontSize: "13px",
                                        paddingLeft: "48px",
                                        borderRadius: "5px",
                                        fontWeight: "600",
                                        border: `1px solid ${
                                            this.state.numberError
                                                ? "red"
                                                : "lightgray"
                                        }`,
                                    }}
                                    value={this.state.number}
                                    onChange={(number) => {
                                        this.setState({
                                            number,
                                            numberError: "",
                                        });
                                    }}
                                />
                                {this.state.numberError && (
                                    <span className="text-red-500 text-xs">
                                        {this.state.numberError}
                                    </span>
                                )}
                            </div>
                            <div className="flex flex-col w-full">
                                <label
                                    for="date"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Date of Birth
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <DatePicker
                                    selected={this.state.date}
                                    dateFormat="MMMM d, yyyy "
                                    placeholderText="Birth Date"
                                    maxDate={new Date()}
                                    isClearable
                                    showYearDropdown
                                    scrollableYearDropdown
                                    onChange={(date) => {
                                        this.setState({ dateError: "" });
                                        this.setState({
                                            date: date,
                                        });
                                    }}
                                    className={`${
                                        this.state.dateError
                                            ? "border-red-500"
                                            : "border"
                                    } border h-10 rounded px-2 text-sm font-medium w-full `}
                                />
                                {this.state.dateError && (
                                    <span className="text-red-500 text-xs">
                                        {this.state.dateError}
                                    </span>
                                )}
                            </div>
                            <div className="flex flex-col w-full">
                                <label
                                    for="gender"
                                    className="font-medium mb-1 items-center"
                                >
                                    Gender{" "}
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <select
                                    name="gender"
                                    value={this.state.gender}
                                    onChange={(e) => {
                                        this.setState({ genderError: "" });
                                        this.handleInput(e);
                                    }}
                                    className={`${
                                        this.state.genderError
                                            ? "border-red-500"
                                            : "border"
                                    } h-10 rounded px-2 border text-sm font-medium`}
                                >
                                    <option>Select Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                                {this.state.genderError && (
                                    <span className="text-red-500 text-xs">
                                        {this.state.genderError}
                                    </span>
                                )}
                            </div>
                            <div className="flex flex-col w-full">
                                <label
                                    for="status"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Status
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <select
                                    name="status"
                                    onChange={(e) => {
                                        this.setState({
                                            statusError: "",
                                        });
                                        this.handleInput(e);
                                    }}
                                    value={this.state.status}
                                    className={`${
                                        this.state.statusError
                                            ? "border-red-500"
                                            : "border"
                                    } h-10 rounded px-2 border text-sm font-medium`}
                                >
                                    <option>Select status</option>
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                                {this.state.statusError && (
                                    <span className="text-red-500 text-xs">
                                        {this.state.statusError}
                                    </span>
                                )}
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(CreateUser);
