import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { CgAsterisk } from "react-icons/cg";
import TimeKeeper from "react-timekeeper";
import isURL from "validator/lib/isURL";
import { connect } from "react-redux";
import PageHeading from "../../components/pages/PageHeading";
import { addNewEvent } from "../../Redux/actions/eventsAction";
import { withRouter } from "react-router-dom";
import axios from "axios";
import moment from "moment";
import { notify } from "../../utility";

const userInfo = JSON.parse(localStorage.getItem("userInfo"));
let access_token;
if (userInfo) {
    access_token = userInfo.access_token;
}
class AddEvent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            setShowTime: false,

            id: null,
            title: "",
            recurrence: "1",
            startDate: "",
            endDate: "",
            eventTime: "00:00pm",
            status: "1",
            url: null,
            description: "",
            address1: "",
            address2: "",
            city: "",
            state: "",
            zipCode: null,
            country: "",

            titleError: "",
            startDateError: "",
            endDateError: "",
            eventTimeError: "",
            urlError: "",
        };
    }

    fetchEvent = (id) => {
        let config = {
            method: "get",
            url: "http://localhost:5000/events/" + id,
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${access_token}`,
            },
        };
        axios(config)
            .then((response) => {
                const data = response.data.events;
                this.setState({
                    id: data.id,
                    title: data.title,
                    recurrence: data.recurrence,
                    startDate: moment(data.startDate).toDate(),
                    endDate: moment(data.endDate).toDate(),
                    eventTime: moment(data.eventTime, "HH:mm").format(
                        "hh:mm A"
                    ),
                    status: data.status,
                    url: data.url,
                    description: data.description,
                    address1: data.address,
                    city: data.city,
                    state: data.state,
                    zipCode: data.zipcode,
                    country: data.country,
                });
            })
            .catch((error) => {
                console.log("error", error);
            });
    };
    componentDidMount() {
        let id = this.props.match.params.id;
        if (id) {
            this.fetchEvent(id);
        }
    }
    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    validateUrl = (value) => {
        if (isURL(value)) {
            this.setState({
                url: value,
                urlError: "",
            });
        } else {
            this.setState({
                url: null,
                urlError: "URL must contain http:// or https://",
            });
        }
    };

    handleCancelBtn = () => {
        this.props.history.goBack();
    };

    handleAddEditEvent = () => {
        let titleError = "";
        let startDateError = "";
        let endDateError = "";
        let eventTimeError = "";
        let urlError = "";
        let sDate = new Date(this.state.startDate);
        let eDate = new Date(this.state.endDate);
        if (this.state.title.trim() === "") {
            titleError = "Enter Event title";
        }
        if (this.state.startDate === "") {
            startDateError = "Enter Event start date";
        }
        if (sDate > eDate) {
            startDateError =
                "End Date is greater than or equal to the Start Date";
        }
        if (this.state.endDate === "") {
            endDateError = "Enter Event end date";
        }
        if (this.state.eventTime === "00:00pm") {
            eventTimeError = "Select Event time";
        }
        if (this.state.url === null) {
            urlError = "Enter url";
        }

        if (
            titleError !== "" ||
            startDateError !== "" ||
            endDateError !== "" ||
            eventTimeError !== "" ||
            urlError !== ""
        ) {
            this.setState({
                titleError: titleError,
                startDateError: startDateError,
                endDateError: endDateError,
                eventTimeError: eventTimeError,
                urlError: urlError,
            });
            return;
        }

        let event = {
            id: this.state.id,
            title: this.state.title,
            recurrence: this.state.recurrence,
            startDate: moment(this.state.startDate).format("YYYY-MM-DD"),
            endDate: moment(this.state.endDate).format("YYYY-MM-DD"),
            eventTime: moment(this.state.eventTime, "hh:mm A").format("HH:mm"),
            status: this.state.status,
            url: this.state.url,
            description: this.state.description,
            address: this.state.address1,
            city: this.state.city,
            state: this.state.state,
            zipcode: this.state.zipCode,
            country: this.state.country,
        };

        if (this.props.match.params.id) {
            let config = {
                method: "put",
                url: "http://localhost:5000/events/editEvent",
                data: event,
                headers: {
                    "Content-type": "application/json",
                    Authorization: `Bearer ${access_token}`,
                },
            };
            axios(config)
                .then(function (response) {
                    const data = response.data;
                    notify(data.message);
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            let config = {
                method: "post",
                url: "http://localhost:5000/events",
                data: event,
                headers: {
                    "Content-type": "application/json",
                    Authorization: `Bearer ${access_token}`,
                },
            };
            axios(config)
                .then(function (response) {
                    const data = response.data;
                    notify(data.message);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    };

    render() {
        return (
            <div
                className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
                    this.props.Sidebar
                        ? "w-full sm:content md:content lg:content xl:content"
                        : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
                }`}
            >
                <div className="flex flex-col items-center w-full">
                    <PageHeading
                        pageHeading={"Add Event"}
                        pageSearch={false}
                        showSaveOptionsBtn={true}
                        cancel={true}
                        handleCancelBtn={this.handleCancelBtn}
                        save={true}
                        saveAndContinue={true}
                        handleSave={this.handleAddEditEvent}
                    />
                    <div className="w-full px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 flex items-center pt-4 pb-4">
                        <div className="grid lg:grid-cols-2  gap-5 w-full">
                            <div className="col-span-2 grid grid-cols-1 gap-5  sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 sm:w-full md:w-full lg:w-full xl:w-full">
                                <div className="flex flex-col w-full">
                                    <label
                                        for="title"
                                        className="font-medium mb-1 flex items-center"
                                    >
                                        Title
                                        <CgAsterisk className="inline text-red-500" />
                                    </label>
                                    <input
                                        type="text"
                                        value={this.state.title}
                                        className={`${
                                            this.state.titleError
                                                ? "border-red-500"
                                                : "border"
                                        } border h-10 rounded px-2 text-sm font-medium `}
                                        placeholder="Event Title"
                                        onChange={(e) => {
                                            this.setState({ titleError: "" });
                                            this.handleInput(e);
                                        }}
                                        name="title"
                                    />
                                    {this.state.titleError && (
                                        <span className="text-red-500 text-xs">
                                            {this.state.titleError}
                                        </span>
                                    )}
                                </div>
                                <div className="flex flex-col w-full">
                                    <label
                                        for="recurrence"
                                        className="font-medium mb-1 items-center"
                                    >
                                        Recurrence
                                    </label>
                                    <select
                                        value={this.state.recurrence}
                                        onChange={(e) => {
                                            this.handleInput(e);
                                        }}
                                        name="recurrence"
                                        className="border h-10 rounded px-2  text-sm font-medium"
                                    >
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Monthly</option>
                                        <option value="4">Yearly</option>
                                    </select>
                                </div>
                            </div>
                            <div className="grid grid-cols-1 gap-2 sm:grid-cols-1  col-span-2 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                                <div className="flex flex-col">
                                    <label
                                        for="startDate"
                                        className="font-medium mb-1 flex items-center"
                                    >
                                        Start Date
                                        <CgAsterisk className="inline text-red-500" />
                                    </label>
                                    <DatePicker
                                        selected={this.state.startDate}
                                        dateFormat="MMMM d, yyyy "
                                        placeholderText="Start Date"
                                        isClearable
                                        showYearDropdown
                                        scrollableYearDropdown
                                        onChange={(date) => {
                                            this.setState({
                                                startDateError: "",
                                            });
                                            this.setState({
                                                startDate: date,
                                            });
                                        }}
                                        className={`${
                                            this.state.startDateError
                                                ? "border-red-500"
                                                : "border"
                                        } border h-10 rounded px-2 text-sm font-medium w-full `}
                                    />
                                    {this.state.startDateError && (
                                        <span className="text-red-500 text-xs">
                                            {this.state.startDateError}
                                        </span>
                                    )}
                                </div>
                                <div className="flex flex-col">
                                    <label
                                        for="endDate"
                                        className="font-medium mb-1 flex items-center"
                                    >
                                        End Date
                                        <CgAsterisk className="inline text-red-500" />
                                    </label>
                                    <DatePicker
                                        selected={this.state.endDate}
                                        dateFormat="MMMM d, yyyy "
                                        placeholderText="End Date"
                                        isClearable
                                        showYearDropdown
                                        scrollableYearDropdown
                                        onChange={(date) => {
                                            this.setState({
                                                endDateError: "",
                                            });
                                            this.setState({
                                                endDate: date,
                                            });
                                        }}
                                        className={`${
                                            this.state.endDateError
                                                ? "border-red-500"
                                                : "border"
                                        } border h-10 rounded px-2 text-sm font-medium w-full `}
                                    />
                                    {this.state.endDateError && (
                                        <span className="text-red-500 text-xs">
                                            {this.state.endDateError}
                                        </span>
                                    )}
                                </div>
                            </div>
                            <div className="grid grid-cols-1 gap-2 sm:grid-cols-1 col-span-2 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                                <div className="flex flex-col w-full overflow-hidden">
                                    <label
                                        for="eventTime"
                                        className="font-medium mb-1 flex items-center"
                                    >
                                        Event Time
                                        <CgAsterisk className="inline text-red-500" />
                                    </label>
                                    {this.state.setShowTime && (
                                        <div className="w-full overflow-hidden">
                                            <TimeKeeper
                                                time={this.state.eventTime}
                                                onChange={(newTime) =>
                                                    this.setState({
                                                        eventTime:
                                                            newTime.formatted12,
                                                    })
                                                }
                                                onDoneClick={() => {
                                                    this.setState({
                                                        setShowTime: false,
                                                    });
                                                }}
                                            />
                                        </div>
                                    )}

                                    {!this.state.setShowTime && (
                                        <input
                                            value={this.state.eventTime}
                                            className={`${
                                                this.state.eventTimeError
                                                    ? "border-red-500"
                                                    : "border"
                                            } border h-10 rounded px-2 text-sm font-medium w-full `}
                                            onClick={() => {
                                                this.setState({
                                                    eventTimeError: "",
                                                    setShowTime: true,
                                                });
                                            }}
                                        />
                                    )}
                                    {this.state.eventTimeError && (
                                        <span className="text-red-500 text-xs">
                                            {this.state.eventTimeError}
                                        </span>
                                    )}
                                </div>
                                <div className="flex flex-col w-full">
                                    <label
                                        for="status"
                                        className="font-medium mb-1 items-center"
                                    >
                                        Status
                                    </label>
                                    <select
                                        name="status"
                                        value={this.state.status}
                                        onChange={(e) => {
                                            this.handleInput(e);
                                        }}
                                        className="border h-10 rounded px-2  text-sm font-medium"
                                    >
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div className="flex flex-col w-full lg:col-span-1 sm:col-span-2">
                                <label
                                    for="url"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    URL
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <input
                                    type="text"
                                    value={this.state.url}
                                    className={`${
                                        this.state.urlError
                                            ? "border-red-500"
                                            : "border"
                                    } border h-10 rounded px-2 text-sm font-medium `}
                                    placeholder="Url"
                                    onChange={(e) => {
                                        this.validateUrl(e.target.value);
                                    }}
                                    name="url"
                                />
                                {this.state.urlError && (
                                    <span className="text-red-500 text-xs">
                                        {this.state.urlError}
                                    </span>
                                )}
                            </div>
                            <div className="flex flex-col w-full col-span-2">
                                <label
                                    for="description"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Description
                                </label>
                                <textarea
                                    value={this.state.description}
                                    className="border h-40 rounded px-2 text-sm font-medium"
                                    placeholder="Enter Event description"
                                    onChange={(e) => {
                                        this.handleInput(e);
                                    }}
                                    name="description"
                                ></textarea>
                            </div>
                            <div className="flex flex-col w-full col-span-2 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                                <label
                                    for="address1"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Address 1
                                </label>
                                <input
                                    type="text"
                                    name="address1"
                                    value={this.state.address1}
                                    className="border h-10 rounded px-2 text-sm font-medium "
                                    onChange={(e) => {
                                        this.handleInput(e);
                                    }}
                                />
                            </div>
                            <div className="flex flex-col w-full col-span-2 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                                <label
                                    for="address2"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Address 2
                                </label>
                                <input
                                    type="text"
                                    name="address2"
                                    value={this.state.address2}
                                    className="border h-10 rounded px-2 text-sm font-medium "
                                    onChange={(e) => {
                                        this.handleInput(e);
                                    }}
                                />
                            </div>
                            <div className="flex flex-col w-full col-span-2 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                                <label
                                    for="city"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    City
                                </label>
                                <input
                                    type="text"
                                    name="city"
                                    value={this.state.city}
                                    className="border h-10 rounded px-2 text-sm font-medium "
                                    onChange={(e) => {
                                        this.handleInput(e);
                                    }}
                                />
                            </div>
                            <div className="flex flex-col w-full col-span-2 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                                <label
                                    for="state"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    State
                                </label>
                                <input
                                    type="text"
                                    name="state"
                                    value={this.state.state}
                                    className="border h-10 rounded px-2 text-sm font-medium "
                                    onChange={(e) => {
                                        this.handleInput(e);
                                    }}
                                />
                            </div>
                            <div className="flex flex-col w-full col-span-2 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                                <label
                                    for="zipCode"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Zipcode
                                </label>
                                <input
                                    type="number"
                                    name="zipCode"
                                    value={this.state.zipCode}
                                    className="border h-10 rounded px-2 text-sm font-medium "
                                    onChange={(e) => {
                                        this.handleInput(e);
                                    }}
                                />
                            </div>
                            <div className="flex flex-col w-full col-span-2 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                                <label
                                    for="country"
                                    className="font-medium mb-1 flex items-center"
                                >
                                    Country
                                </label>
                                <input
                                    type="text"
                                    name="country"
                                    value={this.state.country}
                                    className="border h-10 rounded px-2 text-sm font-medium "
                                    onChange={(e) => {
                                        this.handleInput(e);
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addNewEvent: (newEvent) => dispatch(addNewEvent(newEvent)),
    };
};

export default connect(null, mapDispatchToProps)(withRouter(AddEvent));
