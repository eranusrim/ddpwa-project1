/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";
import PageHeading from "../pages/PageHeading";
import EventsTable from "./eventsTable";
import axios from "axios";
import { notify } from "../../utility";
import moment from "moment";

const Event = (props) => {
  const [events, setEvents] = useState([]);

  let [currentPage, setCurrentPage] = useState(1);
  let [eventsPerPage, setEventsPerPage] = useState(10);

  let [selectAllCheckbox, setSelectAllCheckbox] = useState(false);

  let [search, setSearch] = useState("");
  let [pageSearch, setPageSearch] = useState(true);
  let [activeInactive, setActiveInactive] = useState(false);

  let [sortByTitle, setSortByTitle] = useState(true);
  let [sortStartDate, setSortStartDate] = useState(true);
  let [sortEndDate, setSortEndDate] = useState(true);
  let [sortByTime, setSortByTime] = useState(true);
  let [sortByRecurrence, setSortByRecurrence] = useState(true);
  let [sortByModified, setSortByModified] = useState(true);

  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const access_token = userInfo.access_token;

  const fetchEvents = () => {
    let config = {
      method: "get",
      url: "http://localhost:5000/events",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };
    axios(config)
      .then(function (response) {
        const data = response.data.events;
        console.log("response eee", response.data.events);
        const sorted = data.sort((a, b) => {
          const isReverse = sortByTitle === true ? 1 : -1;
          return isReverse * a.title.localeCompare(b.title);
        });
        setEventsPerPage(data.length < 10 ? data.length : 10);
        setEvents(
          sorted.map((event) => {
            return {
              select: false,
              id: event.id,
              title: event.title,
              startDate: moment(event.startDate).format("YYYY-MM-DD"),
              endDate: moment(event.endDate).format("YYYY-MM-DD"),
              eventTime: moment(event.eventTime, "HH:mm").format("hh:mm A"),
              recurrence:
                event.recurrence === 1
                  ? "Daily"
                  : "monthly"
                  ? event.recurrence === 2
                    ? "Weekly"
                    : "monthly"
                  : event.recurrence === 3
                  ? "monthly"
                  : "daily",
              modifiedOn: event.modifiedOn
                ? moment(event.modifiedOn).format("YYYY-MM-DD")
                : null,
              status: event.status,
            };
          })
        );
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchEvents();
  }, []);

  const indexOfLastEvent = currentPage * eventsPerPage;
  const indexOfFirstPost = indexOfLastEvent - eventsPerPage;
  const currentEvents = events.slice(indexOfFirstPost, indexOfLastEvent);

  const paginate = (number) => {
    setCurrentPage(number);
  };
  const setNumberOfEvent = (number) => {
    setEventsPerPage(parseInt(number));
  };

  const selectAll = (e) => {
    let checked = e.target.checked;
    if (checked) {
      setPageSearch(false);
      setActiveInactive(true);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
    setEvents(
      events.map((d) => {
        d.select = checked;
        return d;
      })
    );

    console.log(events);
  };

  const selectSingle = (e, id) => {
    let checked = e.target.checked;
    setEvents(
      events.map((event) => {
        if (id === event.id) {
          event.select = checked;
          console.log(event);
        }

        return event;
      })
    );

    console.log(events);

    const result = events.some(function (data) {
      return data.select === true;
    });
    if (result) {
      setActiveInactive(true);
      setPageSearch(false);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
  };

  const handleSearch = () => {
    let searchData = events.filter((d) => {
      if (d.title.toLocaleLowerCase().includes(search.toLocaleLowerCase())) {
        console.log(d);
        return d;
      }
    });
    console.log("length", searchData.length);
    console.log(searchData);
    if (searchData.length === 0) {
      setEvents(events);
    } else {
      setEvents(searchData);
      setCurrentPage(1);
    }
  };
  const handleReset = () => {
    setSearch("");
    fetchEvents();
  };
  const handleClose = () => {
    setSearch("");
    fetchEvents();
  };

  const sortTitle = (sortByTitle) => {
    const sorted = events.sort((a, b) => {
      const isReverse = sortByTitle === true ? 1 : -1;
      return isReverse * a.title.trim().localeCompare(b.title.trim());
    });
    setEvents(sorted);
  };

  const StartDateSort = (sortStartDate) => {
    const sorted = events.sort((a, b) => {
      let dateA = new Date(a.startDate);
      let dateB = new Date(b.startDate);
      return sortStartDate ? dateA - dateB : dateB - dateA;
    });
    setEvents(sorted);
  };

  const EndDateSort = (sortEndDate) => {
    const sorted = events.sort((a, b) => {
      let dateA = new Date(a.endDate);
      let dateB = new Date(b.endDate);
      return sortEndDate ? dateA - dateB : dateB - dateA;
    });
    setEvents(sorted);
  };

  const SortTime = (sortByTime) => {
    let reA = /[^a-zA-Z]/g;
    let reN = /[^0-9]/g;
    const sorted = events.sort((a, b) => {
      let aA = a.eventTime.toString().replace(reA, "");
      let bA = b.eventTime.toString().replace(reA, "");
      if (sortByTime) {
        if (aA === bA) {
          let aN = parseInt(a.eventTime.toString().replace(reN, ""), 10);
          let bN = parseInt(b.eventTime.toString().replace(reN, ""), 10);
          console.log("AN, BN", aN, bN);
          return aN === bN ? 0 : aN > bN ? 1 : -1;
        } else {
          return aA > bA ? 1 : -1;
        }
      } else {
        if (aA === bA) {
          let aN = parseInt(a.eventTime.toString().replace(reN, ""), 10);
          let bN = parseInt(b.eventTime.toString().replace(reN, ""), 10);
          console.log("AN, BN", aN, bN);
          return aN === bN ? 0 : aN < bN ? 1 : -1;
        } else {
          return aA < bA ? 1 : -1;
        }
      }
    });
    console.log("sorted time", sorted);
    setEvents(sorted);
  };

  const sortRecurrence = (sortByRecurrence) => {
    const sorted = events.sort((a, b) => {
      const isReverse = sortByRecurrence === true ? 1 : -1;
      return isReverse * a.recurrence.localeCompare(b.recurrence);
    });
    console.log("sorted recurrence", sorted);
    setEvents(sorted);
  };

  const sortModified = (sortByModified) => {
    const sorted = events.sort((a, b) => {
      let dateA = new Date(a.modifiedOn);
      let dateB = new Date(b.modifiedOn);
      return sortByModified ? dateA - dateB : dateB - dateA;
    });
    console.log("sorted date", sorted);
    setEvents(sorted);
  };

  const handleDelete = () => {
    let a = window.confirm("Are you sure you want to delete this");
    if (a) {
      let arrayId = [];
      events.forEach((d) => {
        if (d.select) {
          arrayId.push(d.id);
        }
      });
      axios
        .delete("http://localhost:5000/events/deleteEvents", {
          data: {
            events_ids: arrayId,
          },
        })
        .then((response) => {
          notify(response.data.message);
          setActiveInactive(!activeInactive);
          setPageSearch(!pageSearch);
          setSelectAllCheckbox(false);
          fetchEvents();
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  };
  const handleActive = () => {
    let arrayId = [];
    events.forEach((d) => {
      if (d.select) {
        arrayId.push(d.id);
      }
    });
    const data = {
      status: 1,
      events_ids: arrayId,
    };
    axios
      .put("http://localhost:5000/events/activeInactiveEvent", data)
      .then((response) => {
        notify(response.data.message);
        setSelectAllCheckbox(false);
        setActiveInactive(false);
        setPageSearch(true);
        fetchEvents();
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  const handleInActive = () => {
    let arrayId = [];
    events.forEach((d) => {
      if (d.select) {
        arrayId.push(d.id);
      }
    });
    const data = {
      status: 0,
      events_ids: arrayId,
    };
    axios
      .put("http://localhost:5000/events/activeInactiveEvent", data)
      .then((response) => {
        notify(response.data.message);
        setSelectAllCheckbox(false);
        setActiveInactive(false);
        setPageSearch(true);
        fetchEvents();
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  return (
    <div
      className={`content-container relative bg-gray-100 dark:bg-gray-700 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex-col items-center w-full">
        <PageHeading
          pageHeading={"Events"}
          searchLabel={"Title"}
          pageSearch={pageSearch}
          activeInactive={activeInactive}
          handleActive={handleActive}
          handleInActive={handleInActive}
          deleteBtn={true}
          handleDelete={handleDelete}
          search={search}
          setSearch={setSearch}
          handleSearch={handleSearch}
          handleReset={handleReset}
          handleClose={handleClose}
          path="/admin/events/addEvent"
        />
        <EventsTable
          events={currentEvents}
          eventsPerPage={eventsPerPage}
          totalEvent={events.length}
          paginate={paginate}
          currentPage={currentPage}
          setNumberOfEvent={setNumberOfEvent}
          selectAll={selectAll}
          selectSingle={selectSingle}
          sortTitle={sortTitle}
          sortByTitle={sortByTitle}
          setSortByTitle={setSortByTitle}
          StartDateSort={StartDateSort}
          sortStartDate={sortStartDate}
          setSortStartDate={setSortStartDate}
          EndDateSort={EndDateSort}
          sortEndDate={sortEndDate}
          setSortEndDate={setSortEndDate}
          SortTime={SortTime}
          sortByTime={sortByTime}
          setSortByTime={setSortByTime}
          sortRecurrence={sortRecurrence}
          sortByRecurrence={sortByRecurrence}
          setSortByRecurrence={setSortByRecurrence}
          sortModified={sortModified}
          sortByModified={sortByModified}
          setSortByModified={setSortByModified}
          selectAllCheckbox={selectAllCheckbox}
          setSelectAllCheckbox={setSelectAllCheckbox}
        />
      </div>
    </div>
  );
};

export default Event;
