import React, { useState } from "react";
import { RiArrowDownFill, RiArrowUpFill } from "react-icons/ri";
import { useHistory } from "react-router-dom";

const EventsTable = ({
    events,
    eventsPerPage,
    totalEvent,
    paginate,
    currentPage,
    setNumberOfEvent,
    selectAll,
    selectSingle,
    sortTitle,
    sortByTitle,
    setSortByTitle,
    StartDateSort,
    sortStartDate,
    setSortStartDate,
    EndDateSort,
    sortEndDate,
    setSortEndDate,
    SortTime,
    sortByTime,
    setSortByTime,
    sortRecurrence,
    sortByRecurrence,
    setSortByRecurrence,
    sortModified,
    sortByModified,
    setSortByModified,
    selectAllCheckbox,
    setSelectAllCheckbox,
}) => {
    const pageNumbers = [];
    let history = useHistory();
    for (let i = 1; i <= Math.ceil(totalEvent / eventsPerPage); i++) {
        pageNumbers.push(i);
    }
    let [sortTitleArrows, setSortTitleArrows] = useState(false);
    let [sortStartDateArrow, setSortStartDateArrow] = useState(true);
    let [sortEndDateArrow, setSortEndDateArrow] = useState(true);
    let [sortTimeArrow, setSortTimeArrow] = useState(true);
    let [sortRecurrenceArrow, setSortRecurrenceArrow] = useState(true);
    let [sortModifiedOnArrow, setModifiedOnArrow] = useState(true);

    return (
        <div className="overflow-x-scroll scrollbar-hide  dark:bg-gray-700 py-10 px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10">
            <table className="w-full  border-r hidden sm:table md:table lg:table xl:table border-gray-200 border-b">
                <thead className="bg-gray-200 dark:bg-gray-300 dark:text-white ">
                    <tr className="border-l-8 border-gray-400">
                        <th
                            scope="col"
                            className="dark:text-gray-700 px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            <input
                                checked={selectAllCheckbox}
                                onChange={(e) => {
                                    setSelectAllCheckbox(!selectAllCheckbox);
                                    selectAll(e);
                                }}
                                type="checkbox"
                            />
                        </th>
                        <th
                            onClick={() => {
                                sortTitle(sortByTitle);
                                setSortByTitle(!sortByTitle);
                                setSortTitleArrows(false);
                                setSortStartDateArrow(true);
                                setSortEndDateArrow(true);
                                setSortTimeArrow(true);
                                setSortRecurrenceArrow(true);
                                setModifiedOnArrow(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700 cursor-pointer px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            <span className="inline-block mr-2">Title</span>
                            {sortTitleArrows && (
                                <>
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortTitleArrows && (
                                <>
                                    {sortByTitle === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                StartDateSort(sortStartDate);
                                setSortStartDate(!sortStartDate);
                                setSortTitleArrows(true);
                                setSortStartDateArrow(false);
                                setSortEndDateArrow(true);
                                setSortTimeArrow(true);
                                setSortRecurrenceArrow(true);
                                setModifiedOnArrow(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 cursor-pointer text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Start Date
                            {sortStartDateArrow && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortStartDateArrow && (
                                <>
                                    {sortStartDate === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                EndDateSort(sortEndDate);
                                setSortEndDate(!sortEndDate);
                                setSortTitleArrows(true);
                                setSortStartDateArrow(true);
                                setSortEndDateArrow(false);
                                setSortTimeArrow(true);
                                setSortRecurrenceArrow(true);
                                setModifiedOnArrow(true);
                            }}
                            scope="col"
                            className=" dark:text-gray-700 px-6 py-3 cursor-pointer text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap "
                        >
                            End Date{" "}
                            {sortEndDateArrow && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortEndDateArrow && (
                                <>
                                    {sortEndDate === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}{" "}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                SortTime(sortByTime);
                                setSortByTime(!sortByTime);
                                setSortTitleArrows(true);
                                setSortStartDateArrow(true);
                                setSortEndDateArrow(true);
                                setSortTimeArrow(false);
                                setSortRecurrenceArrow(true);
                                setModifiedOnArrow(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 text-left text-xs cursor-pointer font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Event Time{" "}
                            {sortTimeArrow && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortTimeArrow && (
                                <>
                                    {sortByTime === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}{" "}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                sortRecurrence(sortByRecurrence);
                                setSortByRecurrence(!sortByRecurrence);
                                setSortTitleArrows(true);
                                setSortStartDateArrow(true);
                                setSortEndDateArrow(true);
                                setSortTimeArrow(true);
                                setSortRecurrenceArrow(false);
                                setModifiedOnArrow(true);
                            }}
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 cursor-pointer text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Recurrence{" "}
                            {sortRecurrenceArrow && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortRecurrenceArrow && (
                                <>
                                    {sortByRecurrence === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            onClick={() => {
                                sortModified(sortByModified);
                                setSortByModified(!sortByModified);
                                setSortTitleArrows(true);
                                setSortStartDateArrow(true);
                                setSortEndDateArrow(true);
                                setSortTimeArrow(true);
                                setSortRecurrenceArrow(true);
                                setModifiedOnArrow(false);
                            }}
                            scope="col"
                            className="dark:text-gray-700 cursor-pointer  px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Modification On{" "}
                            {sortModifiedOnArrow && (
                                <>
                                    {" "}
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortModifiedOnArrow && (
                                <>
                                    {sortByModified === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>
                        <th
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Edit
                        </th>
                    </tr>
                </thead>
                <tbody className="bg-white dark:bg-gray-700">
                    {events &&
                        events.map((event) => (
                            <tr
                                key={event.id}
                                className={`${
                                    event.status
                                        ? "border-left-green-8 "
                                        : "border-left-red-8"
                                } dark:hover:bg-gray-500 hover:bg-gray-200  border-b-gray-500 border-b`}
                            >
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500 dark:text-white">
                                    <input
                                        checked={event.select}
                                        onChange={(e) =>
                                            selectSingle(e, event.id)
                                        }
                                        type="checkbox"
                                    />
                                </td>
                                <td className="px-6 py-4 cursor-pointer  text-sm capitalize text-gray-600 dark:text-white">
                                    <span
                                        onClick={() => {
                                            history.push({
                                                pathname:
                                                    "/admin/events/addEvent",
                                                event: event,
                                            });
                                        }}
                                        className="hover:underline pb-2 inline-block hover:text-blue-500 hover:font-semibold"
                                    >
                                        {event.title}
                                    </span>
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    {event.startDate}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    {event.endDate}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    {event.eventTime}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm capitalize text-gray-500  dark:text-white">
                                    {event.recurrence}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm  text-gray-500 dark:text-white">
                                    {event.modifiedOn}
                                </td>
                                <td className=" py-4 whitespace-nowrap text-center text-sm font-medium">
                                    <h1
                                        onClick={() => {
                                            history.push(
                                                "/admin/events/editEvent/" +
                                                    event.id
                                            );
                                        }}
                                        className="text-indigo-600 hover:text-indigo-900 cursor-pointer dark:text-blue-400 dark:hover:text-blue-500"
                                    >
                                        Edit
                                    </h1>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </table>

            <div className="overflow-hidden  block sm:hidden md:hidden lg:hidden xl:hidden border-r dark:bg-gray-600">
                <div className="bg-gray-200 px-5 py-3 w-full border-t">
                    <input
                        className="ml-3"
                        checked={selectAllCheckbox}
                        onChange={(e) => {
                            setSelectAllCheckbox(!selectAllCheckbox);
                            selectAll(e);
                        }}
                        type="checkbox"
                    />
                    <label
                        onClick={() => {
                            sortTitle(sortByTitle);
                            setSortByTitle(!sortByTitle);
                            setSortTitleArrows(false);
                            setSortStartDateArrow(true);
                            setSortEndDateArrow(true);
                            setSortTimeArrow(true);
                            setSortRecurrenceArrow(true);
                            setModifiedOnArrow(true);
                        }}
                        className="px-5 py-3 text-sm font-medium leading-normal"
                    >
                        Events{" "}
                        {!sortTitleArrows && (
                            <>
                                {sortByTitle === true ? (
                                    <RiArrowUpFill className="inline-block ml-2" />
                                ) : (
                                    <RiArrowDownFill className="inline-block ml-2" />
                                )}
                            </>
                        )}
                    </label>
                </div>
                {events &&
                    events.map((event) => (
                        <div
                            className={`tab w-full border-t ${
                                event.status
                                    ? "border-left-green-8 "
                                    : "border-left-red-8"
                            }`}
                        >
                            <input
                                className="absolute opacity-0"
                                id={event.id}
                                type="checkbox"
                                name="tabs"
                            />
                            <label
                                className="flex items-center justify-between px-5 py-3 text-sm font-medium leading-normal cursor-pointer bg-blue-400 text-white dark:bg-gray-700 dark:text-white"
                                for={event.id}
                            >
                                <input
                                    checked={event.select}
                                    onChange={(e) => selectSingle(e, event.id)}
                                    type="checkbox"
                                />
                                <span className="px-5 block w-11/12">
                                    {event.title}
                                </span>
                            </label>
                            <div className="tab-content border-t overflow-hidden w-full">
                                <div className="p-4">
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Title:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {event.title}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Start date:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {event.startDate}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            End date:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {event.endDate}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Event time:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {event.eventTime}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Event time:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {event.eventTime}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Recurrence:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {event.recurrence}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Modified on:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            {event.modifiedOn}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Edit:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            <h1
                                                onClick={() => {
                                                    history.push(
                                                        "/admin/events/editEvent/" +
                                                            event.id
                                                    );
                                                }}
                                                className="cursor-pointer underline text-blue-400 hover:text-blue-500"
                                            >
                                                Edit
                                            </h1>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
            </div>

            {/* pagination */}
            <div className="w-full flex flex-col sm:flex-row md:flex-row lg:flex-row xl:flex-row justify-between mt-5 mb-3">
                <ul className="flex items-center justify-start cursor-pointer space-x-2">
                    {pageNumbers.map((number) => (
                        <li
                            key={number}
                            className={`p-3 text-sm ${
                                currentPage === number
                                    ? "bg-red-500"
                                    : "bg-blue-400"
                            }  text-white liTags`}
                            onClick={() => paginate(number)}
                        >
                            {number}
                        </li>
                    ))}
                </ul>
                <div className="flex items-center justify center mt-3 sm:mt-0 md:mt-0 lg:mt-0 xl:mt-0">
                    <span className="pr-2 dark:text-white">Show</span>
                    <input
                        type="number"
                        value={eventsPerPage}
                        className="px-1 py-1 w-24 border border-black"
                        onChange={(e) => setNumberOfEvent(e.target.value)}
                    />
                    <span className="pl-2 dark:text-white">Entries</span>
                </div>
            </div>
        </div>
    );
};
export default EventsTable;
