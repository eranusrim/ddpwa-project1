/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import PageHeading from "../pages/PageHeading";
import { CategoriesData } from "./CategoriesData";
import { useDispatch, useSelector } from "react-redux";
import {
  FETCH_CATEGORY_REQUEST,
  FETCH_CATEGORY_SUCCESS,
  FETCH_CATEGORY_FAIL,
  DELETE_CATEGORY,
  ACTIVE_CATEGORY,
  INACTIVE_CATEGORY,
} from "../../Redux/actionTypes";
import CategoriesTable from "./CategoriesTable";

const Categories = (props) => {
  let dispatch = useDispatch();

  const [categories, setCategories] = useState([]);

  const [search, setSearch] = useState("");
  const [pageSearch, setPageSearch] = useState(true);
  const [activeInactive, setActiveInactive] = useState(false);

  let [currentPage, setCurrentPage] = useState(1);
  let [categoryPerPage, setCategoryPerPage] = useState(10);

  let [selectAllCheckbox, setSelectAllCheckbox] = useState(false);

  let [sortByTitle, setSortByTitle] = useState(true);
  let [sortBySlug, setSortBySlug] = useState(false);

  const getCategories = () => {
    dispatch({
      type: FETCH_CATEGORY_REQUEST,
    });
    dispatch({
      type: FETCH_CATEGORY_SUCCESS,
      payload: CategoriesData,
    });
    setCategories(
      CategoriesData.map((category) => {
        return {
          select: false,
          id: category.id,
          title: category.title,
          slug: category.slug,
          status: category.status,
        };
      })
    );
  };
  const ReducerCategories = useSelector(
    (state) => state.CategoryReducer.categories
  );
  useEffect(() => {
    getCategories();
  }, []);

  const indexOfLastEvent = currentPage * categoryPerPage;
  const indexOfFirstPost = indexOfLastEvent - categoryPerPage;
  const currentCategories = categories.slice(
    indexOfFirstPost,
    indexOfLastEvent
  );

  const paginate = (number) => {
    setCurrentPage(number);
  };
  const setNumberOfEvent = (number) => {
    setCategoryPerPage(parseInt(number));
  };

  const selectAll = (e) => {
    let checked = e.target.checked;
    if (checked) {
      setPageSearch(false);
      setActiveInactive(true);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
    setCategories(
      categories.map((d) => {
        d.select = checked;
        return d;
      })
    );

    console.log(categories);
  };

  const selectSingle = (e, id) => {
    let checked = e.target.checked;
    setCategories(
      categories.map((category) => {
        if (id === category.id) {
          category.select = checked;
          console.log(category);
        }

        return category;
      })
    );

    console.log(categories);

    const result = categories.some(function (data) {
      return data.select === true;
    });
    if (result) {
      setActiveInactive(true);
      setPageSearch(false);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
  };

  const sortTitle = (sortByTitle) => {
    const sorted = categories.sort((a, b) => {
      const isReverse = sortByTitle === true ? 1 : -1;
      return isReverse * a.title.localeCompare(b.title);
    });
    setCategories(sorted);
  };
  const sortSlug = (sortBySlug) => {
    const sorted = categories.sort((a, b) => {
      const isReverse = sortBySlug === true ? 1 : -1;
      return isReverse * a.slug.localeCompare(b.slug);
    });
    setCategories(sorted);
  };
  const handleSearch = () => {
    let searchData = categories.filter((d) => {
      if (d.title.toLocaleLowerCase().includes(search.toLocaleLowerCase())) {
        console.log(d);
        return d;
      }
    });
    console.log("length", searchData.length);
    console.log(searchData);
    if (searchData.length === 0) {
      setCategories(categories);
    } else {
      setCategories(searchData);
      setCurrentPage(1);
    }
  };
  const handleReset = () => {
    setSearch("");
    if (categories) {
      setCategories(
        ReducerCategories.map((category) => {
          return {
            select: false,
            id: category.id,
            title: category.title,
            slug: category.slug,
            status: category.status,
          };
        })
      );
    }
  };
  const handleClose = () => {
    setSearch("");
    if (categories) {
      setCategories(
        ReducerCategories.map((category) => {
          return {
            select: false,
            id: category.id,
            title: category.title,
            slug: category.slug,
            status: category.status,
          };
        })
      );
    }
  };
  const handleDelete = () => {
    let a = window.confirm("Are you sure you want to delete this");
    console.log("deleting");
    if (a) {
      let arrayId = [];
      categories.forEach((d) => {
        if (d.select) {
          arrayId.push(d.id);
        }
      });
      let arr = ReducerCategories.filter((val) => !arrayId.includes(val.id));
      console.log(arr);
      setCategories(
        arr.map((a) => {
          return {
            select: false,
            id: a.id,
            title: a.title,
            slug: a.slug,
            status: a.status,
          };
        })
      );
      dispatch({
        type: DELETE_CATEGORY,
        payload: arr,
      });
    }
    setActiveInactive(!activeInactive);
    setPageSearch(!pageSearch);
    setSelectAllCheckbox(false);
    console.log("selectAll", selectAllCheckbox);
  };
  const handleActive = () => {
    let activeID = [];
    categories.forEach((active) => {
      if (active.select) {
        activeID.push(active.id);
      }
    });
    let arr = ReducerCategories.filter((val) => {
      if (activeID.includes(val.id)) {
        val.status = "1";
      }
      return val;
    });
    setCategories(
      arr.map((a) => {
        return {
          select: false,
          id: a.id,
          title: a.title,
          slug: a.slug,
          status: a.status,
        };
      })
    );
    console.log("active elements", arr);
    dispatch({
      type: ACTIVE_CATEGORY,
      payload: arr,
    });
    setSelectAllCheckbox(false);
    setActiveInactive(false);
    setPageSearch(true);
  };
  const handleInActive = () => {
    let activeID = [];
    categories.forEach((active) => {
      if (active.select) {
        activeID.push(active.id);
      }
    });
    let arr = ReducerCategories.filter((val) => {
      if (activeID.includes(val.id)) {
        val.status = "0";
      }
      return val;
    });
    setCategories(
      arr.map((a) => {
        return {
          select: false,
          id: a.id,
          title: a.title,
          slug: a.slug,
          status: a.status,
        };
      })
    );
    console.log("active elements", arr);
    dispatch({
      type: INACTIVE_CATEGORY,
      payload: arr,
    });
    setSelectAllCheckbox(false);
    setActiveInactive(false);
    setPageSearch(true);
  };

  if (categories) {
    return (
      <div
        className={`content-container relatives bg-gray-100 dark:bg-gray-700 overflow-y-scroll scrollbar-hide ${
          props.Sidebar
            ? "w-full sm:content md:content lg:content xl:content"
            : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
        }`}
      >
        <div className="flex-col items-center w-full">
          <PageHeading
            pageHeading={"Categories"}
            searchLabel={"Title"}
            pageSearch={pageSearch}
            activeInactive={activeInactive}
            handleActive={handleActive}
            handleInActive={handleInActive}
            deleteBtn={true}
            handleDelete={handleDelete}
            search={search}
            setSearch={setSearch}
            handleSearch={handleSearch}
            handleReset={handleReset}
            handleClose={handleClose}
            path="/admin/categories/addCategory"
          />
          <CategoriesTable
            categories={currentCategories}
            categoryPerPage={categoryPerPage}
            totalCategories={categories.length}
            paginate={paginate}
            currentPage={currentPage}
            setNumberOfEvent={setNumberOfEvent}
            selectAll={selectAll}
            selectSingle={selectSingle}
            sortTitle={sortTitle}
            sortByTitle={sortByTitle}
            setSortByTitle={setSortByTitle}
            sortSlug={sortSlug}
            sortBySlug={sortBySlug}
            setSortBySlug={setSortBySlug}
            selectAllCheckbox={selectAllCheckbox}
            setSelectAllCheckbox={setSelectAllCheckbox}
          />
        </div>
      </div>
    );
  }
};
export default Categories;
