export const CategoriesData = [
    {
        id: 1,
        title: "server",
        slug: "blog development",
        status: "1",
    },
    {
        id: 2,
        title: "visual designer",
        slug: "pk-new",
        status: "0",
    },
    {
        id: 3,
        title: "seo",
        slug: "qa-category-1",
        status: "1",
    },
    {
        id: 4,
        title: "business",
        slug: "snapchat",
        status: "1",
    },
    {
        id: 5,
        title: "technology",
        slug: "test-category",
        status: "1",
    },
    {
        id: 6,
        title: "upcoming blogs",
        slug: "upcoming-blogs",
        status: "1",
    },
];
