/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import PageHeading from "../pages/PageHeading";
import { CgAsterisk } from "react-icons/cg";
import TinyMCE from "react-tinymce";
import Dropzone from "react-dropzone";
import axios from "axios";
import { useHistory } from "react-router-dom";
const AddCategories = (props) => {
  let history = useHistory();
  const [title, setTitle] = useState("");
  const [status, setStatus] = useState("1");
  const [imageAltText, setImageAltText] = useState("");
  const [image, setImage] = useState("");
  const [pageContent, setPageContent] = useState("");
  const [metaTitle, setMetaTitle] = useState("");
  const [metaDescription, setMetaDescription] = useState("");

  const [titleError, setTitleError] = useState("");

  const onImageDrop = (acceptedFiles) => {
    if (acceptedFiles.length > 0) {
      const uploads = acceptedFiles.map((image) => {
        const formData = new FormData();
        formData.append("file", image);
        formData.append("tags", "{TAGS}"); // Add tags for the images - {Array}
        formData.append("upload_preset", "kirfukv0"); // Replace the preset name with your own
        formData.append("api_key", "458894391224168"); // Replace API key with your own Cloudinary API key
        formData.append("timestamp", (Date.now() / 1000) | 0);

        return axios
          .post(
            "https://api.cloudinary.com/v1_1/dev-digital/image/upload",
            formData,
            { headers: { "X-Requested-With": "XMLHttpRequest" } }
          )
          .then((response) => {
            setImage(response.data.url);
            console.log(response.data.url);
          });
      });
    }
  };

  const createPage = () => {
    let titleError = "";

    if (title.trim() === "") {
      titleError = "Enter title";
    }
    if (titleError !== "") {
      setTitleError(titleError);
      return;
    }
    let newCategory = {
      title: title,
      status: status,
      imageAltText: imageAltText,
      image: image,
      pageContent: pageContent,
      metaTitle: metaTitle,
      metaDescription: metaDescription,
    };
    console.log("newPage", newCategory);
  };
  const handleCancelBtn = () => {
    history.goBack();
  };
  useEffect(() => {
    if (history.location.category) {
      let category = history.location.category;
      setTitle(category.title);
      setStatus(category.status);
    }
  });
  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <PageHeading
          pageHeading="Add Category"
          showSaveOptionsBtn={true}
          cancel={true}
          handleCancelBtn={handleCancelBtn}
          save={true}
          saveAndContinue={true}
          handleSave={createPage}
        />
        <div className="flex flex-col items-center w-full px-4 pb-10 sm:px-10 md:px-10 lg:px-10 xl:px-10">
          <div className="w-full mt-5">
            <h1 className="text-2xl">Page Content</h1>
          </div>
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label className="mb-1 font-medium">
                Title
                <CgAsterisk className="inline text-red-500" />
              </label>
              <input
                value={title}
                onChange={(e) => {
                  setTitleError("");
                  setTitle(e.target.value);
                }}
                type="text"
                className={`${
                  titleError ? "border-red-500" : "border"
                } h-10 rounded px-2 text-sm font-medium `}
                placeholder="Enter title"
              />
              {titleError && (
                <span className="text-xs text-red-500">{titleError}</span>
              )}
            </div>
            <div className="flex flex-col">
              <label
                for="status"
                className="flex items-center mb-1 font-medium"
              >
                Status
              </label>
              <select
                value={status}
                onChange={(e) => {
                  setStatus(e.target.value);
                }}
                className="h-10 px-2 text-sm font-medium border rounded"
              >
                <option>Select status</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
              </select>
            </div>
          </div>
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label className="mb-1 font-medium">Image Alt</label>
              <input
                value={imageAltText}
                onChange={(e) => {
                  setImageAltText(e.target.value);
                }}
                type="text"
                className="h-10 px-2 text-sm font-medium border rounded "
              />
            </div>
          </div>
          <div className="w-full mt-5">
            <div>
              <label className="mb-1 font-medium">Image</label>
              <div className="relative flex items-center justify-center w-full h-56 mt-2 border-2 border-gray-700 border-dashed hover:bg-black hover:bg-opacity-40">
                {image && (
                  <img
                    src={image}
                    alt="img"
                    className="object-contain w-full h-4/5"
                  />
                )}
                {image && (
                  <button
                    onClick={() => {
                      setImage("");
                    }}
                    className="absolute z-20 text-white bg-gray-900 top-1 right-1 btn"
                  >
                    Remove
                  </button>
                )}
                {!image && (
                  <Dropzone
                    accept="image/*"
                    onDrop={(acceptedFiles) => {
                      onImageDrop(acceptedFiles);
                    }}
                  >
                    {({ getRootProps, getInputProps }) => (
                      <div
                        {...getRootProps()}
                        className="flex items-center justify-center w-full h-full"
                      >
                        <input {...getInputProps()} />
                        <p>
                          Drag 'n' drop some files here, or click to select
                          files
                        </p>
                      </div>
                    )}
                  </Dropzone>
                )}
              </div>
            </div>
            <div className="mt-1 text-xs">
              Max size 5(MB) and Recommended Size: 1900PX x 1080PX (Allowed only
              jpg, jpeg, png and gif images)
            </div>
          </div>
          <div className="w-full my-5">
            <label for="status" className="flex items-center mb-1 font-medium">
              Description
            </label>

            <div className="w-full py-5">
              <TinyMCE
                content={pageContent}
                // content={this.state.content}
                config={{
                  plugins: "autolink link image lists print preview",
                  toolbar:
                    "undo redo | bold italic | alignleft aligncenter alignright",
                  menubar: "edit | insert | view | format",
                  height: 300,
                }}
                onChange={(e) => {
                  setPageContent(e.target.getContent());
                  console.log(pageContent);
                }}
              />
            </div>
          </div>
          <div className="flex flex-col w-full mb-5">
            <div className="w-full">
              <h1 className="text-2xl font-medium">SEO</h1>
            </div>
            <div className="flex flex-col w-full mt-2 lg:flex-row lg:space-x-2">
              <div className="flex flex-col w-full">
                <label className="mb-1 font-medium">Meta Title</label>
                <input
                  value={metaTitle}
                  type="text"
                  onChange={(e) => {
                    setMetaTitle(e.target.value);
                  }}
                  className="h-10 px-2 text-sm font-medium border rounded"
                />
              </div>
              <div className="flex flex-col w-full">
                <label className="mb-1 font-medium">Meta Description</label>
                <textarea
                  value={metaDescription}
                  onChange={(e) => {
                    setMetaDescription(e.target.value);
                  }}
                  className="h-10 px-2 text-sm font-medium border rounded"
                ></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddCategories;
