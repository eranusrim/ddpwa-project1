/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import { CgAsterisk } from "react-icons/cg";
import { useHistory } from "react-router-dom";
import Auth from "./Auth";
import axios from "axios";
import FormData from "form-data";

const Login = ({ setLogin }) => {
  const history = useHistory();
  const [email, setEmail] = useState(
    localStorage.getItem("userEmail") ? localStorage.getItem("userEmail") : ""
  );
  const [password, setPassword] = useState("");
  const [rememberMe, setRememberMe] = useState(false);

  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const data = new FormData();
  data.append("email", email);
  data.append("password", password);
  data.append("X-localization", "es");

  // var config = {
  //     method: "post",
  //     url: "http://laravelcms.devdigdev.com/api/v1/login",
  //     headers: {
  //         "Content-Type": "application/json",
  //     },
  //     data: data,
  // };

  const handleLogin = (e) => {
    e.preventDefault();
    let emailError = "";
    let passwordError = "";

    if (email.trim() === "") {
      emailError = "The email field is required.";
    }
    if (password.trim() === "") {
      passwordError = "The password field is required.";
    }
    if (emailError !== "" || passwordError !== "") {
      setEmailError(emailError);
      setPasswordError(passwordError);
      return;
    }

    axios
      .post("http://localhost:5000/users/login", {
        email: email,
        password: password,
      })
      .then(function (response) {
        let userInfo = JSON.stringify(response.data.data);
        localStorage.setItem("userInfo", userInfo);
        let userData = JSON.parse(localStorage.getItem("userInfo"));
        if (userData && userData.user_type_id === 1) {
          console.log("login user_type_id", userData.user_type_id);
          Auth.authenticate();
          setLogin(true);
          history.push("/admin/dashboard");
        } else {
          let access_token = userData.access_token;
          const config = {
            method: "post",
            url: "http://laravelcms.devdigdev.com/api/v1/logout",
            headers: {
              Authorization: `Bearer ${access_token}`,
            },
          };
          axios(config)
            .then(function (response) {
              console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
              console.log(error);
            });
          localStorage.removeItem("userInfo");
          setEmailError("You are not admin user");
        }
      })
      .catch(function (error) {
        console.log(error);
        setEmailError("Email is wrong");
        setPasswordError("Password is wrong");
      });

    if (rememberMe) {
      localStorage.setItem("userEmail", email);
    } else {
      localStorage.removeItem("userEmail");
    }
  };

  return (
    <div className="flex items-center justify-center w-full h-screen">
      <div className="w-11/12 p-5 rounded-lg shadow-2xl lg:w-1/4 md:1/4 sm:w-4/5">
        <div className="flex items-center justify-center w-full h-20">
          <img
            className="object-contain"
            src={process.env.PUBLIC_URL + "/images/header-logo.png"}
          />
        </div>
        <div className="flex items-center justify-center my-4 w--full">
          <h1 className="text-3xl font-normal">Login</h1>
        </div>
        <div className="flex flex-col w-full">
          <label className="flex text-sm font-normal tracking-wider item-center">
            Email <CgAsterisk className="inline-block text-red-500" />
          </label>
          <input
            type="email"
            value={email}
            placeholder="Enter your email address"
            onChange={(e) => {
              setEmail(e.target.value);
              setEmailError("");
            }}
            onBlur={() => {
              if (email.trim() === "") {
                setEmailError("The email field is required.");
              }
            }}
            className={`${
              emailError && "border-red-500"
            } py-2 border-b border-gray-300 focus:outline-none focus:border-blue-500`}
          />
          {emailError && (
            <span className="my-1 text-xs tracking-wider text-red-500">
              {emailError}
            </span>
          )}
        </div>
        <div className="flex flex-col w-full my-4">
          <label className="flex text-sm font-normal tracking-wider item-center">
            Password <CgAsterisk className="inline-block text-red-500" />
          </label>
          <input
            type="password"
            placeholder="Enter your password"
            onChange={(e) => {
              setPassword(e.target.value);
              setPasswordError("");
            }}
            onBlur={() => {
              if (password.trim() === "") {
                setPasswordError("The password field is required.");
              }
            }}
            className={`${
              passwordError && "border-red-500"
            } py-2 border-b border-gray-300 focus:outline-none focus:border-blue-500`}
          />
          {passwordError && (
            <span className="my-1 text-xs tracking-wider text-red-500">
              {passwordError}
            </span>
          )}
        </div>
        <div className="flex items-center w-full">
          <input
            id="rememberMe"
            type="checkbox"
            checked={rememberMe}
            onChange={() => {
              setRememberMe(!rememberMe);
            }}
            className="mr-2"
          />
          <label
            for="rememberMe"
            className="text-sm tracking-wider cursor-pointer"
          >
            Remember Me
          </label>
        </div>
        <div className="flex items-center justify-center w-full my-4">
          <button
            onClick={(e) => handleLogin(e)}
            type="submit"
            className="w-full h-10 text-sm tracking-wider text-white bg-blue-500 rounded shadow-lg hover:bg-blue-600"
          >
            Login
          </button>
        </div>
        <div className="flex items-center justify-center w-full">
          <button
            onClick={() => {
              history.push("/admin/forgetPassword");
            }}
            className="text-xs tracking-wider outline-none  hover:underline"
          >
            Forgot Your Password?
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
