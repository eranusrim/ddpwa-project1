/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import { CgAsterisk } from "react-icons/cg";
import { useHistory } from "react-router-dom";
import axios from "axios";
import FormData from "form-data";

const ForgetPassword = () => {
  const data = new FormData();
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState("");

  const forgetPassword = () => {
    if (email.trim() === "") {
      setEmailError("Enter Your Email");
      return;
    }
    console.log("emill", email);
    data.append("email", email);
    const config = {
      method: "post",
      url: "http://laravelcms.devdigdev.com/api/v1/forgotpassword",
      data: data,
    };
    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
        setEmailError("Invalid Email");
      });
  };
  return (
    <div className="flex items-center justify-center w-full h-screen">
      <div className="p-5 rounded-lg shadow-2xl lg:w-1/4 sm:w-4/5">
        <div className="flex items-center justify-center w-full h-20">
          <img
            className="object-contain"
            src={process.env.PUBLIC_URL + "/images/header-logo.png"}
          />
        </div>
        <div className="flex items-center justify-center mt-4 mb-8 w--full">
          <h1 className="text-3xl font-normal">Forget Password</h1>
        </div>
        <div className="flex flex-col w-full">
          <label className="flex text-sm font-normal tracking-wider item-center">
            Email <CgAsterisk className="inline-block text-red-500" />
          </label>
          <input
            type="email"
            required
            placeholder="Enter your email address"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
              setEmailError("");
            }}
            onBlur={() => {
              if (email.trim() === "") {
                setEmailError("The email field is required.");
              }
            }}
            className={`${
              emailError && "border-red-500"
            } py-2 border-b border-gray-300 focus:outline-none focus:border-blue-500`}
          />
          {emailError && (
            <span className="my-1 text-xs tracking-wider text-red-500">
              {emailError}
            </span>
          )}
        </div>

        <div className="flex items-center justify-center w-full my-4">
          <button
            onClick={() => forgetPassword()}
            type="submit"
            className="w-full h-10 text-sm tracking-wider text-white bg-blue-500 rounded shadow-lg hover:bg-blue-600"
          >
            Send Password Reset Link
          </button>
        </div>
        <div className="flex items-center justify-center w-full">
          <button
            onClick={() => {
              history.push("/admin/login");
            }}
            className="text-xs tracking-wider  hover:underline"
          >
            Back to Login
          </button>
        </div>
      </div>
    </div>
  );
};

export default ForgetPassword;
