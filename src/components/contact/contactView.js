/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";
import PageHeading from "../pages/PageHeading";
import ContactTable from "./contactTable";
import axios from "axios";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

const ContactsView = (props) => {
  const [contacts, setContacts] = useState([]);

  let [currentPage, setCurrentPage] = useState(1);
  let [contactPerPage, setContactPerPage] = useState(10);

  let [search, setSearch] = useState("");
  let [pageSearch, setPageSearch] = useState(true);

  let [sortByName, setSortByName] = useState(true);
  let [sortByEmail, setSortByEmail] = useState(true);
  let [sortByPhone, setSortByPhone] = useState(true);
  let [sortBySubmitted, setSortBySubmitted] = useState(true);

  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const access_token = userInfo.access_token;

  const DataSet = [
    {
      columns: [
        {
          title: "Name",
          style: { font: { sz: "10", bold: true } },
          width: { wpx: 125 },
        }, // width in pixels
        {
          title: "Phone",
          style: { font: { sz: "10", bold: true } },
          width: { wch: 30 },
        }, // width in characters
        {
          title: "Email",
          style: { font: { sz: "10", bold: true } },
          width: { wpx: 100 },
        }, // width in pixels
        {
          title: "Message",
          style: { font: { sz: "10", bold: true } },
          width: { wpx: 125 },
        }, // width in pixels
        {
          title: "Submitted on",
          style: { font: { sz: "10", bold: true } },
          width: { wpx: 100 },
        },
      ],
      data: contacts.map((d) => [
        {
          value: d.name,
          style: { font: { sz: "10" } },
        },
        {
          value: d.phone.toString(),
          style: { font: { sz: "10" } },
        },
        {
          value: d.email,
          style: { font: { sz: "10" } },
        },
        {
          value: d.message,
          style: { font: { sz: "10" } },
        },
        {
          value: d.submittedOn,
          style: { font: { sz: "10" } },
        },
      ]),
    },
  ];

  const fetchContacts = () => {
    let config = {
      method: "get",
      url: "http://localhost:5000/contact",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };
    axios(config)
      .then(function (response) {
        const data = response.data.contacts;
        const sorted = data.sort((a, b) => {
          const isReverse = sortByName === true ? 1 : -1;
          return isReverse * a.name.localeCompare(b.name);
        });
        setContactPerPage(data.length < 10 ? data.length : 10);
        setContacts(sorted);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchContacts();
  }, []);

  const indexOfLastContact = currentPage * contactPerPage;
  const indexOfFirstContact = indexOfLastContact - contactPerPage;
  const currentContacts = contacts.slice(
    indexOfFirstContact,
    indexOfLastContact
  );

  const paginate = (number) => {
    setCurrentPage(number);
  };
  const setNumberOfContact = (number) => {
    setContactPerPage(parseInt(number));
  };
  const handleSearch = () => {
    let searchData = contacts.filter((d) => {
      if (d.name.toLocaleLowerCase().includes(search.toLocaleLowerCase())) {
        return d;
      }
    });
    if (searchData.length === 0) {
      setContacts(contacts);
    } else {
      setContacts(searchData);
      setCurrentPage(1);
    }
  };
  const handleReset = () => {
    setSearch("");
    fetchContacts();
  };
  const handleClose = () => {
    setSearch("");
    fetchContacts();
  };
  const sortNames = (sortByName) => {
    const sorted = contacts.sort((a, b) => {
      const isReverse = sortByName === true ? 1 : -1;
      return isReverse * a.name.trim().localeCompare(b.name.trim());
    });
    setContacts(sorted);
  };
  const sortEmails = (sortByEmail) => {
    const sorted = contacts.sort((a, b) => {
      const isReverse = sortByEmail === true ? 1 : -1;
      return isReverse * a.email.trim().localeCompare(b.email.trim());
    });
    setContacts(sorted);
  };
  const sortPhones = (sortByPhone) => {
    const sorted = contacts.sort((a, b) => {
      return sortByPhone ? a.phone - b.phone : b.phone - a.phone;
    });
    setContacts(sorted);
  };
  const sortSubmitted = (sortBySubmitted) => {
    const sorted = contacts.sort((a, b) => {
      let dateA = new Date(a.submittedOn);
      let dateB = new Date(b.submittedOn);
      return sortBySubmitted ? dateA - dateB : dateB - dateA;
    });
    setContacts(sorted);
  };

  return (
    <div
      className={`content-container relative bg-gray-100 dark:bg-gray-700 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex-col items-center w-full">
        <PageHeading
          pageHeading={"Contact Inquiries"}
          searchLabel={"Title"}
          pageSearch={pageSearch}
          deleteBtn={false}
          search={search}
          setSearch={setSearch}
          handleSearch={handleSearch}
          handleReset={handleReset}
          handleClose={handleClose}
        />
        {contacts.length !== 0 ? (
          <div className="px-4 py-4 sm:px-10 md:px-10 lg:px-10 xl:px-10">
            <ExcelFile
              filename="Contacts Reports"
              element={
                <button type="button" className="bg-blue-500 rounded btn">
                  Export to Excel
                </button>
              }
            >
              <ExcelSheet dataSet={DataSet} name="Contacts Reports" />
            </ExcelFile>
          </div>
        ) : null}
        <ContactTable
          contacts={currentContacts}
          contactPerPage={contactPerPage}
          totalContacts={contacts.length}
          paginate={paginate}
          currentPage={currentPage}
          setNumberOfContact={setNumberOfContact}
          sortNames={sortNames}
          sortByName={sortByName}
          setSortByName={setSortByName}
          sortEmails={sortEmails}
          sortByEmail={sortByEmail}
          setSortByEmail={setSortByEmail}
          sortPhones={sortPhones}
          sortByPhone={sortByPhone}
          setSortByPhone={setSortByPhone}
          sortSubmitted={sortSubmitted}
          sortBySubmitted={sortBySubmitted}
          setSortBySubmitted={setSortBySubmitted}
        />
      </div>
    </div>
  );
};

export default ContactsView;
