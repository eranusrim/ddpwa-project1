/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { RiArrowDownFill, RiArrowUpFill } from "react-icons/ri";
import { useHistory } from "react-router-dom";
import moment from "moment";

const ContactTable = ({
  contacts,
  contactPerPage,
  totalContacts,
  paginate,
  currentPage,
  setNumberOfContact,
  sortNames,
  sortByName,
  setSortByName,
  sortEmails,
  sortByEmail,
  setSortByEmail,
  sortPhones,
  sortByPhone,
  setSortByPhone,
  sortSubmitted,
  sortBySubmitted,
  setSortBySubmitted,
}) => {
  const pageNumbers = [];
  let history = useHistory();
  for (let i = 1; i <= Math.ceil(totalContacts / contactPerPage); i++) {
    pageNumbers.push(i);
  }
  let [sortNameArrows, setSortNameArrows] = useState(false);
  let [sortEmailArrow, setSortEmailArrow] = useState(true);
  let [sortPhoneArrow, setSortPhoneArrow] = useState(true);
  let [sortSubmittedArrow, setSortSubmittedArrow] = useState(true);

  return (
    <div className="px-4 py-2 overflow-x-scroll scrollbar-hide dark:bg-gray-700 sm:px-10 md:px-10 lg:px-10 xl:px-10">
      <table className="hidden w-full border border-b border-gray-200 sm:table md:table lg:table xl:table">
        <thead className="bg-gray-200 dark:bg-gray-300 dark:text-white ">
          <tr className="border">
            <th
              onClick={() => {
                sortNames(sortByName);
                setSortByName(!sortByName);
                setSortNameArrows(false);
                setSortEmailArrow(true);
                setSortPhoneArrow(true);
                setSortSubmittedArrow(true);
              }}
              scope="col"
              className="px-6 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700 whitespace-nowrap"
            >
              <span className="inline-block mr-2 ">Name</span>
              {sortNameArrows && (
                <>
                  <RiArrowUpFill className="inline-block ml-1" />
                  <RiArrowDownFill className="inline-block ml-1" />
                </>
              )}
              {!sortNameArrows && (
                <>
                  {sortByName === true ? (
                    <RiArrowUpFill className="inline-block ml-2" />
                  ) : (
                    <RiArrowDownFill className="inline-block ml-2" />
                  )}
                </>
              )}
            </th>
            <th
              onClick={() => {
                sortEmails(sortByEmail);
                setSortByEmail(!sortByEmail);
                setSortNameArrows(true);
                setSortEmailArrow(false);
                setSortPhoneArrow(true);
                setSortSubmittedArrow(true);
              }}
              scope="col"
              className="px-6 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700 whitespace-nowrap"
            >
              <span className="inline-block mr-2 ">Email</span>
              {sortEmailArrow && (
                <>
                  <RiArrowUpFill className="inline-block ml-1" />
                  <RiArrowDownFill className="inline-block ml-1" />
                </>
              )}
              {!sortEmailArrow && (
                <>
                  {sortByEmail === true ? (
                    <RiArrowUpFill className="inline-block ml-2" />
                  ) : (
                    <RiArrowDownFill className="inline-block ml-2" />
                  )}
                </>
              )}
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700 whitespace-nowrap"
            >
              <span className="inline-block mr-2 ">Message</span>
            </th>
            <th
              onClick={() => {
                sortPhones(sortByPhone);
                setSortByPhone(!sortByPhone);
                setSortNameArrows(true);
                setSortEmailArrow(true);
                setSortPhoneArrow(false);
                setSortSubmittedArrow(true);
              }}
              scope="col"
              className="px-6 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700 whitespace-nowrap"
            >
              <span className="inline-block mr-2 ">Phone</span>
              {sortPhoneArrow && (
                <>
                  <RiArrowUpFill className="inline-block ml-1" />
                  <RiArrowDownFill className="inline-block ml-1" />
                </>
              )}
              {!sortPhoneArrow && (
                <>
                  {sortByPhone === true ? (
                    <RiArrowUpFill className="inline-block ml-2" />
                  ) : (
                    <RiArrowDownFill className="inline-block ml-2" />
                  )}
                </>
              )}
            </th>
            <th
              onClick={() => {
                sortSubmitted(sortBySubmitted);
                setSortBySubmitted(!sortBySubmitted);
                setSortNameArrows(true);
                setSortEmailArrow(true);
                setSortPhoneArrow(true);
                setSortSubmittedArrow(false);
              }}
              scope="col"
              className="px-6 py-3 text-xs font-medium text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700 whitespace-nowrap"
            >
              <span className="inline-block mr-2 ">Submitted On</span>
              {sortSubmittedArrow && (
                <>
                  <RiArrowUpFill className="inline-block ml-1" />
                  <RiArrowDownFill className="inline-block ml-1" />
                </>
              )}
              {!sortSubmittedArrow && (
                <>
                  {sortBySubmitted === true ? (
                    <RiArrowUpFill className="inline-block ml-2" />
                  ) : (
                    <RiArrowDownFill className="inline-block ml-2" />
                  )}
                </>
              )}
            </th>
          </tr>
        </thead>
        <tbody className="bg-white dark:bg-gray-700">
          {contacts &&
            contacts.map((contact) => (
              <tr
                key={contact.id}
                className="border-b dark:hover:bg-gray-500 hover:bg-gray-200 border-b-gray-500"
              >
                <td className="px-6 py-4 text-sm text-gray-600 capitalize dark:text-white">
                  <span className="inline-block pb-2 ">{contact.name}</span>
                </td>
                <td className="px-6 py-4 text-sm text-gray-600 capitalize cursor-pointer dark:text-white">
                  <span className="inline-block pb-2 ">{contact.email}</span>
                </td>
                <td className="px-6 py-4 text-sm text-gray-600 capitalize cursor-pointer dark:text-white">
                  <span className="inline-block pb-2 ">{contact.message}</span>
                </td>
                <td className="px-6 py-4 text-sm text-gray-600 capitalize cursor-pointer dark:text-white">
                  <span className="inline-block pb-2 ">{contact.phone}</span>
                </td>
                <td className="px-6 py-4 text-sm text-gray-600 capitalize cursor-pointer dark:text-white">
                  <span className="inline-block pb-2 ">
                    {contact.submittedOn}
                  </span>
                </td>
              </tr>
            ))}
        </tbody>
      </table>

      <div className="block overflow-hidden border-r sm:hidden md:hidden lg:hidden xl:hidden dark:bg-gray-600">
        <div className="w-full px-5 py-3 bg-gray-200 border-t">
          <label
            onClick={() => {
              sortNames(sortByName);
              setSortByName(!sortByName);
              setSortNameArrows(false);
              setSortEmailArrow(true);
              setSortPhoneArrow(true);
              setSortSubmittedArrow(true);
            }}
            className="px-5 py-3 text-sm font-medium leading-normal"
          >
            Names
            {!sortNameArrows && (
              <>
                {sortByName === true ? (
                  <RiArrowUpFill className="inline-block ml-2" />
                ) : (
                  <RiArrowDownFill className="inline-block ml-2" />
                )}
              </>
            )}
          </label>
        </div>
        {contacts &&
          contacts.map((contact) => (
            <div className="w-full border tab" key={contact.id}>
              <input
                className="absolute opacity-0"
                id={contact.id}
                type="checkbox"
                name="tabs"
              />
              <label
                className="flex items-center justify-between px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                for={contact.id}
              >
                <span className="block w-11/12 px-5">{contact.name}</span>
              </label>
              <div className="w-full overflow-hidden border-t tab-content">
                <div className="p-4">
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">Name:</h1>
                    <h1 className="pl-2 text-left dark:text-white line-clamp-3">
                      {contact.fName}
                    </h1>
                  </div>
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">Email:</h1>
                    <h1 className="pl-2 text-left dark:text-white line-clamp-3">
                      {contact.email}
                    </h1>
                  </div>
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">Message:</h1>
                    <h1 className="pl-2 text-left dark:text-white line-clamp-3">
                      {contact.message}
                    </h1>
                  </div>
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">
                      Submitted on:
                    </h1>
                    <h1 className="pl-2 text-left dark:text-white line-clamp-3">
                      {contact.submittedOn}
                    </h1>
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>

      {/* pagination */}
      <div className="flex flex-col justify-between w-full mt-5 mb-3 sm:flex-row md:flex-row lg:flex-row xl:flex-row">
        <ul className="flex items-center justify-start space-x-2 cursor-pointer">
          {pageNumbers.map((number) => (
            <li
              key={number}
              className={`p-3 text-sm ${
                currentPage === number ? "bg-red-500" : "bg-blue-400"
              }  text-white liTags`}
              onClick={() => paginate(number)}
            >
              {number}
            </li>
          ))}
        </ul>
        <div className="flex items-center mt-3 justify center sm:mt-0 md:mt-0 lg:mt-0 xl:mt-0">
          <span className="pr-2 dark:text-white">Show</span>
          <input
            type="number"
            value={contactPerPage}
            className="w-24 px-1 py-1 border border-black"
            onChange={(e) => setNumberOfContact(e.target.value)}
          />
          <span className="pl-2 dark:text-white">Entries</span>
        </div>
      </div>
    </div>
  );
};
export default ContactTable;
