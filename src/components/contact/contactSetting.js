import React, { Component } from "react";
import PageHeading from "../pages/PageHeading";
import { CgAsterisk } from "react-icons/cg";

class ContactSetting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "Leave a request and our manager will contact you.",
            message: "Thank you for contacting us...!",
            email: "vighnu@gmail.com",
            acknowledgment: "yes",

            titleError: "",
            messageError: "",
            emailError: "",

            contactSettingError: true,
        };
    }
    contactSetting = () => {
        let titleError = "";
        let messageError = "";
        let emailError = "";
        let contactSettingError = false;

        if (this.state.title.trim() === "") {
            titleError = "Enter title";
            contactSettingError = true;
        }
        if (this.state.message.trim() === "") {
            messageError = "Enter message";
            contactSettingError = true;
        }
        if (this.state.email.trim() === "") {
            emailError = "Enter email";
            contactSettingError = true;
        }

        if (contactSettingError === true) {
            this.setState({
                titleError: titleError,
                messageError: messageError,
                emailError: emailError,
                contactSettingError: contactSettingError,
            });
            return;
        }

        let contact = {
            title: this.state.title,
            message: this.state.message,
            email: this.state.email,
            acknowledgment: this.state.acknowledgment,
        };
        console.log("contact", contact);
    };

    render() {
        return (
            <div
                className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
                    this.props.Sidebar
                        ? "w-full sm:content md:content lg:content xl:content"
                        : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
                }`}
            >
                <div className="flex flex-col items-center w-full">
                    <PageHeading
                        pageHeading="Setting"
                        showSaveOptionsBtn={true}
                        save={true}
                        createUser={true}
                        handleSave={this.contactSetting}
                    />
                    <div className="w-full px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 my-4 relative ">
                        <div className="shadow-md w-full">
                            <div className="tab w-full overflow-hidden border-t">
                                <input
                                    className="absolute opacity-0"
                                    id="tab-one"
                                    type="checkbox"
                                    checked={this.state.contactSettingError}
                                    onChange={() =>
                                        this.setState({
                                            contactSettingError:
                                                !this.state.contactSettingError,
                                        })
                                    }
                                    name="tabs"
                                />
                                <label
                                    className="block px-5 py-3 text-sm font-medium leading-normal cursor-pointer bg-blue-400 text-white dark:bg-gray-700 dark:text-white"
                                    for="tab-one"
                                >
                                    Contact setting
                                </label>
                                <div className="tab-content overflow-hidden  border-l-4 border-blue-400 dark:border-red-500  leading-normal lg:grid lg:grid-cols-3 w-full">
                                    <div className="p-3 flex flex-col col-span-3">
                                        <label className="text-sm mb-2 flex items-center">
                                            Title
                                            <CgAsterisk className="inline text-red-500" />
                                        </label>
                                        <textarea
                                            type="number"
                                            value={this.state.title}
                                            onChange={(e) => {
                                                this.setState({
                                                    title: e.target.value,
                                                    titleError: "",
                                                });
                                            }}
                                            className={`${
                                                this.state.titleError
                                                    ? "border-red-500"
                                                    : "border"
                                            } border h-20 rounded px-2 text-sm font-medium `}
                                        ></textarea>
                                        {this.state.titleError && (
                                            <span className="text-red-500 text-xs">
                                                {this.state.titleError}
                                            </span>
                                        )}
                                    </div>
                                    <div className="p-3 flex flex-col col-span-3">
                                        <label className="text-sm mb-2 flex items-center">
                                            Thank You Message
                                            <CgAsterisk className="inline text-red-500" />
                                        </label>
                                        <textarea
                                            type="number"
                                            value={this.state.message}
                                            onChange={(e) => {
                                                this.setState({
                                                    message: e.target.value,
                                                    messageError: "",
                                                });
                                            }}
                                            className={`${
                                                this.state.messageError
                                                    ? "border-red-500"
                                                    : "border"
                                            } border h-20 rounded px-2 text-sm font-medium `}
                                        ></textarea>
                                        {this.state.messageError && (
                                            <span className="text-red-500 text-xs">
                                                {this.state.messageError}
                                            </span>
                                        )}
                                    </div>
                                    <div className="p-3 flex flex-col">
                                        <label className="text-sm mb-2 flex items-center">
                                            Contact email
                                            <CgAsterisk className="inline text-red-500" />
                                        </label>
                                        <input
                                            type="email"
                                            value={this.state.email}
                                            onChange={(e) => {
                                                this.setState({
                                                    email: e.target.value,
                                                    emailError: "",
                                                });
                                            }}
                                            className={`${
                                                this.state.emailError
                                                    ? "border-red-500"
                                                    : "border"
                                            } border h-10 rounded px-2 text-sm font-medium `}
                                        />
                                        {this.state.emailError && (
                                            <span className="text-red-500 text-xs">
                                                {this.state.emailError}
                                            </span>
                                        )}
                                    </div>
                                    <div className="p-3 flex flex-col">
                                        <label className="text-sm mb-2 flex items-center">
                                            Sent acknowledgment to the user?
                                        </label>
                                        <select className="border h-10 rounded px-2 text-sm font-medium ">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default ContactSetting;
