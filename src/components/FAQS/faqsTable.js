import React, { useState } from "react";
import { RiArrowDownFill, RiArrowUpFill } from "react-icons/ri";
import { useHistory } from "react-router-dom";

const FaqsTable = ({
    faqs,
    faqsPerPage,
    totalFaqs,
    paginate,
    currentPage,
    setNumberOfFaqs,
    selectAll,
    selectSingle,
    sortFaqs,
    sortByQuestion,
    setSortByQuestion,
    selectAllCheckbox,
    setSelectAllCheckbox,
}) => {
    const pageNumbers = [];
    let history = useHistory();
    for (let i = 1; i <= Math.ceil(totalFaqs / faqsPerPage); i++) {
        pageNumbers.push(i);
    }
    let [sortQuestionArrows, setSortQuestionArrows] = useState(false);

    return (
        <div className="overflow-x-scroll scrollbar-hide  dark:bg-gray-700 py-10 px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10">
            <table className="w-full  border-r hidden sm:table md:table lg:table xl:table border-gray-200 border-b">
                <thead className="bg-gray-200 dark:bg-gray-300 dark:text-white ">
                    <tr className="border-l-8 border-gray-400">
                        <th
                            scope="col"
                            className="dark:text-gray-700 px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            <input
                                checked={selectAllCheckbox}
                                onChange={(e) => {
                                    setSelectAllCheckbox(!selectAllCheckbox);
                                    selectAll(e);
                                }}
                                type="checkbox"
                            />
                        </th>
                        <th
                            onClick={() => {
                                sortFaqs(sortByQuestion);
                                setSortByQuestion(!sortByQuestion);
                            }}
                            scope="col"
                            className="dark:text-gray-700 cursor-pointer w-full px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            <span className="inline-block mr-2 ">Question</span>
                            {sortQuestionArrows && (
                                <>
                                    <RiArrowUpFill className="inline-block ml-1" />
                                    <RiArrowDownFill className="inline-block ml-1" />
                                </>
                            )}
                            {!sortQuestionArrows && (
                                <>
                                    {sortByQuestion === true ? (
                                        <RiArrowUpFill className="inline-block ml-2" />
                                    ) : (
                                        <RiArrowDownFill className="inline-block ml-2" />
                                    )}
                                </>
                            )}
                        </th>

                        <th
                            scope="col"
                            className="dark:text-gray-700  px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase whitespace-nowrap"
                        >
                            Edit
                        </th>
                    </tr>
                </thead>
                <tbody className="bg-white dark:bg-gray-700">
                    {faqs &&
                        faqs.map((faq) => (
                            <tr
                                key={faq.id}
                                className={`${
                                    faq.status
                                        ? "border-left-green-8 "
                                        : "border-left-red-8"
                                } dark:hover:bg-gray-500 hover:bg-gray-200  border-b-gray-500 border-b`}
                            >
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500 dark:text-white">
                                    <input
                                        checked={faq.select}
                                        onChange={(e) =>
                                            selectSingle(e, faq.id)
                                        }
                                        type="checkbox"
                                    />
                                </td>
                                <td className="px-6 py-4 cursor-pointer  text-sm capitalize text-gray-600 dark:text-white">
                                    <span className=" pb-2 inline-block">
                                        {faq.question}
                                    </span>
                                </td>
                                <td className=" py-4 whitespace-nowrap text-center text-sm font-medium">
                                    <h1
                                        onClick={() => {
                                            history.push(
                                                "/admin/faqs/" + faq.id
                                            );
                                        }}
                                        className="text-indigo-600 hover:text-indigo-900 cursor-pointer dark:text-blue-400 dark:hover:text-blue-500"
                                    >
                                        Edit
                                    </h1>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </table>

            <div className="overflow-hidden  block sm:hidden md:hidden lg:hidden xl:hidden border-r dark:bg-gray-600">
                <div className="bg-gray-200 px-5 py-3 w-full border-t">
                    <input
                        className="ml-3"
                        checked={selectAllCheckbox}
                        onChange={(e) => {
                            setSelectAllCheckbox(!selectAllCheckbox);
                            selectAll(e);
                        }}
                        type="checkbox"
                    />
                    <label
                        onClick={() => {
                            sortFaqs(sortByQuestion);
                            setSortByQuestion(!sortByQuestion);
                            setSortQuestionArrows(false);
                        }}
                        className="px-5 py-3 text-sm font-medium leading-normal"
                    >
                        Questions{" "}
                        {!sortQuestionArrows && (
                            <>
                                {sortByQuestion === true ? (
                                    <RiArrowUpFill className="inline-block ml-2" />
                                ) : (
                                    <RiArrowDownFill className="inline-block ml-2" />
                                )}
                            </>
                        )}
                    </label>
                </div>
                {faqs &&
                    faqs.map((faq) => (
                        <div
                            className={`tab w-full border-t ${
                                faq.status
                                    ? "border-left-green-8 "
                                    : "border-left-red-8"
                            }`}
                            key={faq.id}
                        >
                            <input
                                className="absolute opacity-0"
                                id={faq.id}
                                type="checkbox"
                                name="tabs"
                            />
                            <label
                                className="flex items-center justify-between px-5 py-3 text-sm font-medium leading-normal cursor-pointer bg-blue-400 text-white dark:bg-gray-700 dark:text-white"
                                for={faq.id}
                            >
                                <input
                                    checked={faq.select}
                                    onChange={(e) => selectSingle(e, faq.id)}
                                    type="checkbox"
                                />
                                <span className="px-5 block w-11/12">
                                    {faq.question}
                                </span>
                            </label>
                            <div className="tab-content border-t overflow-hidden w-full">
                                <div className="p-4">
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            answer:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2 line-clamp-3">
                                            {faq.answer}
                                        </h1>
                                    </div>
                                    <div className="flex py-1">
                                        <h1 className="dark:text-white font-semibold">
                                            Edit:
                                        </h1>
                                        <h1 className="dark:text-white text-left pl-2">
                                            <h1
                                                onClick={() => {
                                                    history.push(
                                                        "/admin/faqs/" + faq.id
                                                    );
                                                }}
                                                className="cursor-pointer underline text-blue-400 hover:text-blue-500"
                                            >
                                                Edit
                                            </h1>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
            </div>

            {/* pagination */}
            <div className="w-full flex flex-col sm:flex-row md:flex-row lg:flex-row xl:flex-row justify-between mt-5 mb-3">
                <ul className="flex items-center justify-start cursor-pointer space-x-2">
                    {pageNumbers.map((number) => (
                        <li
                            key={number}
                            className={`p-3 text-sm ${
                                currentPage === number
                                    ? "bg-red-500"
                                    : "bg-blue-400"
                            }  text-white liTags`}
                            onClick={() => paginate(number)}
                        >
                            {number}
                        </li>
                    ))}
                </ul>
                <div className="flex items-center justify center mt-3 sm:mt-0 md:mt-0 lg:mt-0 xl:mt-0">
                    <span className="pr-2 dark:text-white">Show</span>
                    <input
                        type="number"
                        value={faqsPerPage}
                        className="px-1 py-1 w-24 border border-black"
                        onChange={(e) => setNumberOfFaqs(e.target.value)}
                    />
                    <span className="pl-2 dark:text-white">Entries</span>
                </div>
            </div>
        </div>
    );
};
export default FaqsTable;
