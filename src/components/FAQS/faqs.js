/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import PageHeading from "../pages/PageHeading";
import FaqsTable from "./faqsTable";
import axios from "axios";
import { notify } from "../../utility";

const Faqs = (props) => {
  const [faqs, setFaqs] = useState([]);

  let [currentPage, setCurrentPage] = useState(1);
  let [faqsPerPage, setFaqsPerPage] = useState(10);

  let [selectAllCheckbox, setSelectAllCheckbox] = useState(false);

  let [search, setSearch] = useState("");
  let [pageSearch, setPageSearch] = useState(true);
  let [activeInactive, setActiveInactive] = useState(false);

  let [sortByQuestion, setSortByQuestion] = useState(true);

  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const access_token = userInfo.access_token;

  const fetchFaqs = () => {
    let config = {
      method: "get",
      url: "http://localhost:5000/faqs",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };
    axios(config)
      .then(function (response) {
        const data = response.data.faqs;
        const sorted = data.sort((a, b) => {
          const isReverse = sortByQuestion === true ? 1 : -1;
          return isReverse * a.question.localeCompare(b.question);
        });
        setFaqsPerPage(data.length < 10 ? data.length : 10);
        setFaqs(
          sorted.map((faq) => {
            return {
              select: false,
              id: faq.id,
              question: faq.question,
              answer: faq.answer,
              status: faq.status,
            };
          })
        );
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchFaqs();
  }, []);

  const indexOfLastQuestion = currentPage * faqsPerPage;
  const indexOfFirstQuestion = indexOfLastQuestion - faqsPerPage;
  const currentFaqs = faqs.slice(indexOfFirstQuestion, indexOfLastQuestion);

  const paginate = (number) => {
    setCurrentPage(number);
  };
  const setNumberOfFaqs = (number) => {
    setFaqsPerPage(parseInt(number));
  };
  const selectAll = (e) => {
    let checked = e.target.checked;
    if (checked) {
      setPageSearch(false);
      setActiveInactive(true);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
    setFaqs(
      faqs.map((d) => {
        d.select = checked;
        return d;
      })
    );
  };
  const selectSingle = (e, id) => {
    let checked = e.target.checked;
    setFaqs(
      faqs.map((faq) => {
        if (id === faq.id) {
          faq.select = checked;
        }
        return faq;
      })
    );

    const result = faqs.some(function (data) {
      return data.select === true;
    });
    if (result) {
      setActiveInactive(true);
      setPageSearch(false);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
  };
  const handleSearch = () => {
    let searchData = faqs.filter((d) => {
      if (d.question.toLocaleLowerCase().includes(search.toLocaleLowerCase())) {
        return d;
      }
    });
    if (searchData.length === 0) {
      setFaqs(faqs);
    } else {
      setFaqs(searchData);
      setCurrentPage(1);
    }
  };
  const handleReset = () => {
    setSearch("");
    fetchFaqs();
  };
  const handleClose = () => {
    setSearch("");
    fetchFaqs();
  };
  const sortFaqs = (sortByQuestion) => {
    const sorted = faqs.sort((a, b) => {
      const isReverse = sortByQuestion === true ? 1 : -1;
      return isReverse * a.question.trim().localeCompare(b.question.trim());
    });
    setFaqs(sorted);
  };
  const handleDelete = () => {
    let a = window.confirm("Are you sure you want to delete this");
    if (a) {
      let arrayId = [];
      faqs.forEach((d) => {
        if (d.select) {
          arrayId.push(d.id);
        }
      });
      axios
        .delete("http://localhost:5000/faqs/deleteFaqs", {
          data: {
            faqs_ids: arrayId,
          },
        })
        .then((response) => {
          notify(response.data.message);
          setActiveInactive(!activeInactive);
          setPageSearch(!pageSearch);
          setSelectAllCheckbox(false);
          fetchFaqs();
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  };
  const handleActive = () => {
    let arrayId = [];
    faqs.forEach((d) => {
      if (d.select) {
        arrayId.push(d.id);
      }
    });
    const data = {
      status: 1,
      faqs_ids: arrayId,
    };
    axios
      .put("http://localhost:5000/faqs/activeInactiveFaqs", data)
      .then((response) => {
        notify(response.data.message);
        setSelectAllCheckbox(false);
        setActiveInactive(false);
        setPageSearch(true);
        fetchFaqs();
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  const handleInActive = () => {
    let arrayId = [];
    faqs.forEach((d) => {
      if (d.select) {
        arrayId.push(d.id);
      }
    });
    const data = {
      status: 0,
      faqs_ids: arrayId,
    };
    axios
      .put("http://localhost:5000/faqs/activeInactiveFaqs", data)
      .then((response) => {
        notify(response.data.message);
        setSelectAllCheckbox(false);
        setActiveInactive(false);
        setPageSearch(true);
        fetchFaqs();
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  return (
    <div
      className={`content-container relative bg-gray-100 dark:bg-gray-700 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex-col items-center w-full">
        <PageHeading
          pageHeading={"Faqs"}
          searchLabel={"Title"}
          pageSearch={pageSearch}
          activeInactive={activeInactive}
          handleActive={handleActive}
          handleInActive={handleInActive}
          deleteBtn={true}
          handleDelete={handleDelete}
          search={search}
          setSearch={setSearch}
          handleSearch={handleSearch}
          handleReset={handleReset}
          handleClose={handleClose}
          path="/admin/faqs/addFaq"
        />
        <FaqsTable
          faqs={currentFaqs}
          faqsPerPage={faqsPerPage}
          totalFaqs={faqs.length}
          paginate={paginate}
          currentPage={currentPage}
          setNumberOfFaqs={setNumberOfFaqs}
          selectAll={selectAll}
          selectSingle={selectSingle}
          sortFaqs={sortFaqs}
          sortByQuestion={sortByQuestion}
          setSortByQuestion={setSortByQuestion}
          selectAllCheckbox={selectAllCheckbox}
          setSelectAllCheckbox={setSelectAllCheckbox}
        />
      </div>
    </div>
  );
};

export default Faqs;
