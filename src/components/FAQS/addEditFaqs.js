/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import PageHeading from "../pages/PageHeading";
import { CgAsterisk } from "react-icons/cg";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { notify } from "../../utility";

const AddEditFaqs = (props) => {
  let history = useHistory();
  const [question, setQuestion] = useState("");
  const [answer, setAnswer] = useState("");
  const [status, setStatus] = useState("1");

  const [questionError, setQuestionError] = useState("");
  const [answerError, setAnswerError] = useState("");
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const access_token = userInfo.access_token;

  const handleAddEdit = () => {
    let questionError = "";
    let answerError = "";

    if (question.trim() === "") {
      questionError = "Enter Question";
    }
    if (answer.trim() === "") {
      answerError = "Enter Answer";
    }
    if (questionError !== "" || answerError !== "") {
      setQuestionError(questionError);
      setAnswerError(answerError);
      return;
    }

    if (props.match.params.id) {
      let editFaq = {
        question: question,
        answer: answer,
        status: status,
        id: props.match.params.id,
      };
      let config = {
        method: "put",
        url: "http://localhost:5000/faqs/editFaqs",
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
        data: editFaq,
      };
      axios(config)
        .then((response) => {
          notify(response.data.message);
          history.push("/admin/faqs");
        })
        .catch((error) => {
          console.log("error", error);
        });
    } else {
      let faq = {
        question: question,
        answer: answer,
        status: status,
      };
      let config = {
        method: "post",
        url: "http://localhost:5000/faqs",
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
        data: faq,
      };
      axios(config)
        .then((response) => {
          notify(response.data.message);
          history.push("/admin/faqs");
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  };

  const fetchFaq = (id) => {
    let config = {
      method: "get",
      url: "http://localhost:5000/faqs/" + id,
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    };
    axios(config)
      .then((response) => {
        const data = response.data.Faqs;
        setQuestion(data.question);
        setAnswer(data.answer);
        setStatus(data.status);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const handleCancelBtn = () => {
    history.goBack();
  };

  useEffect(() => {
    const id = props.match.params.id;
    if (id) {
      fetchFaq(id);
    }
  }, []);

  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <PageHeading
          pageHeading={props.match.params.id ? "Edit FAQS" : "Add FAQS"}
          showSaveOptionsBtn={true}
          cancel={true}
          handleCancelBtn={handleCancelBtn}
          save={true}
          saveAndContinue={true}
          handleSave={handleAddEdit}
        />
        <div className="flex flex-col items-center w-full px-4 pb-10 sm:px-10 md:px-10 lg:px-10 xl:px-10">
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label className="mb-1 font-medium">
                Question
                <CgAsterisk className="inline text-red-500" />
              </label>
              <input
                value={question}
                onChange={(e) => {
                  setQuestionError("");
                  setQuestion(e.target.value);
                }}
                type="text"
                className={`${
                  questionError ? "border-red-500" : "border"
                } h-10 rounded px-2 text-sm font-medium `}
                placeholder="Enter title"
              />
              {questionError && (
                <span className="text-xs text-red-500">{questionError}</span>
              )}
            </div>
            <div className="flex flex-col">
              <label className="mb-1 font-medium">Status</label>
              <select
                className="h-10 px-2 text-sm font-medium border rounded"
                value={status}
                onChange={(e) => {
                  setStatus(e.target.value);
                }}
              >
                <option value="1">Active</option>
                <option value="0">Inactive</option>
              </select>
            </div>
          </div>
          <div className="w-full my-5">
            <label className="flex items-center mb-1 font-medium">
              Answer
              <CgAsterisk className="inline text-red-500" />
            </label>
            <div className="w-full py-5">
              <textarea
                className="w-full h-56 p-2 border rounded"
                value={answer}
                onChange={(e) => {
                  setAnswer(e.target.value);
                  setAnswerError("");
                }}
              />
              {answerError && (
                <span className="text-xs text-red-500">{answerError}</span>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default AddEditFaqs;
