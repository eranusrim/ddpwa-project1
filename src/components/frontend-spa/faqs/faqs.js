import React, { useState, useEffect } from "react";
import axios from "axios";

const Faqs = () => {
  const [faqs, setFaqs] = useState([]);
  const [id, setId] = useState("");

  const fetchFaqs = () => {
    axios
      .get("http://localhost:5000/faqs")
      .then((response) => {
        const faqs = response.data.faqs;
        setFaqs(faqs);
        setId(faqs[0].id);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchFaqs();
  }, []);
  return (
    <div className="w-full h-screen px-2 pt-16 pb-6 mt-0 dark:bg-gray-500 sm:mt-6 sm:px-10 md:px-10 lg:px-10 xl:px-10">
      <h1 className="w-full my-4 text-2xl font-medium text-center capitalize dark:text-white">
        FAQS
      </h1>
      {faqs &&
        faqs.length > 0 &&
        faqs.map((faq) => (
          <div className="w-full border">
            <input
              className="absolute opacity-0"
              id={faq.id}
              type="checkbox"
              onChange={(e) => {
                setId(e.target.id);
                if (id === faq.id) {
                  setId("");
                }
              }}
            />
            <label
              className="flex items-center justify-between px-5 py-5 font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
              for={faq.id}
            >
              <h1 className="capitalize">{faq.question}</h1>
              <h1 className="flex items-center justify-center w-6 h-6 text-white bg-gray-800 border rounded-full">
                {id === faq.id ? "-" : "+"}
              </h1>
            </label>
            <div
              className={`${
                id === faq.id ? "flex" : " hidden"
              }   border-l-4 border-blue-400 dark:border-red-500 w-full px-5 py-4 font-medium dark:bg-gray-400 dark:text-white`}
            >
              {faq.answer}
            </div>
          </div>
        ))}
    </div>
  );
};

export default Faqs;
