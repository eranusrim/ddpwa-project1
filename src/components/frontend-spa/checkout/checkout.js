/* eslint-disable array-callback-return */
import React, { Component } from "react";
import { CgAsterisk } from "react-icons/cg";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import ReactPhoneInput from "react-phone-input-2";
import axios from "axios";
import { notify } from "../../../utility";

class Checkout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fName: "",
      lName: "",
      email: "",
      address: "",
      pincode: null,
      city: "",
      state: "",
      country: "",
      phone: null,
      user_type: 0,
      addressId: "",
      user: [],
      userData: {},
      addressPop: false,

      fNameError: "",
      lNameError: "",
      emailError: "",
      addressError: "",
      pincodeError: "",
      cityError: "",
      stateError: "",
      countryError: "",
      phoneError: "",
    };
  }

  handleProcessed = () => {
    if (this.state.user_type === 0) {
      let fNameError = "";
      let lNameError = "";
      let emailError = "";
      let addressError = "";
      let pincodeError = "";
      let cityError = "";
      let stateError = "";
      let countryError = "";
      let phoneError = "";

      if (this.state.fName === "") {
        fNameError = "Enter first name";
      }
      if (this.state.lName === "") {
        lNameError = "Enter last name";
      }
      if (this.state.email === "") {
        emailError = "Enter email";
      }
      if (this.state.address === "") {
        addressError = "Enter address";
      }
      if (this.state.pincode === null) {
        pincodeError = "Enter pincode";
      }
      if (this.state.city === "") {
        cityError = "Enter city name";
      }
      if (this.state.state === "") {
        stateError = "Enter state name";
      }
      if (this.state.country === "") {
        countryError = "Enter country name";
      }
      if (this.state.phone === null) {
        phoneError = "Enter contact no";
      }

      if (
        fNameError !== "" ||
        lNameError !== "" ||
        emailError !== "" ||
        addressError !== "" ||
        pincodeError !== "" ||
        cityError !== "" ||
        stateError !== "" ||
        countryError !== "" ||
        phoneError !== ""
      ) {
        this.setState({
          fNameError: fNameError,
          lNameError: lNameError,
          emailError: emailError,
          addressError: addressError,
          pincodeError: pincodeError,
          cityError: cityError,
          stateError: stateError,
          countryError: countryError,
          phoneError: phoneError,
        });
        return;
      }
      let payingAmount = this.props.cartItems
        .reduce((a, c) => a + c.price * c.count, 0)
        .toFixed(2);
      const orderInfo = {
        fName: this.state.fName,
        lName: this.state.lName,
        email: this.state.email,
        address: this.state.address,
        pincode: this.state.pincode,
        city: this.state.city,
        state: this.state.state,
        country: this.state.country,
        payingAmount: payingAmount,
        phone: this.state.phone,
        user_type: this.state.user_type,
      };
      this.props.history.push({
        pathname: "/payment",
        state: { orderInfo: orderInfo },
      });
    } else {
      if (this.state.addressId === "") {
        alert("Please select address");
      } else {
        let payingAmount = this.props.cartItems
          .reduce((a, c) => a + c.price * c.count, 0)
          .toFixed(2);

        const address = this.state.user[0].address;
        const a = [];
        address.map((add) => {
          if (add.id === this.state.addressId) {
            a.push(add);
          }
        });
        const orderInfo = {
          fName: this.state.user[0].first_name,
          lName: this.state.user[0].last_name,
          email: this.state.user[0].email,
          address: a[0].address,
          pincode: a[0].zipcode,
          city: a[0].city,
          state: a[0].state,
          country: a[0].country,
          payingAmount: payingAmount,
          phone: 789654123,
          user_type: this.state.user_type,
        };
        this.props.history.push({
          pathname: "/payment",
          state: { orderInfo: orderInfo },
        });
      }
    }
  };
  fetchUser = (id) => {
    axios
      .get("http://localhost:5000/users/" + id)
      .then((response) => {
        console.log("response", response);
        this.setState({ user: response.data.user });
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  addAddress = () => {
    let addressError = "";
    let pincodeError = "";
    let cityError = "";
    let stateError = "";
    let countryError = "";

    if (this.state.address === "") {
      addressError = "Enter address";
    }
    if (this.state.pincode === null) {
      pincodeError = "Enter pincode";
    }
    if (this.state.city === "") {
      cityError = "Enter city name";
    }
    if (this.state.state === "") {
      stateError = "Enter state name";
    }
    if (this.state.country === "") {
      countryError = "Enter country name";
    }

    if (
      addressError !== "" ||
      pincodeError !== "" ||
      cityError !== "" ||
      stateError !== "" ||
      countryError !== ""
    ) {
      this.setState({
        addressError: addressError,
        pincodeError: pincodeError,
        cityError: cityError,
        stateError: stateError,
        countryError: countryError,
      });
      return;
    }
    const address = {
      address: this.state.address,
      zipcode: this.state.pincode,
      city: this.state.city,
      state: this.state.state,
      country: this.state.country,
      user_id: this.state.userData.id,
    };
    let config = {
      method: "post",
      url: "http://localhost:5000/users/addUserAddress",
      data: address,
    };
    axios(config)
      .then((response) => {
        const data = response.data;
        notify(data.message);
        this.fetchUser(this.state.userData.id);
        this.setState({
          addressPop: false,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  removeAddress = (id, user_id) => {
    const config = {
      method: "delete",
      url: "http://localhost:5000/users/deleteAddress",
      // headers: {
      //     Authorization: `Bearer ${access_token}`,
      // },
      data: {
        id: id,
      },
    };
    axios(config)
      .then((response) => {
        notify(response.data.message);
        this.fetchUser(user_id);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  componentDidMount() {
    const userData = JSON.parse(localStorage.getItem("userData"));
    this.setState({ userData: userData });
    if (userData) {
      this.fetchUser(userData.id);
      this.setState({
        user_type: 1,
      });
    }
  }
  render() {
    const {
      fName,
      lName,
      email,
      address,
      pincode,
      city,
      state,
      country,
      phone,
      user,
      addressPop,
      fNameError,
      lNameError,
      emailError,
      addressError,
      pincodeError,
      cityError,
      stateError,
      countryError,
      phoneError,
    } = this.state;
    return (
      <div className="flex items-center justify-center w-full min-h-screen pt-8 pb-10 mt-14 mb-14 sm:mb-0 md:mb-0 lg:mb-0 xl:mb-0 dark:bg-gray-700">
        <div className="flex flex-col items-center w-full px-2 justify-evenly md:px-4 lg:px-10 xl:px-10 md:flex-row md:items-start lg:flex-row lg:items-start xl:flex-row xl:items-start ">
          {this.state.user_type === 0 ? (
            <div className="grid w-11/12 grid-cols-2 gap-4 p-5 bg-gray-100 rounded shadow-lg md:w-5/12 lg:w-5/12 xl:w-5/12 dark:bg-gray-300">
              <div className="flex items-center justify-center col-span-2 py-2">
                <h1 className="text-lg font-semibold text-yellow-500">
                  Shipping Details
                </h1>
              </div>
              <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                <label className="text-sm font-semibold text-yellow-500">
                  First Name
                  <CgAsterisk className="inline-block text-red-500" />
                </label>
                <input
                  className="p-2 text-sm font-semibold rounded"
                  type="text"
                  name="fName"
                  value={fName}
                  onChange={(e) => {
                    this.setState({
                      fName: e.target.value,
                      fNameError: "",
                    });
                  }}
                />
                <span className="text-xs text-red-500">{fNameError}</span>
              </div>
              <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                <label className="text-sm font-semibold text-yellow-500">
                  Last Name
                  <CgAsterisk className="inline-block text-red-500" />
                </label>
                <input
                  className="p-2 text-sm font-semibold text-yellow-500 rounded"
                  type="text"
                  name="lName"
                  value={lName}
                  onChange={(e) => {
                    this.setState({
                      lName: e.target.value,
                      lNameError: "",
                    });
                  }}
                />
                <span className="text-xs text-red-500">{lNameError}</span>
              </div>
              <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                <label className="text-sm font-semibold text-yellow-500">
                  Email
                  <CgAsterisk className="inline-block text-red-500" />
                </label>
                <input
                  className="p-2 text-sm font-semibold text-yellow-500 rounded"
                  type="email"
                  name="email"
                  value={email}
                  onChange={(e) => {
                    this.setState({
                      email: e.target.value,
                      emailError: "",
                    });
                  }}
                />
                <span className="text-xs text-red-500">{emailError}</span>
              </div>
              <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                <label className="text-sm font-semibold text-yellow-500">
                  Phone
                </label>
                <ReactPhoneInput
                  country={"in"}
                  inputStyle={{
                    width: "100%",
                    height: "2.5rem",
                    fontSize: "13px",
                    paddingLeft: "48px",
                    borderRadius: "5px",
                    fontWeight: "600",
                    border: "1px solid lightgray",
                  }}
                  value={phone}
                  onChange={(phone) => {
                    this.setState({
                      phone,
                      phoneError: "",
                    });
                  }}
                />
                <span className="text-xs text-red-500">{phoneError}</span>
              </div>
              <div className="flex flex-col col-span-2 space-y-1">
                <label className="text-sm font-semibold text-yellow-500">
                  Address
                  <CgAsterisk className="inline-block text-red-500" />
                </label>
                <textarea
                  name="address"
                  value={address}
                  className="h-20 p-2 text-sm font-semibold rounded"
                  onChange={(e) => {
                    this.setState({
                      address: e.target.value,
                      addressError: "",
                    });
                  }}
                ></textarea>
                <span className="text-xs text-red-500">{addressError}</span>
              </div>
              <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                <label className="text-sm font-semibold text-yellow-500">
                  Pincode
                  <CgAsterisk className="inline-block text-red-500" />
                </label>
                <input
                  className="p-2 text-sm font-semibold text-yellow-500 rounded"
                  type="number"
                  name="pincode"
                  value={pincode}
                  onChange={(e) => {
                    this.setState({
                      pincode: e.target.value,
                      pincodeError: "",
                    });
                  }}
                />

                <span className="text-xs text-red-500">{pincodeError}</span>
              </div>
              <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                <label className="text-sm font-semibold text-yellow-500">
                  City
                  <CgAsterisk className="inline-block text-red-500" />
                </label>
                <input
                  className="p-2 text-sm font-semibold text-yellow-500 rounded"
                  type="text"
                  name="city"
                  value={city}
                  onChange={(e) => {
                    this.setState({
                      city: e.target.value,
                      cityError: "",
                    });
                  }}
                />
                <span className="text-xs text-red-500">{cityError}</span>
              </div>{" "}
              <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                <label className="text-sm font-semibold text-yellow-500">
                  State
                  <CgAsterisk className="inline-block text-red-500" />
                </label>
                <input
                  className="p-2 text-sm font-semibold text-yellow-500 rounded"
                  type="text"
                  name="state"
                  value={state}
                  onChange={(e) => {
                    this.setState({
                      state: e.target.value,
                      stateError: "",
                    });
                  }}
                />
                <span className="text-xs text-red-500">{stateError}</span>
              </div>
              <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                <label className="text-sm font-semibold text-yellow-500">
                  Country
                  <CgAsterisk className="inline-block text-red-500" />
                </label>
                <input
                  className="p-2 text-sm font-semibold text-yellow-500 rounded"
                  type="text"
                  name="country"
                  value={country}
                  onChange={(e) => {
                    this.setState({
                      country: e.target.value,
                      countryError: "",
                    });
                  }}
                />
                <span className="text-xs text-red-500">{countryError}</span>
              </div>
            </div>
          ) : (
            <div className="flex flex-col w-11/12 p-5 rounded shadow-lg md:w-5/12 lg:w-5/12 xl:w-5/12">
              <div className="flex flex-wrap items-center my-10 ">
                {user &&
                  user.length > 0 &&
                  user[0].address.map((a) => (
                    <div className="p-3 m-4 border">
                      <div
                        className="flex items-center justify-between"
                        id={a.id}
                      >
                        Address:{" "}
                        <div className="flex space-x-1">
                          <button
                            onClick={() => this.removeAddress(a.id, a.user_id)}
                            className="flex items-center justify-center w-4 h-4 text-xs font-bold text-yellow-500 bg-gray-700 rounded-full hover:bg-yellow-500 hover:text-gray-700"
                          >
                            x
                          </button>
                          <input
                            type="radio"
                            name="address"
                            className="w-4 h-4"
                            id={a.id}
                            onChange={() => {
                              this.setState({
                                addressId: a.id,
                              });
                            }}
                          />
                        </div>
                      </div>
                      <h3>Address: {a.address}</h3>
                      <h3>City:{a.city}</h3>
                      <h3>State:{a.state}</h3>
                      <h3>Country:{a.country}</h3>
                      <h3>pincode:{a.zipcode}</h3>
                    </div>
                  ))}

                <button
                  onClick={() => {
                    this.setState({
                      addressPop: !this.state.addressPop,
                    });
                  }}
                  className="p-2 m-4 text-sm text-white bg-yellow-500 hover:bg-yellow-400"
                >
                  Add address
                </button>
              </div>
              {addressPop && (
                <div className="absolute inset-0 z-50 flex items-center justify-center bg-black bg-opacity-70">
                  <div className="grid w-full grid-cols-2 gap-4 p-5 bg-gray-100 rounded shadow-lg md:w-2/3 lg:w-1/2 xl:w-1/2 dark:bg-gray-300 ">
                    <div className="flex items-center justify-between col-span-2 py-2">
                      <h1 className="text-lg font-semibold text-yellow-500">
                        Add new address
                      </h1>
                      <button
                        onClick={() => {
                          this.setState({
                            addressPop: !this.state.addressPop,
                          });
                        }}
                        className="flex items-center justify-center w-6 h-6 text-white bg-gray-700 rounded-full"
                      >
                        x
                      </button>
                    </div>
                    <div className="flex flex-col col-span-2 space-y-1">
                      <label className="text-sm font-semibold text-yellow-500">
                        Address
                        <CgAsterisk className="inline-block text-red-500" />
                      </label>
                      <textarea
                        name="address"
                        value={address}
                        className="h-20 p-2 text-sm font-semibold rounded"
                        onChange={(e) => {
                          this.setState({
                            address: e.target.value,
                            addressError: "",
                          });
                        }}
                      ></textarea>
                      <span className="text-xs text-red-500">
                        {addressError}
                      </span>
                    </div>
                    <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                      <label className="text-sm font-semibold text-yellow-500">
                        Pincode
                        <CgAsterisk className="inline-block text-red-500" />
                      </label>
                      <input
                        className="p-2 text-sm font-semibold text-yellow-500 rounded"
                        type="number"
                        name="pincode"
                        value={pincode}
                        onChange={(e) => {
                          this.setState({
                            pincode: e.target.value,
                            pincodeError: "",
                          });
                        }}
                      />

                      <span className="text-xs text-red-500">
                        {pincodeError}
                      </span>
                    </div>
                    <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                      <label className="text-sm font-semibold text-yellow-500">
                        City
                        <CgAsterisk className="inline-block text-red-500" />
                      </label>
                      <input
                        className="p-2 text-sm font-semibold text-yellow-500 rounded"
                        type="text"
                        name="city"
                        value={city}
                        onChange={(e) => {
                          this.setState({
                            city: e.target.value,
                            cityError: "",
                          });
                        }}
                      />
                      <span className="text-xs text-red-500">{cityError}</span>
                    </div>{" "}
                    <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                      <label className="text-sm font-semibold text-yellow-500">
                        State
                        <CgAsterisk className="inline-block text-red-500" />
                      </label>
                      <input
                        className="p-2 text-sm font-semibold text-yellow-500 rounded"
                        type="text"
                        name="state"
                        value={state}
                        onChange={(e) => {
                          this.setState({
                            state: e.target.value,
                            stateError: "",
                          });
                        }}
                      />
                      <span className="text-xs text-red-500">{stateError}</span>
                    </div>
                    <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                      <label className="text-sm font-semibold text-yellow-500">
                        Country
                        <CgAsterisk className="inline-block text-red-500" />
                      </label>
                      <input
                        className="p-2 text-sm font-semibold text-yellow-500 rounded"
                        type="text"
                        name="country"
                        value={country}
                        onChange={(e) => {
                          this.setState({
                            country: e.target.value,
                            countryError: "",
                          });
                        }}
                      />
                      <span className="text-xs text-red-500">
                        {countryError}
                      </span>
                    </div>
                    <div className="flex flex-col col-span-2 space-y-1 md:col-span-1 lg:col-span-1 xl:col-span-1">
                      <button
                        onClick={() => {
                          this.addAddress();
                        }}
                        className="bg-yellow-500 btn"
                      >
                        Add
                      </button>
                    </div>
                  </div>
                </div>
              )}
            </div>
          )}
          <div className="w-11/12 p-5 bg-gray-100 rounded shadow-lg md:w-5/12 lg:w-5/12 xl:w-5/12 dark:bg-gray-300 ">
            <div className="mt-2">
              <h1 className="text-lg font-semibold text-yellow-500">
                Order summary
              </h1>
            </div>
            {this.props.cartItems.map((p) => (
              <div className="pb-2 border-b-2 border-gray-500">
                <div className="flex justify-between">
                  <h1 className="w-1/2 text-sm font-semibold text-gray-600 capitalize line-clamp-2">
                    {p.title}
                  </h1>
                  <h1 className="flex items-center justify-center w-40 text-sm font-semibold">
                    1 x {p.count} = $ {(p.count * p.price).toFixed(2)}
                  </h1>
                </div>
              </div>
            ))}
            <div className="flex justify-end mt-4 ">
              <h1 className="flex items-center justify-center w-40 font-semibold">
                Total = ${" "}
                {this.props.cartItems
                  .reduce((a, c) => a + c.price * c.count, 0)
                  .toFixed(2)}
              </h1>
            </div>
            <div className="flex justify-end mt-4">
              <button
                onClick={() => this.handleProcessed()}
                className="h-10 p-2 text-sm text-white bg-yellow-500 hover:bg-yellow-400"
              >
                Processed to pay
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cartItems: state.CartReducer.cartItems,
});

export default connect(mapStateToProps, null)(withRouter(Checkout));
