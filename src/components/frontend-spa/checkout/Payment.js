/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom";
import { emptyCart } from "../../../Redux/actions/cartActions";
import { useDispatch } from "react-redux";
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import axios from "axios";
import { notify } from "../../../utility";
import { TiTickOutline } from "react-icons/ti";

const CARD_OPTIONS = {
  hidePostalCode: true,
  iconStyle: "solid",
  style: {
    base: {
      iconColor: "#F59E0B",
      color: "black",
      fontWeight: 500,
      fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
      fontSize: "16px",
      fontSmoothing: "antialiased",
      ":-webkit-autofill": {
        color: "#fce883",
      },
      "::placeholder": {
        color: "#87bbfd",
      },
    },
    invalid: {
      iconColor: "EF5356",
      color: "EF5356",
    },
  },
};

const CardField = ({ onChange }) => (
  <div className="w-full px-1 py-2 m-2 border-b">
    <CardElement options={CARD_OPTIONS} onChange={onChange} />
  </div>
);

const Field = ({
  label,
  id,
  type,
  placeholder,
  required,
  autoComplete,
  value,
  onChange,
}) => (
  <div className="flex items-center justify-between w-full px-1 py-2 m-2 border-b">
    <label htmlFor={id} className="w-1/6 font-semibold ">
      {label}
    </label>
    <input
      className="w-10/12 h-10 px-2 font-semibold bg-gray-100 rounded "
      id={id}
      type={type}
      placeholder={placeholder}
      required={required}
      autoComplete={autoComplete}
      value={value}
      onChange={onChange}
    />
  </div>
);

const SubmitButton = ({ processing, cardFieldError, children, disabled }) => (
  <button
    className={` font-semibold w-full rounded h-10 mt-4 bg-yellow-500 ${
      cardFieldError ? "text-red-500 " : " text-white"
    }`}
    type="submit"
    disabled={processing || disabled}
  >
    {processing ? "Processing..." : children}
  </button>
);

const ErrorMessage = ({ children }) => (
  <div
    className="flex justify-center w-full m-2 text-xs font-semibold text-red-500"
    role="alert"
  >
    {children}
  </div>
);

const ResetButton = ({ onClick }) => (
  <button
    type="button"
    className="px-4 py-2 text-sm font-semibold text-white bg-yellow-500 rounded hover:bg-yellow-400"
    onClick={onClick}
  >
    Close
  </button>
);

const Payment = () => {
  const stripe = useStripe();
  const elements = useElements();
  const [cardFieldError, setCardFieldError] = useState(null);
  const [cardComplete, setCardComplete] = useState(false);
  const [processing, setProcessing] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState(null);
  const [email, setEmail] = useState("");
  const [phone, setPhoneNo] = useState("");
  const [name, setName] = useState("");
  const [success, setSuccess] = useState(false);

  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const orderInfo = location.state.orderInfo;
  const userData = JSON.parse(localStorage.getItem("userData"));
  let access_token;
  if (userData) {
    access_token = userData.access_token;
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    if (cardFieldError) {
      elements.getElement("card").focus();
      return;
    }

    if (cardComplete) {
      setProcessing(true);
    }

    const billingDetails = {
      name: name,
      email: email,
      phone: phone,
      address: {
        city: orderInfo.city,
        line1: orderInfo.address,
        state: orderInfo.state,
        postal_code: orderInfo.pincode,
        country: "US",
      },
    };

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement),
      billing_details: billingDetails,
    });

    if (!error) {
      try {
        const { id } = paymentMethod;
        console.log("id", id);
        const response = await axios.post(
          "http://localhost:5000/stripe/payment",
          {
            amount: parseInt(orderInfo.payingAmount),
            id,
          }
        );
        if (response.data.success) {
          const confirmPayment = await stripe.confirmCardPayment(
            response.data.payment.client_secret,
            { payment_method: id }
          );

          if (confirmPayment.error) {
            console.log("error", confirmPayment.error);
            alert(confirmPayment.error.message);
            setProcessing(false);
          } else {
            setSuccess(true);
            const { paymentIntent } = confirmPayment;
            if (paymentIntent.status === "succeeded") {
              const paymentData = {
                user_type: orderInfo.user_type,
                first_name: orderInfo.fName,
                last_name: orderInfo.lName,
                email: orderInfo.email,
                order_id: paymentIntent.id,
                amount: orderInfo.payingAmount,
                city: orderInfo.city,
                state: orderInfo.state,
                country: orderInfo.country,
                zipcode: orderInfo.pincode,
                address: orderInfo.address,
              };
              if (orderInfo.user_type === 1) {
                paymentData["customer_id"] = userData.id;
              }
              let config = {
                method: "post",
                url: "http://localhost:5000/users/orderPost",
                data: paymentData,
              };
              axios(config)
                .then(function (response) {
                  const data = response.data;
                  notify(data.message);
                  setSuccess(true);
                  setPaymentMethod(paymentIntent);
                  setProcessing(false);
                  dispatch(emptyCart());
                })
                .catch(function (error) {
                  console.log(error);
                });
            } else {
              alert("payment fail");
              setProcessing(false);
            }
          }
        }
      } catch (error) {
        setProcessing(false);
      }
    } else {
      alert(error.message);
      setProcessing(false);
    }
  };

  const reset = () => {
    setCardFieldError(null);
    setProcessing(false);
    setPaymentMethod(null);
    setProcessing(false);
    setName("");
    setEmail("");
    setPhoneNo("");
    history.push("/");
  };
  return success && paymentMethod !== null ? (
    <div className="flex items-center justify-center w-full min-h-screen mt-20 bg-gray-100 dark:bg-gray-700">
      <div className="flex flex-col items-center justify-center w-11/12 p-6 bg-white rounded shadow-xl sm:w-10/12 lg:w-2/5 sm:p-10 md:p-14 lg:p-20">
        <div className="">
          <div className="flex items-center justify-center w-10 h-10 bg-green-500 rounded-full">
            <TiTickOutline className="text-white" size={24} />
          </div>
        </div>
        <div className="text-xl font-semibold text-yellow-500" role="alert">
          Payment successful
        </div>
        <div className="mt-4 font-medium">
          Thanks for trying Stripe Elements. No money was charged, but we
          generated a Payment id: {paymentMethod.id}
        </div>
        <div className="w-full mt-4">
          <div className="flex w-full">
            <h1 className="font-semibold text-yellow-500 underline">
              Shipping Details
            </h1>
          </div>
          <div className="flex w-full mt-1">
            <h1 className="font-semibold text-gray-500">Name:</h1>
            <h1 className="pl-2 font-semibold">
              {orderInfo.fName} {orderInfo.lName}{" "}
            </h1>
          </div>
          <div className="flex w-full mt-1">
            <h1 className="font-semibold text-gray-500">Contact no:</h1>
            <h1 className="pl-2 font-semibold">{orderInfo.phone}</h1>
          </div>
          <div className="flex w-full mt-1">
            <h1 className="font-semibold text-gray-500">Address:</h1>
            <div>
              <h1 className="pl-2 font-semibold">{orderInfo.address}</h1>
              <h1 className="pl-2 font-semibold">
                {orderInfo.pincode} {orderInfo.city} {orderInfo.state}{" "}
                {orderInfo.country}
              </h1>
            </div>
          </div>
          <div className="flex w-full mt-1">
            <h1 className="font-semibold text-gray-500">Amount paid:</h1>
            <h1 className="pl-2 font-semibold">{orderInfo.payingAmount}$</h1>
          </div>
        </div>
        <div className="flex justify-end w-full mt-6">
          <ResetButton onClick={reset} />
        </div>
      </div>
    </div>
  ) : (
    <div className="flex items-center justify-center w-full min-h-screen bg-gray-100 dark:bg-gray-700 lg:mt-10">
      <form
        className="flex flex-col items-center justify-center w-11/12 p-6 bg-white rounded shadow-xl sm:w-10/12 lg:w-2/5 sm:-10 md:p-14 lg:p-20"
        onSubmit={handleSubmit}
      >
        <Field
          label="Name"
          id="name"
          type="text"
          placeholder="Jane Doe"
          required
          autoComplete="name"
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
        />
        <Field
          label="Email"
          id="email"
          type="email"
          placeholder="janedoe@gmail.com"
          required
          autoComplete="email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
        <Field
          label="Phone"
          id="phone"
          type="tel"
          placeholder="(941) 555-0123"
          required
          autoComplete="tel"
          value={phone}
          onChange={(e) => {
            setPhoneNo(e.target.value);
          }}
        />

        <CardField
          onChange={(e) => {
            setCardFieldError(e.error);
            setCardComplete(e.complete);
          }}
        />
        {cardFieldError && (
          <ErrorMessage>{cardFieldError.message}</ErrorMessage>
        )}
        <SubmitButton
          processing={processing}
          cardFieldError={cardFieldError}
          disabled={!stripe}
        >
          Pay ${orderInfo.payingAmount}
        </SubmitButton>
      </form>
    </div>
  );
};

export default Payment;
