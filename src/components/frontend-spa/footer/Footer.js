import React from "react";

const FrontSpaFooter = () => {
    let date = new Date();
    return (
        <div className="w-full hidden sm:flex md:flex lg:flex xl:flex flex-col  md:flex-row lg:h-10 lg:flex-row items-center justify-between bg-gray-800 py-4 px-3">
            <div>
                <h1 className="text-white text-sm  font-normal">
                    © 2019 - {date.getFullYear()} DevDigital LLC. All Rights
                    Reserved
                </h1>
            </div>
            <div className="mt:2 lg:mt-0">
                <span className="text-xs text-white">Powered By </span>
                <span className="text-sm text-blue-400">
                    {" "}
                    DevDigital : Nashville Software Development
                </span>
            </div>
        </div>
    );
};

export default FrontSpaFooter;
