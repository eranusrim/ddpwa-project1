import React, { useState, useEffect } from "react";

const FlashCard = ({ question, answer, incrementIndex }) => {
    const [showAnswer, setShowAnswer] = useState(false);

    useEffect(() => setShowAnswer(false), [question]);

    return (
        <>
            <div
                className="p-3 rounded bg-gray-400"
                onClick={() => setShowAnswer(!showAnswer)}
            >
                {!showAnswer && question}
                {showAnswer && answer}
            </div>
            {showAnswer && (
                <button
                    onClick={incrementIndex}
                    className="p-3 text-white rounded bg-blue-500 border-blue-400 w-42 m-2"
                >
                    Next question
                </button>
            )}
        </>
    );
};

export default FlashCard;
