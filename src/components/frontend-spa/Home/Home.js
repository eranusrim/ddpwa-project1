import React from "react";
import ImageSlider from "../slider/Slider";
import About from "../about/about";
import Calender from "../calender/Calender";
import Contact from "../contact/Contact";
import Testimonials from "../Testimonials/Testimonials";

const Home = () => {
    return (
        <>
            <ImageSlider />
            <About />
            <Calender />
            <Testimonials />
            <Contact />
        </>
    );
};

export default Home;
