/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
import interactionPlugin from "@fullcalendar/interaction";
import listPlugin from "@fullcalendar/list";
import axios from "axios";

const Calender = () => {
  const [events, setEvents] = useState([]);
  const [headerToolbar, setHeaderToolbar] = useState({
    left: "prev,next,today",
    center: "title",
    right: "dayGridMonth,dayGridWeek,dayGridDay,listWeek",
  });

  const fetchEvents = (startDate, endDate) => {
    axios
      .post("http://localhost:5000/events/calenderEvents", {
        startDate,
        endDate,
      })
      .then((response) => {
        const data = response.data.events;
        console.log("data", data);
        setEvents(data);
      })
      .catch((err) => {
        console.log("error", err);
      });
  };
  const calendarRef = React.createRef();
  useEffect(() => {
    let calendarApi = calendarRef.current.getApi();
    // fetchEvents();
    if (window.innerWidth < 600) {
      setHeaderToolbar({
        left: "today",
        center: "title",
        right: "dayGridMonth,dayGridDay,listWeek",
      });
    }
    if (window.innerWidth < 400) {
      setHeaderToolbar({
        left: "today",
        center: "title",
        right: "listWeek",
      });
    }
  }, [window.innerWidth]);
  return (
    <div
      id="calender"
      className="w-full px-10 py-10 overflow-hidden bg-white dark:bg-gray-50 lg:px-40 md:py-20 lg:py-20"
    >
      <h1 className="mb-8 text-xl text-center uppercase  md:text-2xl lg:text-3xl dark:text-yellow-400">
        Calender
      </h1>
      <FullCalendar
        plugins={[dayGridPlugin, interactionPlugin, listPlugin]}
        initialView="dayGridMonth"
        events={events}
        headerToolbar={headerToolbar}
        ref={calendarRef}
        datesSet={(dateInfo) => {
          fetchEvents(dateInfo.start, dateInfo.end);
        }}
      />
    </div>
  );
};
export default Calender;

// eventSources: [
//     {
//         events: function(start, end, timezone, callback) {
//             $.ajax({
//                     url: "{{ route('event.filter') }}",
//                     type: 'POST',
//                     dataType: 'json',
//                     data: {
//                             'location': $(".js-location-btn.active").data('location'),
//                             'start':start.unix(),
//                             'end':end.unix(),
//                     },
//                     beforeSend: function(xhr) {
//                             xhr.setRequestHeader('X-CSRF-Token', csrf_token);
//                         },
//                     success: function(response) {
//                             var events = [];
//                             $.each(response.events, function(key, val) {
//                                 events.push({
//                                     id:key,
//                                     title: val.title,
//                                     start: val.start_date,
//                                     end: val.end_date,
//                                     displayEventTime: false,
//                                     event_title_time :val.event_title_time,
//                                     borderColor_1 :val.borderColor,
//                                 });
//                             });
//                             callback(events);
//                 },
//             });
//         },
//     }
// ],
