/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { useHistory } from "react-router-dom";

const GalleriesCard = ({ gallery }) => {
  const history = useHistory();
  return (
    <div
      onClick={() => {
        history.push("/galleries/" + gallery.id);
      }}
      className="flex flex-col p-4 bg-white border rounded shadow cursor-pointer"
    >
      <div className="w-full h-40">
        <img src={gallery.image} className="object-contain w-full h-full" />
      </div>
      <h1 className="w-full my-2 font-semibold line-clamp-2">
        {gallery.title}
      </h1>
      <p className="w-full text-sm font-medium text-gray-400 line-clamp-4">
        {gallery.description}
      </p>
    </div>
  );
};

export default GalleriesCard;
