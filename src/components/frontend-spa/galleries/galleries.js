/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/heading-has-content */
import React, { useState, useEffect } from "react";
import GalleriesCard from "./galleriesCard";
import axios from "axios";

const FrontendGalleries = () => {
  const [galleries, setGalleries] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchGalleries = () => {
    axios
      .get("http://localhost:5000/gallery")
      .then((response) => {
        const galleries = response.data.gallery;
        setGalleries(galleries);
        setLoading(false);
      })
      .catch((error) => {});
  };
  useEffect(() => {
    fetchGalleries();
  }, []);
  return (
    <div className="flex-wrap justify-center w-full h-full pt-10 mx-auto mt-10 bg-gray-100 lg:mt-10 mb-14 sm:mb-0 md:mb-0 lg:mb-0 xl:mb-0 dark:bg-gray-700 2xl:p-10 xl:p-10 lg:p-10 md:flex md:items-start lg:flex lg:items-start">
      {loading ? (
        <>
          <h1 className="w-full my-8 text-2xl font-medium text-center capitalize dark:text-white">
            Galleries
          </h1>
          <div className="grid w-full grid-cols-1 gap-4 p-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4">
            {Array(15)
              .fill()
              .map(() => (
                <div className="flex flex-col p-2 bg-white border rounded animate-pulse">
                  <div className="w-full h-40 bg-white">
                    <img
                      src={"/images/image.png"}
                      className="object-contain w-full h-full"
                    />
                  </div>

                  <h1 className="w-2/3 h-4 my-2 bg-gray-500 rounded"></h1>
                  <p className="w-full h-6 my-2 bg-gray-400 rounded"></p>
                </div>
              ))}
          </div>
        </>
      ) : (
        <>
          <h1 className="w-full my-8 text-2xl font-medium text-center capitalize dark:text-white">
            Galleries
          </h1>
          <div className="grid w-full grid-cols-1 gap-4 p-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4">
            {galleries?.map((gallery) => (
              <GalleriesCard gallery={gallery} />
            ))}
          </div>
        </>
      )}
    </div>
  );
};

export default FrontendGalleries;
