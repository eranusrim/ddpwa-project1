/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { AiOutlineEye } from "react-icons/ai";
import axios from "axios";
import { useHistory } from "react-router-dom";

const GalleryDescription = (props) => {
  const params = useParams();
  const [galleries, setGalleries] = useState([]);
  const [gallery, setGallery] = useState([]);
  const [images, setImages] = useState([]);
  const [links, setLinks] = useState([]);
  const [selectOpt, setSelectOpt] = useState("");
  const [ytLink, setYtLink] = useState("");
  const [ytPopUp, setYtPopUp] = useState(false);
  const [imgLink, setImgLink] = useState("");
  const [imgPopUp, setImgPopUp] = useState(false);

  const history = useHistory();

  const fetchGallery = (id) => {
    axios
      .get("http://localhost:5000/gallery/" + id)
      .then((response) => {
        const gallery = response.data.gallery;
        setGallery(gallery);
        setSelectOpt(gallery[0].id);
        setImages(gallery[0].images);
        if (gallery[0].links.length > 0) {
          const arr = [];
          gallery[0].links.map((ab) => {
            const a = ab.link.split("/");
            const b = a[a.length - 1];
            arr.push({
              id: ab.id,
              link: b,
              gallery_id: ab.gallery_id,
            });
          });
          setLinks(arr);
        }
      })
      .catch((error) => {});
  };
  const fetchGalleries = () => {
    axios
      .get("http://localhost:5000/gallery")
      .then((response) => {
        const galleries = response.data.gallery;
        setGalleries(galleries);
      })
      .catch((error) => {});
  };

  useEffect(() => {
    const id = params.id;
    if (id) {
      fetchGallery(id);
      fetchGalleries();
    }
  }, []);
  return (
    <div className="w-full pt-10 mt-10 lg:mt-10 mb-14 sm:mb-0 md:mb-0 lg:mb-0 xl:mb-0 dark:bg-gray-700 2xl:p-10 xl:p-10 lg:p-10">
      {gallery.length > 0 && (
        <div className="flex flex-col justify-center w-full">
          <h1 className="w-full pt-8 text-3xl font-semibold text-center capitalize dark:text-white">
            {gallery[0].title}
          </h1>
          <div className="flex items-center justify-center w-full my-5">
            <select
              value={selectOpt}
              onChange={(e) => {
                fetchGallery(e.target.value);
                history.push("/galleries/" + e.target.value);
              }}
              className="px-4 py-2 my-4 border rounded"
            >
              {galleries.map((g) => (
                <option value={g.id} key={g.id}>
                  {g.title}
                </option>
              ))}
            </select>
          </div>
          <h1 className="w-full mb-8 font-medium text-center capitalize dark:text-white">
            {gallery[0].description}
          </h1>
          <div className="grid grid-cols-1 gap-4 m-4 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4">
            {images.length > 0 &&
              images.map((i) => (
                <div className="flex items-center justify-center w-full h-48 p-1 bg-white border rounded shadow cursor-pointer">
                  <img
                    src={i.imageUrl}
                    onClick={() => {
                      setImgPopUp(true);
                      setImgLink(i.imageUrl);
                    }}
                    className="object-contain w-full h-full"
                  />
                </div>
              ))}
            {links.length > 0 &&
              links.map((i) => (
                <div className="relative flex items-center justify-center w-full h-48 p-1 bg-white border rounded shadow cursor-pointer imgHover">
                  <iframe
                    src={`https://www.youtube.com/embed/${i.link}`}
                    frameborder="0"
                    allow="autoplay; encrypted-media"
                    allowfullscreen
                    title="video"
                    className="w-full h-full border-2"
                  />
                  <div className="absolute inset-0 items-center justify-center hidden w-full h-full space-x-2 text-white bg-black imgHoverDisplay bg-opacity-70">
                    <button
                      onClick={() => {
                        setYtPopUp(true);

                        setYtLink(i.link);
                      }}
                      className="flex items-center justify-center w-8 h-8 bg-blue-400 rounded-full"
                    >
                      <AiOutlineEye />
                    </button>
                  </div>
                </div>
              ))}
          </div>
        </div>
      )}
      {ytPopUp && (
        <div className="absolute inset-0 z-50 flex flex-col items-center justify-center w-full bg-black bg-opacity-70">
          <div className="flex flex-col w-1/2 mt-10 bg-white rounded h-1/2">
            <div className="flex justify-end w-full h-10 item-center">
              <h1
                onClick={() => {
                  setYtPopUp(false);
                  setYtLink("");
                }}
                className="flex items-center justify-center w-10 h-10 text-white bg-black border-2 cursor-pointer"
              >
                x
              </h1>
            </div>
            <iframe
              src={`https://www.youtube.com/embed/${ytLink}`}
              frameborder="0"
              allow="autoplay; encrypted-media"
              allowfullscreen
              title="video"
              className="w-full h-full border-2"
            />
          </div>
        </div>
      )}
      {imgPopUp && (
        <div className="absolute inset-0 z-50 flex flex-col items-center justify-center w-full bg-black bg-opacity-70">
          <div className="flex flex-col w-1/2 mt-10 bg-white rounded h-1/2">
            <div className="flex justify-end w-full h-10 bg-white item-center">
              <h1
                onClick={() => {
                  setImgPopUp(false);
                  setImgLink("");
                }}
                className="flex items-center justify-center w-10 h-10 text-white bg-black border-2 cursor-pointer"
              >
                x
              </h1>
            </div>
            <img
              src={imgLink}
              className="object-contain w-full h-full bg-white border-2"
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default GalleryDescription;
