/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  removeFromCart,
  increaseProductCount,
  decreaseProductCount,
} from "../../../Redux/actions/cartActions";

class Cart extends Component {
  componentDidMount() {
    console.log("this.props.cartItems", this.props.cartItems);
  }
  componentDidUpdate() {
    console.log("this.props.cartItems update ui", this.props.cartItems);
  }

  render() {
    return (
      <div className="flex flex-col items-center w-full min-h-screen pt-16 pb-16 mt-10 bg-gray-100 shadow-md dark:bg-gray-700">
        {this.props.cartItems.length > 0 &&
          this.props.cartItems.map((p) => (
            <div className="flex flex-wrap items-center justify-between w-full px-2 my-2 border-b-2 sm:px-10 md:px-10 lg:px-10 xl:px-10">
              <div className="flex items-center justify-center w-10 p-1  h-14 sm:w-20 md:w-20 lg:w-20 xl:w-20">
                <img
                  src={p.image}
                  className="object-contain w-full h-full rounded cursor-pointer"
                  onClick={() => this.props.history.push("/product/" + p.id)}
                />
              </div>
              <div className="flex flex-col items-start justify-start w-3/4 sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2 ">
                <p className="text-xs font-semibold text-yellow-500 sm:text-sm md:text-sm lg:text-sm xl:text-sm line-clamp-1">
                  {p.title}
                </p>
                <p className="mt-2 text-xs font-semibold text-yellow-500 sm:text-sm md:text-sm lg:text-sm xl:text-sm">
                  Price: $ {p.price}
                </p>
                <div className="flex items-center mt-1">
                  <button
                    onClick={() => {
                      if (p.count === 1) {
                        this.props.removeFromCart(p);
                      } else {
                        this.props.decreaseProductCount(p);
                      }
                    }}
                    className="flex items-center justify-center w-5 h-5 m-1 bg-white border-2 border-gray-500 outline-none"
                  >
                    -
                  </button>
                  <p className="m-1 text-sm font-semibold dark:text-white ">
                    {p.count}
                  </p>
                  <button
                    onClick={() => {
                      this.props.increaseProductCount(p);
                    }}
                    className="flex items-center justify-center w-5 h-5 m-1 bg-white border-2 border-gray-500"
                  >
                    +
                  </button>
                </div>
              </div>
              <div className="flex flex-row items-center justify-end w-full mb-2 sm:w-auto md:w-auto lg:w-auto xl:w-auto sm:flex-row md:flex-row lg:flex-row xl:flex-row sm:items-center md:items-center lg:items-center xl:items-center">
                <h1 className="text-xs font-semibold sm:text-sm md:text-sm lg:text-sm xl:text-sm dark:text-gray-400">
                  Subtotal:{" "}
                  <span className="text-xs font-semibold text-yellow-500 sm:text-sm md:text-sm lg:text-sm xl:text-sm">
                    $ {(p.count * p.price).toFixed(2)}
                  </span>
                </h1>

                <button
                  className="flex items-center justify-center h-6 p-2 ml-2 text-xs text-white bg-black rounded  sm:h-8 md:h-8 lg:h-8 xl:h-8 hover:bg-gray-800"
                  onClick={() => {
                    this.props.removeFromCart(p);
                  }}
                >
                  Remove
                </button>
              </div>
            </div>
          ))}
        {this.props.cartItems.length > 0 && (
          <div className="flex items-center justify-end w-full px-2 my-2 dark:text-white sm:px-10 md:px-10 lg:px-10 xl:px-10">
            <h1 className="text-sm font-semibold sm:text-sm md:text-sm lg:text-xl xl:text-xl">
              Total:
              <span className="pl-2 text-yellow-500">
                ${" "}
                {this.props.cartItems
                  .reduce((a, c) => a + c.price * c.count, 0)
                  .toFixed(2)}
              </span>
            </h1>
            <button
              onClick={() => {
                this.props.history.push("/checkout");
              }}
              className="px-2 py-2 ml-2 text-sm font-semibold text-white bg-yellow-500 rounded sm:px-4 hover:bg-yellow-400"
            >
              Checkout
            </button>
          </div>
        )}

        {this.props.cartItems.length === 0 && (
          <h1 className="mt-10 text-2xl text-center dark:text-white">
            No items in cart
          </h1>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cartItems: state.CartReducer.cartItems,
});
const mapDispatchToProps = (dispatch) => {
  return {
    removeFromCart: (p) => dispatch(removeFromCart(p)),
    increaseProductCount: (p) => dispatch(increaseProductCount(p)),
    decreaseProductCount: (p) => dispatch(decreaseProductCount(p)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Cart));
