import React from "react";

const About = () => {
    return (
        <div
            id="about"
            className="flex justify-between items-center flex-col px-10  py-10 md:py-20 lg:py-20 bg-gray-100 dark:bg-gray-700"
        >
            <h1 className=" uppercase text-xl md:text-2xl lg:text-3xl  mb-8 dark:text-yellow-400">
                About us
            </h1>
            <p className="tracking-wide dark:text-white">
                "At vero eos et accusamus et iusto odio dignissimos ducimus qui
                blanditiis praesentium voluptatum deleniti atque corrupti quos
                dolores et quas molestias excepturi sint occaecati cupiditate
                non provident, similique sunt in culpa qui officia deserunt
                mollitia animi, id est laborum et dolorum fuga. Et harum quidem
                rerum facilis est et expedita distinctio. Nam libero tempore,
                cum soluta nobis est eligendi optio cumque nihil impedit quo
                minus id quod maxime placeat facere possimus, omnis voluptas
                assumenda est, omnis dolor repellendus. Temporibus autem
                quibusdam et aut officiis debitis aut rerum necessitatibus saepe
                eveniet ut et voluptates repudiandae sint et molestiae non
                recusandae. Itaque earum rerum hic tenetur a sapiente delectus,
                ut aut reiciendis voluptatibus maiores alias consequatur aut
                perferendis doloribus asperiores repellat."
            </p>
        </div>
    );
};

export default About;
