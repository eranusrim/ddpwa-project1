/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import Dropzone from "react-dropzone";
import axios from "axios";
import { FaFileUpload } from "react-icons/fa";
import { AiFillCheckCircle } from "react-icons/ai";
import moment from "moment";
import { notify } from "../../../utility";

const ClientDashboard = () => {
  const [requestForDocs, setRequestForDocs] = useState([]);
  const [selectedUploadList, setSelectedUploadList] = useState([]);
  const [agent, setAgent] = useState({});
  let userData = JSON.parse(localStorage.getItem("userData"));

  const fetchDocRequests = () => {
    const id = userData.id;
    const bodyParameters = {
      id: id,
    };
    axios
      .post("http://localhost:5000/doc/getClientsDocumentReq", bodyParameters)
      .then((res) => {
        const data = res.data.doc.map((d) => {
          let requestStatus = "";
          if (d.currentprogress !== d.totalprogress) {
            requestStatus = moment(d.dueDate).isBefore(moment())
              ? "OVERDUE"
              : moment(d.requestDueDate).isAfter(moment())
              ? "INPROGRESS"
              : "UNOPENED";
          } else {
            requestStatus = "READYFORREVIEW";
          }
          const a = d;
          a.requestStatus = requestStatus;
          return a;
        });
        setRequestForDocs(data);
      })
      .catch((err) => console.log(err));
  };

  const onImageDrop = (acceptedFiles, id, requestedtableid) => {
    if (acceptedFiles.length > 0) {
      const uploads = acceptedFiles.map((image) => {
        let data = new FormData();
        data.append("folder", "document");
        data.append("image", image);
        const config = {
          method: "post",
          url: "http://localhost:5000/uploads",

          data: data,
        };

        axios(config)
          .then(function (response) {
            const d = {
              id: id,
              requestedtableid: requestedtableid,
              documentname: response.data.imageName,
            };
            const conf = {
              method: "put",
              url: "http://localhost:5000/doc/uploadDoc",

              data: d,
            };
            axios(conf)
              .then((res) => {
                notify(response.data.message);
                fetchDocRequests();
              })
              .catch((err) => {
                console.log("error", err);
              });
          })
          .catch(function (error) {
            console.log(error);
          });
      });
    }
  };

  const handleRadioChange = (doc) => {
    setAgent(doc);
    const bodyParameters = {
      id: doc.id,
    };
    axios
      .post("http://localhost:5000/doc/getSelectAgentDocReq", bodyParameters)
      .then((res) => {
        setSelectedUploadList(res.data.docList);
        console.log(res.data.docList);
      })
      .catch((err) => console.log(err));
  };

  const removeDoc = (id, requestedtableid, documentname) => {
    let arr = documentname.split("/");
    let docName = arr[arr.length - 1];
    const conf = {
      method: "delete",
      url: "http://localhost:5000/uploads",
      data: {
        folder: "document",
        image: docName,
      },
    };
    axios(conf)
      .then(function (response) {
        const bodyParameters = {
          id: id,
          requestedtableid: requestedtableid,
        };
        const config = {
          method: "delete",
          url: "http://localhost:5000/doc/deleteDoc",
          data: bodyParameters,
        };
        axios(config)
          .then((response) => {
            notify(response.data.message);
            fetchDocRequests();
            handleRadioChange(agent);
          })
          .catch((err) => console.log(err));
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchDocRequests();
  }, []);
  return (
    <div className="grid w-full min-h-screen grid-cols-1 gap-4 px-10 pt-8 pb-10 mt-14 mb-14 sm:mb-0 md:mb-0 lg:mb-0 xl:mb-0 dark:bg-gray-700 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-2 xl:grid-cols-2">
      <div className="flex flex-col w-full mt-2 ">
        {requestForDocs &&
          requestForDocs.map((doc) => (
            <>
              <input
                id={doc.id}
                className="hidden"
                type="radio"
                name="docs"
                onChange={(e) => handleRadioChange(doc)}
              />
              <label
                for={doc.id}
                key={doc.id}
                className="grid grid-cols-3 gap-2 p-4 m-2 border rounded shadow-sm cursor-pointer dark:bg-white"
              >
                <div className="col-span-2">
                  <h1 className="font-semibold capitalize">
                    <span className="mr-2 font-semibold capitalize">
                      Agent Name:
                    </span>
                    {doc.agent_name}
                  </h1>
                  <div className="flex items-center w-full ">
                    <h1 className="mr-2 font-semibold">Requested documents:</h1>
                    <h1 className="mr-2 font-semibold">
                      {doc.currentprogress} /{doc.totalprogress}
                    </h1>
                    <h1
                      className="flex items-center h-4 overflow-hidden bg-gray-300 border rounded-full "
                      style={{ width: "250px" }}
                    >
                      <span
                        className="inline-block h-4 bg-blue-400 rounded-full "
                        style={{
                          width:
                            (250 / doc.totalprogress) * doc.currentprogress +
                            "px",
                        }}
                      ></span>
                    </h1>
                    {doc.totalprogress === doc.currentprogress && (
                      <AiFillCheckCircle className="text-2xl text-yellow-500" />
                    )}
                  </div>
                  <div className="flex">
                    <h1 className="font-semibold">Date:</h1>
                    <h1 className="pr-1 font-semibold text-yellow-500">
                      {moment(doc.date).format("DD/MM/YYYY")}
                    </h1>
                  </div>
                  <div className="flex">
                    <h1 className="font-semibold">Due Date:</h1>
                    <h1 className="pr-1 font-semibold text-yellow-500">
                      {moment(doc.duedate).format("DD/MM/YYYY")}
                    </h1>
                  </div>
                </div>
                <div className="">
                  <h1 className="my-1 text-xs font-semibold text-center lg:text-sm">
                    Status
                  </h1>
                  <h1
                    className={`text-xs lg:text-sm font-semibold border-2 text-center p-1 ${
                      doc.requestStatus === "OVERDUE"
                        ? "border-red-500 text-red-500"
                        : "" || doc.requestStatus === "READYFORREVIEW"
                        ? "border-green-500 text-green-500"
                        : "" || doc.requestStatus === "INPROGRESS"
                        ? "border-yellow-500 text-yellow-500"
                        : "" || doc.requestStatus === "UNOPENED"
                        ? "border-black text-black"
                        : ""
                    }`}
                  >
                    {doc.requestStatus}
                  </h1>
                </div>
              </label>
            </>
          ))}
      </div>
      <div className="flex flex-col w-full p-4 mt-4 border ">
        {selectedUploadList.length > 0 && (
          <h1 className="w-full text-xl font-medium text-center capitalize dark:text-white">
            {agent.agent_name}
          </h1>
        )}
        {selectedUploadList.length > 0 &&
          selectedUploadList.map((i) => (
            <div
              key={i.id}
              className={`${
                i.status === 0
                  ? "border border-gray-500"
                  : i.review === "wrong"
                  ? "border border-red-500"
                  : "border border-green-500"
              } flex flex-col border-4  p-2 my-8 mx-2 shadow-md dark:bg-white rounded`}
            >
              <h1 className="text-lg font-medium capitalize">
                {i.documenttype}
              </h1>
              {i.status === 0 ? (
                <div className="flex items-center justify-center w-full">
                  <Dropzone
                    // accept="image/*"
                    onDrop={(acceptedFiles) => {
                      onImageDrop(acceptedFiles, i.id, i.requestedtableid);
                    }}
                  >
                    {({ getRootProps, getInputProps }) => (
                      <div
                        {...getRootProps()}
                        className="flex flex-col items-center justify-center w-1/2 p-2 border border-black border-dashed h-30"
                      >
                        <input {...getInputProps()} />
                        <FaFileUpload
                          className="flex items-center justify-center w-full text-gray-400 "
                          size={30}
                        />
                        <p className="p-2">
                          Drag 'n' drop {i.documenttype} here, or click to
                          select {i.documenttype}
                        </p>
                      </div>
                    )}
                  </Dropzone>
                </div>
              ) : (
                <div className="relative flex items-center justify-center w-full">
                  <div className="flex items-center justify-center w-full h-40 p-4 border imgHover">
                    <img
                      src={i.documentname}
                      className="object-contain w-full h-full"
                    />
                    <button
                      onClick={() => {
                        removeDoc(i.id, i.requestedtableid, i.documentname);
                      }}
                      className="absolute z-20 text-white bg-gray-900 imgHoverDisplay top-1 right-1 btn"
                    >
                      Remove
                    </button>
                  </div>
                </div>
              )}
              {i.status === 1 && i.note && (
                <div className="relative flex items-center justify-start w-full my-2">
                  <h1 className="font-semibold capitalize">
                    Note: <span className="text-yellow-500">{i.note}</span>
                  </h1>
                </div>
              )}
            </div>
          ))}
      </div>
    </div>
  );
};

export default ClientDashboard;
