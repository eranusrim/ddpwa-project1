/* eslint-disable jsx-a11y/iframe-has-title */
import React, { useState } from "react";
import { CgAsterisk } from "react-icons/cg";
import ReactPhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import ReCAPTCHA from "react-google-recaptcha";
import axios from "axios";
import { notify } from "../../../utility";

const Contact = () => {
  const [fName, setFName] = useState("");
  const [fnameError, setFNameError] = useState("");

  const [lName, setLName] = useState("");

  const [phone, setPhone] = useState(null);

  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState("");

  const [message, setMessage] = useState("");
  const [messageError, setMessageError] = useState("");

  const [verified, setVerified] = useState(false);
  const [verifiedError, setVerifiedError] = useState("");

  let captcha;
  const setCaptchaRef = (ref) => {
    if (ref) {
      return (captcha = ref);
    }
  };

  const resetCaptcha = () => {
    // maybe set it till after is submitted
    captcha.reset();
  };
  const handleRecaptcha = (value) => {
    console.log("captcha value", value);
    setVerified(true);
    setVerifiedError("");
  };
  const handleSubmit = (e) => {
    let fnameError = "";
    let emailError = "";
    let messageError = "";
    let verifiedError = "";

    if (fName.trim() === "") {
      fnameError = "Enter First Name";
    }
    if (email.trim() === "") {
      emailError = "Enter Email";
    }
    if (message.trim() === "") {
      messageError = "Type Your Message";
    }
    if (!verified) {
      verifiedError = "Please Enter Captcha";
    }

    if (
      fnameError !== "" ||
      emailError !== "" ||
      messageError !== "" ||
      verifiedError !== ""
    ) {
      setFNameError(fnameError);
      setEmailError(emailError);
      setMessageError(messageError);
      setVerifiedError(verifiedError);
      return;
    }

    let userMessage = {
      fName: fName,
      lName: lName,
      phone: phone,
      email: email,
      message: message,
    };

    let config = {
      method: "post",
      url: "http://localhost:5000/contact",
      data: userMessage,
    };
    axios(config)
      .then(function (response) {
        const data = response.data;
        notify(data.message);
        setFName("");
        setLName("");
        setPhone(null);
        setEmail("");
        setMessage("");
        setVerified(false);
        resetCaptcha();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <div
        id="contact"
        className="flex flex-col items-center justify-center w-full py-20 bg-gray-100 dark:bg-gray-600"
      >
        <div className="w-5/6 p-4 shadow-lg lg:w-1/2 md:w-3/4 lg:p-10 md:p-10 dark:bg-gray-100">
          <div className="flex items-center justify-center mb-5">
            <h1 className="text-xl text-center uppercase md:text-2xl lg:text-3xl dark:text-yellow-500">
              Leave a request and our manager will contact you.
            </h1>
          </div>
          <div className="flex flex-col w-full mb-5">
            <label className="flex items-center text-sm tracking-wide">
              First Name <CgAsterisk className="text-red-500" />
            </label>
            <input
              type="text"
              name="fName"
              value={fName}
              onChange={(e) => {
                setFName(e.target.value);
                setFNameError("");
              }}
              className={`${
                fnameError && "border-red-500 border"
              } border-2 h-10 my-1 px-2`}
            />
            {fnameError && (
              <span className="text-xs tracking-wider text-red-500">
                {fnameError}
              </span>
            )}
          </div>
          <div className="flex flex-col w-full mb-5">
            <label className="flex items-center text-sm tracking-wide">
              Last Name
            </label>
            <input
              value={lName}
              type="text"
              name="lName"
              onChange={(e) => {
                setLName(e.target.value);
              }}
              className="h-10 px-2 my-1 border-2"
            />
          </div>
          <div className="flex flex-col w-full mb-5">
            <label className="flex items-center text-sm tracking-wide">
              Phone
            </label>
            <ReactPhoneInput
              country={"in"}
              inputStyle={{
                width: "100%",
                height: "2.5rem",
                fontSize: "13px",
                paddingLeft: "48px",
                borderRadius: "5px",
                fontWeight: "600",
                border: "2px solid #E5E7EB",
              }}
              value={phone}
              onChange={(phone) => {
                setPhone(phone);
              }}
            />
          </div>
          <div className="flex flex-col w-full mb-5">
            <label className="flex items-center text-sm tracking-wide">
              Email <CgAsterisk className="text-red-500" />
            </label>
            <input
              type="email"
              name="email"
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
                setEmailError("");
              }}
              className={`${
                emailError && "border-red-500 border"
              } border-2 h-10 my-1 px-2`}
            />
            {emailError && (
              <span className="text-xs tracking-wider text-red-500">
                {emailError}
              </span>
            )}
          </div>
          <div className="flex flex-col w-full mb-5">
            <label className="flex items-center text-sm tracking-wide">
              Message <CgAsterisk className="text-red-500" />
            </label>
            <textarea
              name="message"
              value={message}
              onChange={(e) => {
                setMessage(e.target.value);
                setMessageError("");
              }}
              className={`${
                messageError && "border-red-500 border"
              } border-2 max-h-40 my-1 px-2`}
            />
            {messageError && (
              <span className="text-xs tracking-wider text-red-500">
                {messageError}
              </span>
            )}
          </div>
          <div className="flex flex-col w-full mb-5 overflow-hidden">
            <ReCAPTCHA
              ref={(r) => setCaptchaRef(r)}
              sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
              onChange={handleRecaptcha}
            />
            {verifiedError && (
              <span className="text-xs tracking-wider text-red-500">
                {verifiedError}
              </span>
            )}
          </div>
          <div className="flex flex-col w-full mb-5">
            <button
              onClick={(e) => handleSubmit(e)}
              className="w-1/3 py-3 text-white bg-yellow-500 rounded hover:bg-yellow-400"
            >
              Submit
            </button>
          </div>
        </div>
        <div className="flex flex-col items-center w-full lg:w-9/12 lg:justify-between mt-14 lg:flex-row">
          <div className="w-full p-2 mb-5 lg:mb-0 sm:p-2 md:p-2 sm:w-full md:w-full lg:w-auto xl:w-auto">
            <div className="mb-2">
              <a
                href="tel:9999999999"
                className="text-blue-400 hover:underline"
              >
                999-999-9999
              </a>
            </div>
            <ul className="text-sm dark:text-white">
              <li>DevDigital</li>
              <li>801 2nd Ave.N</li>
              <li>3rd Floor</li>
              <li>Nashville, TN 37201, USA</li>
            </ul>
          </div>
          <div className="flex w-full overflow-hidden lg:w-1/2">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3220.8028446848048!2d-86.78386438477024!3d36.171351980082946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x886466f60caf6ae7%3A0xd4db82ec4192b543!2sDevDigital!5e0!3m2!1sen!2sin!4v1574155490019!5m2!1sen!2sin"
              // width="600"
              className="w-full"
              height="310"
              frameborder="0"
              style={{ border: "0" }}
              allowfullscreen=""
            ></iframe>
          </div>
        </div>
      </div>
    </>
  );
};

export default Contact;
