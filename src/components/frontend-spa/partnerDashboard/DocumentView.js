/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { FaFileDownload } from "react-icons/fa";
import { notify } from "../../../utility";
import { saveAs } from "file-saver";

const DocumentView = () => {
  const params = useParams();
  const id = params.id;
  const name = params.name;
  const [docList, setDocList] = useState([]);
  const [docPopUp, setDocPopUp] = useState(false);
  const [docLink, setDocLink] = useState("");
  const [note, setNote] = useState("");

  const fetchSelectClientDocs = (id) => {
    const bodyParameters = {
      id: id,
    };
    axios
      .post("http://localhost:5000/doc/getSelectAgentDocReq", bodyParameters)
      .then((res) => {
        console.log("res.data.docList", res.data.docList);
        setDocList(res.data.docList);
      })
      .catch((err) => console.log(err));
  };

  const downloadImage = (url) => {
    saveAs(url, "image.jpg"); // Put your image url here.
  };

  const handleReviewChange = (review, id) => {
    const data = {
      id: id,
      review: review,
    };
    const config = {
      method: "put",
      url: "http://localhost:5000/doc/setReview",
      data: data,
    };
    axios(config)
      .then((response) => {
        notify(response.data.message);
      })
      .catch((err) => console.log(err));
  };

  const handleSendNote = (id) => {
    const data = {
      id: id,
      note: note,
    };
    const config = {
      method: "put",
      url: "http://localhost:5000/doc/setNote",
      data: data,
    };
    axios(config)
      .then((response) => {
        notify(response.data.message);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    if (id) {
      fetchSelectClientDocs(id);
    }
  }, []);

  return (
    <div className="w-full min-h-screen gap-4 px-2 pt-8 pb-10 mt-14 sm:px-4 md:px-6 lg:px-10 xl:px-10 mb-14 sm:mb-0 md:mb-0 lg:mb-0 xl:mb-0 dark:bg-gray-700">
      <div className="flex flex-col">
        {docList.length > 0 && (
          <h1 className="text-xs font-semibold capitalize lg:text-lg xl:text-lg dark:text-white">
            Client Name :<span className="text-yellow-500">{name}</span>
          </h1>
        )}
        <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4">
          {docList &&
            docList.map((doc) => (
              <div
                key={doc.id}
                className="p-2 mt-3 bg-gray-200 border rounded shadow-sm dark:bg-white"
              >
                <h1 className="mt-2 text-xs font-semibold capitalize lg:text-sm xl:text-sm ">
                  Document Name :
                  <span className="text-yellow-500">{doc.documenttype}</span>
                </h1>
                <div className="flex items-center justify-between my-4">
                  <h1 className="text-xs font-semibold capitalize lg:text-sm xl:text-sm ">
                    Status :
                    <span className="text-yellow-500">
                      {doc.status === 0 ? "Not uploaded" : "Uploaded"}
                    </span>
                  </h1>
                  {doc.status === 1 && (
                    <FaFileDownload
                      className="text-2xl text-yellow-500 cursor-pointer"
                      onClick={() => {
                        downloadImage(doc.documentname);
                      }}
                    />
                  )}
                </div>
                {doc.status === 0 ? (
                  <div className="flex items-center justify-center w-full h-40 mt-2 overflow-hidden">
                    <img
                      src={process.env.PUBLIC_URL + "/images/image.png"}
                      className="object-contain w-full h-full"
                    />
                  </div>
                ) : (
                  <div className="flex items-center justify-center w-full h-40 mt-2 overflow-hidden">
                    <img
                      src={doc.documentname}
                      onClick={() => {
                        setDocPopUp(true);
                        setDocLink(doc.documentname);
                      }}
                      className="object-contain w-full h-full cursor-pointer"
                    />
                  </div>
                )}
                {doc.status === 1 && (
                  <div className="flex items-center justify-between w-full my-3 overflow-hidden">
                    <label className="w-3/12 text-xs font-semibold capitalize lg:text-sm xl:text-sm ">
                      Review
                    </label>
                    <select
                      onChange={(e) => {
                        handleReviewChange(e.target.value, doc.id);
                      }}
                      className="w-9/12 px-4 py-2 text-xs font-semibold capitalize border rounded lg:text-sm xl:text-sm"
                    >
                      <option>Select</option>
                      <option value="done">Done</option>
                      <option value="wrong">wrong</option>
                    </select>
                  </div>
                )}
                {doc.status === 1 && (
                  <div className="flex items-center justify-between w-full my-3 overflow-hidden">
                    <label className="w-3/12 text-xs font-semibold capitalize lg:text-sm xl:text-sm ">
                      Note
                    </label>
                    <textarea
                      onChange={(e) => {
                        setNote(e.target.value);
                      }}
                      className="w-9/12 p-2 rounded max-h-40"
                    ></textarea>
                  </div>
                )}
                {doc.status === 1 && (
                  <div className="flex items-center justify-end w-full my-3 overflow-hidden">
                    <button
                      onClick={() => {
                        handleSendNote(doc.id);
                      }}
                      className="bg-yellow-500 btn"
                    >
                      Send note
                    </button>
                  </div>
                )}
              </div>
            ))}
        </div>
      </div>
      {docPopUp && (
        <div className="absolute inset-0 z-50 flex flex-col items-center justify-center w-full bg-black bg-opacity-70">
          <div className="flex flex-col w-1/2 mt-10 bg-white rounded h-1/2">
            <div className="flex justify-end w-full h-10 bg-white item-center">
              <h1
                onClick={() => {
                  setDocPopUp(false);
                  setDocLink("");
                }}
                className="flex items-center justify-center w-10 h-10 text-white bg-black border-2 cursor-pointer"
              >
                x
              </h1>
            </div>
            <img
              src={docLink}
              className="object-contain w-full h-full bg-white border-2"
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default DocumentView;
