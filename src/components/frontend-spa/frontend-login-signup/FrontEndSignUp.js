/* eslint-disable no-unused-vars */
/* eslint-disable no-empty-pattern */
import React, { useState } from "react";
import { CgAsterisk } from "react-icons/cg";
import { useHistory } from "react-router-dom";
import axios from "axios";
import FormData from "form-data";

const FrontEndSignUp = ({}) => {
  const history = useHistory();
  const [fName, setFname] = useState("");
  const [fNameError, setFnameError] = useState("");

  const [lName, setLname] = useState("");

  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState("");

  const handleSignup = (e) => {
    e.preventDefault();
    let emailError = "";
    let fNameError = "";
    if (fName.trim() === "") {
      fNameError = "The first name is required.";
    }
    if (email.trim() === "") {
      emailError = "The email field is required.";
    }
    if (fNameError !== "" || emailError !== "") {
      setFnameError(fNameError);
      setEmailError(emailError);
      return;
    }
    history.push("/frontendLogin");
    // const data = new FormData();
    // data.append("first_name", fName);
    // data.append("last_name", lName);
    // data.append("email", email);
    // data.append("X-localization", "es");

    // var config = {
    //     method: "post",
    //     url: "http://laravelcms.devdigdev.com/api/v1/login",

    //     data: data,
    // };

    // axios(config)
    //     .then(function (response) {
    //         let userInfo = JSON.stringify(response.data);
    //         localStorage.setItem("userInfo", userInfo);

    //         history.push("/frontendLogin");
    //     })
    //     .catch(function (error) {
    //         console.log(error);
    //         setEmailError("Email is wrong");
    //     });
  };

  return (
    <div className="flex items-center justify-center w-full h-screen dark:bg-gray-600">
      <div className="w-9/12 p-5 rounded-lg shadow-2xl lg:w-1/3 sm:w-3/5 dark:bg-gray-500">
        <div className="flex items-center justify-center my-4 w--full">
          <h1 className="text-3xl font-normal dark:text-white">Sign Up</h1>
        </div>
        <div className="flex flex-col w-full my-4">
          <label className="flex text-sm font-normal tracking-wider item-center dark:text-white">
            First Name
            <CgAsterisk className="inline-block text-red-500" />
          </label>
          <input
            type="text"
            value={fName}
            placeholder="Enter your first name"
            onChange={(e) => {
              setFname(e.target.value);
              setFnameError("");
            }}
            onBlur={() => {
              if (fName.trim() === "") {
                setFnameError("The email field is required.");
              }
            }}
            className={`${
              fNameError && "border-red-500"
            } py-2 border-b border-gray-300  px-2 dark:bg-gray-200 rounded focus:outline-none focus:border-blue-500`}
          />
          {fNameError && (
            <span className="my-1 text-xs tracking-wider text-red-500">
              {fNameError}
            </span>
          )}
        </div>
        <div className="flex flex-col w-full my-4">
          <label className="flex text-sm font-normal tracking-wider item-center dark:text-white">
            Last Name
          </label>
          <input
            type="text"
            value={lName}
            placeholder="Enter your Last name"
            onChange={(e) => {
              setLname(e.target.value);
            }}
            className="px-2 py-2 border-b border-gray-300 rounded dark:bg-gray-200 focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="flex flex-col w-full">
          <label className="flex text-sm font-normal tracking-wider item-center dark:text-white">
            Email <CgAsterisk className="inline-block text-red-500 " />
          </label>
          <input
            type="email"
            value={email}
            placeholder="Enter your email address"
            onChange={(e) => {
              setEmail(e.target.value);
              setEmailError("");
            }}
            onBlur={() => {
              if (email.trim() === "") {
                setEmailError("The email field is required.");
              }
            }}
            className={`${
              emailError && "border-red-500"
            } py-2 border-b rounded  px-2 dark:bg-gray-200 border-gray-300 focus:outline-none focus:border-blue-500`}
          />
          {emailError && (
            <span className="my-1 text-xs tracking-wider text-red-500">
              {emailError}
            </span>
          )}
        </div>
        <div className="flex items-center justify-center w-full my-4">
          <button
            onClick={(e) => handleSignup(e)}
            type="submit"
            className="w-full h-10 text-sm tracking-wider text-white bg-blue-500 rounded shadow-lg hover:bg-blue-600 dark:bg-gray-800 dark:hover:bg-gray-700"
          >
            Sign Up
          </button>
        </div>
        <div className="flex items-center justify-center w-full">
          <button
            onClick={() => {
              history.push("/frontendLogin");
            }}
            className="text-xs tracking-wider outline-none hover:underline"
          >
            Back to the Login
          </button>
        </div>
      </div>
    </div>
  );
};

export default FrontEndSignUp;
