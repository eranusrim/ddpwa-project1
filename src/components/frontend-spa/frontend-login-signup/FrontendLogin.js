/* eslint-disable no-empty-pattern */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { CgAsterisk } from "react-icons/cg";
import { useHistory } from "react-router-dom";
import axios from "axios";
import FormData from "form-data";

const FrontendLogin = ({}) => {
  const history = useHistory();
  const [email, setEmail] = useState(
    localStorage.getItem("userEmail") ? localStorage.getItem("userEmail") : ""
  );
  const [password, setPassword] = useState("");
  const [rememberMe, setRememberMe] = useState(false);

  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  // const data = new FormData();
  // data.append("email", email);
  // data.append("password", password);
  // data.append("X-localization", "es");

  // var config = {
  //     method: "post",
  //     url: "",

  //     data: data,
  // };

  const handleLogin = (e) => {
    e.preventDefault();
    let emailError = "";
    let passwordError = "";

    if (email.trim() === "") {
      emailError = "The email field is required.";
    }
    if (password.trim() === "") {
      passwordError = "The password field is required.";
    }
    if (emailError !== "" || passwordError !== "") {
      setEmailError(emailError);
      setPasswordError(passwordError);
      return;
    }

    axios
      .post("http://localhost:5000/users/login", {
        email: email,
        password: password,
      })
      .then(function (response) {
        console.log("ffff", response.data.data);
        let userData = JSON.stringify(response.data.data);
        localStorage.setItem("userData", userData);

        history.push("/");
      })
      .catch(function (error) {
        console.log(error);
        setEmailError("Email is wrong");
        setPasswordError("Password is wrong");
      });

    if (rememberMe) {
      localStorage.setItem("userEmail", email);
    } else {
      localStorage.removeItem("userEmail");
    }
  };

  return (
    <div className="flex items-center justify-center w-full h-screen dark:bg-gray-600">
      <div className="w-9/12 p-5 rounded-lg shadow-2xl lg:w-1/3 sm:w-3/5 dark:bg-gray-500">
        <div className="flex items-center justify-center my-4 w--full">
          <h1 className="text-3xl font-normal dark:text-white">Login</h1>
        </div>
        <div className="flex flex-col w-full">
          <label className="flex text-sm font-normal tracking-wider item-center dark:text-white">
            Email <CgAsterisk className="inline-block text-red-500" />
          </label>
          <input
            type="email"
            value={email}
            placeholder="Enter your email address"
            onChange={(e) => {
              setEmail(e.target.value);
              setEmailError("");
            }}
            onBlur={() => {
              if (email.trim() === "") {
                setEmailError("The email field is required.");
              }
            }}
            className={`${
              emailError && "border-red-500"
            } py-2 px-2 dark:bg-gray-200 border-b rounded border-gray-300 focus:outline-none focus:border-blue-500`}
          />
          {emailError && (
            <span className="my-1 text-xs tracking-wider text-red-500">
              {emailError}
            </span>
          )}
        </div>
        <div className="flex flex-col w-full my-4">
          <label className="flex text-sm font-normal tracking-wider item-center dark:text-white">
            Password <CgAsterisk className="inline-block text-red-500" />
          </label>
          <input
            type="password"
            placeholder="Enter your password"
            onChange={(e) => {
              setPassword(e.target.value);
              setPasswordError("");
            }}
            onBlur={() => {
              if (password.trim() === "") {
                setPasswordError("The password field is required.");
              }
            }}
            className={`${
              passwordError && "border-red-500"
            } py-2 px-2 dark:bg-gray-200 rounded  border-b border-gray-300  focus:outline-none focus:border-blue-500`}
          />
          {passwordError && (
            <span className="my-1 text-xs tracking-wider text-red-500">
              {passwordError}
            </span>
          )}
        </div>
        <div className="flex items-center w-full">
          <input
            id="rememberMe"
            type="checkbox"
            checked={rememberMe}
            onChange={() => {
              setRememberMe(!rememberMe);
            }}
            className="mr-2"
          />
          <label
            for="rememberMe"
            className="text-sm tracking-wider cursor-pointer dark:text-white"
          >
            Remember Me
          </label>
        </div>
        <div className="flex items-center justify-center w-full my-4">
          <button
            onClick={(e) => handleLogin(e)}
            type="submit"
            className="w-full h-10 text-sm tracking-wider text-white bg-blue-500 rounded shadow-lg hover:bg-blue-600 dark:bg-gray-800 dark:hover:bg-gray-700"
          >
            Login
          </button>
        </div>
        <div className="flex items-center justify-center w-full">
          <button
            onClick={() => {
              history.push("/forgetPassword");
            }}
            className="text-xs tracking-wider outline-none hover:underline dark:text-white"
          >
            Forgot Your Password?
          </button>
        </div>
        <div className="flex items-center justify-center w-full mt-4">
          <button
            onClick={() => {
              history.push("/frontendSignUp");
            }}
            className="text-sm font-semibold tracking-wider text-blue-400 outline-none hover:underline"
          >
            Sign Up
          </button>
        </div>
      </div>
    </div>
  );
};

export default FrontendLogin;
