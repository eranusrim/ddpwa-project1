/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import axios from "axios";

const Options = (props) => {
  const [options, setOptions] = useState([]);
  const fetchOptions = () => {
    axios
      .get("http://localhost:5000/chatbot")
      .then((response) => {
        const data = response.data;
        const a = [];
        console.log(data);
        data.map((d) => {
          a.push({
            text: d.options,
            handler: props.actionProvider.handleQuiz,
            id: d.id,
          });
        });
        setOptions(a);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    fetchOptions();
  }, []);

  const buttonsMarkup =
    options &&
    options.map((option) => (
      <button
        key={option.id}
        onClick={() => {
          option.handler(option.text);
        }}
        className="px-3 py-2 m-2 text-center border border-blue-400 rounded-full"
      >
        {option.text}
      </button>
    ));

  return <div className="flex flex-wrap">{options && buttonsMarkup}</div>;
};

export default Options;
