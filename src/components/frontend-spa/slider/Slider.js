/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { SidebarData } from "../../HomePageSlider/SliderImagesData";

const ImageSlider = () => {
  const sliderData = SidebarData;
  const settings = {
    // dots: true,
    fade: true,
    infinite: true,
    slideToShow: 1,
    arrows: true,
    slideToScroll: 1,
    // className: "slides",
    className: "slider",
    autoplay: true,
    autoplaySpeed: 1500,
    lazyLoad: true,
  };

  return (
    // <div className="w-full overflow-hidden bg-white dark:bg-gray-700 lg:mt-20">
    //     <Slider {...settings}>
    //         {sliderData.map((data, i) => {
    //             if (data.activeSlider) {
    //                 return (
    //                     <div className="w-full" key={i}>
    //                         <img
    //                             src={data.image}
    //                             className="object-center w-full h-screen "
    //                         />
    //                     </div>
    //                 );
    //             }
    //         })}
    //     </Slider>
    // </div>
    <div className="">
      <Slider {...settings}>
        {sliderData.map((data, i) => {
          if (data.activeSlider) {
            return (
              <div className="" key={i}>
                <img src={data.image} className="" />
              </div>
            );
          }
        })}
      </Slider>
    </div>
  );
};

export default ImageSlider;
