/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Testimonials = () => {
  const [windowWidth, setWindowWidth] = useState(null);

  const products = [
    {
      img: "https://picsum.photos/id/0/367/267",
      title: "Dolore magna",
      text: "Lorem ipsum dolor sit amet elit. Mattis rhoncus urna neque viverra justo Mattis rhoncus urna neque viverra justo",
    },
    {
      img: "https://picsum.photos/id/1011/367/267",
      title: "Odio ut enim",
      text: "Mattis rhoncus urna neque viverra justo. Mattis rhoncus urna neque viverra justo Mattis rhoncus urna neque viverra justo",
    },
    {
      img: "https://picsum.photos/id/10/367/267",
      title: "Eget est lorem",
      text: "Lorem Ipsum adipiscing elit ipsum.Mattis rhoncus urna neque viverra justo Mattis rhoncus urna neque viverra justo",
    },
    {
      img: "https://picsum.photos/id/1001/367/267",
      title: "Tempus imperdiet",
      text: "Orci porta non pulvinar neque laoreet.Mattis rhoncus urna neque viverra justo Mattis rhoncus urna neque viverra justo",
    },
    {
      img: "https://picsum.photos/id/1011/367/267",
      title: "Odio ut enim",
      text: "Mattis rhoncus urna neque viverra justo. Mattis rhoncus urna neque viverra justo Mattis rhoncus urna neque viverra justo",
    },
  ];
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    className: "multiSlides",
    slidesToShow: 4,
    slidesToScroll: 1,
    centerMode: true, // enable center mode
    // centerPadding: "50px", // set center padding
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <div
      id="testimonials"
      className="w-full px-10 py-10 md:py-20 lg:py-20 dark:bg-gray-200"
    >
      <div className="flex items-center justify-center mb-8">
        <h1 className="text-xl text-center uppercase md:text-2xl lg:text-3xl dark:text-yellow-500">
          OUR TEAM MEMBERS
        </h1>
      </div>
      <Slider {...settings}>
        {products.map((x, i) => {
          return (
            <div
              key={i}
              className="h-48 overflow-hidden text-black bg-gray-500 rounded-sm shadow sm:h-72 md:h-72 lg:h-72 xl:h-72"
            >
              <img
                className="w-full h-24 rounded-t-sm sm:h-48 md:h-48 lg:h-48 xl:h-48"
                src={x.img}
              />
              <div className="p-3">
                <div className="text-lg font-medium text-center text-yellow-500 line-clamp-1">
                  {x.title}
                </div>
                <div className="text-sm text-white line-clamp-2">{x.text}</div>
              </div>
            </div>
          );
        })}
      </Slider>
    </div>
  );
};

export default Testimonials;
