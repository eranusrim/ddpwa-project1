/* eslint-disable no-unused-vars */
/* eslint-disable no-sequences */
/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-useless-concat */
import React, { useState, useEffect } from "react";
import { BlogsData } from "../../blogs/BlogsData";
import { useDispatch } from "react-redux";
import { useLocation, useHistory } from "react-router-dom";
import {
  FETCH_BLOGS_REQUEST,
  FETCH_BLOGS_SUCCESS,
} from "../../../Redux/actionTypes";
import BlogsPage from "./blogsPage";
import BlogsSearch from "./blogsSearch";
import BlogsCategories from "./blogsCategories";

const FrontEndBlogListing = (props) => {
  const history = useHistory();
  let dispatch = useDispatch();
  let [blogs, setBlogs] = useState([]);
  let [currentPage, setCurrentPage] = useState(1);
  let [blogPerPage, setBlogPerPage] = useState(2);

  let [categories, setCategories] = useState();
  let [activeCategory, setActiveCategory] = useState("all");

  let [search, setSearch] = useState("");
  let [searchIcon, setSearchIcon] = useState(true);

  const fetchBlogs = () => {
    dispatch({
      type: FETCH_BLOGS_REQUEST,
    });
    dispatch({
      type: FETCH_BLOGS_SUCCESS,
      payload: BlogsData,
    });
    const sorted = BlogsData.sort((a, b) => {
      let dateA = new Date(a.publishDate);
      let dateB = new Date(b.publishDate);
      return dateB - dateA;
    });
    setBlogs(sorted);
  };

  const setActiveCategoryFunc = (key) => {
    setActiveCategory(key);
  };
  const categoriesFilter = (key) => {
    history.push("/blogs");
    let blogsFilter = BlogsData.filter((val) => {
      if (val.blogCategory === key) {
        return val;
      }
    });

    console.log(blogsFilter);
    setBlogs(blogsFilter);
    setActiveCategory(key);
    setCurrentPage(1);
    setSearch("");
    setSearchIcon(true);
  };
  const setAllBlog = () => {
    history.push("/blogs");
    const sorted = BlogsData.sort((a, b) => {
      let dateA = new Date(a.publishDate);
      let dateB = new Date(b.publishDate);
      return dateB - dateA;
    });
    setBlogs(sorted);
    setActiveCategory("all");
    setCurrentPage(1);
    setSearch("");
    setSearchIcon(true);
  };

  const setBlogCategories = () => {
    var result = BlogsData.reduce(
      (acc, o) => ((acc[o.blogCategory] = (acc[o.blogCategory] || 0) + 1), acc),
      {}
    );
    setCategories(result);
  };

  const setSearchValue = (value) => {
    setSearch(value);
  };
  const handleSearch = () => {
    if (search !== "") {
      let searchData = BlogsData.filter((d) => {
        if (d.title.toLocaleLowerCase().includes(search.toLocaleLowerCase())) {
          console.log(d);
          return d;
        }
      });
      console.log("length", searchData.length);
      console.log("searchData", searchData);
      if (searchData.length === 0) {
        setSearchIcon(false);
        setBlogs([]);
        setCurrentPage(1);
      } else {
        setBlogs(searchData);
        setSearchIcon(false);
        setCurrentPage(1);
        setActiveCategory("");
      }
    }
  };
  const handleClose = () => {
    setSearch("");
    if (BlogsData) {
      const sorted = BlogsData.sort((a, b) => {
        let dateA = new Date(a.publishDate);
        let dateB = new Date(b.publishDate);
        return dateB - dateA;
      });
      setBlogs(sorted);
      setSearchIcon(!searchIcon);
      setCurrentPage(1);
      setSearch("");
      setActiveCategory("all");
      history.push("/blogs");
    }
  };
  const location = useLocation();
  const searchValue = location.search;
  useEffect(() => {
    window.scroll(0, 0);
    const searchData = new URLSearchParams(searchValue).get("search");
    if (searchData !== null) {
      setSearch(searchData);
    }

    if (location.state) {
      if (location.state.category === "all") {
        setAllBlog();
      } else {
        categoriesFilter(location.state.category);
      }
    } else if (search !== "" && searchData !== null) {
      handleSearch();
    } else {
      fetchBlogs();
    }

    setBlogCategories();
  }, [searchValue, search]);

  const indexOfLastBlog = currentPage * blogPerPage;
  const indexOfFirstBlog = indexOfLastBlog - blogPerPage;
  const currentBlogs = blogs.slice(indexOfFirstBlog, indexOfLastBlog);
  const paginate = (number) => {
    setCurrentPage(number);
  };

  return (
    <div className="flex-wrap justify-center w-full h-full pt-10 mx-auto mt-10 bg-gray-100 lg:mt-10 mb-14 sm:mb-0 md:mb-0 lg:mb-0 xl:mb-0 dark:bg-gray-700 2xl:p-10 xl:p-10 lg:p-10 md:flex md:items-start lg:flex lg:items-start">
      <h1 className="w-full my-8 text-2xl font-medium text-center capitalize dark:text-white">
        {activeCategory === "all"
          ? "All Blogs"
          : activeCategory + " " + "Blogs"}
      </h1>
      <div className="w-full px-5 lg:px:10 md:px-10 md:w-3/5 lg:w-3/5">
        <BlogsPage
          blogs={currentBlogs}
          blogPerPage={blogPerPage}
          totalBlogs={blogs.length}
          paginate={paginate}
          currentPage={currentPage}
        />

        {blogs.length === 0 && (
          <div className="w-full h-screen">
            <h1 className="w-full text-4xl dark:text-white">No blog found</h1>
          </div>
        )}
      </div>
      <div className="flex flex-wrap w-full px-5 md:w-2/6 lg:w-2/6 fle-col lg:px:10 md:px-10">
        <BlogsSearch
          search={search}
          setSearchValue={setSearchValue}
          searchIcon={searchIcon}
          handleSearch={handleSearch}
          handleClose={handleClose}
        />
        <BlogsCategories
          setAllBlog={setAllBlog}
          categories={categories}
          activeCategory={activeCategory}
          setActiveCategoryFunc={setActiveCategoryFunc}
          categoriesFilter={categoriesFilter}
        />
      </div>
    </div>
  );
};

export default FrontEndBlogListing;
