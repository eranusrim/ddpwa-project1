/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { useHistory } from "react-router-dom";

const BlogsPage = ({
  blogs,
  blogPerPage,
  totalBlogs,
  paginate,
  currentPage,
}) => {
  const history = useHistory();
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalBlogs / blogPerPage); i++) {
    pageNumbers.push(i);
  }
  return (
    <>
      {blogs &&
        blogs.map((blog) => (
          <div className="w-full mb-10 bg-gray-200 border rounded-md dark:bg-gray-800">
            <div className="w-full">
              <img
                src={blog.image}
                className="object-contain w-full rounded"
              />
            </div>
            <h1 className="w-full px-6 mt-5 text-2xl text-blue-400 capitalize line-clamp-1">
              {blog.title}
            </h1>
            <p className="w-full px-6 mt-5 text-xs font-medium text-gray-400">
              by{" "}
              <span className="px-1 font-bold text-gray-600 dark:text-white">
                {blog.authorName}
              </span>
              Posted On {blog.publishDate}
            </p>
            <p className="px-6 mt-5 line-clamp-4 dark:text-white">
              {blog.description}
            </p>
            <button
              className="px-5 py-2 mx-6 my-5 text-sm text-white bg-yellow-500 border rounded"
              onClick={() => {
                history.push("/blog/" + blog.id);
              }}
            >
              Read More...
            </button>
          </div>
        ))}
      <ul className="flex items-center justify-start my-10 space-x-2 cursor-pointer">
        {pageNumbers.map((number) => (
          <li
            key={number}
            className={`${
              currentPage === number ? "bg-yellow-500" : "bg-gray-900"
            } p-3 text-sm   text-white`}
            onClick={() => {
              paginate(number);
              window.scroll(0, 0);
            }}
          >
            {number}
          </li>
        ))}
      </ul>
    </>
  );
};

export default BlogsPage;
