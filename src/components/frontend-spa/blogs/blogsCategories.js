import React from "react";

const BlogsCategories = (props) => {
    return (
        <div className="w-full flex flex-col border my-4 rounded-md dark:bg-gray-800 bg-white">
            <div className="w-full border-b h-10 flex items-center pl-3 bg-gray-50">
                <label>Categories</label>
            </div>
            <div className="w-full flex h-20 pl-3  items-center my-2">
                <ul className="">
                    <li
                        onClick={() => {
                            props.setAllBlog();
                        }}
                        className={`${
                            props.activeCategory === "all"
                                ? "text-yellow-500 font-semibold"
                                : "text-blue-400"
                        } text-sm underline  capitalize mb-2 cursor-pointer  hover:text-blue-500`}
                    >
                        All Blogs
                    </li>
                    {props.categories &&
                        Object.entries(props.categories).map(([key, value]) => {
                            return (
                                key && (
                                    <li
                                        onClick={() => {
                                            props.categoriesFilter(key);
                                            props.setActiveCategoryFunc(key);
                                        }}
                                        className={`${
                                            props.activeCategory === key
                                                ? "text-yellow-500 font-semibold"
                                                : "text-blue-400"
                                        } text-sm underline  capitalize mb-2 cursor-pointer  hover:text-blue-500`}
                                    >
                                        {key}[{value}]
                                    </li>
                                )
                            );
                        })}
                </ul>
            </div>
        </div>
    );
};

export default BlogsCategories;
