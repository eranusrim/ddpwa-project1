/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable no-sequences */
/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { BlogsData } from "../../blogs/BlogsData";
import { CgAsterisk } from "react-icons/cg";
import {
  AiOutlineTwitter,
  AiOutlineFacebook,
  AiOutlineMail,
  AiOutlineShareAlt,
  AiOutlineLinkedin,
} from "react-icons/ai";
import { RiArrowDropLeftFill, RiArrowDropRightFill } from "react-icons/ri";
import ReCAPTCHA from "react-google-recaptcha";
import { AiOutlineSearch } from "react-icons/ai";
import {
  EmailIcon,
  FacebookIcon,
  LinkedinIcon,
  PinterestIcon,
  TwitterIcon,
  WhatsappIcon,
  HatenaIcon,
  InstapaperIcon,
  LineIcon,
  TelegramIcon,
} from "react-share";
import {
  EmailShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  PinterestShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  HatenaShareButton,
  InstapaperShareButton,
  LineShareButton,
  TelegramShareButton,
} from "react-share";

let captcha;
class BlogDescription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blogData: [],
      nextBlog: null,
      previousBlog: null,
      nextBlogName: "",
      previousBlogName: "",
      verified: false,
      verifiedError: "",
      fName: "",
      lName: "",
      comment: "",
      fNameError: "",
      lNameError: "",
      commentError: "",
      AllSocialIcon: false,

      search: "",
    };
  }

  setBlog = (id) => {
    BlogsData.map((d, index) => {
      if (d.id === id) {
        this.setState({ blogData: d });
        if (index + 1 !== BlogsData.length) {
          let nextBlog = BlogsData[index + 1];
          this.setState({ nextBlog });
          let title = nextBlog.title;
          if (title.length > 10) {
            let nextBlogName = title.substring(0, 25);
            this.setState({ nextBlogName });
          }
        }
        if (index !== 0) {
          let previousBlog = BlogsData[index - 1];
          this.setState({ previousBlog });
          let title = previousBlog.title;
          if (title.length > 10) {
            let previousBlogName = title.substring(0, 25);
            this.setState({ previousBlogName });
          }
        }
      }
    });
  };
  componentDidMount() {
    window.scroll(0, 0);
    let id = this.props.match.params.id;
    this.setBlog(id);
    this.setBlogCategories();
    console.log(`http://localhost:3000${this.props.location.pathname}`);
  }
  handleRecaptcha = (value) => {
    console.log("captcha value", value);
    this.setState({ verified: true, verifiedError: "" });
  };

  setCaptchaRef = (ref) => {
    if (ref) {
      return (captcha = ref);
    }
  };
  resetCaptcha = () => {
    // maybe set it till after is submitted
    captcha.reset();
  };
  postComment = () => {
    let fNameError = "";
    let lNameError = "";
    let commentError = "";
    let verifiedError = "";

    if (this.state.fName.trim() === "") {
      fNameError = "Enter first name";
    }
    if (this.state.lName.trim() === "") {
      lNameError = "Enter last name";
    }
    if (this.state.comment.trim() === "") {
      commentError = "Enter comment";
    }
    if (!this.state.verified) {
      verifiedError = "Please Enter Captcha";
    }
    if (
      fNameError !== "" ||
      lNameError !== "" ||
      commentError !== "" ||
      verifiedError !== ""
    ) {
      this.setState({
        fNameError,
        lNameError,
        commentError,
        verifiedError,
      });
      return;
    }
    let comment = {
      fname: this.state.fName,
      lname: this.state.lName,
      comment: this.state.comment,
    };
    this.resetCaptcha();
    this.setState({
      fName: "",
      lName: "",
      comment: "",
      verified: false,
      fNameError: "",
      lNameError: "",
      commentError: "",
      verifiedError: "",
    });
    console.log("comment", comment);
  };

  allSocialShareIcon = () => (
    <>
      <div className="flex flex-wrap items-center justify-center w-full p-32 space-x-2">
        <TwitterShareButton
          className="flex items-center justify-center h-10 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#00ACED" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
          quote="share this blog on Twitter"
        >
          <TwitterIcon size="32"></TwitterIcon>
          <span className="text-xs text-white">Twitter</span>
        </TwitterShareButton>
        <FacebookShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#3B5998" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <FacebookIcon size="32"></FacebookIcon>
          <span className="text-xs text-white">Facebook</span>
        </FacebookShareButton>
        <EmailShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#7F7F7F" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <EmailIcon size="32"></EmailIcon>
          <span className="text-xs text-white">Email</span>
        </EmailShareButton>
        <LinkedinShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#007FB1" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <LinkedinIcon size="32"></LinkedinIcon>
          <span className="text-xs text-white">Linkedin</span>
        </LinkedinShareButton>
        <PinterestShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#CB2128" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <PinterestIcon size="32"></PinterestIcon>
          <span className="text-xs text-white">Pinterest</span>
        </PinterestShareButton>
        <WhatsappShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#25D366" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <WhatsappIcon size="32"></WhatsappIcon>
          <span className="text-xs text-white">WhatsApp</span>
        </WhatsappShareButton>
        <HatenaShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#009AD9" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <HatenaIcon size="32"></HatenaIcon>
          <span className="text-xs text-white">Hatena</span>
        </HatenaShareButton>
        <InstapaperShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#1F1F1F" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <InstapaperIcon size="32"></InstapaperIcon>
          <span className="text-xs text-white">Instapaper</span>
        </InstapaperShareButton>
        <LineShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#00B800" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <LineIcon size="32"></LineIcon>
          <span className="text-xs text-white">Line</span>
        </LineShareButton>
        <TelegramShareButton
          className="flex items-center justify-center h-10 my-2 duration-100 transform rounded w-28 hover:-translate-y-1"
          style={{ backgroundColor: "#37AEE2" }}
          url={`http://localhost:3000${this.props.location.pathname}`}
        >
          <TelegramIcon size="32"></TelegramIcon>
          <span className="text-xs text-white">Telegram</span>
        </TelegramShareButton>
      </div>
      <div className="absolute z-40 w-full bottom-10 -right-5">
        <button
          className="p-2 text-sm text-white bg-yellow-500 rounded"
          onClick={() =>
            this.setState({
              AllSocialIcon: !this.state.AllSocialIcon,
            })
          }
        >
          Close
        </button>
      </div>
    </>
  );

  setBlogCategories = () => {
    var result = BlogsData.reduce(
      (acc, o) => ((acc[o.blogCategory] = (acc[o.blogCategory] || 0) + 1), acc),
      {}
    );
    this.setState({ categories: result });
  };

  render() {
    const {
      blogData,
      nextBlog,
      previousBlog,
      nextBlogName,
      previousBlogName,
      fName,
      lName,
      comment,
      fNameError,
      lNameError,
      commentError,
      verifiedError,
    } = this.state;
    return (
      <>
        <div className="flex-wrap justify-center w-full h-full pt-10 mx-auto mt-10 bg-gray-100 lg:mt-10 mb-14 sm:mb-0 md:mb-0 lg:mb-0 xl:mb-0 dark:bg-gray-700 2xl:p-10 xl:p-10 lg:p-10 md:flex md:items-start lg:flex lg:items-start">
          <h1 className="w-full my-8 text-2xl font-medium text-center capitalize dark:text-white ">
            Blog
          </h1>
          <div className="w-full px-5 lg:px:10 md:px-10 md:w-3/5 lg:w-3/5">
            <div className="w-full mb-10 bg-gray-200 border rounded-md dark:bg-gray-800 border-box">
              <h1 className="w-full px-6 mt-5 text-2xl text-blue-400 capitalize line-clamp-1">
                {blogData.title}
              </h1>
              <p className="w-full px-6 mt-5 mb-5 text-xs font-medium text-gray-400">
                by{" "}
                <span className="px-1 font-bold text-gray-600 dark:text-white">
                  {blogData.authorName}
                </span>
                Posted On {blogData.publishDate}
              </p>
              <div className="w-full">
                <img src={blogData.image} className="object-contain w-full" />
              </div>

              <p className="px-6 my-5 dark:text-white">
                {blogData.description}
              </p>
            </div>
            <div className="flex justify-center w-full px-6 py-3 my-4 space-x-2 border-t-2 border-b-2 border-gray-400">
              <TwitterShareButton
                className="flex items-center justify-center w-24 duration-100 transform rounded hover:-translate-y-1"
                style={{ backgroundColor: "#00ACED" }}
                url={`http://localhost:3000${this.props.location.pathname}`}
                quote="share this blog on Twitter"
              >
                <TwitterIcon size="32"></TwitterIcon>
              </TwitterShareButton>
              <FacebookShareButton
                className="flex items-center justify-center w-24 duration-100 transform rounded hover:-translate-y-1"
                style={{ backgroundColor: "#3B5998" }}
                url={`http://localhost:3000${this.props.location.pathname}`}
              >
                <FacebookIcon size="32"></FacebookIcon>
              </FacebookShareButton>
              <EmailShareButton
                className="flex items-center justify-center w-24 duration-100 transform rounded hover:-translate-y-1"
                style={{ backgroundColor: "#7F7F7F" }}
                url={`http://localhost:3000${this.props.location.pathname}`}
              >
                <EmailIcon size="32"></EmailIcon>
              </EmailShareButton>

              <button
                onClick={() =>
                  this.setState({
                    AllSocialIcon: !this.state.AllSocialIcon,
                  })
                }
                class="flex w-24 items-center justify-center rounded bg-green-400 transform duration-100 hover:-translate-y-1"
              >
                <AiOutlineShareAlt color="white" size={28} />
              </button>
              <LinkedinShareButton
                className="flex items-center justify-center w-24 duration-100 transform rounded hover:-translate-y-1"
                style={{ backgroundColor: "#007FB1" }}
                url={`http://localhost:3000${this.props.location.pathname}`}
              >
                <LinkedinIcon size="32"></LinkedinIcon>
              </LinkedinShareButton>
              <PinterestShareButton
                className="flex items-center justify-center w-24 duration-100 transform rounded hover:-translate-y-1"
                style={{ backgroundColor: "#CB2128" }}
                url={`http://localhost:3000${this.props.location.pathname}`}
              >
                <PinterestIcon size="32"></PinterestIcon>
              </PinterestShareButton>
            </div>

            <div className="flex justify-between w-full px-6 py-3 my-4">
              {previousBlog && previousBlog.id !== blogData.id && (
                <button
                  className="flex items-center h-10 px-2 py-1 text-xs border-2 border-blue-400 rounded dark:bg-white"
                  onClick={() => {
                    this.setBlog(previousBlog.id);
                    this.props.history.replace({
                      pathname: `/blog/${previousBlog.id}`,
                    });
                  }}
                >
                  <RiArrowDropLeftFill size={30} className="text-blue-400" />
                  {previousBlogName}
                </button>
              )}
              {nextBlog && nextBlog.id !== blogData.id && (
                <button
                  className="flex items-center h-10 px-1 py-1 ml-auto overflow-hidden text-xs border-2 border-blue-400 rounded dark:bg-white"
                  onClick={() => {
                    this.setBlog(nextBlog.id);
                    this.props.history.replace({
                      pathname: `/blog/${nextBlog.id}`,
                    });
                  }}
                >
                  {nextBlogName}
                  <RiArrowDropRightFill size={30} className="text-blue-400" />
                </button>
              )}
            </div>
            <div className="w-full px-6 py-3 my-4 bg-white border rounded dark:bg-gray-700">
              <h1 className="w-full text-lg dark:text-white">Post Comment</h1>
              <div className="flex flex-col justify-between w-full mt-3 sm:flex-row lg:flex-row xl:flex-row">
                <div className="flex flex-col w-full sm:w-5/12 lg:w-5/12 xl:w-5/12">
                  <label className="text-sm dark:text-white">
                    First Name
                    <CgAsterisk className="inline text-red-500" />
                  </label>
                  <input
                    type="text"
                    value={fName}
                    onChange={(e) => {
                      this.setState({
                        fName: e.target.value,
                        fNameError: "",
                      });
                    }}
                    className="w-full h-10 mt-2 border-2 rounded"
                  />
                  {fNameError && (
                    <span className="text-xs text-red-500">{fNameError}</span>
                  )}
                </div>
                <div className="flex flex-col w-full sm:w-5/12 lg:w-5/12 xl:w-5/12">
                  <label className="text-sm dark:text-white">
                    Last Name
                    <CgAsterisk className="inline text-red-500" />
                  </label>
                  <input
                    value={lName}
                    onChange={(e) => {
                      this.setState({
                        lName: e.target.value,
                        lNameError: "",
                      });
                    }}
                    type="text"
                    className="w-full h-10 mt-2 border-2 rounded"
                  />
                  {lNameError && (
                    <span className="text-xs text-red-500">{lNameError}</span>
                  )}
                </div>
              </div>
              <div className="flex flex-col justify-between w-full mt-3">
                <label className="text-sm dark:text-white">
                  Comment
                  <CgAsterisk className="inline text-red-500" />
                </label>
                <textarea
                  value={comment}
                  onChange={(e) => {
                    this.setState({
                      comment: e.target.value,
                      commentError: "",
                    });
                  }}
                  className="w-full h-20 mt-2 border-2 rounded"
                ></textarea>
                {commentError && (
                  <span className="text-xs text-red-500">{commentError}</span>
                )}
              </div>
              <div className="w-full mt-3 overflow-hidden">
                <ReCAPTCHA
                  ref={(r) => this.setCaptchaRef(r)}
                  sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
                  onChange={this.handleRecaptcha}
                />
                {verifiedError && (
                  <span className="text-xs tracking-wider text-red-500">
                    {verifiedError}
                  </span>
                )}
              </div>
              <div className="w-full mt-3 ">
                <button
                  onClick={() => this.postComment()}
                  className="px-3 py-2 text-sm text-white bg-blue-400 rounded"
                >
                  Submit
                </button>
              </div>
            </div>

            {blogData.comment && blogData.comment.length > 0 && (
              <div className="w-full my-4 text-2xl dark:text-white">
                Read Comments
              </div>
            )}
            {blogData.comment &&
              blogData.comment.length > 0 &&
              blogData.comment.map((c) => (
                <div className="flex flex-col w-full px-2 py-3 my-4 bg-white border rounded dark:bg-gray-700">
                  <div className="flex justify-between w-full">
                    <h1 className="capitalize dark:text-white">
                      {c.name}{" "}
                      <span className="text-xs text-gray-400">says</span>
                    </h1>
                    <h1 className="text-sm dark:text-white">{c.createdOn}</h1>
                  </div>
                  <h1 className="mt-2 dark:text-gray-300">{c.comment}</h1>
                </div>
              ))}

            <div className="flex justify-between w-full py-3 my-4">
              <button
                className="px-3 py-2 text-sm text-white bg-gray-700 rounded dark:bg-white dark:text-gray-700"
                onClick={() => {
                  this.props.history.push("/blogs");
                }}
              >
                Back to blog list
              </button>
            </div>
          </div>
          <div className="flex flex-wrap w-full px-5 md:w-2/6 lg:w-2/6 fle-col lg:px:10 md:px-10">
            <div className="flex flex-col w-full mb-4 bg-white border rounded-md dark:bg-gray-800">
              <div className="flex items-center w-full h-10 pl-3 border-b bg-gray-50">
                <label>Search</label>
              </div>
              <div className="flex items-center w-full h-20 pl-3">
                <input
                  type="text"
                  value={this.state.search}
                  onChange={(e) => {
                    this.setState({
                      search: e.target.value,
                    });
                  }}
                  className="w-3/4 h-10 px-3 border rounded-tl-md rounded-bl-md"
                />

                <button
                  onClick={() => {
                    // props.handleSearch();
                    if (this.state.search !== "") {
                      this.props.history.push(
                        "/blogs?search=" + this.state.search
                      );
                    }
                  }}
                  className="flex items-center justify-center w-10 h-10 bg-gray-200 border-t border-b border-r cursor-pointer rounded-tr-md rounded-br-md"
                >
                  <AiOutlineSearch />
                </button>
              </div>
            </div>
            <div className="flex flex-col w-full my-4 bg-white border rounded-md dark:bg-gray-800">
              <div className="flex items-center w-full h-10 pl-3 border-b bg-gray-50">
                <label>Categories</label>
              </div>
              <div className="flex items-center w-full h-20 pl-3 my-2">
                <ul className="">
                  <li
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/blogs",
                        state: {
                          category: "all",
                        },
                      });
                    }}
                    className="mt-2 mb-2 text-sm text-blue-400 underline capitalize cursor-pointer hover:text-blue-500"
                  >
                    All Blogs
                  </li>
                  {this.state.categories &&
                    Object.entries(this.state.categories).map(
                      ([key, value]) => {
                        return (
                          key && (
                            <li
                              onClick={() => {
                                this.props.history.push({
                                  pathname: "/blogs",
                                  state: {
                                    category: key,
                                  },
                                });
                              }}
                              className="mb-2 text-sm text-blue-400 underline capitalize cursor-pointer hover:text-blue-500"
                            >
                              {key}[{value}]
                            </li>
                          )
                        );
                      }
                    )}
                </ul>
              </div>
            </div>
          </div>
        </div>
        {this.state.AllSocialIcon === true && (
          <div className="fixed inset-0 z-20 flex items-center justify-center h-screen bg-black bg-opacity-70">
            {this.allSocialShareIcon()}
          </div>
        )}
      </>
    );
  }
}

export default withRouter(BlogDescription);
