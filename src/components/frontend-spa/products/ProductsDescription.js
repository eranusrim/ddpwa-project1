/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import ReactImageMagnify from "react-image-magnify";
import { useDispatch } from "react-redux";
import { addToCart } from "../../../Redux/actions/cartActions";
import { __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED } from "react-dom/cjs/react-dom.development";

const ProductsDescription = (props) => {
  let dispatch = useDispatch();
  const [data, setData] = useState(
    __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED
  );
  const [mainImg, setMainImg] = useState("");
  const [colors, setColors] = useState([]);
  const [sizes, setSize] = useState([]);
  const params = useParams();
  const id = params.id;
  const category = params.category;
  const subCategory = params.subCategory;

  const getClothDetails = async () => {
    if (params.id) {
      await axios
        .get("http://localhost:5000/products/" + id)
        .then((res) => {
          if (res.status === 200) {
            let a = [];
            let b = [];
            res.data.product[0].attribute.map((att) => {
              if (!a.includes(att.color)) {
                a.push(att.color);
              }
              if (!b.includes(att.size)) {
                b.push(att.size);
              }
            });
            setSize(b);
            setColors(a);
            setData(res.data.product[0]);
            setMainImg(res.data.product[0].image);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const getMobileDetails = async () => {
    if (params.id) {
      await axios
        .get("http://localhost:5000/products/mobiles/" + id)
        .then((res) => {
          if (res.status === 200) {
            let color = res.data.mobile[0].color;
            let a = [];
            a.push(color);
            setColors(a);
            setData(res.data.mobile[0]);
            setMainImg(res.data.mobile[0].image);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const getLaptopDetails = async () => {
    if (params.id) {
      await axios
        .get("http://localhost:5000/products/laptops/" + id)
        .then((res) => {
          if (res.status === 200) {
            let color = res.data.laptop[0].color;
            let a = [];
            a.push(color);
            setColors(a);
            setData(res.data.laptop[0]);
            setMainImg(res.data.laptop[0].image);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const imageProps = {
    smallImage: {
      alt: data.title,
      isFluidWidth: true,
      src: mainImg,
    },
    largeImage: {
      src: mainImg,
      width: 1200,
      height: 1800,
    },
    enlargedImageContainerStyle: { background: "#fff", zIndex: 9 },
  };
  useEffect(() => {
    window.scroll(0, 0);
    if (category === "clothes") {
      getClothDetails();
    }
    if (category === "electronics" && subCategory === "mobiles") {
      getMobileDetails();
    }
    if (category === "electronics" && subCategory === "laptop") {
      getLaptopDetails();
    }
  }, []);
  return (
    <div className="grid w-full min-h-screen grid-cols-1 px-10 pt-16 pb-16 overflow-hidden bg-gray-100 mt-14 dark:bg-gray-700 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2">
      <div className="flex flex-col px-2 pb-20">
        <ReactImageMagnify {...imageProps} />

        <div className="grid w-full h-20 grid-flow-col grid-cols-4 gap-2 mt-10 sm:grid-cols-6 lg:grid-cols-6">
          {data && (
            <img
              onClick={() => {
                setMainImg(mainImg);
              }}
              src={mainImg}
              className="object-contain w-20 h-full bg-white border-2 border-gray-400 rounded cursor-pointer hover:border-gray-700"
            />
          )}
          {data &&
            data.images &&
            data.images.length > 0 &&
            data.images.map((i) => (
              <img
                onClick={() => {
                  setMainImg(i.imageUrl);
                }}
                src={i.imageUrl}
                className="object-contain w-20 h-full bg-white border-2 border-gray-400 rounded cursor-pointer hover:border-gray-700"
              />
            ))}
        </div>
      </div>
      <div className="pt-2 pl-0 mt-20 sm:pl-10 lg:pl-10 xl:pl-10 sm:mt-0 md:mt-0 lg:mt-0 xl:mt-0">
        <h1 className="w-full text-xl font-medium dark:text-white">
          {data.title}
        </h1>
        <p className="w-full mt-6 text-sm font-medium text-gray-500 dark:text-gray-300">
          {data.description}
        </p>
        <p className="w-full mt-6 text-lg font-semibold text-yellow-500">
          ${data.price}
        </p>
        <div className="flex w-full mt-2 space-x-2">
          {colors.length > 0 &&
            colors.map((color) => (
              <div
                style={{ background: color }}
                className="w-4 h-4 rounded-full cursor-pointer"
              ></div>
            ))}
        </div>
        <div className="flex w-full mt-2 space-x-4">
          {sizes.length > 0 &&
            sizes.map((size) => (
              <div className="flex items-center justify-center w-6 h-6 text-xs font-semibold bg-gray-200 border border-yellow-500 rounded cursor-pointer">
                {size}
              </div>
            ))}
        </div>
        <div className="w-full mt-6">
          <button
            onClick={() => {
              dispatch(addToCart(data));
            }}
            className="px-4 py-2 text-sm font-normal text-white bg-black hover:text-yellow-500"
          >
            Add to cart
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductsDescription;
