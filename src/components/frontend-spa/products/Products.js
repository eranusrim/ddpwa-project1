/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/heading-has-content */
import React, { useState, useEffect } from "react";
import axios from "axios";
import Product from "./Product";

const Products = () => {
  const [clothes, setClothes] = useState([]);
  const [mobiles, setMobiles] = useState([]);
  const [laptops, setLaptops] = useState([]);
  const [loading, setLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const [category, setCategory] = useState("");
  const [subCategory, setSubCategory] = useState("");

  const fetchClothes = () => {
    axios
      .get("http://localhost:5000/products")
      .then(function (response) {
        if (response.status === 200) {
          if (category === "clothes") {
            setProducts(response.data.products);
            setLoading(false);
          } else {
            setClothes(response.data.products);
            setLoading(false);
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const fetchMobiles = () => {
    axios
      .get("http://localhost:5000/products/getMobiles")
      .then(function (response) {
        if (response.status === 200) {
          if (subCategory === "mobiles") {
            setProducts(response.data.mobiles);
            setLoading(false);
          } else {
            setMobiles(response.data.mobiles);
            setLoading(false);
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const fetchLaptops = () => {
    axios
      .get("http://localhost:5000/products/getLaptops")
      .then(function (response) {
        if (response.status === 200) {
          if (subCategory === "laptop") {
            setProducts(response.data.laptops);
            setLoading(false);
          } else {
            setLaptops(response.data.laptops);
            setLoading(false);
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const fetchCategory = () => {
    if (category === "clothes") {
      fetchClothes();
    } else if (subCategory === "mobiles") {
      fetchMobiles();
    } else if (subCategory === "laptop") {
      fetchLaptops();
    }
  };
  useEffect(() => {
    window.scroll(0, 0);
    fetchClothes();
    fetchMobiles();
    fetchLaptops();
  }, [subCategory, category]);
  return (
    <>
      <div className="w-full h-full px-2 pt-16 pb-6 mt-0 sm:mt-6 sm:px-10 md:px-10 lg:px-10 xl:px-10">
        {loading ? (
          <div className="grid w-full grid-cols-2 gap-2 my-6 sm:gap-6 md:gap-6 lg:gap-6 xl:gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
            {Array(15)
              .fill()
              .map(() => (
                <div className="w-full overflow-hidden bg-gray-300 rounded animate-pulse ">
                  <div className="w-full h-24 p-2 bg-white sm:h-48 md:h-48 lg:h-48 xl:h-48">
                    <img
                      src={"/images/image.png"}
                      className="object-contain w-full h-full"
                    />
                  </div>
                  <div className="flex items-center justify-center w-full p-2 sm:p-4 lg:p-4 xl:p-4">
                    <h1 className="w-full overflow-hidden text-xs font-medium line-clamp-2 sm:line-clamp-1 md:line-clamp-1 lg:line-clamp-1 xl:line-clamp-1 sm:text-sm md:text-sm lg:text-sm xl:text-sm"></h1>
                    <h1 className="font-medium text-yellow-500"></h1>
                  </div>
                  <div className="flex items-center w-full px-0 py-2 space-x-2 justify-evenly sm:justify-end md:justify-end lg:justify-end xl:justify-end sm:p-4 lg:p-4 xl:p-4">
                    <button className="px-1 py-1 text-xs font-normal text-yellow-500 bg-white border-2 border-yellow-500 rounded sm:px-2 md:px-2 lg:px-2 xl:px-2 hover:bg-yellow-500 hover:text-white sm:text-sm md:text-sm lg:text-sm xl:text-sm sm:font-medium">
                      Add To Cart
                    </button>
                    <button className="px-1 py-1 text-xs font-normal text-white bg-yellow-500 border-2 border-transparent rounded sm:px-2 md:px-2 lg:px-2 xl:px-2 hover:bg-white hover:text-yellow-500 hover:border-yellow-500 sm:text-sm md:text-sm lg:text-sm xl:text-sm sm:font-medium">
                      View more
                    </button>
                  </div>
                </div>
              ))}
          </div>
        ) : (
          <div className="flex flex-col w-full sm:flex-col md:flex-col lg:flex-row xl:flex-row">
            <div className="w-full p-3 mt-4 mb-2 overflow-hidden bg-white rounded shadow lg:w-1/4 lg:mr-2 lg:my-6 dark:bg-gray-600">
              <div className="w-full p-4 bg-gray-300 border rounded">
                <h1 className="w-full my-4 text-xl font-medium text-center capitalize dark:text-black">
                  Categories
                </h1>
                <div className="flex items-center justify-between w-full my-1">
                  <label className="w-1/2 mr-2 text-sm font-medium">
                    Clothes
                  </label>
                  <select
                    onChange={(e) => {
                      setCategory("clothes");
                      setSubCategory(e.target.value);
                      fetchCategory();
                    }}
                    className="w-1/2 h-8 border rounded"
                  >
                    <option>select</option>
                    <option values="mens">Mens</option>
                    <option values="women">Women</option>
                    <option values="Kids">Kids</option>
                  </select>
                </div>
                <div className="flex items-center justify-between w-full my-1">
                  <label className="w-1/2 text-sm font-medium">
                    Electronics
                  </label>
                  <select
                    onChange={(e) => {
                      setCategory("electronics");
                      setSubCategory(e.target.value);
                      fetchCategory();
                    }}
                    className="w-1/2 h-8 border rounded"
                  >
                    <option>select</option>
                    <option values="mobiles">mobiles</option>
                    <option values="laptop">laptop</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="w-full lg:w-10/12 ">
              {category === "" && clothes && clothes.length > 0 && (
                <div className="w-full p-3 my-6 bg-gray-100 rounded dark:bg-gray-600">
                  <h1 className="w-full my-4 text-2xl font-medium text-center capitalize dark:text-white">
                    Clothes
                  </h1>
                  <div className="grid w-full grid-cols-2 gap-2 sm:gap-6 md:gap-6 lg:gap-6 xl:gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
                    {clothes.map((p) => (
                      <Product
                        product={p}
                        category="clothes"
                        subCategory={p.category}
                      />
                    ))}
                  </div>
                </div>
              )}
              {category === "" && mobiles && mobiles.length > 0 && (
                <div className="w-full p-3 my-6 bg-gray-100 rounded dark:bg-gray-600">
                  <h1 className="w-full my-4 text-2xl font-medium text-center capitalize dark:text-white">
                    Mobiles
                  </h1>
                  <div className="grid w-full grid-cols-2 gap-2 sm:gap-6 md:gap-6 lg:gap-6 xl:gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
                    {mobiles.map((p) => (
                      <Product
                        product={p}
                        category="electronics"
                        subCategory="mobiles"
                      />
                    ))}
                  </div>
                </div>
              )}
              {category === "" && laptops && laptops.length > 0 && (
                <div className="w-full p-3 my-6 bg-gray-100 rounded dark:bg-gray-600">
                  <h1 className="w-full my-4 text-2xl font-medium text-center capitalize dark:text-white">
                    Laptops
                  </h1>
                  <div className="grid w-full grid-cols-2 gap-2 sm:gap-6 md:gap-6 lg:gap-6 xl:gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
                    {laptops.map((p) => (
                      <Product
                        product={p}
                        category="electronics"
                        subCategory="laptop"
                      />
                    ))}
                  </div>
                </div>
              )}

              {category !== "" && products && products.length > 0 && (
                <div className="w-full p-3 my-6 bg-gray-100 rounded dark:bg-gray-600">
                  <h1 className="w-full my-4 text-2xl font-medium text-center capitalize dark:text-white">
                    {category}
                  </h1>
                  <div className="grid w-full grid-cols-2 gap-2 sm:gap-6 md:gap-6 lg:gap-6 xl:gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
                    {products.map((p) => (
                      <Product
                        product={p}
                        category={category}
                        subCategory={subCategory}
                      />
                    ))}
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Products;
