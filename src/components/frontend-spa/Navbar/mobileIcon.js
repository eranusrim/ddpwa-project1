import React from "react";
import { NavLink } from "react-router-dom";
import { FaHome, FaBlogger } from "react-icons/fa";
import { RiShoppingBag3Line, RiContactsBook2Fill } from "react-icons/ri";
import { useHistory } from "react-router-dom";

const MobileIconMenu = () => {
    let history = useHistory();
    return (
        <div className="flex w-full sm:hidden md:hidden lg:hidden xl:hidden fixed bottom-0 z-10 dark:bg-gray-800 bg-white h-14 justify-evenly items-center shadow-xl">
            <NavLink
                exact
                to="/"
                activeClassName="text-blue-400 dark:text-blue-400"
            >
                <FaHome size="20" />
            </NavLink>
            <NavLink
                to="/blogs"
                activeClassName="text-blue-400 dark:text-blue-400"
            >
                <FaBlogger size="20" />
            </NavLink>
            <NavLink
                to="/products"
                activeClassName="text-blue-400 dark:text-blue-400"
            >
                <RiShoppingBag3Line size="20" />
            </NavLink>
            <a href="#contact">
                <RiContactsBook2Fill
                    size="20"
                    onClick={() => {
                        history.push("/");
                    }}
                />
            </a>
        </div>
    );
};

export default MobileIconMenu;
