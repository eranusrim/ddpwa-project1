/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from "react";
import { HiOutlineLightBulb } from "react-icons/hi";
import { GiHamburgerMenu } from "react-icons/gi";
import {
  AiOutlineLogin,
  AiOutlineTwitter,
  AiFillLinkedin,
} from "react-icons/ai";
import useDarkMode from "../../../useDarkMode";
import { Link, useHistory } from "react-router-dom";
import { FaChevronDown, FaUserAlt, FaFacebookSquare } from "react-icons/fa";
import { GiShoppingCart } from "react-icons/gi";
import axios from "axios";
import { useSelector } from "react-redux";

const Front_spa_Navbar = () => {
  let history = useHistory();
  const [colorTheme, setTheme] = useDarkMode();
  const [windowSize, setWindowSize] = useState("");

  const [menu, setMenu] = useState(false);

  const [cartPop, setCartPop] = useState(false);

  const cart = useSelector((state) => state.CartReducer.cartItems);

  window.addEventListener("resize", () => {
    setWindowSize(window.innerWidth);
  });
  if (windowSize > 768) {
    let navLink = document.getElementById("navLink");
    navLink.classList.remove("hidden");
  }
  let access_token;
  let userData = JSON.parse(localStorage.getItem("userData"));

  if (userData) {
    access_token = userData.access_token;
  }

  const logOut = () => {
    localStorage.removeItem("userData");
    setMenu(false);
    history.push("/");
    window.location.reload();
  };

  useEffect(() => {}, [windowSize, cart]);
  return (
    <div className="fixed z-20 flex flex-col items-center justify-between w-full px-3 py-2 bg-gray-100 shadow-lg xl:h-20 lg:h-20 md:h-20 dark:bg-gray-800 border-box md:flex-row lg:flex-row">
      <Link
        to="/"
        onClick={() => {
          let navLink = document.getElementById("navLink");
          if (window.innerWidth < 768) {
            navLink.classList.add("hidden");
          }
        }}
        className="flex items-center w-full md:w-auto lg:w-auto h-14"
      >
        <img
          onClick={() => window.scroll(0, 0)}
          className="object-contain w-auto h-6 sm:h-14 md:h-14 lg:h-14 xl:h-14"
          src={"/images/header-logo.png"}
        />
      </Link>
      <div className="flex flex-col items-center justify-center w-full md:w-auto md:flex-row md:mt-0 lg:flex-row lg:w-auto lg:mt-0">
        <ul
          id="navLink"
          className="flex flex-col items-center justify-center order-2 w-full mt-5 text-black capitalize dark:text-white md:space-x-1 md:order-1 md:mt-0 md:flex-row lg:space-x-3 lg:order-1 lg:mt-0 lg:flex-row"
        >
          <li className="w-full my-4 border-b md:border-none md:w-auto md:my-0 lg:border-none lg:w-auto lg:my-0">
            <a
              href="#about"
              className="px-3 py-1 text-sm font-normal tracking-wide rounded-full hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white"
              to="/frontend-spa/about"
              onClick={() => {
                history.push("/");
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
            >
              About
            </a>
          </li>
          <li className="w-full my-4 border-b md:border-none md:w-auto md:my-0 lg:border-none lg:w-auto lg:my-0">
            <a
              href="#calender"
              onClick={() => {
                history.push("/");
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              className="px-3 py-1 text-sm font-normal tracking-wide rounded-full hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white"
            >
              Calender
            </a>
          </li>
          <li className="w-full my-4 border-b md:border-none md:w-auto md:my-0 lg:border-none lg:w-auto lg:my-0">
            <a
              href="#testimonials"
              onClick={() => {
                history.push("/");
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              className="px-3 py-1 text-sm font-normal tracking-wide rounded-full hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white"
            >
              Testimonials
            </a>
          </li>
          <li className="hidden w-full my-4 border-b sm:flex md:flex lg:flex xl:flex md:border-none md:w-auto md:my-0 lg:border-none lg:w-auto lg:my-0">
            <a
              onClick={() => {
                history.push("/");
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              href="#contact"
              className="px-3 py-1 text-sm font-normal tracking-wide rounded-full hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white"
            >
              contact
            </a>
          </li>
          <li className="hidden w-full my-4 border-b sm:flex md:flex lg:flex xl:flex md:border-none md:w-auto md:my-0 lg:border-none lg:w-auto lg:my-0">
            <Link
              to="/blogs"
              onClick={() => {
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              className="px-3 py-1 text-sm font-normal tracking-wide rounded-full hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white"
            >
              Blogs
            </Link>
          </li>
          <li className="hidden w-full my-4 border-b sm:flex md:flex lg:flex xl:flex md:border-none md:w-auto md:my-0 lg:border-none lg:w-auto lg:my-0">
            <Link
              to="/products"
              onClick={() => {
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              className="px-3 py-1 text-sm font-normal tracking-wide rounded-full hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white"
            >
              Products
            </Link>
          </li>

          <li className="w-full my-4 border-b md:w-auto md:my-0 lg:w-auto lg:my-0 md:border-none lg:border-none xl:border-none">
            <p
              onClick={() => {
                history.push("/galleries");
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              className="px-3 py-1 text-sm rounded-full cursor-pointer hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white "
            >
              Galleries
            </p>
          </li>
          <li className="w-full my-4 border-b md:w-auto md:my-0 lg:w-auto lg:my-0 md:border-none lg:border-none xl:border-none">
            <p
              onClick={() => {
                history.push("/faqs");
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              className="px-3 py-1 text-sm rounded-full cursor-pointer hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white "
            >
              FAQS
            </p>
          </li>
          {userData && userData.user_type_id === 0 && (
            <li className="w-full my-4 border-b md:w-auto md:my-0 lg:w-auto lg:my-0 md:border-none lg:border-none xl:border-none">
              <p
                onClick={() => {
                  history.push("/clientDashboard");
                  let navLink = document.getElementById("navLink");
                  if (window.innerWidth < 768) {
                    navLink.classList.add("hidden");
                  }
                }}
                className="px-3 py-1 text-sm rounded-full cursor-pointer hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white "
              >
                Dashboard
              </p>
            </li>
          )}
          {userData && userData.user_type_id === 1 && (
            <li className="w-full my-4 border-b md:w-auto md:my-0 lg:w-auto lg:my-0 md:border-none lg:border-none xl:border-none">
              <p
                onClick={() => {
                  history.push("/PartnerDashboard");
                  let navLink = document.getElementById("navLink");
                  if (window.innerWidth < 768) {
                    navLink.classList.add("hidden");
                  }
                }}
                className="px-3 py-1 text-sm rounded-full cursor-pointer hover:text-yellow-500 md:hover:bg-yellow-500 md:hover:text-white lg:hover:bg-yellow-500 lg:hover:text-white "
              >
                Dashboard
              </p>
            </li>
          )}
          <li className="w-full my-4 border-b md:w-auto md:my-0 lg:w-auto lg:my-0 md:border-none lg:border-none xl:border-none">
            <p
              onClick={() => {
                if (cart.length === 0) {
                  setCartPop(!cartPop);
                } else {
                  history.push("/cart");
                }
                let navLink = document.getElementById("navLink");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              className="relative px-1 py-1 cursor-pointer "
            >
              <GiShoppingCart size="27" className="text-yellow-500" />
              {cart.length > 0 && (
                <p className="absolute top-0 flex items-center justify-center w-4 h-4 text-xs text-white bg-yellow-600 rounded-full left-3 sm:right-0 md:right-0 lg:right-0 xl:right-0">
                  {cart.length}
                </p>
              )}
            </p>
          </li>
          <li className="flex justify-center w-full my-4 border-b md:w-auto md:my-0 lg:w-auto lg:my-0 md:border-none lg:border-none xl:border-none">
            <a href="http://www.facebook.com/" target="_blank" className="px-2">
              <FaFacebookSquare size="27" color="#4267B2" />
            </a>
            <a
              href="https://twitter.com/Dev_Digital"
              target="_blank"
              className="px-2"
            >
              <AiOutlineTwitter size="27" color="#1DA1F2" />
            </a>
            <a
              href="https://www.linkedin.com/company/dev-digital-llc-"
              target="_blank"
              className="px-2"
            >
              <AiFillLinkedin size="27" color="#0e76a8" />
            </a>
          </li>
          {!userData && (
            <li
              onClick={() => {
                history.push("/frontendLogin");
                let navLink = document.getElementById("navLink");
                navLink.classList.toggle("hidden");
                if (window.innerWidth < 768) {
                  navLink.classList.add("hidden");
                }
              }}
              className="flex flex-col w-full mt-4 cursor-pointer lg:hidden md:hidden xl:hidden"
            >
              <p className="w-full py-2 font-semibold text-center text-gray-900 bg-yellow-500">
                Login
              </p>
            </li>
          )}

          {userData ? (
            <>
              <li
                onClick={() => setMenu(!menu)}
                className="relative flex-col items-start hidden w-full my-4 border-b cursor-pointer lg:flex md:flex xl:flex xl:items-center lg:items-center md:items-center md:border-none md:w-auto md:my-0 lg:border-none lg:w-auto lg:my-0"
              >
                <div
                  onClick={() => setMenu(!menu)}
                  className="flex items-center px-1"
                >
                  <p className="py-1 pr-1 text-sm font-normal tracking-wide rounded-full ">
                    {userData.first_name} {userData.last_name}
                  </p>
                  <div className="flex items-center justify-center w-8 h-8 mr-1 overflow-hidden bg-white rounded-full shadow">
                    {userData.profile_image ? (
                      <img
                        className="object-contain w-full"
                        src={`${userData.profile_image}`}
                      />
                    ) : (
                      <FaUserAlt className="dark:text-gray-700" />
                    )}
                  </div>
                  <FaChevronDown />
                </div>
                {menu && (
                  <ul className="relative flex flex-col items-center w-full my-4 bg-gray-400 xl:my-0 lg:my-0 xl:absolute lg:absolute md:absolute dark:bg-gray-600 lg:top-14 md:top-14 xl:top-14">
                    <li
                      onClick={() => {
                        logOut();
                        let navLink = document.getElementById("navLink");

                        // navLink.classList.toggle(
                        //     "hidden"
                        // );
                      }}
                      className="flex items-center justify-center w-full py-2 hover:bg-yellow-500 hover:text-white"
                    >
                      <p>Logout</p>
                    </li>
                  </ul>
                )}
              </li>
              <li
                onClick={() => {
                  logOut();
                  let navLink = document.getElementById("navLink");
                  // navLink.classList.toggle("hidden");
                }}
                className="flex flex-col w-full mt-4 cursor-pointer lg:hidden md:hidden xl:hidden"
              >
                <p className="w-full py-2 font-semibold text-center text-gray-900 bg-yellow-500">
                  Logout
                </p>
              </li>
            </>
          ) : (
            <li className="relative flex-col items-start hidden w-full px-2 py-1 my-4 border-b rounded-full md:flex lg:flex xl:flex lg:items-center md:items-center md:border-none md:w-auto md:my-0 lg:border-none lg:w-auto lg:my-0 hover:bg-yellow-500 hover:text-white">
              <p
                onClick={() => setMenu(!menu)}
                className="flex items-center w-full px-3 text-sm font-normal tracking-wide border-b cursor-pointer md:border-none lg:border-"
              >
                Login
                <FaChevronDown className="ml-1 cursor-pointer group" />
              </p>
              {menu && (
                <ul className="flex flex-col items-center w-full border lg:absolute lg:flex-col lg:top-14 md:absolute md:flex-col md:top-14 lg:bg-gray-400 lg:dark:bg-gray-600 md:bg-gray-400 md:dark:bg-gray-600">
                  <li
                    onClick={() => {
                      setMenu(!menu);
                      let navLink = document.getElementById("navLink");
                      // navLink.classList.toggle("hidden");
                    }}
                    className="flex items-center justify-center w-full py-2 my-4 border-b hover:text-yellow-500 lg:hover:bg-yellow-500 lg:hover:text-white md:hover:bg-yellow-500 md:hover:text-white md:my-0 lg:my-0"
                  >
                    <Link
                      to="/frontendLogin"
                      className="w-full px-3 text-sm font-normal text-start lg:text-center md:text-center md:my-0 lg:my-0"
                    >
                      Login
                    </Link>
                  </li>
                  <li
                    onClick={() => {
                      setMenu(!menu);
                      let navLink = document.getElementById("navLink");
                      // navLink.classList.toggle("hidden");
                    }}
                    className="flex items-center justify-center w-full py-2 hover:text-yellow-500 lg:hover:bg-yellow-500 lg:hover:text-white md:hover:bg-yellow-500 md:hover:text-white"
                  >
                    <Link
                      to="/frontendSignUp"
                      className="w-full px-3 text-sm font-normal text-start lg:text-center md:text-center"
                    >
                      Sign up
                    </Link>
                  </li>
                </ul>
              )}
            </li>
          )}
        </ul>
        <div
          onClick={() => setTheme(colorTheme)}
          className={`border-1 border-black p-2 ml-2 order-1  cursor-pointer rounded-full absolute  right-2 top-4  md:order-2 md:relative md:top-0 md:right-0 lg:order-2 lg:relative lg:top-0 lg:right-0 ${
            colorTheme === "light" ? "bg-white" : "bg-gray-800"
          }`}
        >
          <HiOutlineLightBulb
            className={`${
              colorTheme === "light" ? "text-black" : "text-white"
            } `}
          />
        </div>
        {
          userData && (
            <div className="absolute flex text-black dark:text-white right-20 top-4 md:hidden lg:hidden">
              <div className="flex items-center px-1">
                <div className="flex items-center justify-center w-8 h-8 mr-1 overflow-hidden bg-white rounded-full shadow">
                  {userData.profile_image ? (
                    <img
                      className="object-contain w-full"
                      src={`http://laravelcms.devdigdev.com/storage/profile_image/${userData.profile_image}`}
                    />
                  ) : (
                    <FaUserAlt className="dark:text-gray-700" />
                  )}
                </div>
              </div>
            </div>
          )
          //: (
          //     <div className="absolute flex text-black dark:text-white right-20 top-4 md:hidden lg:hidden">
          //         <div
          //             onClick={() => {
          //                 history.push("/frontendLogin");
          //                 let navLink =
          //                     document.getElementById("navLink");
          //                 navLink.classList.toggle("hidden");
          //                 if (window.innerWidth < 768) {
          //                     navLink.classList.add("hidden");
          //                 }
          //             }}
          //             className="flex items-center justify-center w-8 h-8 mr-2 overflow-hidden bg-gray-300 rounded-full shadow cursor-pointer"
          //         >
          //             <AiOutlineLogin className="dark:text-gray-700" />
          //         </div>
          //     </div>
          // )
        }
        <div className="absolute flex text-black dark:text-white sm:flex right-12 top-5 md:hidden lg:hidden">
          {" "}
          <GiHamburgerMenu
            className="cursor-pointer"
            size={26}
            onClick={() => {
              let navLink = document.getElementById("navLink");
              navLink.classList.toggle("hidden");
            }}
          />
        </div>
      </div>
      {cartPop && (
        <div className="fixed inset-0 flex items-center justify-center w-full h-full bg-black bg-opacity-70 ">
          <div className="px-10 py-8 bg-white rounded dark:bg-black">
            <h1 className="font-semibold dark:text-white">
              No items in cart Go to the product page
            </h1>
            <div className="flex justify-end w-full h-auto mt-10">
              <button
                onClick={() => {
                  setCartPop(!cartPop);
                  history.push("./products");
                }}
                className="w-20 h-8 text-sm text-white bg-yellow-500 rounded hover:bg-yellow-400"
              >
                Ok
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Front_spa_Navbar;
