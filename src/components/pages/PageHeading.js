import React, { useState } from "react";
import {
    AiOutlineSearch,
    AiOutlinePlus,
    AiOutlineDelete,
} from "react-icons/ai";
import { RiRecordCircleLine } from "react-icons/ri";
import { VscCircleOutline } from "react-icons/vsc";
import { useHistory } from "react-router-dom";

const PageHeading = ({
    pageHeading,
    pageSearch,
    path,
    showSaveOptionsBtn,
    cancel,
    handleCancelBtn,
    save,
    searchLabel,
    saveAndContinue,
    handleSave,
    activeInactive,
    deleteBtn,
    handleDelete,
    search,
    setSearch,
    handleSearch,
    handleReset,
    handleClose,
    handleActive,
    handleInActive,
    backBtn,
}) => {
    const [showSearchBar, setShowSearchBar] = useState(false);
    const history = useHistory();
    return (
        <div className="w-full px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 py-5 dark:bg-gray-400 bg-gray-300 flex justify-between items-start flex-col">
            <div className="w-full  flex justify-between items-center">
                <div className="flex items-center justify-start">
                    <h1 className="text-xl sm:text-2xl md:text-3xl lg:text-4xl xl:text-4xl font-semibold sm:font-normal">
                        {pageHeading}
                    </h1>
                </div>
                {pageSearch && (
                    <div className="flex items-center space-x-1">
                        <button
                            className="btn bg-blue-600 flex items-center"
                            onClick={() => setShowSearchBar(!showSearchBar)}
                        >
                            <AiOutlineSearch />{" "}
                            <span className="pl-1">Search</span>
                        </button>
                        <button
                            className="btn bg-blue-600 flex items-center"
                            onClick={() => history.push(path)}
                        >
                            <AiOutlinePlus /> <span className="pl-1">Add</span>
                        </button>
                        {backBtn && (
                            <button
                                onClick={() => history.goBack()}
                                className="btn bg-blue-600 flex items-center "
                            >
                                <span className="pl-1">Back</span>
                            </button>
                        )}
                    </div>
                )}
                {activeInactive && (
                    <div className="flex items-center space-x-1">
                        <button
                            onClick={() => handleActive()}
                            className="btn bg-blue-600 flex items-center"
                        >
                            <RiRecordCircleLine />
                            <span className="pl-1 font-normal">Active</span>
                        </button>
                        <button
                            onClick={() => handleInActive()}
                            className="btn bg-blue-600 flex items-center "
                        >
                            <VscCircleOutline size={18} />
                            <span className="pl-1">Inactive</span>
                        </button>
                        {deleteBtn && (
                            <button
                                onClick={() => handleDelete()}
                                className="btn bg-blue-600 flex items-center "
                            >
                                <AiOutlineDelete />
                                <span className="pl-1">Delete</span>
                            </button>
                        )}
                    </div>
                )}

                {showSaveOptionsBtn && (
                    <div className="flex items-center space-x-1">
                        {cancel && (
                            <button
                                className="btn bg-blue-600 flex items-center"
                                // onClick={() => history.goBack()}
                                onClick={() => handleCancelBtn()}
                            >
                                cancel
                            </button>
                        )}

                        {save && (
                            <button
                                className="btn bg-blue-600 flex items-center"
                                onClick={() => handleSave()}
                            >
                                save
                            </button>
                        )}
                        {saveAndContinue && (
                            <button className="btn bg-blue-600 flex items-center">
                                save & continue
                            </button>
                        )}
                    </div>
                )}
            </div>
            {pageSearch && showSearchBar && (
                <div className="mt-4 w-full">
                    <div className="flex w-full space-x-2">
                        <div className="flex flex-col w-52">
                            <label fro="search" className="text-sm font-medium">
                                {searchLabel}
                            </label>
                            <input
                                type="Search"
                                name="search"
                                value={search}
                                onChange={(e) => setSearch(e.target.value)}
                                placeholder="Search Here..."
                                className="h-9 border-none rounded bg px-2 text-sm"
                            />
                        </div>
                        <div className="flex flex-col w-52">
                            <label for="active" className="text-sm font-medium">
                                Status
                            </label>
                            <select
                                name="active"
                                className="h-9 border-none rounded  text-sm"
                            >
                                <option>Select status</option>
                                <option value="option">Active</option>
                                <option value="option">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div className="flex space-x-2 mt-4">
                        <button
                            onClick={() => handleSearch()}
                            className="btn bg-blue-600 flex items-center"
                        >
                            Search
                        </button>
                        <button
                            onClick={() => handleReset()}
                            className="btn bg-blue-600 flex items-center"
                        >
                            Reset
                        </button>
                        <button
                            onClick={() => {
                                handleClose();
                                setShowSearchBar(!showSearchBar);
                            }}
                            className="btn bg-gray-900 flex items-center"
                        >
                            Close
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
};

export default PageHeading;
