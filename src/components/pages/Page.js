/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import PageHeading from "./PageHeading";
import PageTable from "./PageTable";

const Page = (props) => {
  let users = useSelector((state) => state.UsersReducer.Users);
  let state = useSelector((state) => state.UsersReducer);

  const [loading, setLoading] = useState(true);

  let [posts, setPosts] = useState([]);
  let [currentPage, setCurrentPage] = useState(1);
  let [postPerPage, setPostPerPage] = useState(10);

  let [sortNameAsc, setSortNameDesc] = useState(false);
  let [sortId, setSortID] = useState(true);

  let [search, setSearch] = useState("");
  let [pageSearch, setPageSearch] = useState(true);
  let [activeInactive, setActiveInactive] = useState(false);

  useEffect(() => {
    setLoading(state.loading);
    setPosts(
      users.map((user) => {
        return {
          select: false,
          created_at: user.created_at,
          created_by: user.created_by,
          deleted_at: user.deleted_at,
          email: user.email,
          email_verified_at: user.email_verified_at,
          first_name: user.first_name,
          id: user.id,
          last_login: user.PageHeadinglast_login,
          last_login_at: user.last_login_at,
          last_login_ip: user.last_login_ip,
          last_name: user.last_name,
          profile_image: user.profile_image,
          status: user.status,
          updated_at: user.updated_at,
          updated_by: user.updated_by,
          user_type_id: user.user_type_id,
        };
      })
    );
  }, [users]);

  const indexOfLastPost = currentPage * postPerPage;
  const indexOfFirstPost = indexOfLastPost - postPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = (number) => {
    setCurrentPage(number);
  };
  const setNumberOfPost = (number) => {
    if (number > posts.length) {
      setPostPerPage(parseInt(posts.length));
    }
    setPostPerPage(parseInt(number));
  };

  const sortByName = (sortNameAsc) => {
    const sorted = posts.sort((a, b) => {
      const isReverse = sortNameAsc === true ? 1 : -1;
      return isReverse * a.first_name.localeCompare(b.first_name);
    });
    console.log("sorted name", sorted);
    setPosts(sorted);
  };
  const sortById = (sortId) => {
    const sorted = posts.sort((a, b) => {
      const isReverse = sortId === true ? a.id - b.id : b.id - a.id;
      return isReverse;
    });
    console.log("sorted id", sorted);
    setPosts(sorted);
  };

  const selectAll = (e) => {
    let checked = e.target.checked;
    if (checked) {
      setPageSearch(false);
      setActiveInactive(true);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
    setPosts(
      posts.map((d) => {
        d.select = checked;
        return d;
      })
    );

    console.log(posts);
  };

  const selectSingle = (e, id) => {
    let checked = e.target.checked;
    setPosts(
      posts.map((data) => {
        if (id === data.id) {
          data.select = checked;
          console.log(data);
        }

        return data;
      })
    );

    console.log(posts);

    const result = posts.some(function (data) {
      return data.select === true;
    });
    if (result) {
      setActiveInactive(true);
      setPageSearch(false);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
  };

  const handleSearch = () => {
    let searchData = posts.filter((d) => {
      if (
        d.first_name.toLocaleLowerCase().includes(search.toLocaleLowerCase()) ||
        d.email.toLocaleLowerCase().includes(search.toLocaleLowerCase())
      ) {
        console.log(d);
        return d;
      }
    });
    console.log("length", searchData.length);
    console.log(searchData);
    if (searchData.length === 0) {
      setPosts(posts);
    } else {
      setPosts(searchData);
      setCurrentPage(1);
    }
  };
  const handleReset = () => {
    setSearch("");
    if (users) {
      setPosts(
        users.map((user) => {
          return {
            select: false,
            created_at: user.created_at,
            created_by: user.created_by,
            deleted_at: user.deleted_at,
            email: user.email,
            email_verified_at: user.email_verified_at,
            first_name: user.first_name,
            id: user.id,
            last_login: user.PageHeadinglast_login,
            last_login_at: user.last_login_at,
            last_login_ip: user.last_login_ip,
            last_name: user.last_name,
            profile_image: user.profile_image,
            status: user.status,
            updated_at: user.updated_at,
            updated_by: user.updated_by,
            user_type_id: user.user_type_id,
          };
        })
      );
    }
  };
  const handleClose = () => {
    setSearch("");
    if (users) {
      setPosts(
        users.map((user) => {
          return {
            select: false,
            created_at: user.created_at,
            created_by: user.created_by,
            deleted_at: user.deleted_at,
            email: user.email,
            email_verified_at: user.email_verified_at,
            first_name: user.first_name,
            id: user.id,
            last_login: user.PageHeadinglast_login,
            last_login_at: user.last_login_at,
            last_login_ip: user.last_login_ip,
            last_name: user.last_name,
            profile_image: user.profile_image,
            status: user.status,
            updated_at: user.updated_at,
            updated_by: user.updated_by,
            user_type_id: user.user_type_id,
          };
        })
      );
    }
  };

  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide relative ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex-col items-center w-full">
        {!loading && (
          <PageHeading
            pageHeading={"Pages"}
            searchLabel={"Name/Email"}
            pageSearch={pageSearch}
            activeInactive={activeInactive}
            search={search}
            setSearch={setSearch}
            handleSearch={handleSearch}
            handleReset={handleReset}
            handleClose={handleClose}
            path="/admin/pages/addPage"
          />
        )}
        {!loading && (
          <PageTable
            posts={currentPosts}
            postPerPage={postPerPage}
            totalPosts={posts.length}
            paginate={paginate}
            currentPage={currentPage}
            setNumberOfPost={setNumberOfPost}
            sortByName={sortByName}
            sortNameAsc={sortNameAsc}
            setSortNameDesc={setSortNameDesc}
            sortId={sortId}
            setSortID={setSortID}
            sortById={sortById}
            selectAll={selectAll}
            selectSingle={selectSingle}
          />
        )}
      </div>
    </div>
  );
};

export default Page;
