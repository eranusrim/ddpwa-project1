import React, { useState } from "react";
import PageHeading from "./PageHeading";
import { CgAsterisk } from "react-icons/cg";
import MyDropzone from "../imgUploader/ImgUploader";
import TinyMCE from "react-tinymce";
import { useHistory } from "react-router-dom";

const AddPage = (props) => {
    const history = useHistory();
    const [showBanner, hideBanner] = useState(false);

    const [title, setTitle] = useState("");
    const [status, setStatus] = useState("");
    const [banner, setBanner] = useState([]);
    const [pageContent, setPageContent] = useState("");
    const [metaTitle, setMetaTitle] = useState("");
    const [metaDescription, setMetaDescription] = useState("");

    const [titleError, setTitleError] = useState("");
    const [statusError, setStatusError] = useState("");
    const [bannerError, setBannerError] = useState("");
    const [pageContentError, setPageContentError] = useState("");
    const [metaTitleError, setMetaTitleError] = useState("");
    const [metaDescriptionError, setMetaDescriptionError] = useState("");

    const getBanner = (banner) => {
        setBanner(banner);
    };
    const setBannerErrorMsg = () => {
        setBannerError("");
    };

    const handleCancelBtn = () => {
        history.goBack();
    };

    const createPage = () => {
        let titleError = "";
        let statusError = "";
        let bannerError = "";
        let pageContentError = "";
        let metaTitleError = "";
        let metaDescriptionError = "";

        if (title.trim() === "") {
            titleError = "Enter title";
        }
        if (status.trim() === "") {
            statusError = "Select Status";
        }
        if (showBanner && banner.length === 0) {
            bannerError = "Upload Banner";
        }
        if (pageContent.trim() === "") {
            pageContentError = "Enter Page Content";
        }
        if (metaTitle.trim() === "") {
            metaTitleError = "Enter Meta Title";
        }
        if (metaDescription.trim() === "") {
            metaDescriptionError = "Enter Meta Description ";
        }
        if (
            titleError !== "" ||
            statusError !== "" ||
            bannerError !== "" ||
            pageContentError !== "" ||
            metaTitleError !== "" ||
            metaDescriptionError !== ""
        ) {
            setTitleError(titleError);
            setStatusError(statusError);
            setBannerError(bannerError);
            setPageContentError(pageContentError);
            setMetaTitleError(metaTitleError);
            setMetaDescriptionError(metaDescriptionError);
            return;
        }

        let newPage = {
            title: title,
            status: status,
            banner: showBanner ? banner : [],
            pageContent: pageContent,
            metaTitle: metaTitle,
            metaDescription: metaDescription,
        };

        console.log("newPage", newPage);
    };

    return (
        <div
            className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
                props.Sidebar
                    ? "w-full sm:content md:content lg:content xl:content"
                    : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
            }`}
        >
            <div className="flex flex-col items-center w-full">
                <PageHeading
                    pageHeading="Add Page"
                    showSaveOptionsBtn={true}
                    cancel={true}
                    handleCancelBtn={handleCancelBtn}
                    save={true}
                    saveAndContinue={true}
                    handleSave={createPage}
                />
                <div className="px-4 sm:px-10 md:px-10 lg:px-10 xl:px-10 flex items-center flex-col pb-10 w-full">
                    <div className="w-full mt-5">
                        <h1 className="text-2xl">Page Content</h1>
                    </div>
                    <div className="w-full mt-5 grid lg:grid-cols-2 gap-4">
                        <div className="flex flex-col">
                            <label className="font-medium mb-1">
                                Title
                                <CgAsterisk className="inline text-red-500" />
                            </label>
                            <input
                                value={title}
                                onChange={(e) => {
                                    setTitleError("");
                                    setTitle(e.target.value);
                                }}
                                type="text"
                                className={`${
                                    titleError ? "border-red-500" : "border"
                                } h-10 rounded px-2 text-sm font-medium `}
                                placeholder="Enter title"
                            />
                            {titleError && (
                                <span className="text-red-500 text-xs">
                                    {titleError}
                                </span>
                            )}
                        </div>
                        <div className="flex flex-col">
                            <label
                                for="status"
                                className="font-medium mb-1 flex items-center"
                            >
                                Status
                                <CgAsterisk className="inline text-red-500" />
                            </label>
                            <select
                                value={status}
                                onChange={(e) => {
                                    setStatusError("");
                                    setStatus(e.target.value);
                                }}
                                className={`${
                                    statusError ? "border-red-500" : "border"
                                } h-10 rounded px-2 text-sm font-medium `}
                            >
                                <option>Select status</option>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                            </select>
                            {statusError && (
                                <span className="text-red-500 text-xs">
                                    {statusError}
                                </span>
                            )}
                        </div>
                    </div>
                    <div className="w-full mt-5">
                        <div className="w-full flex items-center mb-4">
                            <div>
                                <input
                                    type="checkbox"
                                    name=""
                                    id="toggle"
                                    className="hidden"
                                    value={showBanner}
                                    onChange={() => hideBanner(!showBanner)}
                                />
                                <label
                                    for="toggle"
                                    className="flex items-center space-x-2"
                                >
                                    <label>Banner</label>
                                    <div className=" w-20 h-7 flex items-center bg-gray-300 rounded-full dark:bg-gray-600">
                                        <div
                                            className={`w-6 h-6 bg-white rounded-full shadow-md cursor-pointer transform duration-300 ease-in-out ${
                                                showBanner
                                                    ? "translate-x-12 bg-blue-400"
                                                    : ""
                                            }`}
                                        ></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        {showBanner && (
                            <div>
                                <label className="font-medium mb-1">
                                    Banner
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <MyDropzone
                                    getBanner={getBanner}
                                    bannerError={bannerError}
                                    setBannerErrorMsg={setBannerErrorMsg}
                                />
                            </div>
                        )}
                    </div>
                    <div className="w-full my-5">
                        <label
                            for="status"
                            className="font-medium mb-1 flex items-center"
                        >
                            Page Content
                            <CgAsterisk className="inline text-red-500" />
                        </label>

                        <div className="w-full py-5">
                            <TinyMCE
                                content={pageContent}
                                // content={this.state.content}
                                config={{
                                    plugins:
                                        "autolink link image lists print preview",
                                    toolbar:
                                        "undo redo | bold italic | alignleft aligncenter alignright",
                                    menubar: "edit | insert | view | format",
                                    height: 300,
                                }}
                                onChange={(e) => {
                                    setPageContentError("");
                                    setPageContent(e.target.getContent());
                                    console.log(pageContent);
                                }}
                            />
                            {pageContentError && (
                                <span className="text-red-500 text-xs">
                                    {pageContentError}
                                </span>
                            )}
                        </div>
                    </div>
                    <div className="w-full mb-5 flex flex-col">
                        <div className="w-full">
                            <h1 className="text-2xl font-medium">SEO</h1>
                        </div>
                        <div className="w-full flex flex-col lg:flex-row lg:space-x-2 mt-2">
                            <div className="w-full flex flex-col">
                                <label className="font-medium mb-1">
                                    Meta Title
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <input
                                    value={metaTitle}
                                    type="text"
                                    onChange={(e) => {
                                        setMetaTitleError("");
                                        setMetaTitle(e.target.value);
                                    }}
                                    className="h-10 rounded px-2 text-sm font-medium border"
                                />
                                {metaTitleError && (
                                    <span className="text-red-500 text-xs">
                                        {metaTitleError}
                                    </span>
                                )}
                            </div>
                            <div className="w-full flex flex-col">
                                <label className="font-medium mb-1">
                                    Meta Description
                                    <CgAsterisk className="inline text-red-500" />
                                </label>
                                <textarea
                                    value={metaDescription}
                                    onChange={(e) => {
                                        setMetaDescriptionError("");
                                        setMetaDescription(e.target.value);
                                    }}
                                    className="h-10 rounded px-2 text-sm font-medium border"
                                ></textarea>
                                {metaDescriptionError && (
                                    <span className="text-red-500 text-xs">
                                        {metaDescriptionError}
                                    </span>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AddPage;
