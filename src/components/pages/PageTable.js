/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { RiArrowDownFill, RiArrowUpFill } from "react-icons/ri";
import { useHistory } from "react-router-dom";

const PageTable = ({
  posts,
  postPerPage,
  totalPosts,
  paginate,
  currentPage,
  setNumberOfPost,
  sortNameAsc,
  setSortNameDesc,
  sortByName,
  sortById,
  setSortID,
  sortId,
  selectAll,
  selectSingle,
}) => {
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalPosts / postPerPage); i++) {
    pageNumbers.push(i);
  }
  const history = useHistory();

  return (
    <div className="px-4 py-10 overflow-x-scroll border-b border-gray-200 shadow scrollbar-hide dark:bg-gray-700 sm:px-10 md:px-10 lg:px-10 xl:px-10">
      <table className="hidden min-w-full border divide-y divide-gray-200 sm:table md:table lg:table xl:table">
        <thead className="bg-gray-50 dark:bg-gray-300 dark:text-white">
          <tr>
            <th
              scope="col"
              className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-gray-700"
            >
              <input onChange={(e) => selectAll(e)} type="checkbox" />
            </th>
            <th
              onClick={() => {
                sortById(sortId);
                setSortID(!sortId);
              }}
              scope="col"
              className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700"
            >
              <span className="inline-block mr-2">ID</span>
              {sortId === true ? (
                <RiArrowUpFill className="inline-block" />
              ) : (
                <RiArrowDownFill className="inline-block" />
              )}
            </th>
            <th
              onClick={() => {
                sortByName(sortNameAsc);
                setSortNameDesc(!sortNameAsc);
              }}
              scope="col"
              className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase cursor-pointer dark:text-gray-700"
            >
              <span className="inline-block mr-2">Name</span>
              {sortNameAsc === true ? (
                <RiArrowUpFill className="inline-block" />
              ) : (
                <RiArrowDownFill className="inline-block" />
              )}
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-gray-700"
            >
              Email
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase  dark:text-gray-700"
            >
              Last Login
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-gray-700"
            >
              Created On
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-gray-700"
            >
              Reset Password
            </th>
            <th scope="col" className="relative px-6 py-3">
              <span className="sr-only">Edit</span>
            </th>
          </tr>
        </thead>
        <tbody className="bg-white divide-y divide-gray-200 dark:bg-gray-700">
          {posts &&
            posts.map((person) => (
              <tr key={person.email}>
                <td className="px-6 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                  <input
                    checked={person.select}
                    onChange={(e) => selectSingle(e, person.id)}
                    type="checkbox"
                  />
                </td>
                <td className="px-6 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                  {person.id}
                </td>
                <td className="px-6 py-4 whitespace-nowrap">
                  <div className="flex items-center">
                    <div className="flex-shrink-0 w-10 h-10">
                      <img
                        className="w-10 h-10 rounded-full"
                        src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60"
                        alt=""
                      />
                    </div>
                    <div className="ml-4">
                      <div className="text-sm font-medium text-gray-900 dark:text-white">
                        {person.first_name}
                      </div>
                    </div>
                  </div>
                </td>
                <td className="px-6 py-4 whitespace-nowrap">
                  <div className="text-sm text-gray-900 dark:text-white">
                    {person.email}
                  </div>
                </td>
                <td className="px-6 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                  {person.updated_at}
                </td>
                <td className="px-6 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                  {person.created_at}
                </td>
                <td className="px-6 py-4 text-sm text-gray-500 whitespace-nowrap dark:text-white">
                  <a href="#" className="text-blue-400 underline">
                    Reset Password
                  </a>
                </td>
                <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                  <h1
                    onClick={() => history.push("/admin/users/create")}
                    className="text-indigo-600 hover:text-indigo-900 dark:text-blue-400 dark:hover:text-blue-500"
                  >
                    Edit
                  </h1>
                </td>
              </tr>
            ))}
        </tbody>
      </table>

      <div className="block overflow-hidden border-r sm:hidden md:hidden lg:hidden xl:hidden dark:bg-gray-600">
        <div className="w-full px-5 py-3 bg-gray-200 border-t">
          <input
            className="ml-3"
            onChange={(e) => selectAll(e)}
            type="checkbox"
          />
          <label
            onClick={() => {
              sortByName(sortNameAsc);
              setSortNameDesc(!sortNameAsc);
            }}
            className="px-5 py-3 text-sm font-medium leading-normal"
          >
            Name{" "}
            {sortNameAsc === true ? (
              <RiArrowUpFill className="inline-block" />
            ) : (
              <RiArrowDownFill className="inline-block" />
            )}
          </label>
        </div>
        {posts &&
          posts.map((person) => (
            <div
              className={`tab w-full border-t ${
                person.status === "1"
                  ? "border-left-green-8 "
                  : "border-left-red-8"
              }`}
            >
              <input
                className="absolute opacity-0"
                id={person.id}
                type="checkbox"
                name="tabs"
              />
              <label
                className="flex items-center justify-between px-5 py-3 text-sm font-medium leading-normal text-white bg-blue-400 cursor-pointer dark:bg-gray-700 dark:text-white"
                for={person.id}
              >
                <input
                  checked={person.select}
                  onChange={(e) => selectSingle(e, person.id)}
                  type="checkbox"
                />
                <span className="block w-11/12 px-5">
                  {person.first_name} {person.last_name}
                </span>
              </label>
              <div className="w-full overflow-hidden border-t tab-content">
                <div className="p-4">
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">Email:</h1>
                    <h1 className="pl-2 text-left dark:text-white">
                      {person.email}
                    </h1>
                  </div>
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">
                      Last Login:
                    </h1>
                    <h1 className="pl-2 text-left dark:text-white">
                      {person.updated_at}
                    </h1>
                  </div>
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">
                      Created on:
                    </h1>
                    <h1 className="pl-2 text-left dark:text-white">
                      {person.created_at}
                    </h1>
                  </div>
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">Password:</h1>
                    <h1 className="pl-2 text-left dark:text-white">
                      <a href="#" className="text-blue-400 underline">
                        Reset Password
                      </a>
                    </h1>
                  </div>
                  <div className="flex py-1">
                    <h1 className="font-semibold dark:text-white">Edit:</h1>
                    <h1 className="pl-2 text-left dark:text-white">
                      <a
                        href="/"
                        className="text-indigo-600 underline hover:text-indigo-900 dark:text-blue-400 dark:hover:text-blue-500"
                      >
                        Edit
                      </a>
                    </h1>
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
      {/* pagination */}
      <div className="flex flex-col justify-between w-full mt-5 mb-3 sm:flex-row md:flex-row lg:flex-row xl:flex-row">
        <ul className="flex items-center justify-start space-x-2 cursor-pointer">
          {pageNumbers.map((number) => (
            <li
              currentPage
              key={number}
              className={`p-3 text-sm ${
                currentPage === number ? "bg-red-500" : "bg-blue-400"
              }  text-white liTags`}
              onClick={(e) => {
                paginate(number);
              }}
            >
              {number}
            </li>
          ))}
        </ul>
        <div className="flex items-center mt-3 justify center sm:mt-0 md:mt-0 lg:mt-0 xl:mt-0">
          <span className="pr-2 dark:text-white">Show</span>
          <input
            type="number"
            value={postPerPage}
            className="w-24 px-1 py-1 border border-black"
            onChange={(e) => setNumberOfPost(e.target.value)}
          />
          <span className="pl-2 dark:text-white">Entries</span>
        </div>
      </div>
    </div>
  );
};

export default PageTable;
