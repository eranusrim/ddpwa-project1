import React from "react";

const Footer = () => {
    let date = new Date();
    return (
        <div className="dark:bg-gray-800 bg-white z-10 w-full h-8 hidden sm:flex md:flex lg:flex xl:flex items-center justify-center">
            <h1 className="dark:text-white text-sm font-normal">
                © 2019 - {date.getFullYear()} DevDigital LLC. All Rights
                Reserved
            </h1>
        </div>
    );
};

export default Footer;
