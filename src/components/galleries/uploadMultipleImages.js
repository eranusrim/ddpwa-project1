/* eslint-disable no-useless-escape */
import { useEffect, useState } from "react";

function UploadMultipleImages({
  acceptedFiles,
  getRootProps,
  getInputProps,
  open,
  fileRejections,
  handleCancel,
  uploadSingle,
  multipleImagesUpload,
  links,
  setLinks,
}) {
  const [showPop, setPopUp] = useState(false);
  const [ytLink, setYTLink] = useState("");
  const [ytError, setYtError] = useState(false);
  fileRejections.map((file) => alert("file size must be less  than 10 mb"));
  const acceptedFileItems = acceptedFiles.map((file) => (
    <li
      key={file.path}
      className="flex items-center justify-between w-full px-1 pb-2 my-4 border-b border-gray-800"
    >
      <img
        className="w-14 h-14"
        alt={file.path}
        // src={URL.createObjectURL(file)}
        src={file.webkitRelativePath}
      />
      <h1 className="px-2 text-sm">{file.path}</h1>
      <h1>{file.size} bytes</h1>
      <div className="flex space-x-1">
        <button
          className="text-sm bg-blue-400 btn"
          onClick={() => uploadSingle(file)}
        >
          Upload
        </button>
        <button
          className="text-sm bg-black btn"
          onClick={() => handleCancel(file)}
        >
          Cancel
        </button>
      </div>
    </li>
  ));
  useEffect(() => {}, []);
  return (
    <section className="container">
      <div className="flex w-full space-x-3">
        <button
          type="button"
          className="text-xs bg-green-600 btn"
          onClick={open}
        >
          Add Files
        </button>
        <button
          type="button"
          className="text-xs bg-yellow-600 btn"
          onClick={() => {
            setPopUp(true);
          }}
        >
          Add youtube link
        </button>

        <button
          type="button"
          className="text-xs bg-blue-400 btn"
          onClick={() => multipleImagesUpload()}
        >
          Start Upload
        </button>
      </div>
      <div class="w-full mt-2">
        <span className="text-sm">
          Max size 10(MB) and Recommended Size: 1900PX x 1080PX (Allowed only
          jpg, jpeg, png and gif images)
        </span>
      </div>
      <div
        {...getRootProps({ className: "dropzone" })}
        className="flex items-center justify-center w-full h-40 my-4 bg-gray-200 border-2 border-black border-dashed rounded cursor-pointer"
      >
        <input {...getInputProps()} />
        <p>Drag 'n' drop some files here</p>
      </div>

      <ul className="w-full">{acceptedFileItems}</ul>
      {showPop && (
        <div className="absolute inset-0 z-50 flex items-start justify-center w-full bg-black bg-opacity-70">
          <div className="flex flex-col w-1/2 mt-10 bg-white rounded sm:w-2/5 md:w-2/5 lg:w-1/3 xl:w-1/3">
            <div className="w-full p-10">
              <h1 className="mb-4 text-xl">Add youtube video link</h1>
              <label className="">Youtube video link</label>
              <input
                type="text"
                onChange={(e) => {
                  setYTLink(e.target.value);
                  setYtError(false);
                }}
                className={`w-full  rounded p-2 mt-2 ${
                  ytError ? "border-2 border-red-500" : "border"
                }`}
              />
              <span className="text-xs">
                Please add youtube share URL [e.g. https://youtu.be/WVZ2PhmDa2M]
              </span>
            </div>
            <div className="flex justify-end w-full p-4 space-x-2">
              <button
                onClick={() => {
                  if (ytLink === "") {
                    setYtError(true);
                  } else {
                    let regEX =
                      /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/gm;
                    if (ytLink.match(regEX)) {
                      const a = [...links];
                      console.log("links", links);
                      a.push(ytLink);
                      setLinks(a);
                      setPopUp(false);
                    } else {
                      setYtError(true);
                    }
                  }
                }}
                className="px-4 py-2 text-sm text-white bg-blue-500 rounded"
              >
                Add link
              </button>
              <button
                onClick={() => {
                  setPopUp(false);
                  setYTLink("");
                }}
                className="px-4 py-2 text-sm text-white bg-black rounded"
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      )}
    </section>
  );
}

export default UploadMultipleImages;
