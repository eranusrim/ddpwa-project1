/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect, useCallback } from "react";
import PageHeading from "../pages/PageHeading";
import { CgAsterisk } from "react-icons/cg";
import { AiOutlineEye } from "react-icons/ai";
import TinyMCE from "react-tinymce";
import Dropzone from "react-dropzone";
import axios from "axios";
import { useHistory } from "react-router-dom";
import UploadMultipleImages from "./uploadMultipleImages";
import { useDropzone } from "react-dropzone";
import { reject } from "lodash";
import { notify } from "../../utility";

const AddEditGallery = (props) => {
  let history = useHistory();
  const [title, setTitle] = useState("");
  const [displayOrder, setDisplayOrder] = useState("1");
  const [status, setStatus] = useState("1");
  const [imageAltText, setImageAltText] = useState("");
  const [image, setImage] = useState("");
  const [imageName, setImageName] = useState("");
  const [pageContent, setPageContent] = useState("");
  const [metaTitle, setMetaTitle] = useState("");
  const [metaDescription, setMetaDescription] = useState("");
  const [links, setLinks] = useState([]);
  const [myFiles, setMyFiles] = useState([]);
  const [youtubeLinks, setYoutubeLinks] = useState([]);
  const [ytLink, setYtLink] = useState("");
  const [ytPopUp, setYtPopUp] = useState(false);
  const [imgLink, setImgLink] = useState("");
  const [imgPopUp, setImgPopUp] = useState(false);
  const [uploadedData, setUploadedData] = useState([]);

  const [titleError, setTitleError] = useState("");
  const [imageError, setImageError] = useState("");
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const access_token = userInfo.access_token;

  const onImageDrop = (acceptedFiles) => {
    if (acceptedFiles.length > 0) {
      const uploads = acceptedFiles.map((image) => {
        let data = new FormData();
        data.append("folder", "galleryImages");
        data.append("image", image);
        const config = {
          method: "post",
          url: "http://localhost:5000/uploads",
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
          data: data,
        };
        axios(config)
          .then(function (response) {
            notify(response.data.message);
            setImage(response.data.imageUrl);
            setImageName(response.data.imageName);
            setImageError("");
          })
          .catch(function (error) {
            console.log(error);
          });
      });
    }
  };

  const onDrop = useCallback(
    (acceptedFiles) => {
      const files = [...myFiles, ...acceptedFiles];
      const filesWithId = files.map((item, index) => {
        return {
          id: index + 1,
          lastModified: item.lastModified,
          lastModifiedDate: item.lastModifiedDate,
          name: item.name,
          size: item.size,
          type: item.type,
          webkitRelativePath: !item.id
            ? URL.createObjectURL(item)
            : item.webkitRelativePath,
        };
      });
      setMyFiles(filesWithId);
    },
    [myFiles]
  );

  const { acceptedFiles, getRootProps, getInputProps, open, fileRejections } =
    useDropzone({
      accept: "image/*",
      noKeyboard: true,
      noClick: true,
      maxSize: 10485760,
      onDrop,
    });

  const handleCancel = (file) => {
    const newFiles = [...myFiles];
    setMyFiles(reject(newFiles, { id: file.id }));
  };
  const uploadSingle = (file) => {
    acceptedFiles.map((f) => {
      if (f.name === file.name) {
        let data = new FormData();
        data.append("folder", "galleryImages");
        data.append("image", f);
        const config = {
          method: "post",
          url: "http://localhost:5000/uploads",
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
          data: data,
        };
        axios(config)
          .then(function (response) {
            if (response.status === 200) {
              const d = [...uploadedData];
              d.push({
                imageName: response.data.imageName,
                imageUrl: response.data.imageUrl,
              });

              setUploadedData(d);
              notify(response.data.message);
              handleCancel(file);
              if (props.match.params.id && response.status === 200) {
                const insertImages = [];
                insertImages.push({
                  imageName: response.data.imageName,
                  gallery_id: props.match.params.id,
                });
                let config = {
                  method: "post",
                  url: "http://localhost:5000/gallery/insertImageLink",
                  headers: {
                    Authorization: `Bearer ${access_token}`,
                  },
                  data: {
                    insertImages: insertImages,
                  },
                };
                axios(config)
                  .then((res) => {
                    console.log("res", res);
                  })
                  .catch((err) => {
                    console.log("err", err);
                  });
              }
            }
          })
          .catch(function (error) {
            console.log("error", error);
          });
      }
    });
  };
  const multipleImagesUpload = () => {
    let data = new FormData();
    data.append("folder", "galleryImages");
    let arr = [];
    acceptedFiles.map((file) => {
      myFiles.map((f) => {
        if (file.name === f.name) {
          arr.push(file);
        }
      });
    });
    for (let i = 0; i < arr.length; i++) {
      data.append("image", arr[i]);
    }
    const config = {
      method: "post",
      url: "http://localhost:5000/uploads/multipleUpload",
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        if (response.status === 200) {
          const images = response.data.images;
          const d = uploadedData;
          images.map((image) => {
            d.push({
              imageName: image.imageName,
              imageUrl: image.imageUrl,
            });
          });
          setUploadedData(d);
          notify(response.data.message);
          setMyFiles([]);
        }
      })
      .catch(function (error) {
        console.log("error", error);
      });
  };
  const remove = (imageUrl) => {
    let arr = imageUrl.split("/");
    let image = arr[arr.length - 1];
    const config = {
      method: "delete",
      url: "http://localhost:5000/uploads",
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      data: {
        folder: "galleryImages",
        image: image,
      },
    };
    axios(config)
      .then(function (response) {
        if (props.match.params.id) {
          const con = {
            method: "put",
            url: "http://localhost:5000/gallery/deleteImage",
            headers: {
              Authorization: `Bearer ${access_token}`,
            },
            data: {
              id: props.match.params.id,
            },
          };
          axios(con)
            .then((res) => {
              notify(res.data.message);
              setImage("");
              setImageName("");
            })
            .catch((err) => {
              console.log(err);
            });
        } else {
          notify(response.data.message);
          setImage("");
          setImageName("");
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const removeImg = (imageUrl) => {
    let arr = imageUrl.split("/");
    let image = arr[arr.length - 1];
    const config = {
      method: "delete",
      url: "http://localhost:5000/uploads",
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      data: {
        folder: "galleryImages",
        image: image,
      },
    };
    axios(config)
      .then(function (response) {
        const data = uploadedData.filter((d) => d.imageUrl !== imageUrl);
        setUploadedData(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const uploadedImage = (imageUrl, imageId, gallery_id) => {
    let arr = imageUrl.split("/");
    let image = arr[arr.length - 1];
    const config = {
      method: "delete",
      url: "http://localhost:5000/uploads",
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      data: {
        folder: "galleryImages",
        image: image,
      },
    };
    axios(config)
      .then(function (response) {
        const data = uploadedData.filter((d) => d.imageUrl !== imageUrl);
        setUploadedData(data);
        const con = {
          method: "delete",
          url: "http://localhost:5000/gallery/deleteGalleryImage",
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
          data: {
            imageId: imageId,
            gallery_id: gallery_id,
          },
        };
        axios(con)
          .then((res) => {
            notify(res.data.message);
          })
          .catch((err) => {
            notify(err);
          });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const deleteLink = (id, gallery_id) => {
    let config = {
      method: "delete",
      url: "http://localhost:5000/gallery/deleteYoutuebLink",
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      data: {
        id: id,
        gallery_id: gallery_id,
      },
    };
    axios(config)
      .then((response) => {
        notify(response.data.message);
        fetchGallery(gallery_id);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  const handleAddEdit = () => {
    let titleError = "";
    let imageError = "";

    if (title.trim() === "") {
      titleError = "Enter title";
    }

    if (image.trim() === "") {
      imageError = "Upload image";
    }
    if (titleError !== "" || imageError !== "") {
      setTitleError(titleError);
      setImageError(imageError);
      return;
    }

    if (props.match.params.id) {
      let editGallery = {
        title: title,
        displayOrder: displayOrder,
        status: status,
        imageAlt: imageAltText,
        image: imageName,
        description: pageContent,
        metaTitle: metaTitle,
        metaDescription: metaDescription,
        id: props.match.params.id,
      };
      let config = {
        method: "put",
        url: "http://localhost:5000/gallery/editGallery",
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
        data: editGallery,
      };
      axios(config)
        .then((response) => {
          notify(response.data.message);
          history.push("/admin/galleries");
        })
        .catch((error) => {
          console.log("error", error);
        });
    } else {
      let a = [];
      uploadedData.map((d) => {
        a.push(d.imageName);
      });
      let newGallery = {
        title: title,
        displayOrder: displayOrder,
        status: status,
        imageAlt: imageAltText,
        image: imageName,
        description: pageContent,
        metaTitle: metaTitle,
        metaDescription: metaDescription,
        links: links,
        images: a,
      };
      let config = {
        method: "post",
        url: "http://localhost:5000/gallery",
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
        data: newGallery,
      };
      axios(config)
        .then((response) => {
          notify(response.data.message);
          history.push("/admin/galleries");
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  };
  const fetchGallery = (id) => {
    let config = {
      method: "get",
      url: "http://localhost:5000/gallery/" + id,
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    };
    axios(config)
      .then((response) => {
        const data = response.data.gallery;
        console.log("response", data);
        setTitle(data[0].title);
        setDisplayOrder(data[0].displayOrder);
        setStatus(data[0].status);
        setImageAltText(data[0].imageAlt);
        setImage(data[0].image);
        setImageName(data[0].imageName);
        setPageContent(data[0].description);
        setMetaTitle(data[0].metaTitle);
        setMetaDescription(data[0].metaDescription);
        const arr = [];
        data[0].links.map((ab) => {
          const a = ab.link.split("/");
          const b = a[a.length - 1];
          arr.push({
            id: ab.id,
            link: b,
            gallery_id: ab.gallery_id,
          });
        });
        // setYoutubeLinks(data[0].links);
        console.log("arr", arr);
        setYoutubeLinks(arr);
        setUploadedData(data[0].images);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  const removeLink = (index) => {
    const data = [...links];
    data.splice(index, 1);
    setLinks(data);
  };
  const handleCancelBtn = () => {
    if (props.match.params.id === undefined) {
      if (image !== "") {
        remove(image);
      }
      if (uploadedData.length > 0) {
        const imagesArr = [];
        uploadedData.map((d) => {
          imagesArr.push({
            folder: "galleryImages",
            image: d.imageName,
          });
        });

        axios
          .delete("http://localhost:5000/uploads/multipleDelete", {
            data: {
              imagesArr: imagesArr,
            },
          })
          .then((response) => {
            history.goBack();
          })
          .catch((error) => {
            console.log("response error", error);
          });
      }

      if (image === "" && uploadedData.length === 0) {
        history.goBack();
      }
    } else {
      history.goBack();
    }
  };

  useEffect(() => {
    const id = props.match.params.id;
    if (id) {
      fetchGallery(id);
    }
  }, []);

  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <PageHeading
          pageHeading="Add Galleries"
          showSaveOptionsBtn={true}
          cancel={true}
          handleCancelBtn={handleCancelBtn}
          save={true}
          saveAndContinue={true}
          handleSave={handleAddEdit}
        />
        <div className="flex flex-col items-center w-full px-4 pb-10 sm:px-10 md:px-10 lg:px-10 xl:px-10">
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label className="mb-1 font-medium">
                Title
                <CgAsterisk className="inline text-red-500" />
              </label>
              <input
                value={title}
                onChange={(e) => {
                  setTitleError("");
                  setTitle(e.target.value);
                }}
                type="text"
                className={`${
                  titleError ? "border-red-500" : "border"
                } h-10 rounded px-2 text-sm font-medium `}
                placeholder="Enter title"
              />
              {titleError && (
                <span className="text-xs text-red-500">{titleError}</span>
              )}
            </div>
            <div className="flex flex-col">
              <label
                for="status"
                className="flex items-center mb-1 font-medium"
              >
                Display Order
              </label>
              <select
                value={displayOrder}
                onChange={(e) => {
                  setDisplayOrder(e.target.value);
                }}
                className="h-10 px-2 text-sm font-medium border rounded"
              >
                <option value="1">1</option>
                <option value="2">1</option>
                <option value="3">3</option>
                <option value="3">4</option>
                <option value="3">5</option>
                <option value="3">6</option>
                <option value="3">7</option>
              </select>
            </div>
          </div>
          <div className="grid w-full gap-4 mt-5 lg:grid-cols-2">
            <div className="flex flex-col">
              <label
                for="status"
                className="flex items-center mb-1 font-medium"
              >
                Status
              </label>
              <select
                value={status}
                onChange={(e) => {
                  setStatus(e.target.value);
                }}
                className="h-10 px-2 text-sm font-medium border rounded"
              >
                <option>Select status</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
              </select>
            </div>
            <div className="flex flex-col">
              <label className="mb-1 font-medium">Image Alt</label>
              <input
                value={imageAltText}
                onChange={(e) => {
                  setImageAltText(e.target.value);
                }}
                type="text"
                className="h-10 px-2 text-sm font-medium border rounded "
              />
            </div>
          </div>
          <div className="w-full mt-5">
            <div>
              <label className="mb-1 font-medium">Image</label>
              <div className="relative flex items-center justify-center w-full h-56 mt-2 border-2 border-gray-700 border-dashed hover:bg-black hover:bg-opacity-40">
                {image && (
                  <img
                    src={image}
                    alt="no img"
                    className="object-contain w-full h-4/5"
                  />
                )}
                {image && (
                  <button
                    onClick={() => {
                      remove(image);
                    }}
                    className="absolute z-20 text-white bg-gray-900 top-1 right-1 btn"
                  >
                    Remove
                  </button>
                )}
                {!image && (
                  <Dropzone
                    accept="image/*"
                    onDrop={(acceptedFiles) => {
                      onImageDrop(acceptedFiles);
                    }}
                  >
                    {({ getRootProps, getInputProps }) => (
                      <div
                        {...getRootProps()}
                        className="flex items-center justify-center w-full h-full"
                      >
                        <input {...getInputProps()} />
                        <p>
                          Drag 'n' drop some files here, or click to select
                          files
                        </p>
                      </div>
                    )}
                  </Dropzone>
                )}
              </div>
            </div>
            <div className="mt-1 text-xs">
              Max size 5(MB) and Recommended Size: 1900PX x 1080PX (Allowed only
              jpg, jpeg, png and gif images)
            </div>
            {imageError && (
              <span className="text-xs text-red-500">{imageError}</span>
            )}
          </div>
          <div className="w-full my-5">
            <label for="status" className="flex items-center mb-1 font-medium">
              Description
            </label>

            {/* <div className="w-full py-5">
                            <TinyMCE
                                content={pageContent}
                                // content={this.state.content}
                                config={{
                                    plugins:
                                        "autolink link image lists print preview",
                                    toolbar:
                                        "undo redo | bold italic | alignleft aligncenter alignright",
                                    menubar: "edit | insert | view | format",
                                    height: 300,
                                }}
                                onChange={(e) => {
                                    setPageContent(e.target.getContent());
                                    console.log(pageContent);
                                }}
                            />
                        </div> */}
            <div className="w-full py-5">
              <textarea
                className="w-full h-48 p-2 border"
                value={pageContent}
                // content={this.state.content}

                onChange={(e) => {
                  setPageContent(e.target.value);
                  console.log(pageContent);
                }}
              ></textarea>
            </div>
          </div>
          <div className="flex flex-col w-full mb-5">
            <div className="flex flex-col w-full mt-2 lg:flex-row lg:space-x-2">
              <div className="flex flex-col w-full">
                <label className="mb-1 font-medium">Meta Title</label>
                <input
                  value={metaTitle}
                  type="text"
                  onChange={(e) => {
                    setMetaTitle(e.target.value);
                  }}
                  className="h-10 px-2 text-sm font-medium border rounded"
                />
              </div>
              <div className="flex flex-col w-full">
                <label className="mb-1 font-medium">Meta Description</label>
                <textarea
                  value={metaDescription}
                  onChange={(e) => {
                    setMetaDescription(e.target.value);
                  }}
                  className="h-10 px-2 text-sm font-medium border rounded"
                ></textarea>
              </div>
            </div>
          </div>
          <div className="w-full my-5">
            <UploadMultipleImages
              acceptedFiles={myFiles}
              getRootProps={getRootProps}
              getInputProps={getInputProps}
              open={open}
              fileRejections={fileRejections}
              handleCancel={handleCancel}
              uploadSingle={uploadSingle}
              multipleImagesUpload={multipleImagesUpload}
              links={links}
              setLinks={setLinks}
            />
          </div>
          <div className="flex flex-col w-full my-5">
            {links &&
              links.length > 0 &&
              links.map((link, index) => (
                <div className="flex items-center justify-between p-2 my-2 bg-gray-300 border-2 rounded">
                  <h1 className="text-sm font-medium">{link}</h1>
                  <button
                    onClick={() => {
                      removeLink(index);
                    }}
                    className="bg-black btn "
                  >
                    x
                  </button>
                </div>
              ))}
          </div>
          <div className="grid w-full grid-cols-1 gap-4 my-5 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4">
            {uploadedData.length > 0 &&
              uploadedData.map((d) => (
                <div className="relative w-full border border-gray-700 imgHover">
                  <img src={d.imageUrl} className="w-full" />
                  <div className="absolute inset-0 items-center justify-center hidden w-full h-full space-x-2 text-white bg-black imgHoverDisplay bg-opacity-70">
                    <button
                      onClick={() => {
                        setImgPopUp(true);
                        setImgLink(d.imageUrl);
                      }}
                      className="flex items-center justify-center w-8 h-8 bg-blue-400 rounded-full"
                    >
                      <AiOutlineEye />
                    </button>
                    <button
                      onClick={() => {
                        let a = window.confirm(
                          "Are you sure you want to delete this"
                        );
                        if (a) {
                          if (props.match.params.id) {
                            uploadedImage(d.imageUrl, d.imageId, d.gallery_id);
                          } else {
                            removeImg(d.imageUrl);
                          }
                        }
                      }}
                      className="flex items-center justify-center w-8 h-8 text-lg bg-black rounded-full"
                    >
                      x
                    </button>
                  </div>
                </div>
              ))}

            {youtubeLinks.length > 0 &&
              youtubeLinks.map((yt) => (
                <div className="relative w-full border border-gray-700 imgHover">
                  <iframe
                    // src={yt.link}
                    src={`https://www.youtube.com/embed/${yt.link}`}
                    frameborder="0"
                    allow="autoplay; encrypted-media"
                    allowfullscreen
                    title="video"
                    className="w-full h-full border-2"
                  />
                  <div className="absolute inset-0 items-center justify-center hidden w-full h-full space-x-2 text-white bg-black imgHoverDisplay bg-opacity-70">
                    <button
                      onClick={() => {
                        setYtPopUp(true);
                        // const a = yt.link.split("/");
                        // const b = a[a.length - 1];
                        // setYtLink(b);
                        setYtLink(yt.link);
                      }}
                      className="flex items-center justify-center w-8 h-8 bg-blue-400 rounded-full"
                    >
                      <AiOutlineEye />
                    </button>
                    <button
                      onClick={() => {
                        let a = window.confirm(
                          "Are you sure you want to delete this"
                        );
                        if (a) {
                          deleteLink(yt.id, yt.gallery_id);
                        }
                      }}
                      className="flex items-center justify-center w-8 h-8 text-lg bg-black rounded-full"
                    >
                      x
                    </button>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
      {ytPopUp && (
        <div className="absolute inset-0 z-50 flex flex-col items-center justify-center w-full bg-black bg-opacity-70">
          <div className="flex flex-col w-1/2 mt-10 bg-white rounded h-1/2">
            <div className="flex justify-end w-full h-10 item-center">
              <h1
                onClick={() => {
                  setYtPopUp(false);
                  setYtLink("");
                }}
                className="flex items-center justify-center w-10 h-10 text-white bg-black border-2 cursor-pointer"
              >
                x
              </h1>
            </div>
            <iframe
              src={`https://www.youtube.com/embed/${ytLink}`}
              frameborder="0"
              allow="autoplay; encrypted-media"
              allowfullscreen
              title="video"
              className="w-full h-full border-2"
            />
          </div>
        </div>
      )}
      {imgPopUp && (
        <div className="absolute inset-0 z-50 flex flex-col items-center justify-center w-full bg-black bg-opacity-70">
          <div className="flex flex-col w-1/2 mt-10 bg-white rounded h-1/2">
            <div className="flex justify-end w-full h-10 item-center">
              <h1
                onClick={() => {
                  setImgPopUp(false);
                  setImgLink("");
                }}
                className="flex items-center justify-center w-10 h-10 text-white bg-black border-2 cursor-pointer"
              >
                x
              </h1>
            </div>
            <img src={imgLink} className="w-full h-full border-2" />
          </div>
        </div>
      )}
    </div>
  );
};

export default AddEditGallery;
