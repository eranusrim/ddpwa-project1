/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
import React, { useState, useEffect } from "react";
import PageHeading from "../pages/PageHeading";
import axios from "axios";
import { notify } from "../../utility";
import GalleryCards from "./galleryCards";

const Galleries = (props) => {
  let [galleries, setGalleries] = useState([]);
  let [search, setSearch] = useState("");
  let [pageSearch, setPageSearch] = useState(true);
  let [activeInactive, setActiveInactive] = useState(false);
  let [selectAllCheckbox, setSelectAllCheckbox] = useState(false);

  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const access_token = userInfo.access_token;

  const fetchGalleries = () => {
    let config = {
      method: "get",
      url: "http://localhost:5000/gallery",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };
    axios(config)
      .then(function (response) {
        const data = response.data.gallery;
        console.log("data", data);
        setGalleries(
          data.map((d) => {
            return {
              select: false,
              id: d.id,
              description: d.description,
              image: d.image,
              imageAlt: d.imageAlt,
              displayOrder: d.displayOrder,
              images: d.images,
              metaDescription: d.metaDescription,
              metaTitle: d.metaTitle,
              status: d.status,
              title: d.title,
            };
          })
        );
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchGalleries();
  }, []);

  const handleDelete = () => {
    let a = window.confirm("Are you sure you want to delete this");
    if (a) {
      let arrayId = [];
      let imagesArr = [];
      galleries.forEach((d) => {
        if (d.select) {
          arrayId.push(d.id);
          let a = d.image.split("/");
          imagesArr.push({
            folder: "galleryImages",
            image: a[a.length - 1],
          });
        }
      });
      galleries.forEach((d) => {
        if (d.select) {
          if (d.images.length > 0) {
            d.images.map((i) => {
              let a = i.split("/");
              imagesArr.push({
                folder: "galleryImages",
                image: a[a.length - 1],
              });
            });
          }
        }
      });
      axios
        .delete("http://localhost:5000/gallery/deleteGallery", {
          data: {
            gallery_ids: arrayId,
          },
        })
        .then((response) => {
          notify(response.data.message);
          setSelectAllCheckbox(false);
          setActiveInactive(false);
          setPageSearch(true);
          fetchGalleries();
        })
        .catch((error) => {
          console.log("error", error);
        });

      axios
        .delete("http://localhost:5000/uploads/multipleDelete", {
          data: {
            imagesArr: imagesArr,
          },
        })
        .then((response) => {
          console.log("response delete images", response);
          setSelectAllCheckbox(false);
          setActiveInactive(false);
          setPageSearch(true);
          fetchGalleries();
        })
        .catch((error) => {
          console.log("response error", error);
        });
    }
  };
  const handleActive = () => {
    let arrayId = [];
    galleries.forEach((d) => {
      if (d.select) {
        arrayId.push(d.id);
      }
    });
    const data = {
      status: 1,
      gallery_ids: arrayId,
    };
    axios
      .put("http://localhost:5000/gallery/activeInactiveGallery", data)
      .then((response) => {
        notify(response.data.message);
        setSelectAllCheckbox(false);
        setActiveInactive(false);
        setPageSearch(true);
        fetchGalleries();
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  const handleInActive = () => {
    let arrayId = [];
    galleries.forEach((d) => {
      if (d.select) {
        arrayId.push(d.id);
      }
    });
    const data = {
      status: 0,
      gallery_ids: arrayId,
    };
    axios
      .put("http://localhost:5000/gallery/activeInactiveGallery", data)
      .then((response) => {
        notify(response.data.message);
        setSelectAllCheckbox(false);
        setActiveInactive(false);
        setPageSearch(true);
        fetchGalleries();
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  const handleSearch = () => {
    let searchData = galleries.filter((d) => {
      if (
        d.title.trim().toLocaleLowerCase().includes(search.toLocaleLowerCase())
      ) {
        console.log(d);
        return d;
      }
    });
    console.log("length", searchData.length);
    console.log(searchData);
    if (searchData.length === 0) {
      setGalleries(galleries);
    } else {
      setGalleries(searchData);
    }
  };
  const handleReset = () => {
    setSearch("");
    fetchGalleries();
  };
  const handleClose = () => {
    setSearch("");
    fetchGalleries();
  };
  const selectAll = (e) => {
    let checked = e.target.checked;
    if (checked) {
      setPageSearch(false);
      setActiveInactive(true);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
    setGalleries(
      galleries.map((d) => {
        d.select = checked;
        return d;
      })
    );
  };
  const selectSingle = (e, id) => {
    let checked = e.target.checked;
    setGalleries(
      galleries.map((d) => {
        if (id === d.id) {
          d.select = checked;
        }

        return d;
      })
    );

    const result = galleries.some(function (data) {
      return data.select === true;
    });
    if (result) {
      setActiveInactive(true);
      setPageSearch(false);
    } else {
      setPageSearch(true);
      setActiveInactive(false);
    }
  };
  return (
    <div
      className={`content-container relative bg-gray-100 dark:bg-gray-700 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex-col items-center w-full">
        <PageHeading
          pageHeading={"Galleries"}
          searchLabel={"Title"}
          pageSearch={pageSearch}
          activeInactive={activeInactive}
          handleActive={handleActive}
          handleInActive={handleInActive}
          deleteBtn={true}
          handleDelete={handleDelete}
          search={search}
          setSearch={setSearch}
          handleSearch={handleSearch}
          handleReset={handleReset}
          handleClose={handleClose}
          path="/admin/galleries/addGallery"
        />
        <GalleryCards
          galleries={galleries}
          selectAllCheckbox={selectAllCheckbox}
          setSelectAllCheckbox={setSelectAllCheckbox}
          selectSingle={selectSingle}
          selectAll={selectAll}
        />
      </div>
    </div>
  );
};

export default Galleries;
