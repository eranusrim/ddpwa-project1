/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { AiOutlineEye } from "react-icons/ai";
import { BsPencil } from "react-icons/bs";

const GalleryCards = ({
  galleries,
  setSelectAllCheckbox,
  selectAllCheckbox,
  selectSingle,
  selectAll,
}) => {
  const history = useHistory();
  const [viewImg, setViewImg] = useState("");
  useEffect(() => {}, []);
  return (
    <div className="flex flex-col px-4 py-10 overflow-x-scroll scrollbar-hide dark:bg-gray-700 sm:px-10 md:px-10 lg:px-10 xl:px-10">
      <div className="mb-5">
        <input
          type="checkbox"
          checked={selectAllCheckbox}
          onChange={(e) => {
            setSelectAllCheckbox(!selectAllCheckbox);
            selectAll(e);
          }}
          id="selectAll"
        />
        <label for="selectAll" className="ml-2 text-lg dark:text-white">
          Select/Unselect All
        </label>
      </div>
      <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 xl:grid-cols-6">
        {galleries.map((gallery) => (
          <div
            className={`border-2 w-full overflow-hidden flex flex-col  rounded   ${
              gallery.status === 1 ? "border-green-500" : "border-red-500"
            }`}
          >
            <div className="relative w-full h-40 bg-white imgHover">
              <img
                className="object-contain w-full h-full p-2"
                src={gallery.image}
                alt={gallery.imageAlt}
              />
              <div className="absolute inset-0 items-center justify-center hidden w-full h-full space-x-2 text-white bg-black imgHoverDisplay bg-opacity-70">
                <button
                  onClick={() => {
                    setViewImg(gallery.image);
                  }}
                  className="flex items-center justify-center w-8 h-8 bg-blue-400 rounded-full"
                >
                  <AiOutlineEye />
                </button>
                <button
                  onClick={() => {
                    history.push("/admin/galleries/editGallery/" + gallery.id);
                  }}
                  className="flex items-center justify-center w-8 h-8 bg-green-500 rounded-full"
                >
                  <BsPencil />
                </button>
                <button
                  onClick={() => {
                    let a = window.confirm(
                      "Are you sure you want to delete this"
                    );
                    if (a) {
                    }
                  }}
                  className="flex items-center justify-center w-8 h-8 text-lg bg-black rounded-full"
                >
                  x
                </button>
              </div>
            </div>

            <div className="flex items-center justify-between p-2 mt-2">
              <div className="w-1/5 ">
                <input
                  type="checkbox"
                  checked={gallery.select}
                  onChange={(e) => selectSingle(e, gallery.id)}
                />
              </div>
              <h1 className="flex justify-end w-4/5 dark:text-white">
                <span className="overflow-ellipsis">{gallery.title}</span>
                <span clssName="">[{gallery.images.length}]</span>
              </h1>
            </div>
          </div>
        ))}
      </div>
      {viewImg && (
        <div className="absolute inset-0 z-50 flex flex-col items-center justify-center w-full bg-black bg-opacity-70">
          <div className="flex flex-col w-1/2 mt-10 bg-white rounded h-1/2">
            <div className="flex justify-end w-full h-10 item-center">
              <h1
                onClick={() => {
                  setViewImg("");
                }}
                className="flex items-center justify-center w-10 h-10 text-white bg-black border-2 cursor-pointer"
              >
                x
              </h1>
            </div>
            <img src={viewImg} className="w-full h-full border-2" />
          </div>
        </div>
      )}
    </div>
  );
};

export default GalleryCards;
