/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { SidebarData } from "../sidebar/SidebarData";
import { Link } from "react-router-dom";
import PageHeading from "../pages/PageHeading";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const notify = (msg) => {
  toast.success(msg, {
    position: "top-center",
  });
};

const Dashboard = (props) => {
  useEffect(() => {
    console.log("Sidebar", props.Sidebar);
  }, []);
  return (
    <div
      className={`content-container bg-gray-100 overflow-y-scroll scrollbar-hide ${
        props.Sidebar
          ? "w-full sm:content md:content lg:content xl:content"
          : "w-full sm:content-extra md:content-extra mg:content-extra xl:content-extra"
      }`}
    >
      <div className="flex flex-col items-center w-full">
        <PageHeading pageHeading="Dashboard" pageSearch={false} />
        <div className="w-full p-5">
          <div className="grid w-full grid-cols-2 gap-3 xl:grid-cols-5 lg:grid-cols-3 md:grid-cols-3 sm:grid-cols-2 first:rotate-45">
            {SidebarData.map((item, index) => {
              return (
                <Link
                  to={item.path}
                  className={`bg-gray-300 w-full h-auto flex flex-col shadow rounded items-center p-6 sm:p-10 md:p-10 lg:p-10 xl:p-10 hover:bg-gray-400 ${
                    index === 0 ? "hidden" : "flex"
                  }`}
                  key={index}
                >
                  <div className="text-2xl sm:text-2xl md:text-2xl lg:text-4xl xl:text-4xl">
                    {item.icon}
                  </div>
                  <p className="mt-2 text-sm font-medium text-center sm:text-sm md:text-sm lg:text-xl xl:text-xl">
                    {item.title}
                  </p>
                </Link>
              );
            })}
          </div>
        </div>
        <ToastContainer />
      </div>
    </div>
  );
};

export default Dashboard;
