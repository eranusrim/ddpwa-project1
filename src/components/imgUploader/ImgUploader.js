import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useDropzone } from "react-dropzone";

const activeStyle = {
    borderColor: "#2196f3",
};

const acceptStyle = {
    borderColor: "#00e676",
};

const rejectStyle = {
    borderColor: "#ff1744",
};

function MyDropzone(props) {
    const [files, setFiles] = useState([]);

    const onDrop = useCallback(
        (acceptedFiles) => {
            setFiles(
                acceptedFiles.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
            props.getBanner(files);
            props.setBannerErrorMsg();
        },
        [files, props]
    );

    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject,
    } = useDropzone({
        onDrop,
        accept: "image/jpeg, image/png",
    });

    useMemo(
        () => ({
            // ...baseStyle,
            ...(isDragActive ? activeStyle : {}),
            ...(isDragAccept ? acceptStyle : {}),
            ...(isDragReject ? rejectStyle : {}),
        }),
        [isDragActive, isDragReject, isDragAccept]
    );

    const thumbs = files.map((file) => (
        <div key={file.name} className="w-full h-48">
            <img
                src={file.preview}
                alt={file.name}
                className="w-full h-full object-contain"
            />
        </div>
    ));

    // clean up
    useEffect(
        () => () => {
            files.forEach((file) => URL.revokeObjectURL(file.preview));
        },
        [files]
    );

    return (
        <>
            <section className="my-4">
                <div
                    {...getRootProps()}
                    className=" h-52 bg-gray-300 dark:bg-gray-100 flex items-center justify-center text-gray-800 rounded border-dashed border-2 border-gray-500 p-3"
                >
                    {files.length <= 0 && <input {...getInputProps()} />}
                    {files.length > 0 ? (
                        <aside className="relative w-full h-full flex items-center justify-center">
                            {thumbs}
                            <div className="flex justify-center items-center absolute">
                                <button
                                    className="btn text-white bg-gray-500"
                                    onClick={() => {
                                        console.log(files);
                                        setFiles([]);
                                        console.log(files);
                                    }}
                                >
                                    Remove
                                </button>
                            </div>
                        </aside>
                    ) : (
                        <div className="flex items-center justify-center w-full h-full">
                            Drag and drop your images here.
                        </div>
                    )}
                </div>
            </section>
            {props.bannerError && (
                <span className="text-red-500 text-xs">
                    {props.bannerError}
                </span>
            )}
        </>
    );
}

export default MyDropzone;
