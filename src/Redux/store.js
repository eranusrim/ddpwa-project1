import { createStore, applyMiddleware, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { UsersReducer } from "./reducers/usersReducers";
import { SliderImagesReducer } from "./reducers/sliderImagesReducer";
import { EventsReducer } from "./reducers/eventsReducers";
import { CategoryReducer } from "./reducers/categoriesReducer";
import { BlogsReducer } from "./reducers/blogsReducer";
import { CartReducer } from "./reducers/cartReducer";

const rootReducer = combineReducers({
    UsersReducer,
    SliderImagesReducer,
    EventsReducer,
    CategoryReducer,
    BlogsReducer,
    CartReducer,
});

const store = createStore(
    rootReducer,
    {},
    composeWithDevTools(applyMiddleware(thunk))
);

export default store;
