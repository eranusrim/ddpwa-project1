/* eslint-disable no-unused-vars */
import request from "../../api";
import axios from "axios";
import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAIL,
} from "../actionTypes";

// export const getUsers = () => async (dispatch, state) => {
// try {
//     dispatch({
//         type: FETCH_USERS_REQUEST,
//     });
//     const Users = await request("/users");
//     dispatch({
//         type: FETCH_USERS_SUCCESS,
//         payload: Users.data,
//     });
// } catch (error) {
//     dispatch({
//         type: FETCH_USERS_FAIL,
//         payload: error,
//     });
// }
// let userInfo = JSON.parse(localStorage.getItem("userInfo"));
// console.log("userInfo", userInfo.access_token);
// let access_token = userInfo.access_token;
// let config = {
//     method: "get",
//     url: "http://laravelcms.devdigdev.com/api/v1/users",
//     headers: {
//         "Content-type": "application/json",
//         Authorization: `Bearer ${access_token}`,
//     },
// };

// dispatch({
//     type: FETCH_USERS_REQUEST,
// });
// await axios(config)
//     .then(function (response) {
//         let users = response.data.users.data;
//         console.log("user", users);
//         dispatch({
//             type: FETCH_USERS_SUCCESS,
//             payload: users,
//         });
//     })
//     .catch(function (error) {
//         console.log(error);
//         dispatch({
//             type: FETCH_USERS_FAIL,
//             payload: error,
//         });
//     });
// };
