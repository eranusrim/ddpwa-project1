/* eslint-disable react/jsx-pascal-case */
import React, { useState, useEffect } from "react";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import "./App.css";
import { Switch, Route, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { useDispatch } from "react-redux";
import { getSliderImages } from "./Redux/actions/fetchSliderImagesAction";
import { fetchEvents } from "./Redux/actions/eventsAction";
import Auth from "./components/login/Auth";
import Login from "./components/login/Login";
import ForgetPassword from "./components/login/ForgetPassword";
import Navbar from "./components/sidebar/Navbar";
import MobileMenuDashboard from "./components/sidebar/MobileMenuDashboard";
import Users from "./components/users/Users.js";
import SidebarComp from "./components/sidebar/Sidebar";
import Dashboard from "./components/Dashboard/Dashboard";
import Page from "./components/pages/Page";
import AddPage from "./components/pages/AddPage";
import Categories from "./components/categories/Categories";
import AddCategories from "./components/categories/AddCategories";
import BlogsViewAll from "./components/blogs/BlogsViewAll";
import AddBlog from "./components/blogs/AddBlog";
import BlogComments from "./components/blogs/Comment";
import EditComment from "./components/blogs/EditComment";
import BlogsSetting from "./components/blogs/BlogsSetting";
import Faqs from "./components/FAQS/faqs";
import AddEditFaqs from "./components/FAQS/addEditFaqs";
import CreateUser from "./components/users/createUser";
import Setting from "./components/setting/Setting";
import SliderView from "./components/HomePageSlider/SliderView";
import SliderSetting from "./components/HomePageSlider/SliderSetting";
import SliderEdit from "./components/HomePageSlider/SliderEdit.js";
import Galleries from "./components/galleries/galleries";
import AddEditGallery from "./components/galleries/addEditGallery";
import Event from "./components/events/Event";
import AddEvent from "./components/events/AddEvent";
import Profile from "./components/profile/Profile";
import ChangePassword from "./components/profile/ChangePassword";
import Footer from "./components/Footer/Footer";
import PageNotFound from "./components/PageNotFound/PageNotFound";
import ProductsDashboard from "./components/products/products";
import AddProducts from "./components/products/addProducts";
import ContactsView from "./components/contact/contactView";
import ContactSetting from "./components/contact/contactSetting";
import ViewChatbotQusAns from "./components/chatbot/ViewChatBotQusAns";
import AddChatBotQueAns from "./components/chatbot/AddChatBotQueAns";

// front end single page application
import Front_spa_Navbar from "./components/frontend-spa/Navbar/navbar";
import MobileIconMenu from "./components/frontend-spa/Navbar/mobileIcon";
import FrontSpaFooter from "./components/frontend-spa/footer/Footer";
import Home from "./components/frontend-spa/Home/Home";
import FrontendLogin from "./components/frontend-spa/frontend-login-signup/FrontendLogin";
import FrontEndSignUp from "./components/frontend-spa/frontend-login-signup/FrontEndSignUp";
import FrontEndBlogListing from "./components/frontend-spa/blogs/FrontEndBlogListing";
import BlogDescription from "./components/frontend-spa/blogs/blogDescription";
import Products from "./components/frontend-spa/products/Products";
import ProductsDescription from "./components/frontend-spa/products/ProductsDescription";
import FrontendGalleries from "./components/frontend-spa/galleries/galleries";
import GalleryDescription from "./components/frontend-spa/galleries/galleryDescription";
import FrontendFaqs from "./components/frontend-spa/faqs/faqs";
import Cart from "./components/frontend-spa/cart/cart";
import Checkout from "./components/frontend-spa/checkout/checkout";
import Payment from "./components/frontend-spa/checkout/Payment";
import ClientDashboard from "./components/frontend-spa/clientDashboard/clientDashboard";
import PartnerDashboard from "./components/frontend-spa/partnerDashboard/PartnerDashboard";
import RequestForDocs from "./components/frontend-spa/partnerDashboard/RequestForDocs";
import DocumentView from "./components/frontend-spa/partnerDashboard/DocumentView";

import Chatbot from "react-chatbot-kit";
import "react-chatbot-kit/build/main.css";
import config from "./components/frontend-spa/chatbot/config";
import ActionProvider from "./components/frontend-spa/chatbot/ActionProvider";
import MessageParser from "./components/frontend-spa/chatbot/MessageParser";
import { AiOutlineMessage } from "react-icons/ai";

const PrivateRoute = ({ component: Component, Sidebar, ...rest }) => (
  <Route
    exact
    {...rest}
    render={(props) =>
      Auth.getAuth() ? (
        <Component {...props} Sidebar={Sidebar} />
      ) : (
        <Redirect
          to={{
            pathname: "/admin/login",
          }}
        />
      )
    }
  />
);

const ELEMENTS_OPTIONS = {
  fonts: [
    {
      cssSrc: "https://fonts.googleapis.com/css?family=Roboto",
    },
  ],
};
// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe(
  "pk_test_51JRVuoSAD7LtpQ5PJdd734KHOzJPmTp47AeF16puecFLboMIHCHItATiS4GnFDMk7pPHuLLkCVyuQuIDyoq9fk3Z00nLizrVFD"
);

const Layout = ({ children }) => {
  const [showChatbot, setChatBot] = useState(false);
  return (
    <div className="w-full">
      <Front_spa_Navbar />
      <div className="w-full min-h-screen">{children}</div>
      <div className="fixed z-20 flex items-center justify-center text-center bg-gray-100 right-2 bottom-14 dark:bg-gray-600">
        {showChatbot === false && (
          <button
            className="p-4 text-white bg-yellow-500 border rounded"
            onClick={() => {
              setChatBot(true);
            }}
          >
            <AiOutlineMessage />
          </button>
        )}

        {showChatbot === true && (
          <div className="p-2 bg-white border border-yellow-500 rounded">
            <div className="flex items-center justify-end w-full ">
              <button
                className="p-3 text-black bg-yellow-500 border rounded"
                onClick={() => {
                  setChatBot(false);
                }}
              >
                X
              </button>
            </div>
            <Chatbot
              config={config}
              actionProvider={ActionProvider}
              messageParser={MessageParser}
            />
          </div>
        )}
      </div>
      <FrontSpaFooter />
      <MobileIconMenu />
    </div>
  );
};

const NotFound = () => (
  <div className="flex flex-col items-center justify-center w-full h-screen bg-gray-50">
    <h1 className="w-full mt-10 text-5xl font-bold text-center text-red-500 uppercase">
      Error
    </h1>
    <p className="w-full mt-5 font-black text-center text-gray-900 uppercase text-9xl">
      404
    </p>
    <p className="w-full mt-5 text-3xl font-bold text-center text-red-500 capitalize">
      page not found
    </p>
  </div>
);
function App() {
  let dispatch = useDispatch();
  const [Sidebar, setSidebar] = useState(true);

  const [login, setLogin] = useState(false);

  const showSidebar = () => {
    setSidebar(!Sidebar);
  };
  const hideSidebarFromMenu = (sidebar) => {
    setSidebar(sidebar);
  };
  useEffect(() => {
    dispatch(getSliderImages());
    dispatch(fetchEvents());
  }, [dispatch]);
  return (
    <>
      {login && <Navbar showSidebar={showSidebar} setLogin={setLogin} />}

      <div className="box-border flex">
        {login && (
          <SidebarComp
            hideSidebarFromMenu={hideSidebarFromMenu}
            Sidebar={Sidebar}
          />
        )}

        <Switch>
          {!login && (
            <Route path="/admin/login" exact>
              <Login setLogin={setLogin} />
            </Route>
          )}
          <Route path="/admin/forgetPassword" exact>
            <ForgetPassword />
          </Route>
          <PrivateRoute
            exact
            path="/admin/dashboard"
            component={Dashboard}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/users"
            component={Users}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/users/create"
            component={CreateUser}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/pages"
            component={Page}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/pages/addPage"
            component={AddPage}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/categories"
            component={Categories}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/categories/addCategory"
            component={AddCategories}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/blog/view"
            component={BlogsViewAll}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/blog/addBlog"
            component={AddBlog}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/blog/editBlog/:id"
            component={AddBlog}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/blog/comment/:id"
            component={BlogComments}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/blog/editComment/:id"
            component={EditComment}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/blog/setting"
            component={BlogsSetting}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/faqs"
            component={Faqs}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/faqs/addFaq"
            component={AddEditFaqs}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/faqs/:id"
            component={AddEditFaqs}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/setting"
            component={Setting}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/galleries"
            component={Galleries}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/galleries/addGallery"
            component={AddEditGallery}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/galleries/editGallery/:id"
            component={AddEditGallery}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/products"
            component={ProductsDashboard}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/products/addProducts"
            component={AddProducts}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/products/editProduct/:id/:category"
            component={AddProducts}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/products/editProduct/:id/:category/:subcategory"
            component={AddProducts}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/contact/view"
            component={ContactsView}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/contact/setting"
            component={ContactSetting}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/events"
            component={Event}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/events/addEvent"
            component={AddEvent}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/events/editEvent/:id"
            component={AddEvent}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/sliders/setting"
            component={SliderSetting}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/sliders/edit"
            component={SliderEdit}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/sliders/view"
            component={SliderView}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/profile"
            component={Profile}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/changePassword"
            component={ChangePassword}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/chatbot"
            component={ViewChatbotQusAns}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/chatbot/addQuestion"
            component={AddChatBotQueAns}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/chatbot/editQuestion/:id"
            component={AddChatBotQueAns}
            Sidebar={Sidebar}
          />
          <PrivateRoute
            exact
            path="/admin/*"
            component={PageNotFound}
            Sidebar={Sidebar}
          />
          {/* FONT END SINGLE PAGE APPLICATION */}
          <Route path="/" exact>
            <Layout>
              <Home />
            </Layout>
          </Route>
          <Route path="/blogs" exact>
            <Layout>
              <FrontEndBlogListing />
            </Layout>
          </Route>
          <Route path="/blog/:id" exact>
            <Layout>
              <BlogDescription />
            </Layout>
          </Route>
          <Route path="/products" exact>
            <Layout>
              <Products />
            </Layout>
          </Route>
          <Route path="/product/:id/:category/:subCategory" exact>
            <Layout>
              <ProductsDescription />
            </Layout>
          </Route>
          <Route path="/galleries" exact>
            <Layout>
              <FrontendGalleries />
            </Layout>
          </Route>
          <Route path="/galleries/:id" exact>
            <Layout>
              <GalleryDescription />
            </Layout>
          </Route>
          <Route path="/faqs" exact>
            <Layout>
              <FrontendFaqs />
            </Layout>
          </Route>

          <Route path="/cart" exact>
            <Layout>
              <Cart />
            </Layout>
          </Route>
          <Route path="/checkout" exact>
            <Layout>
              <Checkout />
            </Layout>
          </Route>
          <Route path="/payment" exact>
            <Layout>
              <Elements stripe={stripePromise} options={ELEMENTS_OPTIONS}>
                <Payment />
              </Elements>
            </Layout>
          </Route>
          <Route path="/frontendLogin" exact>
            <Layout>
              <FrontendLogin />
            </Layout>
          </Route>
          <Route path="/frontEndSignUp" exact>
            <Layout>
              <FrontEndSignUp />
            </Layout>
          </Route>
          <Route path="/clientDashboard">
            <Layout>
              <ClientDashboard />
            </Layout>
          </Route>
          <Route path="/partnerDashboard">
            <Layout>
              <PartnerDashboard />
            </Layout>
          </Route>
          <Route path="/requestForDocs">
            <Layout>
              <RequestForDocs />
            </Layout>
          </Route>
          <Route path="/documentView/:id/:name">
            <Layout>
              <DocumentView />
            </Layout>
          </Route>
          <Route path="*">
            <Layout>
              <NotFound />
            </Layout>
          </Route>
        </Switch>
        <ToastContainer />
      </div>
      {login && <Footer />}
      {login && (
        <MobileMenuDashboard hideSidebarFromMenu={hideSidebarFromMenu} />
      )}
    </>
  );
}

export default App;
