const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const Dotenv = require("dotenv-webpack");
const { InjectManifest } = require("workbox-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
// require("babel-polyfill");
// const webpack = require("webpack");

const webpackPlugins = [
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, "public/index.html"),
    filename: "index.html",
    headerLogo: "./public/header-logo.png",
    manifest: "./public/manifest.json",
  }),
  new Dotenv({
    path: "./.env", // Path to .env file (this is the default)
    systemvars: true,
  }),
  new CopyPlugin({
    patterns: [
      { from: "./public/images/header-logo.png", to: "./images" },
      { from: "./public/images/image.png", to: "./images" },
      { from: "./public/manifest.json", to: "" },
      // { from: "./src/logo192.png", to: "" },
      // { from: "./src/logo512.png", to: "" },
    ],
  }),
  // new webpack.HotModuleReplacementPlugin(),
];

if ("production" === process.env.NODE_ENV) {
  webpackPlugins.push(
    new InjectManifest({
      swSrc: "./src/service-worker.js",
      swDest: "sw.js",
    })
  );
}

module.exports = {
  context: __dirname,
  entry: ["babel-polyfill", "./src/index.js"],
  output: {
    path: path.resolve(__dirname, "dist1"),
    filename: "main.js",
    publicPath: "/",
  },
  devServer: {
    historyApiFallback: true,
    port: 3000,
  },
  resolve: {
    fallback: {
      fs: false,
      tls: false,
      net: false,
      path: false,
      zlib: false,
      http: false,
      https: false,
      stream: false,
      crypto: false,
      "crypto-browserify": require.resolve("crypto-browserify"), //if you want to use this module also don't forget npm i crypto-browserify
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader",
      },
      {
        test: /\.css?$/,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },
      {
        test: /\.(png|j?g|svg|gif)?$/,
        // use: "file-loader?name=./images/[name].[ext]",
        use: "file-loader?name=[name].[ext]'",
      },
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 100000,
            name: "[name].[ext]",
          },
        },
      },
    ],
  },
  plugins: webpackPlugins,
};
